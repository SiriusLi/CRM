package cn.gson.crm.model.entity;

import org.springframework.web.multipart.MultipartFile;

public class BrandModel {  
  
    private Integer brandModelId;  
    private String name;  
    private String description;  
    private String url;  
    private String icon;  
    private MultipartFile sourceFile; //对应<span style="font-family: Arial, Helvetica, sans-serif;"><input class="easyui-filebox" name="sourceFile" style="width:200px"></span>  
	
	public Integer getBrandModelId() {
		return brandModelId;
	}
	public void setBrandModelId(Integer brandModelId) {
		this.brandModelId = brandModelId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public MultipartFile getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(MultipartFile sourceFile) {
		this.sourceFile = sourceFile;
	}
	@Override
	public String toString() {
		return "BrandModel [id=" + brandModelId + ", name=" + name + ", description=" + description + ", url=" + url + ", icon="
				+ icon + ", sourceFile=" + sourceFile + "]";
	}
    
    
}  
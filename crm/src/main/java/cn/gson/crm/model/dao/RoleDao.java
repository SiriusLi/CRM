package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Role;

public interface RoleDao extends PagingAndSortingRepository<Role, Long>{

	List<Role> findAllByEnable(Boolean enable);
}

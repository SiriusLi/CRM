package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 报价实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午8:44:45
 */
@Entity
@Table(name = "crm_quotation")
public class Quotation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 时间
	 */
	private Date time;
	/**
	 * 总报价
	 */
	private double money;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 审批（未审核，通过，否决）
	 */
	private String approval;

	/**
	 * 审核日期
	 */
	@Column(name = "audit_date")
	private Date auditDate;

	/**
	 * 对应销售机会
	 */
	@ManyToOne
	private SalesOpportunity salesOpp;
	/**
	 * 报价员
	 */
	@ManyToOne
	private Employee employee;
	/**
	 * 审批员
	 */
	@ManyToOne
	private Employee approvalEmployee;

	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


	public String getApproval() {
		return approval;
	}


	public void setApproval(String approval) {
		this.approval = approval;
	}

	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}

	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Employee getApprovalEmployee() {
		return approvalEmployee;
	}

	public void setApprovalEmployee(Employee approvalEmployee) {
		this.approvalEmployee = approvalEmployee;
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public Quotation() {
		// TODO Auto-generated constructor stub
	}
	
	public Quotation(Long id, Date time, double money, boolean isDelete, String approval, Date auditDate,
			SalesOpportunity salesOpp, Employee employee, Employee approvalEmployee, Customer customer) {
		super();
		this.id = id;
		this.time = time;
		this.money = money;
		this.isDelete = isDelete;
		this.auditDate = auditDate;
		this.approval = approval;
		this.auditDate = auditDate;
		this.salesOpp = salesOpp;
		this.employee = employee;
		this.approvalEmployee = approvalEmployee;
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Quotation [id=" + id + ", time=" + time + ", money=" + money 
				+ ", approval=" + approval + ", auditDate=" + auditDate + "]";
	}

}

package cn.gson.crm.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 客户需求实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午6:03:09
 */
@Entity
@Table(name = "crm_demand")
public class Demand {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 需求主题
	 */
	private String topic;
	/**
	 * 需求内容
	 */
	private String content;
	/**
	 * 记录时间
	 */
	@Column(name = "record_time")
	private Date recordTime = new Date();
	/**
	 * 重要程度
	 */
	private String importance;
	/**
	 * 是否删除
	 */
	private boolean isDelete = false;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 对应销售机会
	 */
	@ManyToOne
	private SalesOpportunity salesOpp;

	/**
	 * 附件
	 */
	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "crm_demand_attachment", joinColumns = @JoinColumn(name = "demand_id"), inverseJoinColumns = @JoinColumn(name = "attachment_id"))
	@JSONField(serialize = false)
	private List<Attachment> attachment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public List<Attachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}

	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}

	public Date getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(Date recordTime) {
		this.recordTime = recordTime;
	}

	public String getImportance() {
		return importance;
	}

	public void setImportance(String importance) {
		this.importance = importance;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Demand() {
		// TODO Auto-generated constructor stub
	}

	public Demand(Long id, String topic, String content, Date recordTime, String importance, String note) {
		super();
		this.id = id;
		this.topic = topic;
		this.content = content;
		this.recordTime = recordTime;
		this.importance = importance;
		this.note = note;
	}

	@Override
	public String toString() {
		return "Demand [id=" + id + ", topic=" + topic + ", content=" + content + ", recordTime=" + recordTime
				+ ", importance=" + importance + ", note=" + note + "]";
	}

}

package cn.gson.crm.model.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.github.pagehelper.Page;

import cn.gson.crm.model.entity.Repair;
/**
 * 
 * @author LJJ
 *
 */
public interface AfterSaleDao extends PagingAndSortingRepository<Repair, Long> {

//	@Query("from Repair r order by u.id")
//	Repair getRecent();
	
	
}

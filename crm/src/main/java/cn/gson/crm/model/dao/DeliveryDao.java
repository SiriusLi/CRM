package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Delivery;
import cn.gson.crm.model.entity.Order;

/**
 *
 * @author hzl
 * @date 2018年4月23日
 * @time 上午11:14:16
*/
public interface DeliveryDao extends PagingAndSortingRepository<Delivery, Long> {
	//根据发货状态查询发货单
	List<Delivery> findByState(String string);
	List<Delivery> findByOrder(Order order);

}

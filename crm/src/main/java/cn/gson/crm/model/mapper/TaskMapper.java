package cn.gson.crm.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.gson.crm.model.entity.Task;

public interface TaskMapper {

	List<Task> selectTask(@Param("eid") Long eid);
}

package cn.gson.crm.model.mapper;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Order;

/**
 *
 * @author hzl
 * @date 2018年4月18日
 * @time 下午4:07:19
*/
public interface OrderMapper extends PagingAndSortingRepository<Order, Long>{
	//查询所有订单的id和主题
	List<Order> selectTopic();

	//按条件查询总交易条数
	Long NumByDay();
	
	Long NumByWeek();
	
	Long NumByMonth();
	
	Double MoneyByDay();
	
	Double MoneyByWeek();
	
	Double MoneyByMonth();
	
//	Integer selectNumByDay(Integer time);
					
}

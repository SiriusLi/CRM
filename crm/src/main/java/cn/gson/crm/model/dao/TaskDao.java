package cn.gson.crm.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Task;

public interface TaskDao extends PagingAndSortingRepository<Task, Long>{

}

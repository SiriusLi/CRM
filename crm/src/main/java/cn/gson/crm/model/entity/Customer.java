package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 客户实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午4:18:18
 */
@Entity
@Table(name = "crm_customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 采集内容
	 */
	private String content;
	/**
	 * 主管是否处理客户线索（默认未处理）

	 */
	@Column(name = "ma_state",nullable=true)
	private boolean maState = false;
	/**
	 * 判断业务员是否处理线索（默认未处理）
	 */
	@Column(name = "sa_state",nullable=true)
	private boolean saState = false;

	/**
	 * 客户名称
	 */
	private String name;
	/**
	 * 生命周期
	 */
	private String lifecycle;
	/**
	 * 创建日期
	 */
	private Date createDate = new Date();
	/**
	 * 定性（分为有价值、无价值两类）
	 */
	private String qualitative;
	/**
	 * 定级（大单、正常单、小单）
	 */
	private String rank;
	/**
	 * 定量（对成交货物总价值进行估计）
	 */
	@Column(nullable=true)
	private double ration = 0;
	/**
	 * 客户画像
	 */
	private String portrayal;
	/**
	 * 联系人
	 */
	private String contacts;
	/**
	 * 电话
	 */
	private String tel;

	private String email;

	private Date birthday;

	private String sex;
	/**
	 * 拥有者
	 */
	@ManyToOne
	//@JSONField(serialize=false)
	private Employee employee;

	/**
	 * 备注
	 */
	private String note;

	/**
	 * 客户来源
	 */
	private String source;
	/**
	 * 是否删除（默认为0：未删除）
	 */
	private boolean isDelete = false;

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isMaState() {
		return maState;
	}

	public void setMaState(boolean maState) {
		this.maState = maState;
	}

	public boolean isSaState() {
		return saState;
	}

	public void setSaState(boolean saState) {
		this.saState = saState;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLifecycle() {
		return lifecycle;
	}

	public void setLifecycle(String lifecycle) {
		this.lifecycle = lifecycle;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getQualitative() {
		return qualitative;
	}

	public void setQualitative(String qualitative) {
		this.qualitative = qualitative;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public double getRation() {
		return ration;
	}

	public void setRation(double ration) {
		this.ration = ration;
	}

	public String getPortrayal() {
		return portrayal;
	}

	public void setPortrayal(String portrayal) {
		this.portrayal = portrayal;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Customer() {

	}

	public Customer(Long id, boolean maState, boolean saState, String name, String lifecycle,
			Date createDate, String qualitative, String rank, double ration, String portrayal, String contacts,
			String tel, String email, Date birthday, String sex, Employee employee, String note, String source) {
		super();
		this.id = id;
		this.maState = maState;
		this.saState = saState;
		this.name = name;
		this.lifecycle = lifecycle;
		this.createDate = createDate;
		this.qualitative = qualitative;
		this.rank = rank;
		this.ration = ration;
		this.portrayal = portrayal;
		this.contacts = contacts;
		this.tel = tel;
		this.email = email;
		this.birthday = birthday;
		this.sex = sex;
		this.employee = employee;
		this.note = note;
		this.source = source;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", content=" + content + ", maState=" + maState + ", saState=" + saState
				+ ", name=" + name + ", lifecycle=" + lifecycle + ", createDate=" + createDate + ", qualitative="
				+ qualitative + ", rank=" + rank + ", ration=" + ration + ", portrayal=" + portrayal + ", contacts="
				+ contacts + ", tel=" + tel + ", email=" + email + ", birthday=" + birthday + ", sex=" + sex + ", note="
				+ note + ", source=" + source + ", isDelete=" + isDelete + "]";
	}

	
}

package cn.gson.crm.model.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 维修进度记录实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午8:46:29
 */
@Entity
@Table(name = "crm_repair_detail")
public class RepairDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 维修进度
	 */
	private String progress;
	/**
	 * 时间
	 */
	private Date time;
	
	/**
	 * 备注
	 */
	private String content;
	/**
	 * 所有者
	 */
	@ManyToOne
	private Employee employee;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应维修单
	 */
	@ManyToOne
	private Repair repair;
	
	public void setContent(String content) {
		this.content = content;
	}
	public String getContent() {
		return content;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getProgress() {
		return progress;
	}

	public void setProgress(String progress) {
		this.progress = progress;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Repair getRepair() {
		return repair;
	}

	public void setRepair(Repair repair) {
		this.repair = repair;
	}

	public RepairDetail() {
		// TODO Auto-generated constructor stub
	}
	public RepairDetail(Long id, String progress, Date time, String content, Employee employee, boolean isDelete,
			Repair repair) {
		super();
		this.id = id;
		this.progress = progress;
		this.time = time;
		this.content = content;
		this.employee = employee;
		this.isDelete = isDelete;
		this.repair = repair;
	}
	@Override
	public String toString() {
		return "RepairDetail [id=" + id + ", progress=" + progress + ", time=" + time + ", content=" + content
				+ ", isDelete=" + isDelete + ", repair=" + repair + "]";
	}
	
	

	

}

package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 消息实体类
 * @author hzl
 * @date 2018年4月8日
 * @time 下午4:13:10
 */
@Entity
@Table(name = "crm_message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 消息类型（回款，订单，销售机会，客户，审批，发货）
	 */
	private String type;
	/**
	 * 时间
	 */
	private Date time;
	/**
	 * 对应连接
	 */
	private String url;
	/**
	 * 消息主题
	 */
	private String topic;
	/**
	 * 消息内容
	 */
	private String content;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * 无参构造
	 */
	public Message() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 有参构造
	 * @param type
	 * @param time
	 * @param url
	 * @param topic
	 * @param content
	 */
	public Message(String type, Date time, String url, String topic, String content) {
		super();
		this.type = type;
		this.time = time;
		this.url = url;
		this.topic = topic;
		this.content = content;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", type=" + type + ", time=" + time + ", url=" + url + ", topic=" + topic
				+ ", content=" + content + "]";
	}
	
	
	
}

package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Repair;
import cn.gson.crm.model.entity.RepairDetail;

public interface RepairDetailDao extends PagingAndSortingRepository<RepairDetail, Long>{

	List<RepairDetail> findByRepair(Repair repair);

}

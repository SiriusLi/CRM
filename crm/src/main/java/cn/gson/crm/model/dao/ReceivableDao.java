package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.model.entity.Receivable;

/**
 *
 * @author hzl
 * @date 2018年4月18日
 * @time 上午11:11:16
*/
public interface ReceivableDao extends PagingAndSortingRepository<Receivable, Long> {
	//根据回款状态查询回款,分页
	Page<Receivable> findByState(String state,Pageable pa);
	
	//根据员工，回款状态查询回款,分页
	Page<Receivable> findByStateAndEmployee(String state,Employee employee,Pageable pa);
	
	//根据员工查询回款,分页
	Page<Receivable> findByEmployee(Employee employee,Pageable pa);
	
	//根据回款状态查询回款
	List<Receivable> findByState(String state);
	
	//根据订单查询回款
	List<Receivable> findByOrder(Order order);
	
	
}

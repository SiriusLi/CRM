package cn.gson.crm.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Customer;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Solution;

public interface SolutionDao extends PagingAndSortingRepository<Solution, Long> {

		//管理员查询所有，已是删除状态的不显示
		Page<Solution> findAllByIsDelete(boolean b,Pageable pr);
		//员工只查询员工本身的数据
		Page<Solution> findByEmployeeAndIsDelete(Employee employee,boolean b,Pageable pr);
		
}

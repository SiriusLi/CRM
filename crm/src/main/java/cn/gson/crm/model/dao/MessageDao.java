package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Message;

/**
 *
 * @author hzl
 * @date 2018年5月4日
 * @time 下午4:53:38
*/
public interface MessageDao extends PagingAndSortingRepository<Message, Long> {

	@Query("from Message m Order by m.time desc")
	List<Message> getTop5();
}

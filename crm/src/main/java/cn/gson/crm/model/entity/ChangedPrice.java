package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "crm_changeprice")
public class ChangedPrice {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 原价
	 */

	private double preprice;
	/**
	 * 现价
	 */
	private double price;

	/**
	 * 修改时间
	 */
	private Date changetime;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 修改员工
	 */
	@ManyToOne
	private Employee employee;

	/**
	 * 产品
	 */
	@ManyToOne
	private Product product;

	public ChangedPrice() {
		// TODO Auto-generated constructor stub
	}

	public ChangedPrice(Long id, double preprice, double price, Date changetime, Employee employee, Product product) {
		super();
		this.id = id;
		this.preprice = preprice;
		this.price = price;
		this.changetime = changetime;
		this.employee = employee;
		this.product = product;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getPreprice() {
		return preprice;
	}

	public void setPreprice(double preprice) {
		this.preprice = preprice;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getChangetime() {
		return changetime;
	}

	public void setChangetime(Date changetime) {
		this.changetime = changetime;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "ChangedPrice [id=" + id + ", preprice=" + preprice + ", price=" + price + ", changetime=" + changetime
				+ "]";
	}

}

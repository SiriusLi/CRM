package cn.gson.crm.model.mapper;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Employee;

/**
 *
 * @author hzl
 * @date 2018年4月16日
 * @time 下午10:18:12
*/
public interface EmployeeMapper {

	List<Employee> selectName();


	//根据姓名或者id查找
	List<Employee> searchEmp(String seacont);

}

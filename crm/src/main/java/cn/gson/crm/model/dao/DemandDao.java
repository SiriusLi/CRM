package cn.gson.crm.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Demand;

public interface DemandDao extends PagingAndSortingRepository<Demand, Long>{

	//找出某一销售机会相关的需求
	@Query(value = "SELECT dem FROM Demand dem,SalesOpportunity opp,Customer cus,Employee emp WHERE  dem.salesOpp = opp.id AND opp.customer = cus.id AND dem.isDelete = 0 AND cus.employee = emp.id AND emp.id = ?1")
	Page<Demand> findByEmp(Long empid, Pageable pr);

	//显示所有未删除的需求
	Page<Demand> findByIsDelete(boolean b, Pageable pr);

}

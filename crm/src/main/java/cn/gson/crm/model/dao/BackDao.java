package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Back;
import cn.gson.crm.model.entity.Order;

/**
 *
 * @author hzl
 * @date 2018年4月23日
 * @time 上午11:19:24
*/
public interface BackDao extends PagingAndSortingRepository<Back, Long> {
	//根据退货状态查询退货
	List<Back> findByState(String string);
	List<Back> findByOrder(Order order);
}

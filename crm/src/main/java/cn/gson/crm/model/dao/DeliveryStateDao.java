package cn.gson.crm.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.DeliveryStateDetail;

/**
 *发货状态记录
 * @author hzl
 * @date 2018年4月23日
 * @time 下午5:44:09
*/
public interface DeliveryStateDao extends PagingAndSortingRepository<DeliveryStateDetail, Long> {

}

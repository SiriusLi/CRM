package cn.gson.crm.model.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Product;

/**
 * 产品mapper
 
 *
 */
public interface ProductMapper extends PagingAndSortingRepository<Product, Long> {

	/**
	 * 通过关键字搜索产品
	 * @author LJJ
	 * @param pname
	 * @return
	 */
	List<Product> searchProduct(@Param("name")String pname);
	
}

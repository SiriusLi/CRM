package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 发货表
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午8:26:56
 */
@Entity
@Table(name = "crm_delivery")
public class Delivery {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 时间
	 */
	private Date date;
	/**
	 * 发货单号
	 */
	private String number;
	/**
	 * 收货人姓名
	 */
	@Column(name="receive_man")
	private String receiveMan;
	/**
	 * 电话
	 */
	@Column(name="receive_tel")
	private String receiveTel;
	/**
	 * 收货地址
	 */
	@Column(name="receive_address")
	private String receiveAddress;

	/**
	 * 发货状态
	 */
	private String state;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 对应订单
	 */
	@ManyToOne
	private Order order;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getReceiveMan() {
		return receiveMan;
	}
	public void setReceiveMan(String receiveMan) {
		this.receiveMan = receiveMan;
	}
	public String getReceiveTel() {
		return receiveTel;
	}
	public void setReceiveTel(String receiveTel) {
		this.receiveTel = receiveTel;
	}
	public String getReceiveAddress() {
		return receiveAddress;
	}
	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Delivery() {
		// TODO Auto-generated constructor stub
	}

	public Delivery(Long id, Date date, String number, String receiveMan, String receiveTel, String receiveAddress,
			String state, String note, Order order, Customer customer) {
		super();
		this.id = id;
		this.date = date;
		this.number = number;
		this.receiveMan = receiveMan;
		this.receiveTel = receiveTel;
		this.receiveAddress = receiveAddress;
		this.state = state;
		this.note = note;
		this.order = order;
		this.customer = customer;
	}
	public Delivery( Date date, String number, String receiveMan, String receiveTel, String receiveAddress,
			String state, String note, Order order, Customer customer) {
		super();
		this.date = date;
		this.number = number;
		this.receiveMan = receiveMan;
		this.receiveTel = receiveTel;
		this.receiveAddress = receiveAddress;
		this.state = state;
		this.note = note;
		this.order = order;
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "Delivery [id=" + id + ", date=" + date + ", number=" + number + ", receiveMan=" + receiveMan
				+ ", receiveTel=" + receiveTel + ", receiveAddress=" + receiveAddress + ", state=" + state
				+ ", note=" + note + "]";
	}
	
}

package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * 订单状态记录实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午6:21:32
 */
@Entity
@Table(name = "crm_order_state_detail")
public class OrderStateDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 状态
	 */
	private String state;
	/**
	 * 时间
	 */
	private Date time;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应订单
	 */
	@ManyToOne
	private Order order;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public Order getOrder() {
		return order;
	}
	
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public OrderStateDetail() {
		// TODO Auto-generated constructor stub
	}
	public OrderStateDetail(Long id, String state, Date time) {
		super();
		this.id = id;
		this.state = state;
		this.time = time;
	}
	@Override
	public String toString() {
		return "OrderStateDetail [id=" + id + ", state=" + state + ", time=" + time + "]";
	}
	
	
	
}

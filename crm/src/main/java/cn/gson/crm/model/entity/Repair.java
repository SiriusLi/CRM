package cn.gson.crm.model.entity;

import java.util.Date;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *维修实体类
 * @author hzl
 * @date 2018年4月8日
 * @time 下午8:46:08
*/
@Entity
@Table(name = "crm_repair")
public class Repair {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 维修单号
	 */
	private String number;
	/**
	 * 维修主题
	 */
	private String topic;
	/**
	 * 维修内容
	 */
	private String content;

	/**
	 * 时间
	 */
	private Date date;
	/**
	 * 计划截止交付日期
	 */
	private Date delivery;
	/**
	 * 维修进度
	 */
	private String progress;
	/**
	 * 金额
	 */
	private double money;
	/**
	 * 联系人姓名
	 */
	private String name;
	/**
	 * 电话
	 */
	private String tel;
	/**
	 * 状态（执行中，已完成）
	 */
	private Boolean state;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	/**
	 * @author LJJ
	 */
	
	@ManyToOne
	private Product product;
	
	@ManyToOne
	private Employee employee;
	
	
	/**
	 * 附件
	 */
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(
			name="crm_repair_attachment",
			joinColumns=@JoinColumn(name="repair_id"),
			inverseJoinColumns=@JoinColumn(name="attachment_id"))
	private List<Attachment> attachment;
	public void setProduct(Product product) {
		this.product = product;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public Employee getEmployee() {
		return employee;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public List<Attachment> getAttachment() {
		return attachment;
	}
	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public Boolean getState() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getDelivery() {
		return delivery;
	}
	public void setDelivery(Date delivery) {
		this.delivery = delivery;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Repair() {
	}
	public Repair(String number, String topic, String content, Date date, Date delivery, String progress, double money,
			String name, String tel, Boolean state, Customer customer,Product product) {
		super();
		this.number = number;
		this.topic = topic;
		this.content = content;
		this.date = date;
		this.delivery = delivery;
		this.progress = progress;
		this.money = money;
		this.name = name;
		this.tel = tel;
		this.state = state;
		this.customer = customer;
		this.product = product;


	}

	

	
}

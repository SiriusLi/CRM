package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Resource;

public interface ResourceDao extends PagingAndSortingRepository<Resource, Long> {
	
	@Query("from Resource rs where rs.parent is null")
	public List<Resource> findRoot();
	
	@Query("from Resource rs where rs.parent.id = ?1 order by rs.weight desc")
	public List<Resource> findChildren(Long id);
	
	public List<Resource> findByParentOrderByWeightDesc(Resource resource);

	public List<Resource> findByParentIsNullAndEnableOrderByWeightDesc(boolean b);

	public List<Resource> findByParentAndEnableOrderByWeightDesc(Resource resource, boolean b);
	
	int countByIdentify(String identify);
}

package cn.gson.crm.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alibaba.fastjson.annotation.JSONField;
/**
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午3:49:36
 */
@Entity
@Table(name = "crm_resource")
public class Resource {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 父级
	 */
//	@ManyToOne(cascade={CascadeType.ALL})
	@ManyToOne
	@JoinColumn(nullable = true)
	private Resource parent;

	/**
	 * 资源类型
	 */
	@Enumerated(EnumType.STRING)
	private Type type;

	/**
	 * 资源名称
	 */
	private String text;

	/**
	 * 资源标识
	 */
	@Column(nullable = false, unique = true)
	private String identify;

	@Column(name = "menu_href", nullable = true)
	private String menuHref;

	/**
	 * 一个资源所关联的所有请求
	 */
	private String urls;

	/**
	 * 资源是否可用
	 */
	private Boolean enable = false;

	/**
	 * 权重
	 */
	private Integer weight = 0;

	/**
	 * @Transient 用来标注，此字段是非数据库字段
	 */
	@Transient
	private List<Resource> children;

	public enum Type {
		/**
		 * 菜单
		 */
		MENU,
		/**
		 * 功能
		 */
		FUNCTION,
		/**
		 * 页面
		 */
		PAGE
	}

	public Resource() {
	}

	public Resource(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Resource getParent() {
		return parent;
	}

	public void setParent(Resource parent) {
		this.parent = parent;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIdentify() {
		return identify != null ? identify.trim() : null;
	}

	public void setIdentify(String identify) {
		this.identify = identify;
	}

	public String getMenuHref() {
		return menuHref;
	}

	public void setMenuHref(String menuHref) {
		this.menuHref = menuHref;
	}

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public void setChildren(List<Resource> children) {
		this.children = children;
	}

	public List<Resource> getChildren() {
		return children;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resource other = (Resource) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Resource [id=" + id + ", type=" + type + ", text=" + text + ", identify=" + identify + ", menuHref="
				+ menuHref + ", urls=" + urls + ", enable=" + enable + ", weight=" + weight + "]";
	}
}

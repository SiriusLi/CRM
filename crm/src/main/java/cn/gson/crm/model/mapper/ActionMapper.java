package cn.gson.crm.model.mapper;

public interface ActionMapper {

	Long actionByDay();
	
	Long actionByWeek();
	
	Long actionByMonth();
}

package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * 附件实体类
 * @author hzl
 * @date 2018年4月9日
 * @time 下午9:01:23
 */
@Entity(name="crm_attachment")
public class Attachment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	//创建时间
	@Column(name = "create_date", columnDefinition = "timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP")
	private Date createTime;
	
	//文件名称
	@Column(name="file_name")
	private String fileName;
	
	//文件路径
	@Column(name="file_path")
	private String filePath;
	
	//文件大小
	@Column(name="file_size")
	private Long fileSize;
	
	//文件类型(文本、图片、视频或者其它)
	@Column(name="content_type")
	private String contentType;
	
	//文件后缀
	private String suffix;
	/**
	 * 是否删除
	 */
	private boolean isDelete;

	public Attachment() {
		
	}

	public Attachment(Long aid) {
		this.id=aid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	 

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	@Override
	public String toString() {
		return "Attachment [id=" + id + ", createTime=" + createTime + ", fileName=" + fileName + ", filePath="
				+ filePath + ", fileSize=" + fileSize + ", contentType=" + contentType + ", suffix=" + suffix + "]";
	}
	
}

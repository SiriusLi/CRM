package cn.gson.crm.model.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import com.alibaba.fastjson.annotation.JSONField;


/**
 * 员工实体类
 * @author hzl
 * @date 2018年4月8日
 * @time 下午3:40:47
 */
@Entity
@Table(name = "crm_employee")
public class Employee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 员工姓名
	 */
	@Column(name = "employee_name")
	private String empName;
	/**
	 * 密码
	 */
	@JSONField(serialize=false)
	private String password;
	/**
	 *头像
	 */
	private String picture;
	/**
	 * 电话
	 */
	private String telephone;
	/**
	 * 员工邮箱
	 */
	private String email;
	/**
	 * 员工状态（在职，离职）
	 */
	private boolean state;

	/**
	 * 对应角色
	 */
	@ManyToOne
	private Role role;

	/**
	 * 上级
	 */
	//@ManyToOne(cascade={CascadeType.PERSIST})
	@ManyToOne
	@JoinColumn(nullable = true)
	@JSONField(serialize=false)
	private Employee leader;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}


	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Employee getLeader() {
		return leader;
	}

	public void setLeader(Employee leader) {
		this.leader = leader;
	}

	/**
	 * 无参构造
	 */
	public Employee() {
		
	}

	public Employee(Long id, String empName, String password, String picture, String telephone, String email) {
		super();
		this.id = id;
		this.empName = empName;
		this.password = password;
		this.picture = picture;
		this.telephone = telephone;
		this.email = email;
	}
	
	

	public Employee(Long id, String empName, String password, String picture, String telephone, String email,
			boolean state, Role role,Employee leader) {

		super();
		this.id = id;
		this.empName = empName;
		this.password = password;
		this.picture = picture;
		this.telephone = telephone;
		this.email = email;
		this.state = state;
		this.role = role;
		this.leader = leader;
		}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", empName=" + empName + ", password=" + password + ", picture=" + picture
				+ ", telephone=" + telephone + ", email=" + email + ", state=" + state + "]";
	}
	
	

	
}

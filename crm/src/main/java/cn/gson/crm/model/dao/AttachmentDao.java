package cn.gson.crm.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Attachment;


public interface AttachmentDao extends PagingAndSortingRepository<Attachment, Long> {

}

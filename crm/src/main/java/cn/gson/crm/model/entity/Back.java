package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 退货实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午6:33:25
 */
@Entity
@Table(name = "crm_back")
public class Back {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 退货单号
	 */
	private String number;
	/**
	 * 日期
	 */
	private Date date;
	/**
	 * 应退金额
	 */
	private double money;
	/**
	 * 已退金额
	 */
	private double retiredmoney;
	/**
	 * 状态(退货中，已完成)
	 */
	private String state;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 是否删除
	 */
	private boolean isDelete=false;
	/**
	 * 执行人
	 */
	@ManyToOne
	private Employee employee;
	/**
	 * 对应订单
	 */
	@ManyToOne
	private Order order;
	
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public double getRetiredmoney() {
		return retiredmoney;
	}
	public void setRetiredmoney(double retiredmoney) {
		this.retiredmoney = retiredmoney;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Back() {
		// TODO Auto-generated constructor stub
	}
	public Back(Long id, String number, Date date, double money, double retiredmoney, String state, String note) {
		super();
		this.id = id;
		this.number = number;
		this.date = date;
		this.money = money;
		this.retiredmoney = retiredmoney;
		this.state = state;
		this.note = note;
	}
	@Override
	public String toString() {
		return "Back [id=" + id + ", number=" + number + ", date=" + date + ", money=" + money + ", retiredmoney="
				+ retiredmoney + ", state=" + state + ", note=" + note + "]";
	}
	
}

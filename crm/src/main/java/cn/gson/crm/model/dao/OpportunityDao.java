package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Customer;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.SalesOpportunity;

public interface OpportunityDao extends PagingAndSortingRepository<SalesOpportunity, Long> {
	
	Page<SalesOpportunity> findAllByIsDelete(boolean b, Pageable pr);

	List<SalesOpportunity> findAllByIsDelete(boolean b);
	
	//找出与某一客户相关的所有销售机会
	List<SalesOpportunity> findAllByCustomer(Object cu);

	//根据客户id查询对应销售机会
	List<SalesOpportunity> findByCustomer(Customer customer);


	@Query("SELECT opp FROM SalesOpportunity opp,Customer cus,Employee emp WHERE opp.customer = cus.id AND opp.isDelete = 0 AND cus.employee = emp.id AND emp.id = ?1")
	List<SalesOpportunity> findByEmp(Long empid);

	
	//根据角色查询销售机会
//	@Query("SELECT op FROM SalesOpportunity op,Customer cu,Employee ep WHERE op.customer = cu.id AND op.idDelete = 0 AND cu.employee = ep.id AND ep.id =?1")
//	Page<SalesOpportunity> findByEmp2(Long epid,Pageable pr);

}

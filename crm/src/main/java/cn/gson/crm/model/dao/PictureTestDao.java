package cn.gson.crm.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Picture;

public interface PictureTestDao  extends PagingAndSortingRepository<Picture, Long>{

}

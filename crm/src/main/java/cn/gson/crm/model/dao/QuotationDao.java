package cn.gson.crm.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Quotation;
import cn.gson.crm.model.entity.SalesOpportunity;

/**
 *
 * @author hzl
 * @date 2018年4月23日
 * @time 下午9:28:45
*/
public interface QuotationDao extends PagingAndSortingRepository<Quotation, Long> {
	Page<Quotation> findByApproval(String approval,Pageable pa);
	
	//根据销售机会查询报价单
	Quotation findBySalesOpp (SalesOpportunity salesOpp);

}

package cn.gson.crm.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Competitor;
import cn.gson.crm.model.entity.Employee;

public interface CompetitorDao extends PagingAndSortingRepository<Competitor, Long>{

	//找出某一销售机会相关的竞争对手
	@Query(value = "SELECT com FROM Competitor com,SalesOpportunity opp,Customer cus,Employee emp WHERE  com.salesOpp = opp.id AND opp.customer = cus.id AND com.isDelete = 0 AND cus.employee = emp.id AND emp.id = ?1")
	Page<Competitor> findByEmp(Long empid, Pageable pr);

	//找出所有未删除的竞争对手信息
	Page<Competitor> findByIsDelete(boolean b, Pageable pr);


}

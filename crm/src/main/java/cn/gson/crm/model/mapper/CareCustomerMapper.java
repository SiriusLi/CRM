package cn.gson.crm.model.mapper;

public interface CareCustomerMapper {

	//按日查询关怀数
	Long careCustomerByDay();
	
	//按周查询关怀数
	Long careCustomerByWeek();
	
	//按月查询关怀数
	Long careCustomerByMonth();
}

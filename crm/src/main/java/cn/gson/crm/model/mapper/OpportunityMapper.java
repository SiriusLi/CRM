package cn.gson.crm.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Competitor;
import cn.gson.crm.model.entity.SalesOpportunity;

/**
 *
 * @author hzl
 * @date 2018年4月16日
 * @time 下午9:56:41
*/
public interface OpportunityMapper extends PagingAndSortingRepository<SalesOpportunity, Long> {
	List<SalesOpportunity> selectTopic();

	Long oppByDay();
	
	Long oppByWeek();
	
	Long oppByMonth();

	List<SalesOpportunity> selectOpp(@Param("empid")Long empid,@Param("value")String value);
	
	List<SalesOpportunity> selectAllOpp(@Param("value")String value);
	
	List<SalesOpportunity> selectOppByemp(@Param("id")Long id);
}

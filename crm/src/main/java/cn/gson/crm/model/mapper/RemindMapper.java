package cn.gson.crm.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.gson.crm.model.entity.Remind;

public interface RemindMapper {
	List<Remind> selectRemindEmp(@Param("id")Long id);
}

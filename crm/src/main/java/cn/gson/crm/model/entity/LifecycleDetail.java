package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 客户生命周期记录实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午5:45:20
 */
@Entity
@Table(name = "crm_lifecycle_detail")
public class LifecycleDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 生命周期
	 */
	private String lifecycle;
	/**
	 * 日期
	 */
	private Date date;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 所有者
	 */
	@ManyToOne
	private Employee employee;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLifecycle() {
		return lifecycle;
	}

	public void setLifecycle(String lifecycle) {
		this.lifecycle = lifecycle;
	}

	public Date getDate() {
		return date;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public LifecycleDetail() {
		// TODO Auto-generated constructor stub
	}

	public LifecycleDetail(Long id, String lifecycle, Date date) {
		super();
		this.id = id;
		this.lifecycle = lifecycle;
		this.date = date;
	}

	@Override
	public String toString() {
		return "LifecycleDetail [id=" + id + ", lifecycle=" + lifecycle + ", date=" + date + "]";
	}

}

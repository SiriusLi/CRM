package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 销售机会实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午5:24:25
 */
@Entity
@Table(name = "crm_sales_opportunity")
public class SalesOpportunity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 销售机会主题
	 */
	private String topic;

	/**
	 * 销售机会状态
	 */
	private String status;
	/**
	 * 更新时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 阶段
	 */
	private String stage;
	/**
	 * 预计签约时间
	 */
	@Column(name = "expected_time")
	private Date expectedTime;
	/**
	 * 可能性
	 */
	private String possibility;
	/**
	 * 预计签约金额
	 */
	@Column(name = "expected_money")
	private double expectedMoney  = 0;
	/**
	 * 优先级
	 */
	private int priority;
	/**
	 * 是否删除（默认为不删除）
	 */
	private boolean isDelete = false;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public Date getExpectedTime() {
		return expectedTime;
	}

	public void setExpectedTime(Date expectedTime) {
		this.expectedTime = expectedTime;
	}

	public String getPossibility() {
		return possibility;
	}

	public void setPossibility(String possibility) {
		this.possibility = possibility;
	}

	public double getExpectedMoney() {
		return expectedMoney;
	}

	public void setExpectedMoney(double expectedMoney) {
		this.expectedMoney = expectedMoney;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public SalesOpportunity() {
		// TODO Auto-generated constructor stub
	}

	public SalesOpportunity(Long id, String topic, String status, Date updateTime, String stage, Date expectedTime,
			String possibility, double expectedMoney, int priority) {
		super();
		this.id = id;
		this.topic = topic;
		this.status = status;
		this.updateTime = updateTime;
		this.stage = stage;
		this.expectedTime = expectedTime;
		this.possibility = possibility;
		this.expectedMoney = expectedMoney;
		this.priority = priority;
	}

	@Override
	public String toString() {
		return "SalesOpportunity [id=" + id + ", topic=" + topic + ", status=" + status + ", updateTime=" + updateTime
				+ ", stage=" + stage + ", expectedTime=" + expectedTime + ", possibility=" + possibility
				+ ", expectedMoney=" + expectedMoney + ", priority=" + priority + "]";
	}

}

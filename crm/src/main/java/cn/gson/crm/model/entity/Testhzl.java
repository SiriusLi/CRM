package cn.gson.crm.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 测试
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午10:56:52
 */

@Entity
@Table(name = "crm_test")
public class Testhzl {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String testhzl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTesthzl() {
		return testhzl;
	}

	public void setTesthzl(String testhzl) {
		this.testhzl = testhzl;
	}

	public Testhzl() {
		// TODO Auto-generated constructor stub
	}
}

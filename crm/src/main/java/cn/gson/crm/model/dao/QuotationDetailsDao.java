package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Quotation;
import cn.gson.crm.model.entity.QuotationDetails;

/**
 *
 * @author hzl
 * @date 2018年5月6日
 * @time 下午9:41:01
*/
public interface QuotationDetailsDao extends PagingAndSortingRepository<QuotationDetails, Long> {
	//根据报价单查询报价详情
	List<QuotationDetails> findByQuotation(Quotation quotation);
}

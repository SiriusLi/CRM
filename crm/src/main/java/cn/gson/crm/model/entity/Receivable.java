package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 回款实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午8:45:27
 */
@Entity
@Table(name = "crm_receivable")
public class Receivable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 期数
	 */
	private int installment;
	/**
	 * 计划回款时间
	 */
	@Column(name = "plan_time")
	private Date planTime;
	/**
	 * 实际回款时间
	 */
	@Column(name = "actual_time")
	private Date actualTime;
	/**
	 * 回款状态（回款，未回款，逾期未回）
	 */
	private String state;
	/**
	 * 实际回款金额
	 */
	@Column(name = "actual_money")
	private double actuaMoney;
	/**
	 * 计划回款金额
	 */
	@Column(name = "plan_money")
	private double planMoney;
	/**
	 * 付款方式（现金，支票，网上银行。。）
	 */
	@Column(name="payment_way")
	private String paymentWay;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 是否删除
	 */
	private boolean isDelete=false;
	/**
	 * 是否开发票
	 */
	private boolean  receipt;
	/**
	 * 负责人
	 */
	@ManyToOne
	private Employee employee;
	/**
	 * 收款人
	 */
	@ManyToOne
	private Employee payee;
	
	/**
	 * 对应订单
	 */
	@ManyToOne
	private Order order;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getInstallment() {
		return installment;
	}
	public void setInstallment(int installment) {
		this.installment = installment;
	}
	public Date getPlanTime() {
		return planTime;
	}
	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}
	public Date getActualTime() {
		return actualTime;
	}
	public void setActualTime(Date actualTime) {
		this.actualTime = actualTime;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public double getActuaMoney() {
		return actuaMoney;
	}
	
	public Employee getPayee() {
		return payee;
	}
	public void setPayee(Employee payee) {
		this.payee = payee;
	}
	public void setActuaMoney(double actuaMoney) {
		this.actuaMoney = actuaMoney;
	}
	public double getPlanMoney() {
		return planMoney;
	}
	public void setPlanMoney(double planMoney) {
		this.planMoney = planMoney;
	}
	public String getPaymentWay() {
		return paymentWay;
	}
	public void setPaymentWay(String paymentWay) {
		this.paymentWay = paymentWay;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public boolean isReceipt() {
		return receipt;
	}
	public void setReceipt(boolean receipt) {
		this.receipt = receipt;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Receivable() {
		// TODO Auto-generated constructor stub
	}
	public Receivable(Long id, int installment, Date planTime, Date actualTime, String state, double actuaMoney,
			double planMoney, String paymentWay, String note, boolean isDelete, boolean receipt, Employee employee,
			Order order, Customer customer) {
		super();
		this.id = id;
		this.installment = installment;
		this.planTime = planTime;
		this.actualTime = actualTime;
		this.state = state;
		this.actuaMoney = actuaMoney;
		this.planMoney = planMoney;
		this.paymentWay = paymentWay;
		this.note = note;
		this.isDelete = isDelete;
		this.receipt = receipt;
		this.employee = employee;
		this.order = order;
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "Receivable [id=" + id + ", installment=" + installment + ", planTime=" + planTime + ", actualTime="
				+ actualTime + ", state=" + state + ", actuaMoney=" + actuaMoney + ", planMoney=" + planMoney
				+ ", paymentWay=" + paymentWay + ", note=" + note + ", isDelete=" + isDelete + ", receipt=" + receipt
				+ "]";
	}

}

package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Customer;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Order;

/**
 *
 * @author hzl
 * @date 2018年4月16日
 * @time 下午5:10:06
*/
public interface OrderDao extends PagingAndSortingRepository<Order, Long>{
	//根据客户得到对应的订单
	List<Order> findByCustomerAndIsDelete(Customer customer,boolean s);
	
	//根据员工得到对应的订单,分页,未删除
	Page<Order> findByEmployeeAndIsDelete(Employee employee,Pageable pr,boolean s);
	//根据员工得到对应的订单,不分页
	List<Order> findByEmployeeAndIsDelete(Employee employee,boolean s);
	
	//根据客户得到已通过且未发货的订单
	List<Order> findByCustomerAndDeliveryAndAuditAndIsDelete(Customer customer,String delivery,String audit,boolean s);
	
	//根据审批状态查询订单
	Page<Order> findByAuditAndIsDelete(String audit,Pageable pr,boolean s);
	//根据订单状态查询订单
	List<Order> findByStateAndIsDelete(String string,boolean s);
}

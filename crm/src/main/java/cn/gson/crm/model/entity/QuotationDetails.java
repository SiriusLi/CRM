package cn.gson.crm.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 报价明细实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午8:45:10
 */
@Entity
@Table(name = "crm_quotation_details")
public class QuotationDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 数量
	 */
	private int num;
	/**
	 * 成本价（参考价）
	 */
	private double price;
	/**
	 * 报价
	 */
	private double quotedPrice;
	
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应报价实体类
	 */
	@ManyToOne
	private Quotation quotation;
	/**
	 * 对应产品
	 */
	@ManyToOne
	private Product product;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Quotation getQuotation() {
		return quotation;
	}
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public double getQuotedPrice() {
		return quotedPrice;
	}
	public void setQuotedPrice(double quotedPrice) {
		this.quotedPrice = quotedPrice;
	}
	public QuotationDetails() {
		// TODO Auto-generated constructor stub
	}
	public QuotationDetails(Long id, int num, double price, Quotation quotation, Product product) {
		super();
		this.id = id;
		this.num = num;
		this.price = price;
		this.quotation = quotation;
		this.product = product;
	}
	@Override
	public String toString() {
		return "QuotationDetails [id=" + id + ", num=" + num + ", price=" + price + "]";
	}
	
	
}

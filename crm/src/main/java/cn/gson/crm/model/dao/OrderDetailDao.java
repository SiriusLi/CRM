package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Order;
import cn.gson.crm.model.entity.OrderDetail;

/**
 *
 * @author hzl
 * @date 2018年4月20日
 * @time 上午10:53:14
*/
public interface OrderDetailDao extends PagingAndSortingRepository<OrderDetail, Long>{
	//根据类型（订单，退货，发货）查询明细，分页
	Page<OrderDetail> findByType(String type,Pageable pa);
	
	//根据订单id查询出未发货的订单明细
	List<OrderDetail> findByOrderAndTypeAndState(Order order,String type,String state);
	//根据订单id查询出订单明细
	List<OrderDetail> findByOrderAndType(Order order,String type);
}

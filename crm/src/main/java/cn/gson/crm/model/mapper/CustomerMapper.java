package cn.gson.crm.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Customer;

/**
 *
 * @author hzl
 * @date 2018年4月16日
 * @time 下午9:35:44
*/
public interface CustomerMapper {
	List<Customer> selectName();
	
	/**
	 * @author LJJ
	 */
	List<Customer> searchName(@Param("name")String cname);
	
	//通过id和联系人姓名进行模糊查询，且只显示前10条数据
	List<Customer> searchCon(String seacont);
	
	//通过id和客户姓名进行模糊查询，且只显示前10条数据
	List<Customer> searchCusname(String seacont);
	
	//获得线索数
	Long careNumDay();
	
	Long careNumWeek();
	
	Long careNumMonth();
	
	//获得客户数
	Long clientNumDay();
	
	Long clientNumWeek();
	
	Long clientNumMonth();
	
	//通过登录用户对id和客户姓名进行模糊查询，且只显示前10条数据
	List<Customer> searchEmpCusname(String seacont,Long id);

	List<Customer> saSelectCus2(Long id);
}

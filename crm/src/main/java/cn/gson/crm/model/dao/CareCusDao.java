package cn.gson.crm.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.CareCustomer;
import cn.gson.crm.model.entity.Employee;

public interface CareCusDao extends PagingAndSortingRepository<CareCustomer, Long>{

	//已是删除状态的不显示
	Page<CareCustomer> findAllByIsDelete(boolean b,Pageable pr);

	//显示与当前登录用户相关且未删除的客户关怀线索
	Page<CareCustomer> findAllByEmployeeAndIsDelete(Employee emp, boolean b, Pageable pr);
}

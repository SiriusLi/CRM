package cn.gson.crm.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 订单/发货/退货明细实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午6:19:27
 */
@Entity
@Table(name = "crm_order_detail")
public class OrderDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 *单价
	 */
	private double price;
	/**
	 * 数量
	 */
	private int amount;
	/**
	 * 总价
	 */
	private double money;
	/**
	 * 状态(订单：已发货，未发货,已退货;)
	 */
	private String state;
	/**
	 * 类型（订单、发货、退货）
	 */
	private String type;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 是否删除
	 */
	private boolean isDelete=false;
	/**
	 * 对应订单
	 */
	@ManyToOne
	private Order order;
	/**
	 * 对应产品
	 */
	@ManyToOne
	private Product product;
	
	/**
	 * 所有者
	 */
	@ManyToOne
	private Employee employee;

	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getType() {
		return type;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public OrderDetail() {
		super();
	}

	public OrderDetail(Long id, int amount, double money, String type, String note, Order order, Product product) {
		super();
		this.id = id;
		this.amount = amount;
		this.money = money;
		this.type = type;
		this.note = note;
		this.order = order;
		this.product = product;
	}

	@Override
	public String toString() {
		return "OrderDetail [id=" + id + ", amount=" + amount + ", money=" + money + ", type=" + type + ", note=" + note
				+ "]";
	}

}

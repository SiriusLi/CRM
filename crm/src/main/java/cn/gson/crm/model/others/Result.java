package cn.gson.crm.model.others;

public class Result {
	public String message = "";
	public boolean flag = false;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public Result(String message, boolean flag) {
		super();
		this.message = message;
		this.flag = flag;
	}
	public Result() {
		super();
	}
	
	
}

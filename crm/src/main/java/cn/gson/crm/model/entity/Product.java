package cn.gson.crm.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 产品实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午4:42:54
 */
@Entity
// 修改表名-zhn
@Table(name = "crm_product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 产品名称
	 */
	private String name;
	/**
	 * 产品编号
	 */
	private String number;
	/**
	 * 产品图片
	 */
	private String image;
	/**
	 * 价格
	 */
	private double price;
	/**
	 * 规格
	 */
	private String standard;
	/**
	 * 单位
	 */
	private String unit;
	/**
	 * 分类
	 */
	private String type;
	/**
	 * 状态(上架，下架)
	 */
	private boolean station;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 产品库存量
	 */
	private Integer stockSize;
	/**
	 * 仓库名称
	 */
	private String stockName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isStation() {
		return station;
	}

	public void setStation(boolean station) {
		this.station = station;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getStockSize() {
		return stockSize;
	}

	public void setStockSize(Integer stockSize) {
		this.stockSize = stockSize;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public Product() {
	}

	public Product(Long id, String name, String number, String image, double price, String standard, String unit,
			String type, boolean station, String note, Integer stockSize, String stockName) {
		super();
		this.id = id;
		this.name = name;
		this.number = number;
		this.image = image;
		this.price = price;
		this.standard = standard;
		this.unit = unit;
		this.type = type;
		this.station = station;
		this.note = note;
		this.stockSize = stockSize;
		this.stockName = stockName;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", number=" + number + ", image=" + image + ", price=" + price
				+ ", standard=" + standard + ", unit=" + unit + ", type=" + type + ", station=" + station + ", note="
				+ note + ", stockSize=" + stockSize + ", stockName=" + stockName + "]";
	}

}

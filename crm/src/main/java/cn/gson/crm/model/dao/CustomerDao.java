package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Customer;
import cn.gson.crm.model.entity.Employee;

public interface CustomerDao extends PagingAndSortingRepository<Customer, Long>{

	//已是删除状态的不显示
	Page<Customer> findAllByIsDelete(boolean b,Pageable pr);

	//通过联系人名称找到对应的客户
	Customer findByContacts(String contacts);
	
	Customer findByName(String cname);


	//经理显示的客户数据，只需要两个处理都处理好以及没有删除就行。
	Page<Customer> findAllByMaStateAndSaStateAndIsDelete(boolean b, boolean c, boolean d,Pageable pa);

	
	//销售人员显示的客户数据，在经理的基础上还得是符合登录用户的数据
	Page<Customer> findAllByMaStateAndSaStateAndIsDeleteAndEmployee(boolean b, boolean c, boolean d, Employee emp,
			Pageable pa);


	List<Customer> findByNameLike(String cname);

	Customer findOneByName(String cname);

	
	//找出所有和当前登录用户所关联的客户对象
	List<Customer> findAllByEmployee(Employee emp);
	
	//经理显示的客户线索，需要MaState为0且未删除的所有客户线索
	Page<Customer> findAllByMaStateAndIsDelete(boolean b,boolean d,Pageable pa);

	//销售人员显示的客户线索，需要与当前登录用户相关联，且未删除，未处理
	Page<Customer> findAllByEmployeeAndIsDeleteAndSaState(Employee emp, boolean b, boolean c, Pageable pr);

	
}

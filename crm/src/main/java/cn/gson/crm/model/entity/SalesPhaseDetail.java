package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 销售机会阶段跟踪实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午5:50:48
 */
@Entity
@Table(name = "crm_sales_phase_detail")
public class SalesPhaseDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 时间
	 */
	private Date date;
	/**
	 * 阶段
	 */
	private String phase;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	/**
	 * 对应销售机会
	 */
	@ManyToOne
	private SalesOpportunity salesOpp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}

	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}

	public SalesPhaseDetail() {
		// TODO Auto-generated constructor stub
	}

	public SalesPhaseDetail(Long id, Date date, String phase) {
		super();
		this.id = id;
		this.date = date;
		this.phase = phase;
	}

	@Override
	public String toString() {
		return "SalesPhaseDetail [id=" + id + ", date=" + date + ", phase=" + phase + "]";
	}

}

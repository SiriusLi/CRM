package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 任务实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午4:50:15
 */
@Entity
@Table(name = "crm_task")
public class Task {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 任务主题
	 */
	private String topic;
	/**
	 * 任务状态
	 */
	private String state;
	/**
	 * 任务完成期限
	 */
	@Column(name = "complete_date")
	private Date completeDate;/**
	 * 是否删除
	 */
	private boolean isDelete;
	
	/**
	 * 优先级
	 */
	private Integer weight;
	/**
	 * 任务发布时间
	 */
	@Column(name = "create_date")
	private Date createDate;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	/**
	 * 执行者
	 */
	@ManyToOne
	private Employee operator;
	/**
	 * 发布者
	 */
	@ManyToOne
	private Employee creator;
	/**
	 * 添加无参构造-zhn
	 */
	public Task() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getCompleteDate() {
		return completeDate;
	}
	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Employee getOperator() {
		return operator;
	}
	public void setOperator(Employee operator) {
		this.operator = operator;
	}
	public Employee getCreator() {
		return creator;
	}
	public void setCreator(Employee creator) {
		this.creator = creator;
	}
	@Override
	public String toString() {
		return "Task [id=" + id + ", topic=" + topic + ", state=" + state + ", completeDate=" + completeDate
				+ ", weight=" + weight + ", createDate=" + createDate + "]";
	}
	public Task(Long id, String topic, String state, Date completeDate, Integer weight, Date createDate) {
		super();
		this.id = id;
		this.topic = topic;
		this.state = state;
		this.completeDate = completeDate;
		this.weight = weight;
		this.createDate = createDate;
	}
	
}

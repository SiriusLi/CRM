package cn.gson.crm.model.mapper;

import java.util.List;
import java.util.Map;

import cn.gson.crm.model.entity.Repair;

public interface RepairMapper {

	public List<Repair> getRepairsByHighcheck(Map<String,Object> map);
	
	public List<Repair> selectAll();
}

package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 方案实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午6:09:35
 */
@Entity
@Table(name = "crm_solution")
public class Solution {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 方案主题
	 */
	private String topic;
	/**
	 * 时间
	 */
	private Date time;
	/**
	 * 方案内容
	 */
	private String content;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 对应销售机会
	 */
	@ManyToOne
	private SalesOpportunity salesOpp;
	/**
	 * 方案提供者
	 */
	@ManyToOne
	private Employee employee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}

	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Solution() {
		// TODO Auto-generated constructor stub
	}

	public Solution(Long id, String topic, Date time, String content, String note) {
		super();
		this.id = id;
		this.topic = topic;
		this.time = time;
		this.content = content;
		this.note = note;
	}

	@Override
	public String toString() {
		return "Solution [id=" + id + ", topic=" + topic + ", time=" + time + ", content=" + content + ", note=" + note
				+ "]";
	}

}

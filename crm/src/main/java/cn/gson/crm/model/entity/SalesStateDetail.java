package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 销售机会状态跟踪实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午5:52:19
 */
@Entity
@Table(name = "crm_sales_state_detail")
public class SalesStateDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 时间
	 */
	private Date date;
	/**
	 * 状态
	 */
	private String state;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	/**
	 * 对应销售机会
	 */
	@ManyToOne
	private SalesOpportunity salesOpp;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public boolean isDelete() {
		return isDelete;
	}
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public String getState() {
		return state;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getstate() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}
	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}
	public SalesStateDetail() {
		// TODO Auto-generated constructor stub
	}
	public SalesStateDetail(Long id, Date date, String state) {
		super();
		this.id = id;
		this.date = date;
		this.state = state;
	}
	@Override
	public String toString() {
		return "SalesStateDetail [id=" + id + ", date=" + date + ", state=" + state + "]";
	}
}

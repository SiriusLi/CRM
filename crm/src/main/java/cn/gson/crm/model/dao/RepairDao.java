package cn.gson.crm.model.dao;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Repair;

public interface RepairDao extends PagingAndSortingRepository<Repair, Long> {

	//Page<Repair> findAllOrderByState(Pageable pageable );

	Page<Repair> findAllByIsDelete(boolean b, Pageable pr);
	
	@Query("from Repair r ORDER BY FIELD(r.progress,'待接单','维修中','维修完','回访中','已完成') ")
	Page<Repair> getAll(Pageable pageable);
	
	//找到过期的维修工单
	@Query(" from Repair r where NOW()>r.delivery ")
	List<Repair> findOverdue();
	
	@Query(" from Repair r  ")
	List<Repair> selectRepairs();

	Page<Repair> findByProgress(String name,Pageable pageable);

	Page<Repair> findByNumber(String value, Pageable pr);

}

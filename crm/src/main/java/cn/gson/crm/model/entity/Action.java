package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
/**
 * 客户跟进记录实体类
 * @author hzl
 * @date 2018年4月8日
 * @time 下午5:16:29
 */
@Entity
@Table(name="crm_action")
public class Action {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 跟进日期
	 */
	private Date date;
	
	/**
	 * 跟进主题
	 */
	private String topic;
	
	/**
	 * 跟进内容
	 */
	private String content;
	/**
	 * 执行者
	 */
	@ManyToOne
	private Employee employee;
	/**
	 * 发布者（上司）
	 */
	@ManyToOne
	private Employee leader;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Action() {
		// TODO Auto-generated constructor stub
	}
	public Action(Long id, Date date, String topic, String content) {
		super();
		this.id = id;
		this.date = date;
		this.topic = topic;
		this.content = content;
	}
	@Override
	public String toString() {
		return "Action [id=" + id + ", date=" + date + ", topic=" + topic + ", content=" + content + "]";
	}
	
}

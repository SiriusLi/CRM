package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 提醒实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午9:49:17
 */
@Entity
@Table(name = "crm_remind")
public class Remind {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 提醒内容
	 */
	private String content;
	/**
	 * 提醒类型（回款，订单，发货、、、）
	 */
	private String type;
	/**
	 * 状态（是否已经查看）
	 */
	private boolean state=false;
	/**
	 * 时间
	 */
	private Date date;
	/**
	 * 提醒谁看
	 */
	@ManyToOne
	private Employee employee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Remind() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 有参构造
	 * @param content
	 * @param type
	 * @param state
	 * @param date
	 * @param employee
	 */
	public Remind(String content, String type, boolean state, Date date, Employee employee) {
		super();
		this.content = content;
		this.type = type;
		this.state = state;
		this.date = date;
		this.employee = employee;
	}
	

	@Override
	public String toString() {
		return "Remind [id=" + id + ", content=" + content + ", type=" + type + ", state=" + state + ", date=" + date
				+ "]";
	}

}

package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 发货状态明细实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午9:41:29
 */
@Entity
@Table(name = "crm_delivery_state_detail")
public class DeliveryStateDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 时间
	 */
	private Date date;
	/**
	 * 状态
	 */
	private String state;
	/**
	 * 对应发货实体类
	 */
	@ManyToOne
	private Delivery delivery;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Delivery getDelivery() {
		return delivery;
	}
	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
	public DeliveryStateDetail() {
		// TODO Auto-generated constructor stub
	}
	public DeliveryStateDetail(Date date, String state, Delivery delivery) {
		super();
		this.date = date;
		this.state = state;
		this.delivery = delivery;
	}
	@Override
	public String toString() {
		return "DeliveryStateDetail [id=" + id + ", date=" + date + ", state=" + state + "]";
	}
	
}

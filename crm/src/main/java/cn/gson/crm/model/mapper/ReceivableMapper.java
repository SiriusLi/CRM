package cn.gson.crm.model.mapper;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Receivable;

public interface ReceivableMapper extends PagingAndSortingRepository<Receivable,Long> {
	//按日查询实际回款
	Double actualMoneyByDay();
	
	//按周查询实际回款
	Double actualMoneyByWeek();
	
	//按月查询实际回款
	Double actualMoneyByMonth();

}

package cn.gson.crm.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 客户关怀实体类
 * 
 * @author hzl
 * @date 2018年4月12日
 * @time 下午10:11:58
 */
@Entity
@Table(name = "crm_care_customer")
public class CareCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 关怀主题
	 */
	private String topic;
	/**
	 * 关怀内容
	 */
	private String content;
	/**
	 * 关怀日期
	 */
	private Date date;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	/**
	 * 执行人
	 */
	@ManyToOne
	private Employee employee;
	
	/**
	 * 客户反馈
	 */
	private String feedback;

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public CareCustomer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CareCustomer(Long id, String topic, String content, Date date, Customer customer, Employee employee,
			String feedback) {
		super();
		this.id = id;
		this.topic = topic;
		this.content = content;
		this.date = date;
		this.customer = customer;
		this.employee = employee;
		this.feedback = feedback;
	}

	@Override
	public String toString() {
		return "CareCustomer [id=" + id + ", topic=" + topic + ", content=" + content + ", date=" + date + ", feedback=" + feedback + "]";
	}

	

}

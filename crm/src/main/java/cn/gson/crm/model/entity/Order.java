package cn.gson.crm.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 订单实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午5:19:33
 */
@Entity
@Table(name = "crm_order")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 订单编号
	 */
	private String number;
	/**
	 * 订单主题
	 */
	private String topic;
	/**
	 * 时间
	 */
	private Date time;
	/**
	 * 合同金额
	 */
	@Column(name = "order_money")
	private double orderMoney;
	/**
	 * 回款金额
	 */
	@Column(name = "receivable_money")
	private double receivableMoney;
	/**
	 * 是否删除
	 */
	private boolean isDelete=false;
	/**
	 * 状态(执行中)
	 */
	private String state;
	/**
	 * 发货(未发货，已发货，部分发货)
	 */
	private String delivery;
	/**
	 * 审核(未审核，通过，否决)
	 */
	private String audit;
	/**
	 * 审核日期
	 */
	@Column(name = "audit_date")
	private Date auditDate;
	/**
	 * 是否封存（true封存）
	 */
	private boolean sequestration=false;
	/**
	 * 是否退货(true退货)
	 */
	@Column(name = "return_goods")
	private boolean returnGoods=false;

	/**
	 * 收货人姓名
	 */
	@Column(name = "receive_man")
	private String receiveMan;
	/**
	 * 电话
	 */
	@Column(name = "receive_tel")
	private String receiveTel;
	/**
	 * 收货地址
	 */
	@Column(name = "receive_address")
	private String receiveAddress;

	/**
	 * 备注
	 */
	private String note;
	/**
	 * 对应销售机会
	 */
	@OneToOne
	private SalesOpportunity salesOpp;
	/**
	 * 所有者
	 */
	@ManyToOne
	private Employee employee;

	/**
	 * 对应客户
	 */
	@ManyToOne
	private Customer customer;
	/**
	 * 审核人
	 */
	@ManyToOne
	private Employee auditEmployee;

	/**
	 * 附件
	 */
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
			name = "crm_order_attachment", 
			joinColumns = @JoinColumn(name = "order_id"),
			inverseJoinColumns = @JoinColumn(name = "attachment_id"))
	private List<Attachment> attachment;

	/**
	 * 添加无参构造-zhn
	 */
	public Order() {
		super();
	}

	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getReceiveMan() {
		return receiveMan;
	}

	public void setReceiveMan(String receiveMan) {
		this.receiveMan = receiveMan;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getReceiveTel() {
		return receiveTel;
	}

	public void setReceiveTel(String receiveTel) {
		this.receiveTel = receiveTel;
	}

	public String getReceiveAddress() {
		return receiveAddress;
	}

	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public double getOrderMoney() {
		return orderMoney;
	}

	public void setOrderMoney(double orderMoney) {
		this.orderMoney = orderMoney;
	}

	public double getReceivableMoney() {
		return receivableMoney;
	}

	public void setReceivableMoney(double receivableMoney) {
		this.receivableMoney = receivableMoney;
	}

	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}

	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public boolean isSequestration() {
		return sequestration;
	}

	public void setSequestration(boolean sequestration) {
		this.sequestration = sequestration;
	}

	public boolean isReturnGoods() {
		return returnGoods;
	}

	public void setReturnGoods(boolean returnGoods) {
		this.returnGoods = returnGoods;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Employee getAuditEmployee() {
		return auditEmployee;
	}

	public void setAuditEmployee(Employee auditEmployee) {
		this.auditEmployee = auditEmployee;
	}

	public List<Attachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(List<Attachment> attachment) {
		this.attachment = attachment;
	}

	public String getAudit() {
		return audit;
	}

	public void setAudit(String audit) {
		this.audit = audit;
	}

	public Order(Long id, String number, String topic, Date time, double orderMoney, double receivableMoney,
			boolean isDelete, String state, String delivery, String audit, Date auditDate, boolean sequestration,
			boolean returnGoods, String receiveMan, String receiveTel, String receiveAddress, String note,
			SalesOpportunity salesOpp, Employee employee, Customer customer, Employee auditEmployee,
			List<Attachment> attachment) {
		super();
		this.id = id;
		this.number = number;
		this.topic = topic;
		this.time = time;
		this.orderMoney = orderMoney;
		this.receivableMoney = receivableMoney;
		this.isDelete = isDelete;
		this.state = state;
		this.delivery = delivery;
		this.audit = audit;
		this.auditDate = auditDate;
		this.sequestration = sequestration;
		this.returnGoods = returnGoods;
		this.receiveMan = receiveMan;
		this.receiveTel = receiveTel;
		this.receiveAddress = receiveAddress;
		this.note = note;
		this.salesOpp = salesOpp;
		this.employee = employee;
		this.customer = customer;
		this.auditEmployee = auditEmployee;
		this.attachment = attachment;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", number=" + number + ", topic=" + topic + ", time=" + time + ", orderMoney="
				+ orderMoney + ", receivableMoney=" + receivableMoney + ", isDelete=" + isDelete + ", state=" + state
				+ ", delivery=" + delivery + ", audit=" + audit + ", auditDate=" + auditDate + ", sequestration="
				+ sequestration + ", returnGoods=" + returnGoods + ", receiveMan=" + receiveMan + ", receiveTel="
				+ receiveTel + ", receiveAddress=" + receiveAddress + ", note=" + note + "]";
	}


}

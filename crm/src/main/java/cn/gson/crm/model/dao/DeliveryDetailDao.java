package cn.gson.crm.model.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.OrderDetail;

/**
 *发货明细
 * @author hzl
 * @date 2018年5月22日
 * @time 下午7:02:33
*/
public interface DeliveryDetailDao extends PagingAndSortingRepository<OrderDetail, Long> {

}

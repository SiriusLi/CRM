package cn.gson.crm.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 竞争对手实体类
 * 
 * @author hzl
 * @date 2018年4月8日
 * @time 下午6:13:35
 */
@Entity
@Table(name = "crm_competitor")
public class Competitor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 竞争公司名称
	 */
	@Column(name = "company_name")
	private String companyName;
	/**
	 * 报价
	 */
	private double money;
	/**
	 * 竞争力
	 */
	private String power;

	/**
	 * 竞争产品/方案
	 */
	private String comProgram;
	/**
	 * 劣势
	 */
	private String disadvantage;
	/**
	 * 应对方案
	 */
	private String solution;
	/**
	 * 备注
	 */
	private String note;
	/**
	 * 是否删除
	 */
	private boolean isDelete;
	/**
	 * 对应销售机会
	 */
	@ManyToOne
	private SalesOpportunity salesOpp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	public String getComProgram() {
		return comProgram;
	}

	public void setComProgram(String comProgram) {
		this.comProgram = comProgram;
	}

	public String getDisadvantage() {
		return disadvantage;
	}

	public void setDisadvantage(String disadvantage) {
		this.disadvantage = disadvantage;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public SalesOpportunity getSalesOpp() {
		return salesOpp;
	}

	public void setSalesOpp(SalesOpportunity salesOpp) {
		this.salesOpp = salesOpp;
	}

	public Competitor() {
		// TODO Auto-generated constructor stub
	}

	public Competitor(Long id, String companyName, double money, String power, String disadvantage, String solution,
			String note) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.money = money;
		this.power = power;
		this.disadvantage = disadvantage;
		this.solution = solution;
		this.note = note;
	}

	@Override
	public String toString() {
		return "Competitor [id=" + id + ", companyName=" + companyName + ", money=" + money + ", power=" + power
				+ ", disadvantage=" + disadvantage + ", solution=" + solution + ", note=" + note + "]";
	}

}

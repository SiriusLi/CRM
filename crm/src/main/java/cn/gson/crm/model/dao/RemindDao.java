package cn.gson.crm.model.dao;


import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Remind;

/**
 *提醒
 * @author hzl
 * @date 2018年5月25日
 * @time 下午8:32:14
*/

public interface RemindDao extends PagingAndSortingRepository<Remind, Long> {

	
}

package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Employee;

/*
 *
 * @author hzl
 * @date 2018年4月15日
 * @time 下午12:02:12
*/
public interface EmployeeDao extends PagingAndSortingRepository<Employee, Long>{

	Employee findOneByEmpName(String empName);
	
	int countByTelephone(String Telephone);
	
	int countByEmail(String email);
	
	Employee findByEmpName(String empName);
	

//	Iterable<Employee> findByIdNot(Long id);

	//查询所有未离职员工
	List<Employee> findByState(boolean b);


}

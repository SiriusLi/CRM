package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Action;
import cn.gson.crm.model.entity.Customer;

public interface ActionDao extends PagingAndSortingRepository<Action, Long> {

	List<Action> findByCustomer(Customer customer);
}

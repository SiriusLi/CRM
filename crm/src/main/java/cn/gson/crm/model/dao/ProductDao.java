package cn.gson.crm.model.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, Long> {

	//根据类型查找上架产品
	List<Product> findByTypeAndStation(String type,boolean b);

	//查找所有上架产品
	List<Product> findByStation(boolean b);

	Product findOneByName(String pname);

}

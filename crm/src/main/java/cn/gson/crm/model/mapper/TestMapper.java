package cn.gson.crm.model.mapper;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.crm.model.entity.Testhzl;

/**
 *
 * @author hzl
 * @date 2018年4月8日
 * @time 下午11:02:21
*/
public interface TestMapper extends PagingAndSortingRepository<Testhzl, Long>{

}

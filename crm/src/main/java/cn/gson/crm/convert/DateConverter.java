package cn.gson.crm.convert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.springframework.core.convert.converter.Converter;

public class DateConverter implements Converter<String, Date> {

	private String[] pattern;

	public String[] getPattern() {
		return pattern;
	}

	public void setPattern(String[] pattern) {
		this.pattern = pattern;
	}

	public DateConverter(String... pattern) {
		this.pattern = pattern;
	}

	public DateConverter() {
	}

	@Override
	public Date convert(String source) {
		if (source == "") {
		    return null;
		}else {
			Date date = null;		
			SimpleDateFormat df = new SimpleDateFormat();
			for (String string : pattern) {
				try {
					df.applyPattern(string);
					date = df.parse(source);
					break;
				} catch (ParseException e) {
				}
			}
			if (date == null) {
				throw new IllegalArgumentException("日期类型转换异常");
			}
			
			return date;
		}
		

	}

}

package cn.gson.crm.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.gson.crm.model.dao.AfterSaleDao;
import cn.gson.crm.model.entity.Repair;

@Service
public class AfterSaleService {

	@Autowired
	private AfterSaleDao aftersaleDao;
	
	/**
	 * 生成工单号
	 */
	public String createNum(){
		PageRequest page = new PageRequest(0, 10, Direction.DESC,"id");
		Page<Repair> repairs = aftersaleDao.findAll(page);
		if(!repairs.getContent().isEmpty()){
			Repair repart = repairs.getContent().get(0);
			String repairNum = repart.getNumber().substring(2, repart.getNumber().length());
			//自动生成新的单号
			Integer newRepairNum = Integer.parseInt(repairNum)+1;
			String num = "wx"+newRepairNum; 
			return num;
		}else{
			return "wx"+1;
		}
		
	
	}
	
	/**
	 * 获得工单截止日期
	 */
	public Date getResult(){
		Date date = new Date();
		Long miles = date.getTime()+86400000*7;
		date.setTime(miles);
		return date;
		
	}
	
	public String getDate(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
	
	
	public HSSFCell setMyStyle( HSSFRow row,int index ,String content,HSSFCellStyle style){
		HSSFCell cell = row.createCell(index);
		 cell.setCellValue(content);
		cell.setCellStyle(style);
		return cell;
	}
	
	
	

	
	/**
	 * 生成excel文件方法
	 */
	@SuppressWarnings("deprecation")
	public HSSFWorkbook createExcel(List<Repair> repairs,HttpServletRequest req){
		AfterSaleService afterservice = new AfterSaleService();
		//创建一个webbook
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("维修工单表");
		//设置列大小
		for(int i=0;i<15;i++){
			sheet.setColumnWidth(i, 45*100);
		}
		 HSSFRow row = sheet.createRow(0); 
		 HSSFCellStyle style = workbook.createCellStyle();
		 style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 带边框 
		    style.setBorderBottom(HSSFCellStyle.BORDER_THIN); 
		    // 生成一个字体 
		    HSSFFont font = workbook.createFont(); 
		    // 字体增粗 
		    font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); 
		    // 字体大小 
		    font.setFontHeightInPoints((short) 12); 
		    // 把字体应用到当前的样式 
		    style.setFont(font); 
		    //开始设定单元格内容
		    for (int i = 0; i < 15; i++) {
		    	if(i==0){
		    		afterservice.setMyStyle(row, 0, "id", style);
		    	}else if(i==1){
		    		afterservice.setMyStyle(row, 1, "维修内容", style);
		    	}else if(i==2){
		    		afterservice.setMyStyle(row, 2, "维修日期", style);
		    	}else if(i==3){
		    		afterservice.setMyStyle(row, 3, "截至日期", style);
		    	}else if(i==4){
		    		afterservice.setMyStyle(row, 4, "删除状态", style);
		    	}else if(i==5){
		    		afterservice.setMyStyle(row, 5, "维修价格", style);
		    	}else if(i==6){
		    		afterservice.setMyStyle(row, 6, "联系人", style);
		    	}else if(i==7){
		    		afterservice.setMyStyle(row, 7, "工单编号", style);
		    	}else if(i==8){
		    		afterservice.setMyStyle(row, 8, "工单状态", style);
		    	}else if(i==9){
		    		afterservice.setMyStyle(row, 9, "state", style);
		    	}else if(i==10){
		    		afterservice.setMyStyle(row, 10, "联系电话", style);
		    	}else if(i==11){
		    		afterservice.setMyStyle(row, 11, "维修主题", style);
		    	}else if(i==12){
		    		afterservice.setMyStyle(row, 12, "客户姓名", style);
		    	}else if(i==13){
		    		afterservice.setMyStyle(row, 13, "产品内容", style);
		    	}else if(i==14){
		    		afterservice.setMyStyle(row, 14, "对应员工", style);
		    	}
			}
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
		    //设置单元格的具体内容
		    for (int i = 0; i < repairs.size(); i++) {
		    	
		    	 String logTypeDis = "";
		    	 //设置好row对象
		         row = sheet.createRow(i + 1); 
		         Repair repair = repairs.get(i);
		         if(repair.isDelete()){
		        	 logTypeDis = "删除了";
		         }else{
		        	 logTypeDis = "未删除";
		         }
		         String qian = String.valueOf( repair.getMoney());
		         afterservice.setMyStyle(row, 0, repair.getId().toString(), style);
		         afterservice.setMyStyle(row, 1, repair.getContent(), style);
		         afterservice.setMyStyle(row, 2, sdf.format(repair.getDate()), style);
		         afterservice.setMyStyle(row, 3, sdf.format(repair.getDelivery()), style);
		         afterservice.setMyStyle(row, 4, logTypeDis, style);
		         afterservice.setMyStyle(row, 5,qian, style);
		         afterservice.setMyStyle(row, 6, repair.getName(), style);
		         afterservice.setMyStyle(row, 7, repair.getNumber(), style);
		         afterservice.setMyStyle(row, 8, repair.getProgress(), style);
		         afterservice.setMyStyle(row, 9, repair.getState().toString(), style);
		         afterservice.setMyStyle(row, 10, repair.getTel(), style);
		         afterservice.setMyStyle(row, 11, repair.getTopic(), style);
		         afterservice.setMyStyle(row, 12, repair.getCustomer().getName(), style);
		         afterservice.setMyStyle(row, 13, repair.getProduct().getName(), style);
		         afterservice.setMyStyle(row, 14, repair.getEmployee().getEmpName(), style);
			}
		    	return workbook;
	}
	
	
	
	
	
	
	
}

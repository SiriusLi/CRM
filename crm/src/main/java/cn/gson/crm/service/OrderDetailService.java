package cn.gson.crm.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import cn.gson.crm.model.dao.OrderDetailDao;
import cn.gson.crm.model.entity.OrderDetail;

/**
 *
 * @author hzl
 * @date 2018年4月21日
 * @time 下午9:36:35
*/
@Service
public class OrderDetailService {
	@Autowired
	OrderDetailDao orderDetailDao;
	/**
	 * 得到订单/发货/退货详情列表
	 * @param page
	 * @param rows
	 * @param type
	 * @return
	 */
	public Map<String, Object> getlist( int page, int rows,String type) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<OrderDetail> orderDetail = orderDetailDao.findByType(type, pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", orderDetail.getContent());
		data.put("total", orderDetail.getTotalElements());
		return data;
	}
}

package cn.gson.crm.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gson.crm.model.dao.ResourceDao;
import cn.gson.crm.model.dao.RoleDao;
import cn.gson.crm.model.entity.Resource;
import cn.gson.crm.model.entity.Role;

@Service
public class ResourceService {
	
	@Autowired
	ResourceDao rsDao;
	
	@Autowired
	RoleDao rDao;

	public List<Resource> getTreeData() {
		List<Resource> roots = rsDao.findRoot();
		this.recurse(roots);
		return roots;
	}
	
	public List<Resource> getTreeDataByRole(Role role) {
		List<Resource> roots = new LinkedList<>();
		for(Resource r : role.getResources()) {
			if(r.getParent() == null) roots.add(r);
		}
//		this.recurseByRole(roots, role);
//		System.out.println("rrrrrrrrrrrrrrrrrrr"+role.getResources());
//		return roots;
////		return role.getResources();
		
		List<Resource> tree = new ArrayList<>();
		roots.forEach(rs -> {
			if (rs.getParent() == null) {
				this.recursion(role.getResources(), rs);
				tree.add(rs);
			}
		});
		return roots;
	}
	
	private void recursion(List<Resource> menus, Resource parent) {
		List<Resource> children = new ArrayList<>();
		menus.forEach(rs2 -> {
			if (rs2.getParent() != null && parent.getId() == rs2.getParent().getId()) {
				this.recursion(menus, rs2);
				children.add(rs2);
			}
		});
		if(children.size() != 0)
			parent.setChildren(children);
	}
	
	public void recurseByRole(List<Resource> roots,Role role) {
		for (Resource root : roots) {
			List<Resource> children = new LinkedList<>();
			for(Resource r : role.getResources()) {
				if(r.getParent() != null && r.getParent().getId() == root.getId()) 
					children.add(r);
			}
//			this.recurseByRole(children,role);
//			if(children.size() != 0) root.setChildren(children);
		}
	}
//	public List<Resource> findChildren(Resource resource,Role role) {
//		List<Resource> children = new LinkedList<>();
//		for(Resource r : role.getResources()) {
//			if(r.getParent() != null && r.getParent().getId() == resource.getId()) 
//				children.add(r);
//		}
//		return children;
//	}
	private void recurse(List<Resource> roots) {
		for (Resource resource : roots) {
			List<Resource> children = rsDao.findByParentOrderByWeightDesc(resource);
			this.recurse(children);
			resource.setChildren(children);
		}
	}
	
	public List<Map<String, Object>> getGrantTreeData(List<Resource> resources) {
		List<Resource> roots = rsDao.findByParentIsNullAndEnableOrderByWeightDesc(true);
		return this.recurse2(roots, resources);
	}

	private List<Map<String, Object>> recurse2(List<Resource> node, List<Resource> checked) {
		List<Map<String, Object>> treeData = new LinkedList<>();
		for (Resource resource : node) {
			Map<String, Object> tnode = new HashMap<>();
			tnode.put("id", resource.getId());
			tnode.put("text", resource.getText());
			
			List<Resource> children = rsDao.findByParentAndEnableOrderByWeightDesc(resource, true);
			//只在根节点上,标识选中状态
			if(children.size() == 0) {
				tnode.put("checked", checked.contains(resource));
			}
			tnode.put("children", this.recurse2(children, checked));
			treeData.add(tnode);
		}
		return treeData;
	}
}

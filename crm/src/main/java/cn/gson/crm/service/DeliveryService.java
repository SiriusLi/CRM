package cn.gson.crm.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.gson.crm.model.dao.DeliveryDao;
import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.entity.Delivery;
import cn.gson.crm.model.entity.Order;

/**
 * 发货
 * 
 * @author hzl
 * @date 2018年5月25日
 * @time 下午10:55:11
 */
@Service
public class DeliveryService extends Thread{
	@Autowired
	DeliveryDao deliveryDao;
	
	@Autowired
	OrderDao orderdao;

	// 新建全局变量,截止时间记录将存放在这里
	List<Delivery> list = new ArrayList<Delivery>();

	// 服务启动自动加载
	@PostConstruct
	public void init() {
		// 获取数据库存在的订单截止时间记录
		list = deliveryDao.findByState("执行中");
		this.start();
	}

	// 重写run方法,this.start后将运行此方法
	@Override
	public void run() {
		while (!list.isEmpty()) {
			Calendar calendar = Calendar.getInstance();
			for (int i = 0; i < 1; i--) {
				try {
					// 每24个小时运行
					sleep(1000 * 60 * 60 * 24);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				// timer.schedule(new MyTask(), 1000, 2000);
				// 在1秒后执行此任务,每次间隔2秒执行一次,如果传递一个Data参数,就可以在某个固定的时间执行这个任务.
				for (Delivery delivery : list) {
					calendar.setTime(delivery.getDate());
					calendar.add(Calendar.DAY_OF_MONTH, 7);
					if("执行中".equals(delivery.getDate())&&calendar.getTime().before(new Date())){
						delivery.setState("已完成");
						deliveryDao.save(delivery);
						Order order=delivery.getOrder();
						//如果订单没有退货，订单也要完成
						if(!order.isReturnGoods()){
							order.setState("已完成");
							orderdao.save(order);
						}
					}
				}
			}

		}
	}

	// // list中添加数据,创建订单时调用
	public void addDeliveryEndPayTimeList(Delivery delivery) {
		list.add(delivery);
	}

	// list中删除数据,完成支付或者取消订单时调用
	public void removeDeliveryEndPayTimeList(Delivery delivery) {
		if (list.contains(delivery)) {
			List<Delivery> removeList = new ArrayList<Delivery>();
			removeList.add(delivery);
			list.removeAll(removeList);
		}
	}

}

package cn.gson.crm.controller.onsales;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author hzl
 * @date 2018年4月17日
 * @time 上午10:38:29
*/
@Controller
@RequestMapping("test")
public class TestController {
	@GetMapping
	public void test(){
		
	}
}

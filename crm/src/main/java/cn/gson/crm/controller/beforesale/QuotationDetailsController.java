package cn.gson.crm.controller.beforesale;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;

import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.dao.OrderDetailDao;
import cn.gson.crm.model.dao.ProductDao;
import cn.gson.crm.model.dao.QuotationDao;
import cn.gson.crm.model.dao.QuotationDetailsDao;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.model.entity.OrderDetail;
import cn.gson.crm.model.entity.Quotation;
import cn.gson.crm.model.entity.QuotationDetails;
import cn.gson.crm.model.entity.SalesOpportunity;
import cn.gson.crm.service.OrderDetailService;

@Controller
@RequestMapping("quotationDetails")
public class QuotationDetailsController {
	@Autowired
	OrderDetailDao orderDetailDao;

	@Autowired
	OrderDao orderDao;

	@Autowired
	ProductDao productDao;

	@Autowired
	QuotationDao quotationDao;

	@Autowired
	QuotationDetailsDao quotationDetailsDao;

	@Autowired
	OrderDetailService orderDetailService;

	@RequestMapping("list/{quotationId}")
	@ResponseBody
	public Map<String,Object> list(@PathVariable(name="quotationId")Long quotationId){
		Map<String,Object> data = new HashMap<>();
		//判断报价是否存在
		Quotation quotation = quotationDao.findOne(quotationId);
		if(quotation!=null) {
			List<QuotationDetails> details = quotationDetailsDao.findByQuotation(quotation);
			PageInfo<QuotationDetails> info = new PageInfo<>(details);
			data.put("rows",info.getList());
			data.put("total", info.getTotal());
		}
		return data;
	}
	
	// 2查出订单是否有报价单。
	@GetMapping("getQuotationDetails/{id}")
	@ResponseBody
	public JSONArray getQuotationDetails(@PathVariable(name = "id") Long id) {
		JSONArray jsonArray = new JSONArray();
		Quotation quotation = quotationDao.findOne(id);
		if (quotation != null) {
			List<QuotationDetails> qds = quotationDetailsDao.findByQuotation(quotation);
			for (QuotationDetails qd : qds) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("pid", qd.getProduct().getId());
				jsonObject.put("price", qd.getQuotedPrice());
				jsonObject.put("amount", qd.getNum());
				jsonObject.put("money", qd.getQuotedPrice() * qd.getNum());
				jsonObject.put("productName", qd.getProduct().getName());
				jsonArray.add(jsonObject);
			}
		}
		return jsonArray;
	}
	// 3保存在订单生成时的订单明细
	@PostMapping("save/{orderid}")
	@ResponseBody
	public JSONObject saveOrderDetail(@RequestParam("param") String list,
			@PathVariable(name = "orderid") Long orderid) {
		JSONObject result = new JSONObject();
		Quotation quotation = quotationDao.findOne(orderid);
		double orderMoney = 0;
		if (quotation == null) {
			result.put("err", "保存失败！");
			return result;
		}
		JSONArray jsonArray = JSONArray.parseArray(list);
		if (jsonArray.size() > 0) {
			for (int i = 0; i < jsonArray.size(); i++) {
				double money = 0;
				// 遍历 jsonarray 数组，把每一个对象转成 json 对象
				JSONObject obj = jsonArray.getJSONObject(i);
				QuotationDetails quotationDetails = new QuotationDetails();
				// 得到 每个对象中的属性值
				//报价
				quotationDetails.setQuotedPrice(obj.getDoubleValue("price"));
				//数量
				quotationDetails.setNum(obj.getInteger("amount"));
				//对应产品
				quotationDetails.setProduct(productDao.findOne(obj.getLong("pid")));
				//报价实体类
				quotationDetails.setQuotation(quotation);
				//成本价
				quotationDetails.setPrice(quotationDetails.getProduct().getPrice());
				//总价
				money = quotationDetails.getQuotedPrice()*quotationDetails.getNum();
				orderMoney +=money;
				quotationDetailsDao.save(quotationDetails);
			}
		}
		quotation.setMoney(orderMoney);
		quotationDao.save(quotation);
		result.put("quotation", quotation);
		result.put("success", true);
		return result;
	}
	// 4在订单明细中逐条添加订单明细
	@PostMapping("saveone")
	@ResponseBody
	public JSONObject saveOneOrderDetail(@Valid OrderDetail orderDetail, BindingResult br,
			@RequestParam("productid") Long pid, @RequestParam("orderid") Long oid, @RequestParam("type") String type) {
		JSONObject result = new JSONObject();
		if (br.hasErrors()) {
			System.out.println(br);
			result.put("err", "校验失败");
		} else {
			Order order = orderDao.findOne(oid);
			orderDetail.setOrder(order);
			orderDetail.setProduct(productDao.findOne(pid));
			orderDetail.setCustomer(order.getCustomer());
			orderDetail.setType(type);
			orderDetail.setState("未发货");
			orderDetailDao.save(orderDetail);
			result.put("success", true);
		}
		return result;
	}
}


package cn.gson.crm.controller.permission;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
@RequestMapping("permission")
public class PermissionController {

	@GetMapping
	public String indexTest() {
		return "permission/permission_test";
	}
	
	@GetMapping("user")
	public String toUser() {
		return "permission/user";
	}
	
	@GetMapping("role")
	public String toRole() {
		return "permission/role";
	}
	
	@GetMapping("employee")
	public String toEmployee() {
		return "permission/employee";
	}
	
	@GetMapping("resource")
	public String index() {
		return "permission/resource";
	}
	
	
	@RequestMapping({ "role/form", "role/form/{id}" })
	public String getRoleForm(@PathVariable(required = false) Long id, Model model) {
		if (id != null) {
		}
		return "permission/role/form";
	}
	
	
}

package cn.gson.crm.controller.permission;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 个人中心页
 * @author LJJ
 *
 */
@Controller
public class PersonalPageController {

	@GetMapping("personal/mainpage")
	public String mainPage(){
		return "personalpage/person";
	}
	
	@GetMapping("personal/show")
	public String show(){
		return "personalpage/show";
	}
	
	@GetMapping("personal/security")
	public String scurity(){
		
		return "personalpage/security";
		
	}
	
	/**
	 * 修改密码
	 * @return
	 */
	@GetMapping("personal/changepwd")
	public String changePwd(){
		return "personalpage/changepwd";
	}
	
	/**
	 * 修改基本资料
	 * @return
	 */
	@GetMapping("personal/changefile")
	public String changeFile(){
		return "personalpage/changefile";
	}
	/**
	 * 上传头像
	 * @return
	 */
	@GetMapping("personal/upload")
	public String upload(){
		return "personalpage/upload";
	}
	
	
}

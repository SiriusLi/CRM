package cn.gson.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("action")
public class ActionController {

	@RequestMapping
	public String init() {
		return "action/action";
	}
	
	@RequestMapping({"actionform","actionform/{id}"})
	public String cusForm(@PathVariable(required=false) Long id,Model model){
		return "action/actionform";
	}
}

package cn.gson.crm.controller.onsales;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gson.crm.model.dao.ReceivableDao;
import cn.gson.crm.model.entity.Receivable;

/**
 *回款记录
 * @author hzl
 * @date 2018年4月21日
 * @time 下午6:33:05
*/
@Controller
@RequestMapping("crm/receivableRecord")
public class ReceivableRecordController {
	@Autowired
	ReceivableDao receivableDao;
	
	@GetMapping
	public String index(){
		return "onsales/order/receivable_record_list";
	}
	
	/**
	 * 回款记录
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getlist(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Receivable> receivable = receivableDao.findByState("已回款",pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", receivable.getContent());
		data.put("total", receivable.getTotalElements());
		return data;
	}
}

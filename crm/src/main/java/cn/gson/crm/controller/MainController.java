package cn.gson.crm.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import cn.gson.crm.model.dao.MessageDao;
import cn.gson.crm.model.dao.TaskDao;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Message;
import cn.gson.crm.model.entity.Task;
import cn.gson.crm.model.mapper.ActionMapper;
import cn.gson.crm.model.mapper.CareCustomerMapper;
import cn.gson.crm.model.mapper.CustomerMapper;
import cn.gson.crm.model.mapper.OpportunityMapper;
import cn.gson.crm.model.mapper.OrderMapper;
import cn.gson.crm.model.mapper.ReceivableMapper;
import cn.gson.crm.model.mapper.RemindMapper;
import cn.gson.crm.model.mapper.TaskMapper;

/**
 *
 * @author hzl
 * @date 2018年4月14日
 * @time 下午4:00:50
 */
@Controller
@RequestMapping("crm/main")
public class MainController {

	
	@Autowired
	private OrderMapper orderMapper;

	@Autowired
	private ReceivableMapper receivableMapper;
	
	@Autowired
	private OpportunityMapper opportunityMapper;
	
	@Autowired
	private CustomerMapper customerMapper;
	
	@Autowired
	private CareCustomerMapper careCustomerMapper;
	
	@Autowired
	private ActionMapper  actionMapper;
	
	@Autowired
	private MessageDao messageDao;
	
	@Autowired
	private TaskDao taskDao;
	
	@Autowired
	private RemindMapper remindMapper;
	
	@Autowired
	private TaskMapper  taskMapper;

	@GetMapping
	public String main(Model model,HttpSession session) {
//		model.addAttribute("total",orderMapper.NumByDay());
		model.addAttribute("money", orderMapper.MoneyByDay());
		model.addAttribute("rmoney",receivableMapper.actualMoneyByDay());
		model.addAttribute("clue", customerMapper.careNumDay());
		model.addAttribute("client", customerMapper.clientNumDay());
		model.addAttribute("oop", opportunityMapper.oppByDay());
		model.addAttribute("care", careCustomerMapper.careCustomerByDay());
		model.addAttribute("action", actionMapper.actionByDay());
		PageRequest pr = new PageRequest(0, 5,Direction.DESC,"time");
		Page<Message> prm = messageDao.findAll(pr);
		model.addAttribute("messages", prm.getContent());
		Employee employee = (Employee) session.getAttribute("employee");
		model.addAttribute("reminds", remindMapper.selectRemindEmp(employee.getId()));
		List<Task> tasks = taskMapper.selectTask(employee.getId());
		model.addAttribute("tasks", tasks);
		return "main";
	}
	
	@PostMapping
	@ResponseBody
	public JSONArray doMain(@RequestParam(required = false) Integer style){
		//System.out.println(style + "sssssssssssss");
		Long total = null;
		Double money = null;
		Double rmoney = null;
		Long clue = null;
		Long client = null;
		Long oop = null;
		Long care = null;
		Long action = null;
			// 按周查询
			if (style == 2) {
				total = orderMapper.NumByWeek();
				money = orderMapper.MoneyByWeek();
				rmoney = receivableMapper.actualMoneyByWeek();
				clue = customerMapper.careNumWeek();
				client = customerMapper.clientNumWeek();
				oop =  opportunityMapper.oppByWeek();
				care = careCustomerMapper.careCustomerByWeek();
				action = actionMapper.actionByWeek();
			} else if (style == 3) {
				total = orderMapper.NumByMonth();
				money = orderMapper.MoneyByMonth();
				rmoney = receivableMapper.actualMoneyByMonth();
				clue = customerMapper.careNumMonth();
				client = customerMapper.clientNumMonth();
				oop =  opportunityMapper.oppByMonth();
				care = careCustomerMapper.careCustomerByMonth();
				action = actionMapper.actionByMonth();
			}else {
			total = orderMapper.NumByDay();
			money = orderMapper.MoneyByDay();
			rmoney = receivableMapper.actualMoneyByDay();
			clue = customerMapper.careNumDay();
			client = customerMapper.clientNumDay();
			oop =  opportunityMapper.oppByDay();
			care = careCustomerMapper.careCustomerByDay();
			action = actionMapper.actionByDay();
		}
		JSONArray js = new JSONArray();
		js.add(0,total);
		js.add(1,money);
		js.add(2,rmoney);
		js.add(3, clue);
		js.add(4, client);
		js.add(5, oop);
		js.add(6, action);
		js.add(7, care);
		return js;
		
	}

	// 首页
	@GetMapping({ "shouye" })
	public String shouye(Model model) {

		return "system/shouye";
	}
}

package cn.gson.crm.controller.onsales;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;


import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.gson.crm.model.dao.DeliveryDao;
import cn.gson.crm.model.dao.DeliveryStateDao;
import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.entity.Delivery;
import cn.gson.crm.model.entity.DeliveryStateDetail;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.service.DeliveryService;

/**
 * 发货
 * 
 * @author hzl
 * @date 2018年4月13日
 * @time 上午9:02:26
 */
@Controller
@RequestMapping("crm/delivery")
public class DeliveryController {
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	DeliveryDao deliveryDao;
	
	@Autowired
	DeliveryStateDao deliveryStateDao;
	
	@Autowired
	DeliveryService deliveryService;

	/**
	 * 发货列表
	 * @return
	 */
	@GetMapping
	public String deliveryList() {
		return "onsales/order/delivery_list";
	}

	// 获取发货列表
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getlist(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Delivery> delivery = deliveryDao.findAll(pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", delivery.getContent());
		data.put("total", delivery.getTotalElements());
		return data;
	}

	// 保存发货
	@PostMapping("save")
	@ResponseBody
	public JSONObject saveOrder(@Valid Delivery delivery, BindingResult br, @RequestParam("orderid") Long oid) {
		JSONObject result = new JSONObject();
		if (br.hasErrors()) {
			System.out.println(br);
			result.put("err", "校验失败！");
		} else {
			String state="待发货";
			Date date=new Date();
			Order order=orderDao.findOne(oid);
			delivery.setDate(new Date());
			delivery.setState(state);
			delivery.setOrder(order);
			delivery.setCustomer(order.getCustomer());
			 Delivery delivery1=deliveryDao.save(delivery);
			 //生成发货状态
			 deliveryStateDao.save(new DeliveryStateDetail(date,state,delivery1));
			result.put("success", true);
		}
		return result;
	}
	//修改发货单以及订单的状态
	@GetMapping("finishDelivery/{deliveryid}")
	public JSONObject finishDelivery(@PathVariable("deliveryid") Long deliveryid){
		JSONObject result = new JSONObject();
		try {
			Delivery delivery=deliveryDao.findOne(deliveryid);
			delivery.setState("已完成");
			deliveryDao.save(delivery);
			deliveryService.removeDeliveryEndPayTimeList(delivery);
			//生成发货状态记录
			deliveryStateDao.save(new DeliveryStateDetail(new Date(),"完成",delivery));
			 
			Order order=delivery.getOrder();
			order.setState("已完成");
			orderDao.save(order);
			result.put("mesage", "保存成功！");
		} catch (Exception e) {
			result.put("mesage", "失败了！");
		}
		return result;
	}
	/**
	 * 发货明细列表
	 * @return
	 */
	@GetMapping("deliveryDetail")
	public String deliveryDetail(){
		return "onsales/order/delivery_detail_list";
	}
}

package cn.gson.crm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("lee")
public class DemoController {

	@RequestMapping("login")
	public String index() {
		return "login";
	}
	
	@RequestMapping("main")
	public String mian() {
		return "main";
	}
	
	/**
	 * 获客网页1
	 * @author PD
	 */
	@RequestMapping("off")
	public String off() {
		return "off_form";
	}
	
	/**
	 * 获客网页2
	 * @author PD
	 */
	@RequestMapping("off2")
	public String off2() {
		return "off_form2";
	}
	
}

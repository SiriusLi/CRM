package cn.gson.crm.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import cn.gson.crm.model.dao.MessageDao;
import cn.gson.crm.model.entity.Message;
import cn.gson.crm.model.entity.Order;

/**
 *
 * @author hzl
 * @date 2018年5月4日
 * @time 下午4:50:23
 */
@Controller
@RequestMapping("message")
public class MessageController {

	@Autowired
	MessageDao messageDao;
	
	@GetMapping
	public String message(){
		return "system/message";
	}

	// 获取消息列表，分页
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getMessage(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "5") int rows) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Message> message = messageDao.findAll(pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", message.getContent());
		data.put("total", message.getTotalElements());
		return data;
	}

}

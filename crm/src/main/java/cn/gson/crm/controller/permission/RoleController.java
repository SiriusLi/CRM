package cn.gson.crm.controller.permission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;


import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Resource;
import cn.gson.crm.model.dao.RoleDao;

import cn.gson.crm.model.entity.Role;
import cn.gson.crm.service.ResourceService;

@Controller
@RequestMapping("role")
public class RoleController {

	@Autowired
	RoleDao rDao;
	
	@Autowired
	ResourceService rsService;
	
	@PostMapping("list")
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Role> roles = rDao.findAll(pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", roles.getContent());
		data.put("total", roles.getTotalElements());
		return data;
	}
	
	@GetMapping({ "form", "form/{id}" })
	public String getRoleForm(@PathVariable(required = false) Long id, Model model) {
		if (id != null) {
			model.addAttribute("r", rDao.findOne(id));
		}
		return "permission/role/form";
	}
	
	@PostMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid Role role, BindingResult br) {
		JSONObject message = new JSONObject();
		//如果valid校验失败
		if(br.hasErrors()) {
			message.put("error", "校验失败");
		} else {
			rDao.save(role);
			message.put("success", true);
		}
		return message;
	}
	
	@PostMapping("delete/{id}")
	@ResponseBody
	@Transactional
	public JSONObject delete(@PathVariable(required = true)  Long id) {
		JSONObject result = new JSONObject();
		try {
			rDao.delete(id);
			result.put("success", true);
		} catch (Exception e) {
			result.put("success", false);
			result.put("message", e.getMessage());
		}
		return result;
	}
	
	//调出dialog
	@GetMapping("grant/{roleId}")
	public String grant(@PathVariable Long roleId, Model model) {
		model.addAttribute("roleId", roleId);
		return "permission/role/grant";
	}
	
	//得到树数据
	@RequestMapping("resource/{roleId}")
	@ResponseBody
	public List<Map<String, Object>> resource(@PathVariable Long roleId) {
		Role role = rDao.findOne(roleId);
		List<Map<String, Object>> treeData = rsService.getGrantTreeData(role.getResources());
		return treeData;
	}
	
	@PostMapping("grant")
	@ResponseBody
	public Boolean grant(Long roleId, Long[] rid) {
		Role role = rDao.findOne(roleId);

		List<Resource> resources = new ArrayList<>();
		for (Long id : rid) {
			resources.add(new Resource(id));
		}
		role.setResources(resources);
		rDao.save(role);
		
		return true;
	}
}

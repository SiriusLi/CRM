package cn.gson.crm.controller.onsales;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.gson.crm.model.dao.DeliveryDao;
import cn.gson.crm.model.dao.DeliveryDetailDao;
import cn.gson.crm.model.dao.DeliveryStateDao;
import cn.gson.crm.model.dao.MessageDao;
import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.dao.OrderDetailDao;
import cn.gson.crm.model.entity.Delivery;
import cn.gson.crm.model.entity.DeliveryStateDetail;
import cn.gson.crm.model.entity.Message;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.model.entity.OrderDetail;
import cn.gson.crm.model.entity.Product;
import cn.gson.crm.service.DeliveryService;
import cn.gson.crm.service.OrderDetailService;

/**
 * 发货明细
 * 
 * @author hzl
 * @date 2018年4月23日
 * @time 上午10:01:26
 */
@Controller
@RequestMapping("crm/deliveryDetail")
public class DeliveryDetailController {
	@Autowired
	OrderDao orderDao;

	@Autowired
	DeliveryDetailDao deliveryDetailDao;

	@Autowired
	OrderDetailService orderDetailService;

	@Autowired
	DeliveryDao deliveryDao;

	@Autowired
	DeliveryStateDao deliveryStateDao;

	@Autowired
	MessageDao messageDao;
	
	@Autowired
	OrderDetailDao orderDetailDao;

	@Autowired
	DeliveryService deliveryService;


	@GetMapping
	public String deliveryDetailList() {
		return "onsales/order/delivery_detail_list";
	}

	// 获取发货明细列表
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getlist(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		return orderDetailService.getlist(page, rows, "发货");
	}

	// 保存发货明细
	@PostMapping("saveDeliveryDetail/{orderid}")
	@ResponseBody
	public JSONObject saveDeliveryDetail(@RequestParam("orderDetails") String list,@RequestParam("delivery") String delivery_value,
			@PathVariable("orderid") Long orderid) {
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.parseArray(list);
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyMMdd");
		Order order = orderDao.findOne(orderid);
		if (order == null) {
			result.put("err", "后台发生错误了！");
			return result;
		}
		// 1.保存发货明细
		if (jsonArray.size() > 0) {
			for (int i = 0; i < jsonArray.size(); i++) {
				// 遍历 jsonarray 数组，把每一个对象转成 json 对象
				JSONObject obj = jsonArray.getJSONObject(i);
				OrderDetail orderDetail = new OrderDetail();
				// 得到 每个对象中的属性值，得发货明细对象
				orderDetail.setPrice(obj.getDoubleValue("price"));
				orderDetail.setAmount(obj.getInteger("amount"));
				orderDetail.setNote(obj.getString("note"));
				orderDetail.setMoney(obj.getDoubleValue("price") * obj.getInteger("amount"));
				orderDetail.setProduct(obj.getObject("product", Product.class));
				orderDetail.setOrder(order);
				orderDetail.setCustomer(order.getCustomer());
				orderDetail.setEmployee(order.getEmployee());
				orderDetail.setType("发货");
				deliveryDetailDao.save(orderDetail);
				// 将订单明细的由未发货改成已发货状态
				OrderDetail orderDetail1 = orderDetailDao.findOne(obj.getLong("id"));
				orderDetail1.setState("已发货");
				orderDetailDao.save(orderDetail1);
				orderDetailDao.save(orderDetail);
			}
			// 2.生成发货单
			Delivery delivery = new Delivery();
			delivery.setDate(new Date());
			delivery.setNumber("D" + formatter1.format(new Date()) + (10 + (int) (Math.random() * 90))
					+ (char) ('a' + Math.random() * ('z' - 'a' + 1)));
			delivery.setReceiveMan(order.getReceiveMan());
			delivery.setReceiveTel(order.getReceiveTel());
			delivery.setReceiveAddress(order.getReceiveAddress());
			delivery.setState(delivery_value);
			delivery.setOrder(order);
			delivery.setCustomer(order.getCustomer());
			deliveryDao.save(delivery);
			deliveryService.addDeliveryEndPayTimeList(delivery);
			
			// 3.生成发货记录
			deliveryStateDao.save(new DeliveryStateDetail(new Date(), "发货", delivery));
			// 4.将订单的发货状态改为已发货
			order.setDelivery("已发货");
			orderDao.save(order);
			// 5.生成一条发货的消息
			messageDao.save(new Message("发货", new Date(), null, "发货成功", "订单  " + order.getTopic() + "发货成功了！"));

			result.put("success", "成功！");
		}else{
			result.put("err", "没有数据传到后台，所以失败了！");
		}
		return result;
	}
}

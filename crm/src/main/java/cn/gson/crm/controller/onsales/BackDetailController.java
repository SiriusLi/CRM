package cn.gson.crm.controller.onsales;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.gson.crm.model.dao.BackDao;
import cn.gson.crm.model.dao.DeliveryDetailDao;
import cn.gson.crm.model.dao.MessageDao;
import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.dao.OrderDetailDao;
import cn.gson.crm.model.entity.Back;
import cn.gson.crm.model.entity.Delivery;
import cn.gson.crm.model.entity.DeliveryStateDetail;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Message;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.model.entity.OrderDetail;
import cn.gson.crm.model.entity.Product;
import cn.gson.crm.service.BackService;
import cn.gson.crm.service.OrderDetailService;

/**
 * 退货明细
 * 
 * @author hzl
 * @date 2018年4月23日
 * @time 上午11:10:28
 */
@Controller
@RequestMapping("crm/backDetail")
public class BackDetailController {
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	DeliveryDetailDao deliveryDetailDao;

	@Autowired
	OrderDetailService orderDetailService;
	
	@Autowired
	OrderDetailDao orderDetailDao;
	
	@Autowired
	BackDao backDao;
	
	@Autowired
	MessageDao messageDao;
	
	@Autowired
	BackService backService;

	@GetMapping
	public String deliveryDetailList() {
		return "onsales/order/back_detail_list";
	}

	// 获取退货明细列表
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getlist(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		return orderDetailService.getlist(page, rows, "退货");
	}

	// 保存退货明细
	@PostMapping("saveBackDetail/{orderid}")
	@ResponseBody
	public JSONObject saveBackDetail(@RequestParam("orderDetails") String list, 
			@RequestParam("note") String note, 
			@PathVariable("orderid") Long orderid,HttpServletRequest req) {
		System.out.println("haha"+list);
		HttpSession session=req.getSession();
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.parseArray(list);
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyMMdd");
		double backMoney=0;//退货金额
		Order order = orderDao.findOne(orderid);
		if (order == null) {
			result.put("err", "后台发生错误了！");
			return result;
		}
		if (jsonArray.size() > 0) {
			// 1.保存发货明细
			for (int i = 0; i < jsonArray.size(); i++) {
				// 遍历 jsonarray 数组，把每一个对象转成 json 对象
				JSONObject obj = jsonArray.getJSONObject(i);
				OrderDetail orderDetail = new OrderDetail();
				// 得到 每个对象中的属性值
				orderDetail.setPrice(obj.getDoubleValue("price"));
				orderDetail.setAmount(obj.getInteger("amount"));
				orderDetail.setNote(obj.getString("note"));
				orderDetail.setMoney(obj.getDoubleValue("price") * obj.getInteger("amount"));
				orderDetail.setNote(obj.getString("note"));
				orderDetail.setProduct(obj.getObject("product", Product.class));
				orderDetail.setOrder(order);
				orderDetail.setCustomer(order.getCustomer());
				orderDetail.setEmployee(order.getEmployee());
				orderDetail.setType("退货");
				//获取退货金额
				backMoney+=orderDetail.getMoney();
				
				// 将订单明细的由发货改成已退货状态
				OrderDetail orderDetail1 = orderDetailDao.findOne(obj.getLong("id"));
				orderDetail1.setState("已退货");
				deliveryDetailDao.save(orderDetail1);
				deliveryDetailDao.save(orderDetail);
			}
			// 2.生成退货单
			Back back = new Back();
			back.setDate(new Date());
			back.setNumber("B" + formatter1.format(new Date()) + (10 + (int) (Math.random() * 90))
					+ (char) ('a' + Math.random() * ('z' - 'a' + 1)));
			back.setState("退货中");
			back.setMoney(backMoney);
			back.setNote(note);
			back.setRetiredmoney(backMoney);
			back.setEmployee((Employee) session.getAttribute("employee"));
			back.setOrder(order);
			backDao.save(back);
			backService.addBackEndPayTimeList(back);
			
			
			// 4.将订单的退货状态改为true
			order.setReturnGoods(true);
			orderDao.save(order);
			
			// 5.生成一条退货的消息
			messageDao.save(new Message("退货", new Date(), null, "有退货", "订单  " + order.getTopic() + "退货！"));
			result.put("success", "成功！");
		}else{
			result.put("err", "后台发生错误了！");
		}
		return result;
	}
}

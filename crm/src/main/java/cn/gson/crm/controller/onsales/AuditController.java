package cn.gson.crm.controller.onsales;
/**
 *审核
 * @author hzl
 * @date 2018年4月12日
 * @time 下午3:56:25
*/

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.dao.QuotationDao;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.model.entity.Quotation;

@Controller
@RequestMapping("crm/audit")
public class AuditController {
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	QuotationDao quotationDao;

	@GetMapping
	public String auditList() {
		return "onsales/order/auditlist";
	}

	// 保存审批订单
	@PostMapping("saveOrder")
	@ResponseBody
	public JSONObject saveOrder(@RequestParam(value = "id") Long oid, @RequestParam(value = "audit") String audit,
			HttpServletRequest req) {
		System.out.println(oid + audit);
		JSONObject result = new JSONObject();
		Employee emp = (Employee) req.getSession().getAttribute("employee");
		Order order = orderDao.findOne(oid);
		order.setAudit(audit);
		order.setAuditDate(new Date());
		order.setAuditEmployee(emp);
		try {
			orderDao.save(order);
			result.put("success", true);
		} catch (Exception e) {
			System.out.println(e);
			result.put("err", "保存失败！");
		}
		return result;
	}

	// 保存审批报价
	@PostMapping("saveQuotation")
	@ResponseBody
	public JSONObject saveQuotation(@RequestParam(value = "id") Long qid, @RequestParam(value = "approval") String audit,
			HttpServletRequest req) {
		System.out.println(qid + audit);
		JSONObject result = new JSONObject();
		Employee emp = (Employee) req.getSession().getAttribute("employee");
		Quotation quotation=quotationDao.findOne(qid);
		quotation.setAuditDate(new Date());
		quotation.setApproval(audit);
		quotation.setApprovalEmployee(emp);
		try {
			quotationDao.save(quotation);
			
			result.put("success", true);
		} catch (Exception e) {
			System.out.println(e);
			result.put("err", "保存失败！");
		}
		return result;
	}
}

package cn.gson.crm.controller.onsales;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.gson.crm.model.dao.BackDao;
import cn.gson.crm.model.dao.OrderDao;
import cn.gson.crm.model.entity.Back;
import cn.gson.crm.model.entity.Delivery;
import cn.gson.crm.model.entity.DeliveryStateDetail;
import cn.gson.crm.model.entity.Employee;
import cn.gson.crm.model.entity.Order;
import cn.gson.crm.service.BackService;
import cn.gson.crm.service.OrderDetailService;

/**
 * 退货
 * 
 * @author hzl
 * @date 2018年4月13日
 * @time 上午9:33:18
 */
@Controller
@RequestMapping("crm/back")
public class BackController {
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	BackDao backDao;
	
	@Autowired
	BackService backService;

	@Autowired
	OrderDetailService orderDetailService;
	
	@GetMapping
	public String backList() {
		return "onsales/order/back_list";
	}

	// 获取退货列表
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> getlist(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Back> back = backDao.findAll(pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", back.getContent());
		data.put("total", back.getTotalElements());
		return data;
	}

	// 保存退货
	@PostMapping("save")
	@ResponseBody
	public JSONObject saveOrder(@Valid Back back, BindingResult br, @RequestParam(value="orderid",required=false) Long oid,HttpServletRequest req) {
		JSONObject result = new JSONObject();
		if (br.hasErrors()) {
			System.out.println(br);
			result.put("err", "校验失败！");
		} else {
			Employee emp=(Employee) req.getSession().getAttribute("employee");
			back.setOrder(orderDao.findOne(oid));
			back.setEmployee(emp);
			back.setDate(new Date());
			back.setState("退货中");
			backDao.save(back);
			backService.addBackEndPayTimeList(back);
			result.put("success", true);
		}
		return result;
	}
	//修改发货单以及订单的状态
	@GetMapping("finishBack/{backid}")
	public JSONObject finishBack(@PathVariable("backid") Long backid){
		JSONObject result = new JSONObject();
		try {
			Back back=backDao.findOne(backid);
			back.setState("已完成");
			backDao.save(back);
			backService.removeBackEndPayTimeList(back);
			result.put("success",back.getOrder());
		} catch (Exception e) {
			result.put("err", "失败了！");
		}
		return result;
	}
}

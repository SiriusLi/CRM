package cn.gson.crm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
/**
 * 工程的基本路径
 * @author hzl
 * @date 2018年5月27日
 * @time 上午10:58:53
 */
public class AppInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String ctxPath = request.getContextPath();
		request.setAttribute("ctxPath", ctxPath + "/");//保存工程的基本路径
		return true;
	}
}

package cn.gson.crm.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.gson.crm.model.entity.Repair;

/**
 *
 * @author hzl
 * @date 2018年5月27日
 * @time 上午10:50:31
*/
public class SystemSessionInterceptor extends HandlerInterceptorAdapter{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String ctxPath = request.getContextPath();
		if(request.getSession().getAttribute("employee")==null){  
            //判断是否是ajax请求  
             if (request.getHeader("x-requested-with") != null&& request.getHeader("x-requested-with").equalsIgnoreCase("XMLHttpRequest"))//如果是ajax请求响应头会有，x-requested-with；    
             {    
            	 System.out.println("这是ajax请求");
                 response.setHeader("sessionstatus", "timeout");//在响应头设置session状态   
                 return false;    
             }else{  
            	 System.out.println("这不是ajax请求");
                response.sendRedirect(ctxPath+"/crm/login");  
                return false;      
             }  
          }else{  
              return true;      
          }  
	}
}

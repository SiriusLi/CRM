package cn.gson.crm.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
/**
 * 用于对超过规定日期的工单进行作废处理，直接将状态修改为已完成状态
 * @author LJJ
 *
 */

import cn.gson.crm.model.dao.RepairDao;
import cn.gson.crm.model.entity.Repair;
public class RepairInterceptor extends HandlerInterceptorAdapter{
	@Autowired
	private RepairDao repairDao;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		List<Repair> overRepair = repairDao.findOverdue();
		if(overRepair.size()>0){
			overRepair.forEach(repair ->{
				repair.setProgress("已完成");
				//设置订单状态为已完成
				repair.setState(true);
				repairDao.save(repair);
			});
		}
		
		return true;
	}

}

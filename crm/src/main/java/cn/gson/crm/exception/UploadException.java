package cn.gson.crm.exception;

/**
 * 文件上传异常
 * 
 * @author gson
 *
 */
public class UploadException extends RuntimeException {

	public UploadException() {

	}

	public UploadException(String message) {
		super(message);
	}

	public UploadException(String message, Throwable tw) {
		super(message, tw);
	}
}

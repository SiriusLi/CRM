<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<form id="actionForm" method="post" style="margin:0;padding:10px 30px;">

	<input type="hidden" value="" name="id">

	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="topic" value="" required="true" label="跟进主题：" style="width: 350px">
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="content" required="required" data-options="multiline:true" label="跟进内容：" value="" style="width:350px;height:60px">
	</div>
	
	<c:choose>
		<c:when test="true">
			<!-- 编辑，客户不能修改 -->
			<div style="margin-bottom: 10px">
				<input class="easyui-textbox" name="customer" required="true" value="" editable="false" label="对应客户：" style="width: 350px">
			</div>
		</c:when>
		
		<c:otherwise>
			<!-- 新增需求时客户不能手写，只能从已有的客户列表里选择 -->
			<div style="margin-bottom: 10px">
				<input id="addCus" class="easyui-textbox" label="对应客户：" required="true" editable="false" style="width:350px;">
			</div>
			
			<div style="margin-bottom: 10px;margin-left: 85px">
        		<input class="easyui-searchbox" data-options="prompt:'搜索客户。。。',searcher:doSearch" style="width:150px;">
			</div>
			
		</c:otherwise>
	</c:choose>
	
	<div style="margin-bottom: 10px">
		<input id="testDate" class="easyui-datebox" name="date"  editable="false" value="" label="跟进日期：" style="width:200px;">
	</div>
</form>

<script type="text/javascript">
$(function(){
	
	$('#testDate').datebox().datebox('calendar').calendar({
		validator: function(date){
			var now = new Date();
			var d = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			return date<=d;
		}
	});
	
	$("#addCus").textbox({onClickButton:function(){
	    alert("123");
	}});
	
	
});

function doSearch(value){
    alert('You input: ' + value);
};
</script>
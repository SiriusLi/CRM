<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="roleForm" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${r.id }"/>

    <div style="margin-bottom:10px">
    		<c:choose>
	    		<c:when test="${r == null}">
	    		<!-- validType="remote['permission/member/check','account']" 属性测试是否存在 -->
	    			<!-- 新建时需验证 -->
	        		<input name="roleName" class="easyui-textbox" required="true" invalidMessage="角色名已存在" label="角色名：" style="width:350px">
    			</c:when>
    			<c:otherwise>
    				<input name="roleName" class="easyui-textbox" label="角色名:" style="width:350px" value="${r.roleName }">
    			</c:otherwise>
    		</c:choose>
    </div>
    <div style="margin-bottom:10px">
    			<input name="note" class="easyui-textbox" label="角色说明：" style="width:350px" value="${r.note }">
    </div>
    <div style="margin-bottom:10px">
    	<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">状态:</label>
    		<input class="easyui-switchbutton" name="enable" data-options="onText:'启用',offText:'禁用',value:'1',checked:${r==null || r.enable?'true':'false'}">
    </div>
</form>
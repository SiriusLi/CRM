<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="employeeForm" method="post" style="margin:0;padding:10px 30px" enctype="multipart/form-data">
<div>
    <div style="float:left">
   <input type="hidden" name="id" value="${e.id }" />
    <div style="margin-bottom:10px">
    		<input name="empName"  value="${e.empName }" class="easyui-textbox" required="true" label="姓名:" style="width:350px">
    </div>
    <div style="margin-bottom:10px">
        <input name="password"  value="${e.password }" class="easyui-passwordbox" required="true" label="密码:" style="width:350px">
    </div>
    <div style="margin-bottom:10px">
        <select name="role.id" data-options="multiple:false" class="easyui-combobox" editable="false" panelHeight="auto" label="角色:" style="width:350px">
        		<c:forEach items="${roles }" var="role">
        			<option ${e.role==role?'selected':''} value="${role.id }">${role.roleName }</option>
        		</c:forEach>
        		
        </select>
        </div>
        <div style="margin-bottom:10px">
    		<c:choose>
	    		<c:when test="${e == null}">
	        		<div style="margin-bottom:10px">
				    	<input name="email"  value="${e.email }" class="easyui-textbox easyui-validatebox" required="true" invalidMessage="邮箱已被他人使用或格式不正确" data-options="required:true,validType:'email'" label="电子邮箱：" style="width:350px" validType="remote['employee/exist/email','email']">
					</div>
					<div style="margin-bottom:10px">
				    		<input name="telephone"  value="${e.telephone }" class="easyui-textbox easyui-validatebox" required="true" invalidMessage="电话已被他人使用或格式不正确" label="电话：" data-options="required:true,validType:'tel'" style="width:350px" validType="remote['employee/exist/tel','tel']">
					</div>
	        	</c:when>
    			<c:otherwise>
    				<div style="margin-bottom:10px">
					    	<input name="email"  value="${e.email }" class="easyui-textbox easyui-validatebox" required="true" invalidMessage="邮箱已被他人使用或格式不正确" data-options="required:true,validType:'email'" label="电子邮箱：" style="width:350px" validType="remote['employee/exist/email/${e.id }','email']">
						</div>
						<div style="margin-bottom:10px">
					    		<input name="telephone"  value="${e.telephone }" class="easyui-textbox easyui-validatebox" required="true" invalidMessage="电话已被他人使用或格式不正确" label="电话：" data-options="required:true,validType:'tel'" style="width:350px" validType="remote['employee/exist/tel/${e.id }','tel']">
						</div>
				</c:otherwise>
    		</c:choose>
    	</div>
	
	</div>

    <div style="margin-bottom:10px">
    		<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">状态:</label>
    		<input class="easyui-switchbutton" name="state" ${e.state == true?'checked':''} data-options="onText:'在职',offText:'离职'">
    </div> 
   <div style="margin-bottom:10px">
    		<c:choose>
	    		<c:when test="${e == null}">
	        		<input name="leader.id"  value="${e.leader.id }" class="easyui-combobox" style="width:350px" data-options="url:'employee/simplelist',valueField:'id',textField:'empName',label:'上司：',panelHeight:'auto'"style="width:350px">
				</c:when>
    			<c:otherwise>
    				<input name="leader.id"  value="${e.leader.id }" class="easyui-combobox" style="width:350px" data-options="url:'employee/simplelist/${e.id }',valueField:'id',textField:'empName',label:'上司：',panelHeight:'auto'"style="width:350px">
				</c:otherwise>
    		</c:choose>
    	</div>
    
	<div style="margin-bottom:10px">
    	<!-- <input id="file_upload" class="easyui-filebox" name="brandModel.sourceFile" type="text" data-options='onChange:change_photo'>
		 -->
		 <label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">照片:</label>
    	
	<div id="Imgdiv">
        <img id="Img" width="100px" height="100px"/>
    </div>
		 <input id="file_upload" name="sourceFile" type="text" data-options='onChange:change_photo'>
	
	</div>
</div>
</form>
<base href="${ctxPath }">
<script src="js/system/permission/role_.js"></script>
<script>
$(function () {
	$('#file_upload').filebox({    
	    buttonText: '选择文件', 
	    buttonAlign: 'right',
	    accept: 'image/gif,	image/jpeg,image/bmp,image/png',
	    multiple:false
	});
});
function PreviewImage(fileObj,imgPreviewId,divPreviewId){  
    var allowExtention=".jpg,.bmp,.gif,.png";//允许上传文件的后缀名document.getElementById("hfAllowPicSuffix").value;  
    var extention=fileObj.value.substring(fileObj.value.lastIndexOf(".")+1).toLowerCase();              
    var browserVersion= window.navigator.userAgent.toUpperCase();  
    if(allowExtention.indexOf(extention)>-1){   
        if(fileObj.files){//HTML5实现预览，兼容chrome、火狐7+等  
            if(window.FileReader){  
                var reader = new FileReader();   
                reader.onload = function(e){  
                    document.getElementById(imgPreviewId).setAttribute("src",e.target.result);  
                }    
                reader.readAsDataURL(fileObj.files[0]);  
            }else if(browserVersion.indexOf("SAFARI")>-1){  
                alert("不支持Safari6.0以下浏览器的图片预览!");  
            }  
        }else if (browserVersion.indexOf("MSIE")>-1){  
            if(browserVersion.indexOf("MSIE 6")>-1){//ie6  
                document.getElementById(imgPreviewId).setAttribute("src",fileObj.value);  
            }else{//ie[7-9]  
                fileObj.select();  
                if(browserVersion.indexOf("MSIE 9")>-1)  
                    fileObj.blur();//不加上document.selection.createRange().text在ie9会拒绝访问  
                var newPreview =document.getElementById(divPreviewId+"New");  
                if(newPreview==null){  
                    newPreview =document.createElement("div");  
                    newPreview.setAttribute("id",divPreviewId+"New");  
                    newPreview.style.width = document.getElementById(imgPreviewId).width+"px";  
                    newPreview.style.height = document.getElementById(imgPreviewId).height+"px";  
                    newPreview.style.border="solid 1px #d2e2e2";  
                }  
                newPreview.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='scale',src='" + document.selection.createRange().text + "')";                              
                var tempDivPreview=document.getElementById(divPreviewId);  
                tempDivPreview.parentNode.insertBefore(newPreview,tempDivPreview);  
                tempDivPreview.style.display="none";                      
            }  
        }else if(browserVersion.indexOf("FIREFOX")>-1){//firefox  
            var firefoxVersion= parseFloat(browserVersion.toLowerCase().match(/firefox\/([\d.]+)/)[1]);  
            if(firefoxVersion<7){//firefox7以下版本  
                document.getElementById(imgPreviewId).setAttribute("src",fileObj.files[0].getAsDataURL());  
            }else{//firefox7.0+                      
                document.getElementById(imgPreviewId).setAttribute("src",window.URL.createObjectURL(fileObj.files[0]));  
            }  
        }else{  
            document.getElementById(imgPreviewId).setAttribute("src",fileObj.value);  
        }           
    }else{  
        alert("仅支持"+allowExtention+"为后缀名的文件!");  
        fileObj.value="";//清空选中文件  
        if(browserVersion.indexOf("MSIE")>-1){                          
            fileObj.select();  
            document.selection.clear();  
        }                  
        fileObj.outerHTML=fileObj.outerHTML;  
    }  
}

function change_photo(){
    PreviewImage($("input[name='sourceFile']")[0], 'Img', 'Imgdiv');
}
</script>
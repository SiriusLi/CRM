<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>  
<div> 
	<table>
		<tr>
			<td>姓名：</td>
			<td>${e.empName }</td>
		</tr>
		<tr>
			<td>角色：</td>
			<td>
				${e.role.roleName }
			</td>
		</tr>
		<tr>
			<td>电子邮箱：</td>
			<td>${e.email }</td>
		</tr>
		<tr>
			<td>电话：</td>
			<td>${e.telephone }</td>
		</tr>
		<tr>
			<td>状态：</td>
			<td>
				<c:choose>
					<c:when test="${e.state }==1">在职</c:when>
					<c:otherwise>离职</c:otherwise>
				</c:choose>
			</td>
		</tr>
		
		<tr>
			<td>相片：</td>
			<td>
				<img style="width:100px;height: 100px;" src="/file/${e.picture }">
			</td>
		</tr>
	</table>
</div>
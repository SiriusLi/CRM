<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 角色管理 -->
<base href="${ctxPath }">
<script src="js/system/permission/role.js"></script>

	
	<table id="role-grid"></table>
	
	<!-- 工具栏 -->
	<div id="role-toolbar" style="padding: 5px; height: 30px;">
		<div style="margin-bottom: 5px; float: left;">
			<a href="javascript:void(0)" class="easyui-linkbutton add" iconCls="icon-add" 
				plain="true">新增</a>
			<a href="javascript:void(0)" class="easyui-linkbutton delete" iconCls="icon-remove"
				plain="true">删除</a>
			<a href="javascript:void(0)" class="easyui-linkbutton alter" iconCls="icon-edit"
				plain="true">修改</a>
			<a href="javascript:void(0)" class="easyui-linkbutton authorize" iconCls="icon-ok"
				plain="true" >授权</a>
		</div>
	</div>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/system/permission/user.js"></script>
</head>
<body>
<h1>用户管理</h1>
	<!-- 表格 -->
	<table id="user-dg" class="easyui-datagrid" toolbar="#toolbar">
		<thead>
			<tr>
				<th field="id" data-options="width:60">账号</th>
				<th field="role" data-options="width:150">关联角色</th>
				<th field="state" data-options="width:60">状态</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>小猪</td>
				<td>超级管理员</td>
				<td>启用</td>
			</tr>
		</tbody>
	</table>
	
	<table id="userGrid"></table>
	
	<!-- 工具栏 -->
	<div id="user-toolbar" style="padding: 5px; height: 30px;">
		<div style="margin-bottom: 5px; float: left;">
			<a href="javascript:void(0)" class="easyui-linkbutton add" iconCls="icon-add" plain="true" onclick="newUser()">新增</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove"
				plain="true" onclick="deleteUser();">删除</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit"
				plain="true" onclick="alterUser();">修改</a>
		</div>
	</div>
	
	<!-- 新增用户 -->
	<div id="user-dlg" class="easyui-dialog" style="" closed="true"
		buttons="#dlg-buttons">
		<c:import url="user_management/user_add.jsp"></c:import>
	</div>
	
	<!-- 修改用户 -->
	<div id="user-alter-dlg" class="easyui-dialog" style="" closed="true"
		buttons="#dlg-buttons">
		<c:import url="user_management/user_alter.jsp"></c:import>
	</div>
</body>
</html>
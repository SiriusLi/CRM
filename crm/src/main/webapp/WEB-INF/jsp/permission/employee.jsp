<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<base href="${ctxPath }">
<script src="js/system/permission/employee.js"></script>


	<table id="employee-grid"></table>
	
	<div id="employee-toolbar" style="padding: 5px; height: 30px;">
		<div style="margin-bottom: 5px; float: left;">
			<a href="javascript:void(0)" class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
			<a href="javascript:void(0)" class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
			<a href="javascript:void(0)" class="easyui-linkbutton alter" iconCls="icon-edit" plain="true">修改</a>
		</div>
	</div>

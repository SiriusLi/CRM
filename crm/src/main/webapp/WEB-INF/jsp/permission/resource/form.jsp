<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="resourceForm" style="margin:0;padding:10px 30px" method="post">
	<input id="id-input" type="hidden" name="id" value="${rs.id }"/>
   <div style="margin-bottom:10px">
   	<c:choose>
   		<c:when test="${rs == null }">
   			<select name="parent.id" id="parent-select" class="easyui-combotree" style="width:350px;"
		        data-options="state: 'closed',hasDownArrow:true,panelHeight:'auto',url:'resource/parents',onChange:function(newValue){refreshIdentifyInput(newValue);validateIdentify()}" label="父节点:">
			</select>
   		</c:when>
   		<c:otherwise>
   			<select name="parent.id"  class="easyui-combotree" style="width:350px;"
		        data-options="state: 'closed',hasDownArrow:true,panelHeight:'auto',url:'resource/parents',value:'${rs.parent.id}'" label="父节点:">
			</select>
   		</c:otherwise>
   	</c:choose>
		
    </div> 
    <div style="margin-bottom:10px">
        <input name="text" class="easyui-textbox" required="true" label="名称:" style="width:350px" value="${rs.text }">
    </div>
    <div style="margin-bottom:10px">
    <!-- <label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">标识:</label>
    <label id="preIdentify"></label> -->
    <c:choose>
   		<c:when test="${rs == null }">
   			<!-- <input id="identify-input" style="width:262px;" name="identify" class="easyui-textbox easyui-validatebox" required="true" invalidMessage="此标识已被使用" validType="remote['resource/exist/identify','identify']">
   		 -->
   		 <input id="identify-input" style="width:350px;" name="identify" label="标识:"  class="easyui-textbox easyui-validatebox" required="true" invalidMessage="此标识不能以‘:’结尾，或已被其他资源使用"validType="remote['resource/exist/identify','identify']" >
   		</c:when>
   		<c:otherwise>
   			<input id="identify-input" name="identify" class="easyui-textbox" required="true" label="标识:" style="width:350px" value="${rs.identify }" invalidMessage="此标识已被使用" validType="remote['resource/exist/identify/${rs.id }','identify']">
   		</c:otherwise>
   	</c:choose>
    </div>
    <div style="margin-bottom:10px">
    		<select name="type" class="easyui-combobox" editable="false" panelHeight="auto" required="true" label="类型:" style="width:350px">
    			<option value="MENU">菜单</option>
    			<option value="FUNCTION">功能</option>
    			<option value="PAGE">页面</option>
    		</select>
    </div>
    <div style="margin-bottom:10px">
        <input name="menuHref" class="easyui-textbox" label="菜单链接:" style="width:350px" value="${rs.menuHref }">
    </div>
    <div style="margin-bottom:40px">
        <label class="textbox-label textbox-label-before" style="text-align: left;">权重:</label>
    	<input name="weight" class="easyui-slider" style="width:350px;"label="权重:"value="${rs.weight }" data-options="min:0,max:100,step:1,showTip:true,rule:[0,'|',25,'|',50,'|',75,'|',100]"  style="display:inline;float:right;">
    </div>
    <div style="margin-bottom:10px">
      <%--   <input name="urls" class="easyui-textbox" label="资源链接:" multiline="true" style="width:350px;" value="${rs.urls }">
     --%>
     <label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">资源链接：</label>
     <input  id="url-input" class="easyui-textbox" style="width:200px">
     <input  id="urls-input" type="hidden" name="urls" class="easyui-textbox" readonly="readonly" value="${rs.urls }">
     <div style="float:right;margin-right:20px;">
     	<a href="javascript:void(0)" class="easyui-linkbutton add" iconCls="icon-add" plain="true" onClick="addUrl();"></a>
		<a href="javascript:void(0)" class="easyui-linkbutton delete" iconCls="icon-remove" plain="true" onClick="delUrl()"></a>
     </div>
    <c:choose>
    	<c:when test="${rs == null }">
	    	<ul id="url-ul" class="easyui-datalist" textField='url' valueField='url'style="width:350px;height:100px">  
			</ul> 
	    </c:when> 
	    <c:otherwise>
	    	<div style="float:left;padding:0 43px">&nbsp;</div>
	    	<div id="url-ul"></div>
	    </c:otherwise>
    </c:choose>
     </div>
    <div style="margin-bottom:10px">
    	<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">状态:</label>
    		<input class="easyui-switchbutton" name="enable" data-options="onText:'启用',offText:'禁用',checked:${rs==null || rs.enable?'true':'false'}">
    </div>
</form>
<script>
$(function() {
	if($('#id-input').val()!=null && $('#id-input').val()!='') {
		initUrlsul();
	}
	
	/* $.extend($.fn.validatebox.defaults.rules, {    
	    identify: {    
	        validator: function(value,param){ 
	        	if (value == null || value.trim() == '' ) return false;
	        	var identify = $(param[0]).html() + value;

	        	alert(identify);
	        	var flag = false;
	        	$.get('resource/exist/identify',{
	        	    identify:identify  
	        	},function(data){
	        		flag = data.flag;
	        	},'json');
	        	return !flag;
	        },    
	        message: '此标识已经存在!'   
	    }    
	}); */
});
function initUrlsul() {
	var json = '[';
	if($('#urls-input').val()!=null && $('#urls-input').val()!='') {
		var urls = $('#urls-input').val();
		urls = urls.trim().split(';');
		for(let url of urls) {
			if(url != null  && url != '') {
				json += "{'url':'" + url + "'},"
			}
		}
		json = json.substring(0, json.lastIndexOf(','));
	} 
	json += ']';
	var json1 = eval("("+json+")");
	$('#url-ul').datalist({ 
	    data: json1, 
	    valueField : 'url',
	    textField : 'url',
	    width : 260,
	    height: 100,
	});
}
function refreshIdentifyInput(newValue) {
	/* alert(newValue); */
	$.get('resource/single',{
	    id:newValue  
	},function(data){
		var identify = data.identify + ':';
		$('#identify-input').textbox('setValue', identify);
		/* $('#preIdentify').html(identify);
		$('#identify-input').textbox('resize',258 - $('#preIdentify').width()); */
	},'json');
}
function validateIdentify() {
	$('#url-input').validatebox('validate');
}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<table>
	<tr>
		<td>
			<%-- <c:if test="${rs.parent !=null }"><a href='javascript:void(0)' onClick='showInfo(${rs.parent.id });'>${rs.parent.text }</a></c:if>
		 --%>
		 <c:set var="startIndex" value="${fn:length(ancestors)-1 }"></c:set>  
		 <c:forEach items="${ancestors }" var='ancestor' varStatus="status">
		 	<a style='text-decoration: none' href='javascript:void(0)' onClick='showInfo(${ancestors[startIndex - status.index].id });'>${ancestors[startIndex - status.index].text }</a>&nbsp;>&nbsp; 
		 	<%-- <c:if test="${startIndex > status.index}">></c:if> --%>
		 </c:forEach>
		 ${rs.text }
		 </td>
	</tr>
	<tr>
		<td>资源名称：</td>
		<td>${rs.text }</td>
	</tr>
	<tr>
		<td>资源类型：</td>
		<td>
			<c:choose>
			    <c:when test="${rs.type == 'MENU'}">
			       	菜单
			    </c:when>
			    <c:when test="${rs.type == 'FUNCTION'}">
			                      功能
			    </c:when>
			    <c:otherwise>
			       	 页面	
			    </c:otherwise>
			</c:choose>
		</td>
	</tr>
	<tr>
		<td>资源标识：</td>
		<td>${rs.identify }</td>
	</tr>
	<tr>
		<td>权重：</td>
		<td>${rs.weight }</td>
	</tr>
	<tr>
		<td>关联请求：</td>
		<td>${rs.urls }</td>
	</tr>
	<tr>
		<td>状态：</td>
		<td>
			<c:choose>
			    <c:when test="${rs.enable }">
			       	可用
			    </c:when>
			    <c:otherwise>
			       	 禁用
			    </c:otherwise>
			</c:choose>
		</td>
	</tr>
</table>
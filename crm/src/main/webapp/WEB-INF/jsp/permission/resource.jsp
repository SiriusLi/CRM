<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 资源管理 -->

<base href="${ctxPath }">
<script src="js/system/permission/resource.js"></script>


	<!-- 表格 -->	
	<table id="resource-grid"></table>
	
	<!-- 工具栏 -->
	<div id="resource-toolbar">
		<div>
			<a href="javascript:void(0)" class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
			<a href="javascript:void(0)" class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
			<a href="javascript:void(0)" class="easyui-linkbutton alter" iconCls="icon-edit" plain="true">修改</a>
		</div>
	</div>

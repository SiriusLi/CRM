<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath}">
<meta charset="UTF-8">
<title>登录</title>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/popper.js/1.12.5/umd/popper.min.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<style type="text/css">
html, body {
	height: 100%;
}

body {
	display: -ms-flexbox;
	display: -webkit-box;
	display: flex;
	-ms-flex-align: center;
	-ms-flex-pack: center;
	-webkit-box-align: center;
	align-items: center;
	-webkit-box-pack: center;
	justify-content: center;
	background: url(images/login_bg.jpg) no-repeat;
	background-size: cover;
	background-position: center;
}

.form-login {
	width: 330px;
	padding: 15px;
	margin: 0 auto;
}

.form-login .welcome {
	font-family: "黑体";
	font-weight: bold;
	text-align: center;
	margin-bottom: 30px;
}

.form-login .btn:hover {
	cursor: pointer;
}

.form-login .btn {
	margin-top: 20px;
	font-family: "黑体";
	letter-spacing: 7px;
}

.form-login .reg a {
	font-family: "楷体";
	padding-top: 10px;
	display: inline-block;
	float: right;
}

.loginbox {
	width: 380px;
	background-color: rgba(53, 41, 43, 0.5);
	border: 1px #bdbdbd solid;
	margin-top: 44px;
}

.form-control {
	margin: 20px 0px;
}
</style>
</head>
<body>
	<div class="loginbox">
		<form class="form-login" method="post" action="crm/login/dologin">
			<h2 class="welcome">登录CRM</h2>
			<input type="text" name="empName" class="form-control" placeholder="账户" required="required" autofocus="autofocus" /> 
			<input type="password" name="password" class="form-control" required="required" placeholder="密码" />
			<button class="btn btn-primary btn-lg btn-block">登录</button>
		</form>
		<c:if test="${not empty err }">
			<script type="text/javascript">
				alert("${err}");
			</script>
		</c:if>
	</div>
</body>
</html>

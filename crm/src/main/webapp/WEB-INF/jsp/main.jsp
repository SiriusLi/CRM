<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type"
	content="multipart/form-data; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CRM</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="themes/metro/menu-tree.css" id="theme" />
<link rel="stylesheet" href="themes/icon.css" />
<link rel="stylesheet" type="text/css" href="iconfont/iconfont.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/datagrid-filter.js"></script>
<script src="js/datagrid-detailview.js"></script>
<script src="js/datagrid-dnd.js"></script>
<script src="js/distpicker.data.js"></script>
<script src="js/distpicker.js"></script>
<script src="js/main.js"></script>
<script src="js/validator.js"></script>
<script src="js/echarts.js"></script>
<style type="text/css">
.easyui-accordion>div>div>div {
	padding-left: 15px;
}
/* 销售简报 */
.briefing table td {
	width: 235px;
}

.show {
	text-align: center;
}

.bdb td {
	border-bottom: 1px solid black;
}

.bdr td {
	border-right: 1px solid black;
}

.right-ul li {
	padding: 5px 20px;
}

.panel-item {
	margin-left: 10px;
	float: left;
}

.panel-item>ul {
	height: 60px;
	padding: 0px;
}

.panel-item>ul>li>a>img {
	wigth: 23px;
	height: 23px;
}

.panel-item>ul>li {
	width: 50px;
	float: left;
	margin-left: 15px;
}

.li-title {
	font-size: 16px;
	float: left;
	height: 40px;
	background-color: #1E9FFF;
	color: #ffffff;
	padding: 10px;
	width: 40px;
}
</style>
</head>
<body class="easyui-layout">
	<c:if test="${empty employee}">
		<div id="login-dialog" class="easyui-dialog" title="重新登录" modal="true"
			style="width: 400px; height: 350px; padding: 5px;">
			<div class="loginbox">
				<form id="form-login">
					<table style="line-height: 40px; width: 90%; text-align: center;">
						<tr>
							<th><h1>登录CRM</h1></th>
						</tr>
						<tr>
							<td>账户：&nbsp;<input type="text" name="empName"
								required="true" style="height: 25px;" class="easyui-textbox"
								placeholder="账户" autofocus="autofocus" /></td>
						</tr>
						<tr>
							<td>密码：&nbsp;<input type="password" name="password"
								required="true" class="easyui-textbox" style="height: 25px;"
								placeholder="密码" /></td>
						</tr>
						<tr>
							<td><a class="easyui-linkbutton"
			href="javascript:void(0)" onclick="submitForm1()">登录</a></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</c:if>
	<div data-options="region:'west'" style="width: 12%;" title=" ">
		<ul class="easyui-tree" lines="false" id="menu-tree">
			<!-- <li data-options="href:'crm/main/shouye',checked:'true'"><span>首页</span></li>
			<li data-options="href:'cusleads'"><span>获客线索</span></li>
			<li data-options="href:'cus'"><span>客户管理</span></li>
			<li data-options="state:'closed'"><span>售前管理</span>
				<ul>
					<li data-options="href:'quotation'">报价</li>
					<li data-options="state:'closed'"><span>销售机会</span>
						<ul>
							<li data-options="href:'opp'">机会列表</li>
							<li data-options="href:'com'">竞争对手</li>
							<li data-options="href:'solution'">解决方案</li>
							<li data-options="href:'demand'">客户需求</li>
						</ul></li>
					<li data-options="href:'carecus'">客户关怀</li>
				</ul></li>
			<li data-options="state:'closed'"><span>售中管理</span>
				<ul>
					<li data-options="href:'product'">产品</li>
					<li data-options="state:'closed'"><span>订单</span>
						<ul>
							<li data-options="href:'crm/order'">订单</li>
							<li data-options="href:'crm/orderDetail'">订单明细</li>
							<li data-options="href:'crm/audit'">审批</li>
						</ul></li>
					<li data-options="state:'closed'"><span>订单回款</span>
						<ul>
							<li data-options="href:'crm/receivable'">回款计划</li>
							<li data-options="href:'crm/receivableRecord'">回款记录</li>
						</ul></li>
					<li data-options="state:'closed'"><span>订单退货</span>
						<ul>
							<li data-options="href:'crm/back'">退货单</li>
							<li data-options="href:'crm/backDetail'">退货明细</li>
						</ul></li>
					<li data-options="state:'closed'"><span>订单发货</span>
						<ul>
							<li data-options="href:'crm/delivery'">发货单</li>
							<li data-options="href:'crm/deliveryDetail'">发货明细</li>
						</ul></li>
					<li data-options="href:'crm/order'">退货明细</li>
				</ul></li>
			<li data-options="state:'closed'"><span>售后管理</span>
				<ul>
					<li data-options="href:'aftersale/main'">维修工单</li>
				</ul></li>
			<li data-options="state:'closed'"><span>系统管理</span>
				<ul>
					<li data-options="href:'permission/employee'">员工管理</li>
					<li data-options="href:'permission/role'">角色管理</li>
					<li data-options="href:'permission/resource'">资源管理</li>
				</ul></li> -->
		</ul>
	</div>
	<div data-options="region:'center',border:false"
		style="background-color: #8080800d;">
		<div style="height: 60px; border: 1px solid; background-color: #fff;">
			<div class="li-title">快速新建</div>
			<div class="panel-item">
				<ul>
					<li><a onclick="newCus()"> <img src="images/customers.png"><br> <span>
								客户 </span>
					</a></li>
					<!-- <li><a> <img src="images/follow.png"><br> <span>
								跟进 </span>
					</a></li> -->
					<!-- <li><a onclick="newOrder()"> <img src="images/deal.png"><br>
							<span> 订单 </span>
					</a></li> -->
					
					<li><a onclick="newOpp()"> <img src="images/worklog.png"><br> 
						<span style="display: block;margin-left: -10px">销售机会</span>
					</a></li>
					<li><a onclick="newAfter()"> <img src="images/waittask.png"><br> 
						<span style="display: block;margin-left: -10px">维修工单</span>
					</a></li>
				</ul>
			</div>

			<ul class="right-ul">
				<div style="float: right;margin-top:-10px;">
					<div id="avatar" style="width:150px;height: 60px;">
						<div style="float:left;position: absolute;margin-top: 7px;margin-left: -50px;">
							<c:if test="${employee.picture != null}">
								<li class="fr">
									<img style="width: 35px; height: 35px;border-radius: 50%" src="/file/${employee.picture }">
								</li>
							</c:if>
						</div>
						<div style="width: 140px;margin-left: 10px;padding-top: 18px">
							<label style="display:block;margin-top: 0px;font-size: 16px;font-weight: bolder;">${employee.empName} |
							<a href="crm/login/logout" onclick="if(confirm('确认退出吗？')==false)return false;" style="color: #C2C2C2">注销</a></label>
						</div>
					</div>
				</div>

				<!-- <li class="fr"><span>更换主题：</span><select
					style="border-radius: 5px;" id="changeThemes"
					onchange="changeTheme(this)">
						<option value="themes/metro/menu-tree.css">metro</option>
						<option value="themes/default/easyui.css">default</option>
						<option value="themes/black/easyui.css">black</option>
						<option value="themes/bootstrap/easyui.css">bootstrap</option>
						<option value="themes/gray/easyui.css">gray</option>
						<option value="themes/material/easyui.css">material</option>

				</select></li> -->
			</ul>
		</div>
		<div id="p"
			style="height: 85%; padding: 0px 10px; width: 98%; background-color: #fff;">
			<c:import url="system/shouye.jsp"></c:import>
		</div>
	</div>

	<script>
		//快速新建客户
		function newCus() {
			var dialog = $("<div/>").dialog({
				title:"新增客户",
				width:450,
				height:410,
				modal:true,
				href:'cus/cusform',
				onClose:function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#cusForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("cus/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		};
		
		//快速新建订单
		function newAfter() {
			var dialog = $("<div/>").dialog(
					{
						title : "新增维修工单",
						top:70,
						width : 610,
						modal : true,
						//跳到对应的表单
						href : 'aftersale/add',
						onClose : function() {
							//关闭窗户后，立即销毁窗口
							$(this).dialog("destroy");
						},
						buttons : [
								{
									iconCls : "icon-ok",
									text : "保存",
									handler : function() {
										var form = $("#repairForm");
										//校验表单数据
										if (form.form("validate")) {
											$.post("aftersale/save",
													form.serialize(),function(rs) {
													if (rs.err) {
														alert(rs.err);
													} else {
														//alert("222");
														//关闭当前窗口
														dialog.dialog("close");
														$("#repairwx").datagrid("reload");	
													}
											});
										}
									}
								},
								{
									iconCls : "icon-cancel",
									text : "取消",
									handler : function() {
										//关闭当前窗口
										dialog.dialog("close");
									}
								} ]
							});
					};
		
		//快速新建机会
		function newOpp() {
			var dialog = $("<div/>").dialog({
				title: "新增销售机会",
				width:800,
				height: 700,
				modal:true,
				href:'opp/oppaddform',
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#oppaddForm");
							var updateTime=$("input[name='updateTime']").val();
							var expectedTime=$("input[name='expectedTime']").val();
							
							console.log(expectedTime);
							//校验表单数据
							if(form.form("validate")){
								$.post("opp/save",form.serialize(),{expectedTime:'expectedTime',updateTime:'updateTime'},function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		};
	
		$(function() {
			$('#aa').accordion({
				animate : true,
				border : false,
			});
			$('#bb').accordion({
				animate : true,
				border : false,
			});
			$('#cc').accordion({
				border : false,
				animate : true
			});
			//实例化菜单树
			var tree = $(".easyui-tree").tree({
			    url: 'resource/list',
				onSelect : function(node) {
					$('#p').panel({
						href : node.menuHref
					});
					if (node.state == 'closed') {
						$(this).tree('expand', node.target);
					} else {
						$(this).tree('collapse', node.target);
					}
				},
				onLoadSuccess : function() {
					$(".easyui-tree").tree('collapseAll');
				}
			});


		});
		function changeTheme(obj) {
			$("#theme").attr("href", $("#changeThemes").val());
		}
		function submitForm1(){
			var form=$("#form-login");
			if(form.form("validate")){
				$.post("crm/login/doajaxlogin",form.serialize(),function(rs){
					if(rs.err){
						$.messager.alert("提示",rs.err,"error");
					}else{
						$.messager.alert("提示","登录成功！","info");
						$("#login-dialog").dialog("close");
					}
				});
			}
		}
	</script>
</body>
</html>
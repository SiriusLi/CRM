<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客户需求</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
	<table id="demandGird"></table>
	
	<div id="demandToolbar">
	    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
	    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑</a>
	    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
	</div>

	<script src="js/system/beforesales/demand.js"></script>	

</body>
</html>
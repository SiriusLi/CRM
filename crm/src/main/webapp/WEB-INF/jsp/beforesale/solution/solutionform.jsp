<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.textbox-label {
	display: inline-block;
	width: 100px;
	height: 22px;
	line-height: 22px;
	vertical-align: middle;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	margin: 0;
	padding-right: 5px;
}
</style>
<form id="solutionform" method="post"
	style="margin: 0; padding: 10px 30px">
	 <input type="hidden" id="sessionemp"
		value="${employee.role.roleName}">
		<input type="hidden" name="id"
		value="${r.id}">
	<table style="margin-top: 30px">
		<tr>
			<td colspan=2><div style="margin-bottom: 30px">
					<input name="topic" class="easyui-textbox" label="方案主题"
						style="width: 500px; height: 40px" value="${r.topic }">
				</div></td>
		</tr>
		<tr>
			<td colspan=2>
				<div style="margin-bottom: 30px">
					<input class="easyui-textbox" required="required" label="销售机会："
						id="opp" value="${r.salesOpp.topic}" editable="false"
						style="width: 200px;">
					<div style="margin-top: 10px; margin-left:105px">	
						<input class="easyui-searchbox" id="opp2" name="salesOpp.id" 
							value="${r.salesOpp.id}" data-options="prompt:'根据主题或id搜索销售机会',searcher:SearchOpp"
							style="width: 200px" />
					</div>
				</div>
			</td>
			
		</tr>
		<tr>
		<td colspan=2>
				<div
					style="margin-bottom: 30px; margin-left: 105px; float: left; width: 250px;">
					<table id="searchopp"></table>
				</div>
		</td>
		
		</tr>
		<tr>
			<td colspan=2><div style="margin-bottom: 30px">
					<input name="content" class="easyui-textbox"
						data-options="multiline:true" label="方案内容"
						style="width: 500px; height: 120px" value="${r.content }">
				</div></td>
		</tr>
		<tr>
			<td colspan=2>
				<div style="margin-bottom: 30px">
					<input name="time" class="easyui-datebox" label="时间"
						style="width: 270px" value="${r.time }">
				</div>
			</td>
		</tr>

		<tr>
			<td colspan=2>
				<div style="margin-bottom: 30px">
					<input name="note" class="easyui-textbox"
						data-options="multiline:true" label="备注	" style="width: 500px"
						value="${r.note }">
				</div>
			</td>
		</tr>

		
	</table>

</form>
<script type="text/javascript">
	//查询销售机会的方法
	
	var grid1 = $("#searchopp");
	function SearchOpp(value) {
	if($("#sessionemp").val()=="销售员"){
			if (value == "") {
				alert("请输入id和姓名搜索相关的员工信息，再选择添加员工！");
			} else {
				grid1
						.datagrid({
							title : '搜索结果：（只显示前10条结果）',
							width : 265,
							striped : true, //条纹
							collapsible : true, //可折叠的
							singleSelect : true,
							url : 'opp/search/' + value,
							loadMsg : '数据加载中......',
							fitColumns : true,// 允许表格自动缩放, 以适应父容器  
							sortName : 'id',
							sortOrder : 'desc',
							remoteSort : false,
							columns : [ [
									{
										field : 'id',
										width : 100,
										title : 'id'
									},
									{
										field : 'topic',
										width : 100,
										title : '机会主题'
									},
									{
										field : 'add',
										width : 50,
										title : '选择',
										formatter : function(value, row, index) {
											return '<a href="javascript:void(0);"onclick="change('
													+ index + ')"> 添加 </a>';
										}
									} ] ],
						});
		}
	}else{
		if (value == "") {
			alert("请输入id和姓名搜索相关的员工信息，再选择添加员工！");
		} else {
			grid1
					.datagrid({
						title : '搜索结果：（只显示前10条结果）',
						width : 265,
						striped : true, //条纹
						collapsible : true, //可折叠的
						singleSelect : true,
						url : 'opp/search2/' + value,
						loadMsg : '数据加载中......',
						fitColumns : true,// 允许表格自动缩放, 以适应父容器  
						sortName : 'id',
						sortOrder : 'desc',
						remoteSort : false,
						columns : [ [
								{
									field : 'id',
									width : 100,
									title : 'id'
								},
								{
									field : 'topic',
									width : 100,
									title : '机会主题'
								},
								{
									field : 'add',
									width : 50,
									title : '选择',
									formatter : function(value, row, index) {
										return '<a href="javascript:void(0);"onclick="change('
												+ index + ')"> 添加 </a>';
									}
								} ] ],
					});
	}
	}
	};

	function change(index) {
		//实时获取所点击那一行的信息
		$('#searchopp').datagrid('selectRow', index);
		var row = $('#searchopp').datagrid('getSelected');
		if (row) {
			//将搜索框的内容与选中的值相对应
			$("#opp").textbox('setValue', row.topic);
			$("#opp2").val(row.id);
		}

	}
</script>
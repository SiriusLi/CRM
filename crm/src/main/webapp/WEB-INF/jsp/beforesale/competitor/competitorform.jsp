<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>       
<form id="comForm" method="post" style="margin:0;padding:10px 30px;">

	<input type="hidden" value="${c.id }" name="id">
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="companyName" value="${c.companyName }" required="true" label="竞争公司：" style="width: 350px">
	</div>
	

		<c:if test="${c != null }">
			<!-- 编辑，客户不能修改 -->
			<div style="margin-bottom: 10px">
				<input class="easyui-textbox"  required="true" value="${c.salesOpp.customer.name }" editable="false" label="客户：" style="width: 350px">
			</div>
		</c:if>
		
			<!-- 新增需求时客户不能手写，只能从已有的客户列表里选择 -->
			<!-- <input class="easyui-textbox"   required="required" label="对应客户："  id="con" editable="false" style="width: 350px;">
				
			<div style="margin-top: 10px;margin-left: 85px">
       			<input class="easyui-searchbox" id="con2" name="cusid"  data-options="prompt:'根据id和姓名搜索客户。。。',searcher:doSearch" style="width:200px;">
			</div>
		
			<div style="float: right;margin-right: 26px;margin-bottom: 20px;padding-left: 50px">
					<table id="searchcus"></table>
			</div> -->
		

	
	
	
	<c:choose>
		<c:when test="${c != null }">
			<!-- 编辑时只显示销售机会，不可修改 -->
			<div style="margin-bottom: 10px;">
				<input class="easyui-textbox" label="对应机会：" editable="false" value="${c.salesOpp.topic }" style="width: 350px">
			</div>
			<input type="hidden" name="saleid" value="${c.salesOpp.id }">
			
		</c:when>
		<c:otherwise>
			<!-- 新增时，就需要联动显示机会列表了 -->
			<div style="margin-bottom: 10px;margin-top: 20px">	
				<select class="easyui-combobox" id="order-form-salesOppid" required="required" label="对应机会：" panelHeight="auto"   style="width:350px;">
			                
				</select>
				
				<input id="saleid" type="hidden" name="saleid">
				
			</div>
		
		</c:otherwise>
	</c:choose>
	
	
	
	<div style="margin-bottom: 10px">
		<input id="money" class="easyui-numberbox" name="money" value="${c.money }" label="价格（元）：" style="width:200px;" data-options="min:0,precision:2">
	</div>
	
	<div style="margin-bottom: 10px">
		<select class="easyui-combobox" id="power" editable="false" name="power"  label="竞争能力：" panelHeight="auto"  style="width:200px;">
	                <option value="">未选</option>
	                <option value="核心竞争" <c:if test="${c.power == '核心竞争' }">selected</c:if>>核心竞争</option>
	                <option value="有力竞争" <c:if test="${c.power == '有力竞争' }">selected</c:if>>有力竞争</option>
	                <option value="一般竞争" <c:if test="${c.power == '一般竞争' }">selected</c:if>>一般竞争</option>
	                <option value="忽略竞争" <c:if test="${c.power == '忽略竞争' }">selected</c:if>>忽略竞争</option>
		</select>
	</div>
	
	<div style="margin-bottom: 20px">
		<input class="easyui-textbox" name="comProgram" data-options="multiline:true" label="竞争方案：" value="${c.comProgram }" style="width:350px;height:50px">
	</div>
	
	<div style="margin-bottom: 20px">
		<input class="easyui-textbox" name="disadvantage" data-options="multiline:true" label="劣势：" value="${c.disadvantage }" style="width:350px;height:50px">
	</div>
	
	<div style="margin-bottom: 20px">
		<input class="easyui-textbox" name="solution" data-options="multiline:true" label="应对方案：" value="${c.solution }" style="width:350px;height:50px">
	</div>
	
	<div style="margin-bottom: 20px">
		<input class="easyui-textbox" name="note" data-options="multiline:true" label="备注：" value="${c.note }" style="width:350px;height:50px">
	</div>
</form>

<script type="text/javascript">
	

   /*  var grid = $("#searchcus");

    function doSearch(value){
    	if (value == "") {
    		alert("请输入id和姓名搜索相关的客户信息，再选择添加客户！");
    	}else{
    		grid.datagrid({  
    	         title:'搜索结果：（只显示前10条结果）',    
    	         width:265,              
    	         striped:true,  //条纹
    	         collapsible:true,	//可折叠的
    	         singleSelect:true,
    	         url:'cus/search/'+value,  
    	         loadMsg:'数据加载中......',  
    	         fitColumns:true,// 允许表格自动缩放, 以适应父容器  
    	         sortName:'id',  
    	         sortOrder:'desc',  
    	         remoteSort:false,  
    	         columns : [ [ 
    	        	 {field : 'id',width : 100,title : 'id'}, 
    	        	 {field : 'name',width : 100,title : '客户'},
    	        	 {field : 'add',width : 50,title : '选择',formatter : function (value, row, index)  {
    	        		 return '<a href="javascript:void(0);"onclick="change('+index+')"> 添加 </a>'; 
    				 }}
    	        	 ] ], 
    	     });
    	}
    	 
     	
    };

    function change(index) {
    	//实时获取所点击那一行的信息
    	$('#searchcus').datagrid('selectRow',index);
    	var row = $('#searchcus').datagrid('getSelected');  
        if (row){  
    		//将搜索框的内容与选中的值相对应
    		$("#con").textbox('setValue', row.name);
    		//将id值赋给隐藏的input
    		$("#con2").val(row.id);
    		
    		//当选择好客户后，对应机会的下拉框加载与客户对应的销售机会
    		//先清空下拉框
    		$('#order-form-salesOppid').combobox('setValue', "");
    		// url为java后台查询事级列表的方法地址
			var url = 'opp/getOppByCus?customerid=' + row.id;
    		//为下拉框获取对应的机会
			$('#order-form-salesOppid').combobox('reload', url);
			
        }  	
    }; */
    $(function(){ 		
		$("#order-form-salesOppid").combobox({          
			url:'opp/getOppByEmp',
			valueField:'id',
			textField:'topic', 
			});
    }); 
    	

    //对应机会下拉框选择后，获取机会id
    $('#order-form-salesOppid').combobox({
		onSelect : function(record) {
			$("#saleid").val(record.id);
		}
	});
    
</script>
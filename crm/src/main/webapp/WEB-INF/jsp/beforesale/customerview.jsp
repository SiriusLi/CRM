<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="themes/default/easyui.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<link rel="stylesheet" href="themes/icon.css" />
<link rel="stylesheet" href="iconfont/iconfont.css" />
<script src="js/system/beforesales/customerview.js"></script>
<title>客户视图</title>
<style type="text/css">

.middle_div{
	width:1400px;
	height:1400px;
	margin:0px auto; 
}

.big_div{
width:100%;
height:1500px;
background-color:#F4F8FB;
}

*{
margin:0;
padding:0;

}
i{
background-color:#FFFFFF;
}
.middle_div ul{
list-style:none;
width:200px;
height:30px;
line-height:30px;
display:inline-block;
}

.div_li1{
float:left;
width:80px;
height:30px;

box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05);
}

#float_right{
width:500px;
height:35px;
float:right;
}
.details{
width:150px;height:30px;float:right;display:none;color:#337AB7
}
.mysteve {width:150px; height:30px;float:right;color:#337AB7}
#add {width:60px; height:30px;float:right;display:none;color:#337AB7}
#aa:hover{
cursor:pointer;
color:black;
}
#add:hover{
cursor:pointer;
color:black;
}
#bianji:hover{
cursor:pointer;
color:black;

}

#shanchu{
margin-left:12px;margin-right:12px;
}
#shanchu:hover{
cursor:pointer;
color:black;
}
#zhuanyi{
margin-right:12px;
}
#zhuanyi:hover{
cursor:pointer;
color:black;
}
#gongxiang:hover{
cursor:pointer;
color:black;
}
.middle_left{
width:450px;
height:1000px;
float:left;

}

.middle_right{
width:920px;
height:800px;
border:1px solid #EBEEF2;
float:right;
background-color:#FFFFFF;
border-radius:5px;
}

.middle_left1{height:140px;margin-bottom:15px;border:1px solid #EBEEF2;background-color:#FFFFFF;border-radius:5px;}
.middle_left2{height:200px;margin-bottom:15px;border:1px solid #EBEEF2;background-color:#FFFFFF;border-radius:5px}
.middle_left3{height:100px;margin-bottom:15px;border:1px solid #EBEEF2;background-color:#FFFFFF;border-radius:5px}
.middle_left4{height:300px;margin-bottom:15px;border:1px solid #EBEEF2;background-color:#FFFFFF;border-radius:5px}
.middle_left5{height:100px;margin-bottom:15px;border:1px solid #EBEEF2;background-color:#FFFFFF;border-radius:5px}

.circle0{
width: 60px;height: 60px;border-radius: 50%;-moz-border-radius: 50%;-webkit-border-radius: 50%;margin-left:80px;margin-top:50px;
float:left;
}
.circle{
width: 60px;height: 60px;border-radius: 50%;-moz-border-radius: 50%;-webkit-border-radius: 50%;margin-left:40px;margin-top:50px;
float:left;
}
.middle_p{
margin-top:20px;
margin-left:20px;

}
.middle_p_p{
margin-left:10px;
margin-top:15px;
}
.current{
	border-bottom:1px solid black;
}

.dingxing{
float:left;
width:140px;
height:90px;
text-align:center;
border:1px solid black;
}

</style>

</head>
<body>

<div class="big_div">
	<div class="middle_div">
		<div style="height: 40px">

			<ul>
				<li class="div_li1">客户视图</li>
				
			</ul>

			<div id="float_right">
				<div class="mysteve">
					<a id="aa" style="margin-right:10px"><i class="iconfont icon-gengduo1" style="background-color:#F4F8FB;">更多功能</i></a>
					<a id="bianji"><i class="iconfont icon-bianji" style="background-color:#F4F8FB;">编辑</i></a>
				</div>
				
				<div class="details">
					<a id="shanchu">
						<i class="iconfont icon-msnui-trash" style="background-color:#F4F8FB;"></i>删除
					</a> 
					
					<a id="zhuanyi">
						<i class="iconfont icon-wenjiandaochu" style="background-color:#F4F8FB;"></i>转移
					</a>
					
					
				</div>
				
				<div id="add">
					<i class="iconfont icon-shenhebohui" style="background-color:#F4F8FB;">收起</i>
				</div>
				
			</div>

		</div>

		<div style="height: 70px"></div>
		<div style="height: 100px; text-align: center">
			<h4 style="font-size: 25px; margin-bottom: 15px">${cus.name}</h4>
			<input id="cusid" type="hidden" value="${cus.id}">
			<span style="margin-top: 10px">更新：今天， 创建：${cus.createDate} 崔子文 百度查
				天眼查 工商信息</span>
		</div>

		<div class="middle_left">
			<div class="middle_left1">
				<span style="width: 450px"><i class="iconfont icon-ai62"></i>三定一节点</span>
				<br/>
				<ul id="sanding" style="list-style:none;width:430px;height:90px;margin-top:20px;margin-left:10px">
					<li class="dingxing">
						<span>定性</span><br/>
						<span style="font-weight:bold;"><i class="iconfont icon-ai64"></i>${cus.qualitative}</span>
					</li>
					
					<li style="float:left;width:140px;height:90px;text-align:center;border-top:1px solid black;border-bottom:1px solid black">
						<span>定级</span><br/>
						<span style="font-weight:bold;"><i class="iconfont icon-ai64"></i>${cus.rank}</span>
					</li>
					
					<li class="dingxing">
					 <span>定量</span><br/>
					 <span style="font-weight:bold;"><i class="iconfont icon-ai64"></i>${cus.ration}元</span>
					</li>
				</ul>
			</div>
			<div class="middle_left2">
				<p class="middle_p"><i class="iconfont icon-chakanyanjingshishifenxi2"></i>基本信息</p>
				<p class="middle_p">助记简称</span> ${cus.name}(${cus.tel}) 种类 客户</p>
				<p style="margin-left: 20px">来源 ${cus.source }</p>
				<p class="middle_p">备注 个体</p>
				<p class="middle_p">工商信息(由天眼查提供)</p>
			</div>

			<div class="middle_left3">
				<p class="middle_p"><i class="iconfont icon-shezhigongjutianjia"></i>用户画像</p>
				<p class="middle_p">${cus.portrayal}</p>
			</div>

			<div class="middle_left4">
				<p class="middle_p"><i class="iconfont icon-kefu"></i>联系人</p>
				
				<div style="width: 100px; height: 200px; float: left;margin-top:30px">
					<img src="images/customerview/customer.png" style="width:80px;height:100px;margin-left:20px">
				</div>
				
				<div
					style="width: 300px; height: 200px; float: right">
					<p class="middle_p_p"><i class="iconfont icon-shouji"></i>手机：${cus.tel}</p>
					<p class="middle_p_p"><i class="iconfont icon-tubiao209"></i>邮箱：${cus.email}</p>
					<p class="middle_p_p"><i class="iconfont icon-shengriliwu"></i>生日：${cus.birthday}</p>
					<p class="middle_p_p"><i class="iconfont icon-gender"></i>性别：${cus.sex}</p>
				</div>
			</div>

			<div class="middle_left5">
				<p class="middle_p_p"><i class="iconfont icon-dizhi"></i>地址</p>
				<p class="middle_p_p">国家或地区 CN China中国 亚洲 网址 http://</p>

			</div>
		</div>

		<div class="middle_right">
			<p class="middle_p_p">跟单时间线</p>
			<div style="margin: 30px 0 10px 0;"></div>
				<ul style="width:800px;height:60px;list-style:none;background-color:#FFFFFF">
					
					<li style="float:left;width:100px;height:60px;margin-right:20px;text-align:center;background-color:#FFFFFF" id="middleli2">
						<i class="iconfont icon-hetong"></i><br/>
						合同
					</li>
					<li style="float:left;width:100px;height:60px;margin-right:20px;text-align:center;background-color:#FFFFFF" id="middleli3">
					<i class="iconfont icon-ai64"></i><br/>
						销售机会
					</li>
					
					<li style="float:left;width:100px;height:60px;margin-right:20px;text-align:center;background-color:#FFFFFF" id="middleli4">
					<i class="iconfont icon-kefu"></i><br/>
						行动任务
					</li>
				</ul>
				<div style="height:20px"></div>
				<div id="panel">
					
				</div>
				
				
				
				
			</div>
</div>


		<!-- 页首右边效果 -->
		



	</div>
</body>
</html>
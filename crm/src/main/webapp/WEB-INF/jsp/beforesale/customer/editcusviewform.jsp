<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form id="#editcusviewform" method="post"
	style="margin: 0; padding: 10px">
	<div
		style="height: 70px; border-bottom: 1px solid black; line-height: 50px">
		<span style="font-size: 20px">客户信息</span> <input type="hidden"
			value="${cu.id}" name="id">
	</div>
	<div style="height: 160px; margin-top: 35px; background-color: #FFFFFF">

		<div style="height: 120px; margin-top: 20px">
			<table>
				<tr>
					<td colspan="2">
						<div style="width: 370px; height: 60px; margin-left: 10px">

							<input name="name" class="easyui-textbox" label="客户姓名"
								style="width: 300px; height: 40px" disabled="disabled" value="${cu.name}" />
						</div>

					</td>
				</tr>
				<tr>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<input name="ration" class="easyui-textbox" label="定量"
								style="width: 300px; height: 40px" value="${cu.ration }" />
						</div>
					</td>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">

							<span>性别</span> <input type="radio" name="sex" value="男"
								style="width: 20px; height: 20px"
								<c:if test="${cu.sex == '男'}">checked</c:if> />男 <input
								type="radio" name="sex" value="女"
								style="width: 20px; height: 20px"
								<c:if test="${cu.sex == '女'}">checked</c:if> />女
						</div>

					</td>
				</tr>
			</table>
		</div>
	</div>

	<!-- 生命周期 -->

	<div style="height: 80px;">
		<div style="height: 60px">

			<table>
				<tr>
					<td>

						<div style="width: 370px; height: 40px">
							<span style="margin-right: 20px">生命周期</span> 
							<input type="radio"
								name="lifecycle" value="潜在" style="width: 20px; height: 20px"
								<c:if test="${cu.lifecycle == '潜在'}">checked</c:if> />潜在
								
								 <input
								type="radio" name="lifecycle" value="签约"
								style="width: 20px; height: 20px"
								<c:if test="${cu.lifecycle == '签约'}">checked</c:if> />签约 <input
								type="radio" name="lifecycle" value="重复购买"
								style="width: 20px; height: 20px"
								<c:if test="${cu.lifecycle == '重复购买'}">checked</c:if> />重复购买 <input
								type="radio" name="lifecycle" value="失效"
								style="width: 20px; height: 20px"
								<c:if test="${cu.lifecycle == '失效'}">checked</c:if> />失效
						</div>
					</td>
				</tr>

			</table>
		</div>

	</div>

	<!-- 用户画像 -->

	<div style="height: 80px; margin-top: 20px">
		<div style="height: 60px; margin-top: 20px">
			<table>
				<tr>
					<td colspan=2>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<input name="portrayal" class="easyui-textbox" label="用户画像"
								style="width: 300px; height: 80px" value="${cu.portrayal}" />
						</div>
					</td>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<select name="qualitative"  class="easyui-combobox"
								label="定性" 
								style="width: 300px; height: 40px">
								<option value="有价值"<c:if test="${cu.qualitative == '有价值'}">selected</c:if>>有价值</option>
								<option value="无价值"<c:if test="${cu.qualitative == '无价值'}">selected</c:if>>无价值</option>
							</select>
						</div>

					</td>
				</tr>

			</table>
		</div>

	</div>

	<!-- 商务特征 -->
	<div style="height: 100px; margin-top: 30px; background-color: #FFFFFF">
		<div style="height: 120px">
			<table>
				<tr>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<select name="source" class="easyui-combobox"
								style="width: 300px; height: 40px" label="来源">
								<option value="未选"
									<c:if test="${cu.source == '未选'}">selected</c:if>>未选</option>
								<option value="电话来访"
									<c:if test="${cu.source == '电话来访'}">selected</c:if>>电话来访</option>
								<option value="客户介绍"
									<c:if test="${cu.source == '客户介绍'}">selected</c:if>>客户介绍</option>
								<option value="独立开发"
									<c:if test="${cu.source == '独立开发'}">selected</c:if>>独立开发</option>
								<option value="媒体宣传"
									<c:if test="${cu.source == '媒体宣传'}">selected</c:if>>媒体宣传</option>
								<option value="促销活动"
									<c:if test="${cu.source == '促销活动'}">selected</c:if>>促销活动</option>
								<option value="老客户"
									<c:if test="${cu.source == '老客户'}">selected</c:if>>老客户</option>
								<option value="代理商"
									<c:if test="${cu.source == '代理商'}">selected</c:if>>代理商</option>
								<option value="合作伙伴"
									<c:if test="${cu.source == '合作伙伴'}">selected</c:if>>合作伙伴</option>
								<option value="公开招标"
									<c:if test="${cu.source == '公开招标'}">selected</c:if>>公开招标</option>
								<option value="其他"
									<c:if test="${cu.source == '其他'}">selected</c:if>>其他</option>
								<option value="互联网"
									<c:if test="${cu.source == '互联网'}">selected</c:if>>互联网</option>
							</select>
						</div>
					</td>
					
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<select name="rank"  class="easyui-combobox"
								label="定级" 
								style="width: 300px; height: 40px">
								<option value="大单" <c:if test="${cu.rank == '大单'}">selected</c:if>>大单</option>
								<option value="正常单" <c:if test="${cu.rank == '正常单'}">selected</c:if>>正常单</option>
								<option value="小单" <c:if test="${cu.rank == '小单'}">selected</c:if>>小单</option>
							</select>
						</div>

					</td>
					
				</tr>
				
				
			</table>
		</div>

	</div>


	<!-- 联系方式 -->

	<div style="height: 80px">
		<div style="height: 60px">
			<table>
				<tr>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<input name="tel" class="easyui-textbox" label="电话号码"
								style="width: 300px; height: 40px" value="${cu.tel}" />
						</div>

					</td>

					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">

							<input name="email" class="easyui-textbox" label="邮箱地址"
								style="width: 300px; height: 40px" value="${cu.email }" />
						</div>

					</td>
				</tr>

			</table>
		</div>

	</div>

</form>


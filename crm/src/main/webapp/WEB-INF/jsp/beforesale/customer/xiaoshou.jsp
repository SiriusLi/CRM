<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:forEach items="${opps }" var="opps" varStatus="j">
	<li id="oppli" style="list-style:none" onclick="salesopp(this,'${j.index}','${opps.id }')">
		<i class="iconfont icon-ai64"></i>
		<span style="color:#98A6AD">销售机会</span>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		<a style="font-weight:bold">
			<input id="opporid" value="${opps.id }" style="display:none"/>
			<fmt:formatDate value="${opps.updateTime}" pattern="yyyy-MM-dd HH:mm"/> 
			&nbsp&nbsp&nbsp ${opps.topic}
		</a>
	</li>
</c:forEach>

<style>
	a:hover{
		color:#4AA1A6;
		cursor:pointer;
	}
</style>
<script type="text/javascript">
	function salesopp(li,index,id){
		//获取index为具体值那行的li
		var dialog = $("<div/>").dialog({
		title: "编辑销售机会",
		width:800,
		height: 700,
		modal:true,
		href:'opp/oppaddform/'+id,
		onClose: function(){
			//关闭窗户后，立即销毁窗口
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-ok",
				text:"保存",
				handler:function(){
					var id = $("input[name='id']").val();
					var updateTime=$("input[name='updateTime']").val();
					var expectedTime=$("input[name='expectedTime']").val();
					var topic=$("input[name='topic']").val();
					var customerid=$("input[name='customer.id']").val();
					var stage=$("input[name='stage']").val();
					var priority=$("input[name='priority']").val();
					var possibility=$("input[name='possibility']").val();
					var expectedMoney=$("input[name='expectedMoney']").val();
					//console.log(expectedTime);
					//校验表单数据
					//if(form.form("validate")){
					$.ajax({
						url:'opp/saveedit',
						type:"GET",
						data:{id:id,
							updateTime:updateTime,expectedTime:expectedTime,
							topic:topic,customerid:customerid,
							stage:stage,
							priority:priority,possibility:possibility,
							expectedMoney:expectedMoney
						},
							success : function(data) {
								$.messager.alert("消息",data.message,"info");
								alert("请重新刷新销售机会");
								// 关闭当前窗口
								dialog.dialog("close");
							},
							error : function() {
								$.messager.alert("消息","失败！","error");
							}
						})
				}
			},
			{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					//关闭当前窗口
					dialog.dialog("close");
				}
			}
		]
	});
}
</script>
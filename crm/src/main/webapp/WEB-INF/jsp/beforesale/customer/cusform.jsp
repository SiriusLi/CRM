<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!-- 只是一个新建客户信息的弹窗页面 -->
<form id="cusForm" method="post" style="margin:0;padding:10px 30px;">
		
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="name" required="true" label="公司名称：" style="width: 350px">
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="contacts" required="true" label="联系人姓名：" style="width: 350px">
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="tel" label="手机：" style="width: 350px">
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="email" label="邮箱：" style="width: 350px" validType="email">
	</div>
	
	<div style="margin-bottom: 10px">		
		<select class="easyui-combobox" editable="false" name="lifecycle"  label="生命周期：" panelHeight="auto"  style="width:200px;">
	                
	                <option value="潜在客户">潜在客户</option>
	                <option value="签约客户">签约客户</option>
	                <option value="重复购买">重复购买</option>
	                <option value="失效">失效</option>
	            
		</select>
	</div>
	
	
	<div style="margin-bottom: 10px">		
		<select class="easyui-combobox" editable="false" name="source"  label="客户来源：" panelHeight="auto"  style="width:200px;">
	                <option value="">未选</option>
	                <option value="电话来访">电话来访</option>
	                <option value="客户介绍">客户介绍</option>
	                <option value="独立开发">独立开发</option>
	                <option value="媒体宣传">媒体宣传</option>
	                <option value="促销活动">促销活动</option>
	                <option value="老客户">老客户</option>
	                <option value="代理商">代理商</option>
	                <option value="合作伙伴">合作伙伴</option>
	                <option value="公开招标">公开招标</option>	                
	                <option value="互联网">互联网</option>
	                <option value="其他">其他</option>
		</select>
	</div>
	
	<div style="margin-bottom: 20px">
		<input class="easyui-textbox" name="note" data-options="multiline:true" label="备注：" style="width:350px;height:60px">
	</div>
	
	<div style="float: right;">
		<a id="more" class="easyui-linkbutton" >添加详细资料>></a>
	</div>
	
	<!-- 在客户管理界面新建的客户信息，于是两个处理状态都应该为已处理 -->
	<input type="hidden" name="maState" value="true">
	<input type="hidden" name="saState" value="true">
</form>

<script type="text/javascript">
		$("#more").click(function(){
			alert("先添加客户的基本信息后，再编辑，可添加更多详细的信息。");
		})
</script>
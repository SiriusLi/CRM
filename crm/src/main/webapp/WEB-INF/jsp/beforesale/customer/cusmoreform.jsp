<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<!-- 只是一个编辑客户信息的弹窗页面，可添加更多的信息。 -->
<form id="cusMoreForm" method="post" style="margin:0;padding:10px 30px;">
	
	<input type="hidden" value="${c.id }" name="id">
	
	<div style="border-bottom: 1px solid gray;margin-bottom: 20px;margin-top: 10px">
		<h5 style="margin: 0 auto;padding-left: 340px">基本信息</h5>
	</div>
	
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="name" value="${c.name }" required="true" label="公司：" style="width: 350px">		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<select class="easyui-combobox" editable="false" name="source" label="客户来源：" panelHeight="auto"  style="width:200px;">
	                <option value="">未选</option>
	                <option value="电话来访" <c:if test="${c.source == '电话来访' }">selected</c:if>>电话来访</option>
	                <option value="客户介绍" <c:if test="${c.source == '客户介绍' }">selected</c:if>>客户介绍</option>
	                <option value="独立开发" <c:if test="${c.source == '独立开发' }">selected</c:if>>独立开发</option>
	                <option value="媒体宣传" <c:if test="${c.source == '媒体宣传' }">selected</c:if>>媒体宣传</option>
	                <option value="促销活动" <c:if test="${c.source == '促销活动' }">selected</c:if>>促销活动</option>
	                <option value="老客户" <c:if test="${c.source == '老客户' }">selected</c:if>>老客户</option>
	                <option value="代理商" <c:if test="${c.source == '代理商' }">selected</c:if>>代理商</option>
	                <option value="合作伙伴" <c:if test="${c.source == '合作伙伴' }">selected</c:if>>合作伙伴</option>
	                <option value="公开招标" <c:if test="${c.source == '公开招标' }">selected</c:if>>公开招标</option>
	                <option value="互联网" <c:if test="${c.source == '互联网' }">selected</c:if>>互联网</option>
	                <option value="其他" <c:if test="${c.source == '其他' }">selected</c:if>>其他</option>
	                
		</select>
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" value="${c.employee.empName }" editable="false" label="所有者：" style="width: 350px">		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input id="testDate" class="easyui-datebox" name="createDate"  editable="false" value="${c.createDate }" label="创建日期：" style="width:200px;">
	</div>	
	
	<div style="margin-bottom: 30px">		
		<select class="easyui-combobox" editable="false" name="lifecycle" label="生命周期：" panelHeight="auto"  style="width:350px;">
	                
	                <option value="潜在客户" <c:if test="${c.lifecycle == '潜在客户' }">selected</c:if>>潜在客户</option>
	                <option value="签约客户" <c:if test="${c.lifecycle == '签约客户' }">selected</c:if>>签约客户</option>
	                <option value="重复购买" <c:if test="${c.lifecycle == '重复购买' }">selected</c:if>>重复购买</option>
	                <option value="失效" <c:if test="${c.lifecycle == '失效' }">selected</c:if>>失效</option>
	            
		</select>
	</div>
	
	<div style="border-bottom: 1px solid gray;margin-bottom: 20px">
		<h5 style="margin: 0 auto;padding-left: 340px">三一客</h5>
	</div>
	
	<div style="margin-bottom: 10px;">
		<select class="easyui-combobox" editable="false" name="qualitative" label="定	性：" panelHeight="auto"  style="width:200px;">
	                <option value="">未选</option>
	                <option value="有价值" <c:if test="${c.qualitative == '有价值' }">selected</c:if>>有价值</option>
	                <option value="无价值" <c:if test="${c.qualitative == '无价值' }">selected</c:if>>无价值</option>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<select class="easyui-combobox" editable="false" name="rank" label="定	级：" panelHeight="auto"  style="width:200px;">
	                <option value="">未选</option>
	                <option value="大单" <c:if test="${c.rank == '大单' }">selected</c:if>>大单</option>
	                <option value="正常单" <c:if test="${c.rank == '正常单' }">selected</c:if>>正常单</option>
	                <option value="小单" <c:if test="${c.rank == '小单' }">selected</c:if>>小单</option>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input class="easyui-textbox" name="ration" value="${c.ration }" label="定	量：" data-options="prompt:'预估销售额'" style="width: 200px">		
	</div>
	
	<div style="margin-bottom: 30px;">
		<input class="easyui-textbox" name="portrayal" data-options="multiline:true,prompt:'对客户现有情况的一些简单描述。'" label="客户画像：" style="width:500px;height:80px">
	</div>
	
	<div style="border-bottom: 1px solid gray;margin-bottom: 20px">
		<h5 style="margin: 0 auto;padding-left: 340px">联系人信息</h5>
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="contacts" value="${c.contacts }"  label="姓名：" style="width: 350px">		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input class="easyui-textbox" name="tel" value="${c.tel}"  label="电话：" style="width: 350px">		
	</div>	
	
	<div style="margin-bottom: 30px">
		<input class="easyui-textbox" name="email" value="${c.email }"  label="邮箱：" style="width: 350px" validType="email">		
		&nbsp;&nbsp;&nbsp;&nbsp;
		<input id="birDate" class="easyui-datebox" name="birthday" editable="false" value="${c.birthday }" label="生日：" style="width:200px;">	
		&nbsp;&nbsp;&nbsp;&nbsp;
		<label>性别：</label>
		男	<input type="radio" name="sex" value="男" <c:if test="${ c.sex == '男' }">checked="checked"</c:if>>
		女	<input type="radio" name="sex" value="女" <c:if test="${ c.sex == '女' }">checked="checked"</c:if>>
	</div>
	
	<div >
		<input class="easyui-textbox" name="note" data-options="multiline:true" label="备注：" style="width:500px;height:80px">
		<input type="hidden" name="maState" value="1">
		<input type="hidden" name="saState" value="1">
	</div>
</form>

<script type="text/javascript">
/* 日期选择框只能选择当前时间之前的日期 */
$('#testDate').datebox().datebox('calendar').calendar({
	validator: function(date){
		var now = new Date();
		var d = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		return date<=d;
	}
});
$('#birDate').datebox().datebox('calendar').calendar({
	validator: function(date){
		var now = new Date();
		var d = new Date(now.getFullYear(), now.getMonth(), now.getDate());
		return date<=d;
	}
});
</script>
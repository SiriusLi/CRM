<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:forEach items="${actions }" var="ac">
	<li style="list-style:none">
		<i class="iconfont icon-ai62"></i>
		<span style="color:#98A6AD">任务行动</span>
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
		<a id="a" style="font-weight:bold">
		<fmt:formatDate value="${ac.date}" pattern="yyyy-MM-dd HH:mm"/> 
		&nbsp&nbsp&nbsp
		${ac.content }</a>
	</li>

</c:forEach>

<style>
	a:hover{
		color:#4AA1A6;
		
	}

</style>
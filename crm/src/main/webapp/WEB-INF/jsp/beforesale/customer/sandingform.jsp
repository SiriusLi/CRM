<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form id="sandingForm" method="post" style="margin:0;padding:10px 30px;">
<input type="hidden" id="qualitative" value="${cus.qualitative}" />
<input type="hidden" id="rank" value="${cus.rank}" />
<input type="hidden" name="id" value="${cus.id}" />
	<table style="width:300px;height:300px">
		<tr>
			<td>定性：
				<input type="radio" name="qualitative" value="有价值" style="width:20px;height:20px">有价值</input>
				<input type="radio" name="qualitative" value="无价值" style="width:20px;height:20px">无价值</input>		
				
			</td>
			
		</tr>
		
		
		<tr>
			
			<td>定级：
				<input type="radio" name="rank" value="大单" style="width:20px;height:20px">大单</input>
				<input type="radio" name="rank" value="小单" style="width:20px;height:20px">小单</input>		
				<input type="radio" name="rank" value="正常单" style="width:20px;height:20px">正常</input>
			</td>
			

		</tr>
		
		
		<tr>
			<td>
				<input name="ration" class="easyui-textbox" label="定量(总价)"
						style="width: 280px;height:40px" value="${cus.ration}"/>
			</td>
			
		</tr>
	
	</table>

</form>

<script type="text/javascript">
$(function(){
	/**
	 * 编辑功能里面的radio值的获取
	 */
	var radios = $("#qualitative").val();
	//jconsole.log(radios);
	if(radios=='有价值'){
		$("input[type='radio'][name='qualitative'][value='有价值']").attr("checked",true); 
	}else {
				$("input[type='radio'][name='qualitative'][value='无价值']").attr("checked",true);  
	}
	
	var radioss = $("#rank").val();
	if(radioss=='大单'){
		$("input[type='radio'][name='rank'][value='大单']").attr("checked",true); 
	}else if(radioss=='小单'){
				$("input[type='radio'][name='rank'][value='小单']").attr("checked",true);  
	}else{
		$("input[type='radio'][name='rank'][value='正常单']").attr("checked",true);  
	}
	
	
});
</script>


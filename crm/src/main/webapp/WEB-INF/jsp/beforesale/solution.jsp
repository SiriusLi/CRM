<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>解决方案</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/system/beforesales/solution.js"></script>


<style type="text/css">
.datagrid-row-selected{
    background-color:white;
	}
	
	a:hover{
		color:red;
		cursor:pointer;
	}
	
	</style>
</head>
<body>
		<input id="sessionempname" value="${employee.role.roleName}" style="display:none;">
		
		<table id="solutionGrid" class="easyui-datagrid" title="解决方案 " style="height:400px;width:100%;" fit="true"
			fitColumns="true" data-options="rownumbers:true,singleSelect:true,url:'solution/solutionform',method:'get'" toolbar="#solutionToolbar" pagination="true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'id',width:50,align:'center'">ID</th>
				<th data-options="field:'topic',width:200,align:'center'">方案主题</th>
				<th data-options="field:'time',width:200,align:'center',formatter : function(value){
                var date = new Date(value);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                var d = date.getDate();
                return value==null?null:y + '-' +m + '-' + d;
            }">时间</th>
				<th data-options="field:'content',width:200,align:'center'">方案内容</th>
				
				<th data-options="field:'salesOpp',width:200,align:'center',formatter:function(value,row,index){
						return row.topic;	
				}">对应销售机会</th>
				<th data-options="field:'employee',width:200,align:'center',formatter:function(row,value,index){
						 return new Object(value['employee']).empName;
				}">方案提供者</th>
				<th data-options="field:'note',width:200,align:'center'">备注</th>
				
				<th data-options="field:'cz',width:100,align:'center',formatter:caozuo">操作</th>
			</tr>
		</thead>
	</table>
	
	<!-- 无需点击选择框就可以实现对表格数据的选择的操作，此处设置为隐藏 -->
	<div style="margin:10px 0; ">
		选择时操作：
		<select   onchange="$('#solutionGrid').datagrid({singleSelect:(this.value==0)})">
			<option value="0">单选行</option>
			<option value="1">多选行</option>
		</select><br/>
		 
	</div>
	
	<!-- 工具栏 -->
	<div id="solutionToolbar">
	    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
	    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
	</div>
	
	<script type="text/javascript">
	/**
	function details(row,value,index){
		var str = '';
		str += '<a onclick="showdetails()">详情</a>';	
		return str;
	
}
	**/
	
	$(function () {
		var grid = $("#solutionGrid");
		//为工具栏按钮添加绑定事件
		$("#solutionToolbar").on("click","a.add",function(){
			var dialog = $("<div/>").dialog({
				title:"新增解决方案",
				width:700,
				height:700,
				modal:true,
				href:'solution/solutioneditform',
				onClose:function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var topic=$("input[name='topic']").val();
							var salesOppid=$("input[name='salesOpp.id']").val();
							var content=$("input[name='content']").val();
							var time=$("input[name='time']").val();
							var note=$("input[name='note']").val();
							$.ajax({
								url:'solution/savenew',
								type:"GET",
								data:{
									topic:topic,salesOppid:salesOppid,
									content:content,time:time,
									note:note
								},
									success : function(data) {
										$.messager.alert("消息",data.message,"info");
										// 关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
										
									},
									error : function() {
										$.messager.alert("消息","失败！","error");
									}
								})
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}).on("click","a.delete",function(){
			var rows = grid.datagrid("getSelections");
	        if (rows){
	        	 $.messager.confirm('警告','您确定要删除该用户及其关联的视图吗？',function(r){
	             	
	                 if (r){
	                 	var firmIds = [];
	             		for (var i = 0; i<rows.length; i++) {
	             			var ids = rows[i].id;
	             			firmIds.push(ids);
	             		}
	                     $.post('solution/delete/'+firmIds,function(result){
	                         if (result.success){
	                         	grid.datagrid("reload");  
	                         } else {
	                         	$.messager.alert("提示！","系统错误，删除失败！");
	                         }
	                     });
	                 }
	             });
	        }
		});
		
	});
	
	function caozuo(row,value,index){
		var str = '';
		str += '<a onclick="edit('+index+')">编辑</a>';	
		return str;
	
}
	//表格内容的编辑功能
	function edit(index){
		var grid = $("#solutionGrid");
		//实时获取所点击那一行的信息
    	$('#solutionGrid').datagrid('selectRow',index);
    	var row = $('#solutionGrid').datagrid('getSelected');
		//var grid = $('#solutionGrid');
		//var row = grid.datagrid('getSelected',index);
		//alert(row.id);
		if(row){
		var dialog = $("<div/>").dialog({
			title: "编辑解决方案",
			width: 700,
			height: 700,
			modal:true,
			href:'solution/solutioneditform/'+row.id,
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var id = $("input[name='id']").val();
						var topic=$("input[name='topic']").val();
						var salesOppid=$("input[name='salesOpp.id']").val();
						var content=$("input[name='content']").val();
						var time=$("input[name='time']").val();
						var note=$("input[name='note']").val();
						$.ajax({
							url:'solution/saveedit',
							type:"GET",
							data:{id:id,
								topic:topic,salesOppid:salesOppid,
								content:content,time:time,
								note:note
							},
								success : function(data) {
									$.messager.alert("消息",data.message,"info");
									// 关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
									
								},
								error : function() {
									$.messager.alert("消息","失败！","error");
								}
							})
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}
}
	
	
function showdetails(){
	var grid = $('#solutionGrid');
	var row = grid.datagrid("getSelected");
	//alert(row.id);
	var dialog = $("<div/>").dialog({
		title:"详细信息",
		width:660,
		height:600,
		modal:true,
		href:'solution/solutiondetaisform/'+row.id,
		onClose:function(){
			//关闭窗户后，立即销毁窗口
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					//var form = $("#solutiondetaisform");
					//关闭当前窗口
					dialog.dialog("close");
				}
			},
		]
	});
	
}




</script>
	

</body>
</html>
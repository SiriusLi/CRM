<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script src="js/system/beforesales/quotation.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table id="quotationDatagrid"></table>
<div id="quotationToolbar">
	<a class="easyui-linkbutton add"    iconCls="icon-add" plain="true">新增</a>
	<a class="easyui-linkbutton edit"   iconCls="icon-edit" plain="true">编辑</a>
	<a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
	<a class="easyui-linkbutton detail" iconCls="icon-more" plain="true">查看详情</a>
</div>
<!-- 编辑订单 -->
<div id="order-editdlg1" class="easyui-window" style="width: 850px"
	modal="true" height="400px;" closed="true">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:false"
			style="padding: 10px;">
			<div style="width: 80%; margin: 0px auto">
				<form id="orderedit-form1">
					<c:import url="quotation_edit.jsp"></c:import>
				</form>
			</div>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)"
				style="width: 80px" id="saveupdate">保存</a> <a class="easyui-linkbutton"
				data-options="iconCls:'icon-cancel'" href="javascript:void(0)"
				onclick="javascript:$('#order-editdlg1').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>

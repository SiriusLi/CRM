<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<input type="hidden" name="id">
<table style="line-height: 40px;">
	<!-- 客户、销售机会、报价员、审批、审批员、 -->
	<tr>
		<td>对应客户:</td>
		<td><select id="orderadd-customer-cc" class="easyui-combogrid"
			name="customerid" style="width: 160px; height: 25px;border:none;"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'name',
			url: 'cus/customerlist',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '姓名',formatter : function (value, row, index)  {
	        		 return row.name;
				 }},
	        	 ] ], 
			fitColumns: true
		">
		</select></td>
		<td width="80px;"></td>
		<td>对应销售机会:</td>
		<td><select id="orderadd-salesOpp-cc" class="easyui-combogrid"
			name="salesOppid" style="width: 160px; height: 25px;"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'topic',
			method: 'get',
			url:'opp/opportunitylist',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '销售主题',formatter : function (value, row, index)  {
	        		 return row.topic;
				 }},
	        	 ] ], 
			fitColumns: true
		">
	</tr>
	<tr>
		<td>审核状态：</td>
		<td><input readonly="readonly" name="audit1" class="easyui-textbox"
			style="height: 25px;" /></td>
		<td width="80px;"></td>
		<td>审核人：</td>
		<td><select class="easyui-combogrid" name="auditEmployeeid"
			style="width: 160px; height: 25px;" readonly="readonly"
			data-options="
			idField: 'id',
			textField: 'empName',
			url: 'employee/empNamelist',
		">
		</select></td>
	</tr>
	<tr>
		<td>审核日期：</td>
		<td><input name="auditDate1"
			style="height: 25px; border: none;" value="10/11/2012 0:0:0"
			readonly="readonly"></input></td>
	</tr>
	
	<tr>
		<td>报价员：</td>
		<td><select id="orderadd-customer-cc" class="easyui-combogrid"
			name="employeeid" style="width: 160px; height: 25px;" required="true"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'empName',
			url: 'employee/empNamelist',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'empName',width : 100,title : '姓名',formatter : function (value, row, index)  {
	        		 return row.empName;
				 }},
	        	 ] ], 
			fitColumns: true
		">
		</select></td>
	</tr>
	
</table>

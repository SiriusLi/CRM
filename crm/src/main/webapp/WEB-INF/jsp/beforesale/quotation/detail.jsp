<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<table style="line-height: 40px;boder: 1px solid balck;"  >
	<!-- 
		时间，总报价，审批，审批日期，对应销售机会，报价员审批员。对应客户
		明细表格
	 -->
	 <input id="quotationid" type="hidden" value="${q.id}">
	<tr style="boder: 1px solid balck;">
		<td>报价id：</td><td>${q.id}</td>
		<td width="80px;"></td>
		<td>客户：</td><td>${q.customer.name}</td>
		<td width="80px;"></td>
		<td>创建日期：</td><td><fmt:formatDate value="${q.time}" pattern="yyyy-MM-dd HH:mm:ss"/></td></td>
	</tr>
	<tr style="border-bottom: 1px solid black;">
		<td>销售机会：</td><td>${q.salesOpp.topic }</td><td width="80px;"></td>
		<td>报价：</td><td>${q.money}</td><td width="80px;"></td>
		<td>报价人：</td><td>${q.employee.empName }</td>
	</tr>
	<tr>
		<td>审批信息：</td>
	</tr>
	<tr style="border-bottom: 1px solid gray;">
		<td>审批：</td><td>${q.approval}</td><td width="80px;"></td>
		<td>审批日期：</td><td><fmt:formatDate value="${q.auditDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td><td width="80px;"></td>
		<td>审批员：</td><td>${q.approvalEmployee.empName}</td>
	</tr>
</table>
<table id="quotationDetailsDatagrid"></table>
<div id="quotationDetailsToolBar"></div>
<script type="text/javascript">
	$(function() {
		//获取表格并初始化,根据报价单id获取到报价明细（id,数量，成本价，报价，对应报价单，对应产品）
		var qdd = $("#quotationDetailsDatagrid");
		var id = $("#quotationid").val();
		qdd.datagrid({
			title:'报价明细',
			url:'quotationDetails/list/'+id,
			fitColumns:true,
			fit:true,
			toolbar:'#quotationDetailsToolBar',
			singleSelect:true,
			pagination:true,
			idField:'id',
			columns:[[
				{field:'id',title:'id',width:15},
				{field:'product',title:'产品名称',width:30,formatter:function(value,row,index){
					return value.name;
				}},
				{field:'price',title:'成本价',width:15},
				{field:'quotedPrice',title:'报价',width:15},
				{field:'num',title:'数量',width:15},
			]]
		})
	})
</script>
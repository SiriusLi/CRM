<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form id="quotationForm" method="post" style="margin:0;padding:10px 30px;">
	<input type="hidden" value="" name="id">
	<input type="hidden" name="employee.id" value="1">
	<div style="margin-bottom: 10px">
		客户：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id="customerCombobox" class="easyui-combobox" name="customer.id" data-options="panelHeight:'auto',url:'cus/getCustomerList',valueField:'id',textField:'name'" style="width:200px">
		</select>
	</div>
	<div style="margin-bottom: 10px">
		销售机会：<select id="opportunityCombobox" class="easyui-combobox" name="salesOpp.id" data-options="panelHeight:'auto',valueField:'id',textField:'topic'" style="width:200px">
		</select>
	</div>
	
	<!-- <div class="easyui-layout" style="width:100%;height: 400px;">
		<div data-options="region:'west'" title="产品列表" style="width: 40%">
			<table id="productDatagrid"></table>
		</div>
		<div class="targetArea" data-options="region:'center'" title="报价单明细">
			<table id="targetDatagrid" class="easyui-droppable">
				
			</table>
			<div id="targetToolbar">
				<a class="easyui-linkbutton save" iconCls="icon-save" plain="true">保存修改</a>
				<a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
			</div>
		</div>
	</div> -->
</form>
<script type="text/javascript">
	$(function(){
		var ifInDroppoable = false;
		//var pd = $("#productDatagrid");
		var td = $("#targetDatagrid");
		var ta = $(".targetArea");
		var tt = $("#targetToolbar");
		var editIndex = undefined;
		//开启可放置
		/* ta.droppable('enable');
		ta.droppable({
			accept:'tr.datagrid-row',
			onDrop:function(e,source){
				//获得产品在产品表那边的下标
				var productIndex = parseInt($(source).attr('datagrid-row-index'))
				//获取当前页产品信息
				var rows = pd.datagrid('getRows');
				//获取拖进来的产品信息
				var product = rows[productIndex];
				//获取所有报价明细表产品
				var details = td.datagrid('getRows');
				//判断该产品是否已经在报价明细表里
				var ifExist = td.datagrid('getRowIndex',product.id)>=0; 
				if(ifExist){
					//已存在
					alert("您添加的产品"+product.name+"在列表中已经存在了！");
				}else {
					td.datagrid('insertRow',{
						index:details.length-1,
						row:{
							id:product.id,
							name:product.name,
							price:product.price,
							num:1,
							amount:product.price*1
						}
					});
					//更新统计行
					details = td.datagrid('getRows');
					console.log(details);
					var number = 0;
					var totalmoney = 0;
					for(i=0;i<details.length-1;i++){
						number+=details[i].num;
						totalmoney+=details[i].amount;
					}
					td.datagrid('updateRow',{
						index:details.length-1,
						row:{
							num:number,
							amount:totalmoney
						}
					});
				}
			},
			//当可拖动元素被拖进来时触发。source 参数指被拖动的 DOM 元素。
			onDragEnter:function(e,source){
				console.log("拖动元素进来了");
			},
			onDragLeave:function(e,source){
				console.log("拖动元素进离开了");
			}
		}); */
		//报价明细表
		/* td.datagrid({
			fitColumns:true,
			fit:true,
			toolbar:'#targetToolbar',
			singleSelect:true,
			pagination:true,
			idField:'id',
			columns:[[
				{field:'id',title:'id',width:15},
				{field:'name',title:'产品名称',width:20},
				{field:'price',title:'单价',width:20},
				{field:'num',title:'数量',editor:{type:'numberbox'},width:20},
				{field:'amount',title:'金额',width:20}
			]],
			onBeforeLoad:function(){
				var rows = $(this).datagrid('getRows');
				var numTotal = 0;
				var moneyTotal = 0;
				$(this).datagrid('appendRow',{
					id:'<b>合计</b>',
					num:numTotal,
					amount:moneyTotal
				});
				//添加统计行
			},
			onDblClickCell:function(index,field,value){
				console.log(index);
				console.log(field);
				console.log(value);
				$(this).datagrid('beginEdit', index);
				var ed = $(this).datagrid('getEditor', {index:index,field:field});
				$(ed.target).focus();
			}
		}); */
		//产品表
		/* pd.datagrid({
			url:'product/all',
			fitColumns:true,
			fit:true,
			singleSelect:true,
			pagination:true,
			idField:'id',
			onLoadSuccess:function(){
				$(this).datagrid('enableDnd');
			},
			columns:[[
				{field:'id',title:'id',width:15},
				{field:'number',title:'编号',width:30},
				{field:'name',title:'商品名字',width:20},
				{field:'price',title:'售价',width:15},
				{field:'type',title:'类型',width:15}
			]]
		});
		//开启过滤
		pd.datagrid('enableFilter',[
			{
				field:'type',
				type:'combobox',
				options:{
					panelHeight:'auto',
					data:[
						{value:'',text:'全部'},
						{value:'饼干',text:'饼干'},
						{value:'饮料',text:'饮料'},
						{value:'瓜子',text:'瓜子'}
					],
					onChange:function(value,row,index){
						console.log(value);
						if(value==''){
							pd.datagrid('removeFilterRule','type')
						}else {
							pd.datagrid('addFilterRule',{
								field:'type',
								op:'equal',
								value:value
							});
						}
						pd.datagrid('doFilter');
					}
				}
			},
			{
				field:'price',
				type:'numberbox',
				options:{precision:2},
				op:['equal','notequal','less','greater','lessorequal','greaterorequal']
			},
		]); */
		function endEditing(){
            if (editIndex == undefined){return true}
            if ($('#dg').datagrid('validateRow', editIndex)){
                $('#dg').datagrid('endEdit', editIndex);
                editIndex = undefined;
                return true;
            } else {
                return false;
            }
	    }
		function accept(index) {
			if(endEditing()){
				var row = td.datagrid('getSelected');
				var beforeNum = row.num;
				
				td.datagrid('acceptChanges');
				var rows = td.datagrid('getRows');
				if(parseInt(rows[index].num)<=0){
					alert("请输入正确的数量！");
					td.datagrid('updateRow',{
						index:index,
						row:{
							num:parseInt(beforeNum),
							amount:(parseFloat(rows[index].price)*parseInt(rows[index].num))
						}
					})
				}
				console.log(rows[index]);
				td.datagrid('updateRow',{
					index:index,
					row:{
						num:parseInt(rows[index].num),
						amount:(parseFloat(rows[index].price)*parseInt(rows[index].num))
					}
				})
				updateDatagrid();
			}
		}
		function updateDatagrid() {
			var number = 0;
			var totalmoney = 0;
			var details = td.datagrid('getRows');
			for(i=0;i<details.length-1;i++){
				number+=parseInt(details[i].num);
				totalmoney+=parseFloat(details[i].amount);
			}
			td.datagrid('updateRow',{
				index:details.length-1,
				row:{
					num:number,
					amount:totalmoney
				}
			});
		}
		/* tt.on('click','a.save',function(){
			var row = td.datagrid('getSelected');
			var index = td.datagrid('getRowIndex',row);
			accept(index);
		}).on('click','a.delete',function(){
			var row = td.datagrid('getSelected');
			var rows = td.datagrid('getRows');
			if(row&&td.datagrid('getRowIndex',row)!=rows.length-1){
				console.log(row);
				td.datagrid('deleteRow',td.datagrid('getRowIndex',row));
				//更新统计行
				updateDatagrid();
			}
		}) */
		
		//二级联动
		$("#customerCombobox").combobox({
			onSelect:function(record){
				console.log(record.value);
				$("#opportunityCombobox").combobox("setValue","");
				if(record){
					var url = 'opp/getOppByCus?customerid='+record.id;
					$("#opportunityCombobox").combobox("reload",url);
				}
				
			}
		});
	})
</script>
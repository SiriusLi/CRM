<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form id="oppaddForm" method="post" style="margin: 0; padding: 10px">
	<input type="hidden" name="id" value="${r.id }" />
	<input type="hidden" id="priority" value="${r.priority}" />
	<div
		style="height: 70px; border-bottom: 1px solid black; line-height: 50px">
		<span style="font-size: 20px">销售机会</span>
	</div>
	<div style="height: 140px; margin-top: 35px">
		<div style="height: 20px; text-align: center">
			<span
				style="background-color: #EBEFF2 !important; font-weight: bold; width: 30px">机会</span><br />
		</div>
		<div style="height: 120px">
			<table>
				<tr>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<input name="topic" class="easyui-textbox" label="机会主题"
								style="width: 300px; height: 40px" value="${r.topic }" />
						</div>

					</td>

					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">

							<input name="updateTime" class="easyui-datebox" label="更新时间"
								style="width: 300px; height: 40px" value="${r.updateTime }" />

						</div>

					</td>
				</tr>
				<tr>
					<!-- 只能给自己手下的客户添加销售机会，如果是客户视图界面则不能修改客户 -->
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
						<input id="opp1" class="easyui-combobox" label="客户"
						style="width: 300px; height: 40px" value="${r.customer.name }"
						data-options="url:'cus/allcustomerlist',valueField:'id',
    					textField:'name'">
    					<input id="opp2" type="hidden" name="customer.id" value="${r.customer.id }"/>
    					<script type="text/javascript">
    					$('#opp1').combobox({
    						onSelect : function(record) {
    							$("#opp2").val(record.id);
    						}
    					});
    					</script>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- 基本情况 -->

	<div style="height: 140px; margin-top: 40px">
		<div style="height: 20px; text-align: center">
			<span
				style="background-color: #EBEFF2 !important; font-weight: bold; width: 30px">基本情况</span><br />
		</div>
		<div style="height: 120px">
			<table>
				<tr>
					<td>
						<div style="width: 330px; height: 60px; margin-left: 10px">

							<select id="jieduan" name="stage" class="easyui-combobox"
								style="width: 300px; height: 40px" value="${r.stage}"
								 label="阶段">
								<option value="初期沟通"<c:if test="${r.stage == '初期沟通'}">selected</c:if>>初期沟通</option>
								<option value="立项评估"<c:if test="${r.stage == '立项评估'}">selected</c:if>>立项评估</option>
								<option value="需求分析"<c:if test="${r.stage == '需求分析'}">selected</c:if>>需求分析</option>
								<option value="方案制定"<c:if test="${r.stage == '方案制定'}">selected</c:if>>方案制定</option>
								<option value="招投标/竞争"<c:if test="${r.stage == '招投标/竞争'}">selected</c:if>>招投标/竞争</option>
								<option value="商务谈判"<c:if test="${r.stage == '商务谈判'}">selected</c:if>>商务谈判</option>
								<option value="合同签约"<c:if test="${r.stage == '合同签约'}">selected</c:if>>合同签约</option>
							</select>

						</div>

					</td>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 60px">
							<select id= "xiaoshou" name="priority" class="easyui-combobox"
								style="width: 300px; height: 40px"
								label="销售优先级" value="${r.priority }">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>

	</div>

	<!-- 预期 -->

	<div style="height: 140px; margin-top: 20px">
		<div style="height: 20px; text-align: center">
			<span
				style="background-color: #EBEFF2 !important; font-weight: bold; width: 30px">预期效果</span><br />
		</div>
		<div style="height: 120px">
			<table>
				<tr>
					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<input name="expectedTime" class="easyui-datebox" label="签约时间"
								style="width: 300px; height: 40px" value="${r.expectedTime }" />

						</div>

					</td>

					<td>
						<div style="width: 370px; height: 60px; margin-left: 10px">

							<input name="possibility" class="easyui-textbox" label="可能性"
								style="width: 300px; height: 40px" value="${r.possibility }">

						</div>

					</td>
				</tr>

				<tr>
					<td colspan="2">
						<div style="width: 370px; height: 60px; margin-left: 10px">
							<input name="expectedMoney" class="easyui-textbox" label="签约金额"
								style="width: 300px; height: 40px" " value="${r.expectedMoney }">
						</div>
					</td>

				</tr>

			</table>
		</div>

	</div>
</form>

<script type="text/javascript">

$(function(){
	//销售优先级
	 var priority = $("#priority").val();
	
	 if(priority==1){
		 $("#xiaoshou option[value='1']").attr("selected","selected");
	 }if(priority==2){
		 $("#xiaoshou option[value='2']").attr("selected","selected");
	 }if(priority==3){
		 $("#xiaoshou option[value='3']").attr("selected","selected");
	 }if(priority==4){
		 $("#xiaoshou option[value='4']").attr("selected","selected");
	 }

	
});
</script>




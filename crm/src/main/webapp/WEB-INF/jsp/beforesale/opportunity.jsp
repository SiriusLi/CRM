 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>机会列表</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/system/beforesales/opp.js"></script>
<script src="js/echarts.js"></script>
<script src="js/datagrid-filter.js"></script>
<style type="text/css">
	.datagrid-row-selected{
    background-color:white;
	}
	a:hover{
		color:red;
		cursor:pointer;
	}

</style>
</head>
<body>
	
	<table id="dg" class="easyui-datagrid" title="销售机会 " style="height:400px;width:100%" fitColumns="true" fit="true"
			data-options="rownumbers:true,singleSelect:true,url:'opp/oppform',method:'get'" toolbar="#menuToolbar" pagination="true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'id',width:100,align:'center'">ID</th>
				<th data-options="field:'topic',width:100,align:'center'">机会主题</th>
				<th data-options="field:'expectedTime',width:200,align:'center',formatter:operate">预计签单时间</th>
				<th data-options="field:'stage',width:100,align:'center'">阶段</th>
				<th data-options="field:'possibility',width:200,align:'center'">可能性</th>
				<th data-options="field:'expectedMoney',width:180,align:'center'">预计签约金额</th>
				<th data-options="field:'updateTime',width:180,align:'center',formatter:operate">更新时间</th>
				<th data-options="field:'priority',width:100,align:'center'">销售优先级</th>
				<th data-options="field:'customer',width:100,align:'center',formatter:function(row,value,index){
						 return new Object(value['customer']).name;
				}
				">客户</th>
				<th data-options="field:'cz',width:120,align:'center',formatter:caozuo">操作</th>
			</tr>
		</thead>
	</table>
	<!-- 无需点击选择框就可以实现对表格数据的选择的操作，此处设置为隐藏 -->
	<div style="margin:10px 0;">
		<span>选择时操作: </span>
		<select onchange="$('#dg').datagrid({singleSelect:(this.value==0)})">
			<option value="0">单选行</option>
			<option value="1">多选行</option>
		</select><br/>

	</div>
<div id="menuToolbar" style="padding:8px">
        <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
        <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
</div>

   
<!--图表区域-->
    <!-- <div id="main" style="width: 600px;height:400px;margin-top:50px;float:left"></div>
	<div id="main2" style="width: 600px;height:400px;margin-top:50px;margin-left:250px;float:left"></div> -->



<script type="text/javascript">
$(function(){
	//给表格重命名
	var grid = $('#dg');
	grid.datagrid({
		title : '机会',
		width : "100%",
	});
	//添加高级查询行列的内容
	grid.datagrid('enableFilter', [ {
		field : 'topic',
		type : 'text',
	}, {
		field : 'expectedMoney',
		type : 'numberbox',
		options : {
			precision : 1
		},
		op : [ 'equal', 'notequal', 'less', 'greater' ]
	} ]);


	//点击menuToolbar上的add按钮，加载add表格
	$("#menuToolbar").on("click","a.add",function(){
		var grid = $('#dg');
		var dialog = $("<div/>").dialog({
			title: "新增销售机会",
			width:810,
			height: 780,
			modal:true,
			href:'opp/oppaddform',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						//var form = $("#oppaddForm");
						var updateTime=$("input[name='updateTime']").val();
						var expectedTime=$("input[name='expectedTime']").val();
						var topic=$("input[name='topic']").val();
						var customerid=$("input[name='customer.id']").val();
						var stage=$("input[name='stage']").val();
						var priority=$("input[name='priority']").val();
						var possibility=$("input[name='possibility']").val();
						var expectedMoney=$("input[name='expectedMoney']").val();
						$.ajax({
							url:'opp/savenew',
							type:"GET",
							data:{
								updateTime:updateTime,expectedTime:expectedTime,
								topic:topic,customerid:customerid,
								stage:stage,
								priority:priority,possibility:possibility,
								expectedMoney:expectedMoney
							},
								success : function(data) {
									$.messager.alert("消息",data.message,"info");
									// 关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
									
								},
								error : function() {
									$.messager.alert("消息","失败！","error");
								}
							})
					}
						
				},
				
				
				
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.delete",function(){
		var grid = $('#dg');
		var rows = grid.datagrid("getSelections");
	    if (rows){
	        $.messager.confirm('警告','您确定要删除该用户及其关联的视图吗？',function(r){
	            if (r){
	            	var firmIds = [];
	        		for (var i = 0; i<rows.length; i++) {
	        			var ids = rows[i].id;
	        			firmIds.push(ids);
	        		}
	                $.post('opp/delete/'+firmIds,function(result){
	                    if (result.success){
	                    	grid.datagrid("reload");  
	                    } else {
	                    	$.messager.alert("提示！","系统错误，删除失败！");
	                    }
	                });
	            }
	        });
	    }
	});

});



/**
 * 改变easyui时间格式
 */
 function operate(value, row, index) {  
	 var date = new Date(value);
		var year = date.getYear() + 1900;
		var month = date.getMonth() + 1;
		if (month < 10) {
			month = "0" + month;
		}
		var day = date.getDate();
		var hour = date.getHours();
		if (hour < 10) {
			hour = "0" + hour;
		}
		var min = date.getMinutes();
		if (min < 10) {
			min = "0" + min;
		}
		var sec = date.getSeconds();
		if (sec < 10) {
			sec = "0" + sec;
		}
		return year + "-" + month + "-" + day + " " + hour
				+ ":" + min + ":" + sec;
	}

	//添加每行后面的操作按钮
		function caozuo(row,value,index){
			var str = '';
			str += '<a onclick="edit('+index+')">编辑</a>';	
			return str;
		
	}
	
	
		/**
		 * 表格内容的编辑功能
		 */
		function edit(index){
			var grid = $('#dg');
			grid.datagrid('selectRow',index);
			var row = grid.datagrid('getSelected');
			if(row){
				var dialog = $("<div/>").dialog({
					title: "编辑销售机会",
					width: 810,
					height: 780,
					modal:true,
					href:'opp/oppaddform/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
							var id = $("input[name='id']").val();
							var updateTime=$("input[name='updateTime']").val();
							var expectedTime=$("input[name='expectedTime']").val();
							var topic=$("input[name='topic']").val();
							var customerid=$("input[name='customer.id']").val();
							var stage=$("input[name='stage']").val();
							var priority=$("input[name='priority']").val();
							var possibility=$("input[name='possibility']").val();
							var expectedMoney=$("input[name='expectedMoney']").val();
								//校验表单数据
							$.ajax({
							url:'opp/saveedit',
							type:"GET",
							data:{id:id,
								updateTime:updateTime,expectedTime:expectedTime,
								topic:topic,customerid:customerid,
								stage:stage,
								priority:priority,possibility:possibility,
								expectedMoney:expectedMoney
							},
								success : function(data) {
									$.messager.alert("消息",data.message,"info");
									// 关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
									
								},
								error : function() {
									$.messager.alert("消息","失败！","error");
								}
							})
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								//关闭当前窗口
								dialog.dialog("close");
							}
						}
					]
				});
			}
		}
/**
 * 表格内容视图
 */
function showdetails(){
	var grid = $('#dg');
	var row = grid.datagrid("getSelected");
	//alert(row.id);
	var dialog = $("<div/>").dialog({
		title:"详细信息",
		width:660,
		height:600,
		modal:true,
		href:'opp/oppdetailsform/'+row.id,
		onClose:function(){
			//关闭窗户后，立即销毁窗口
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function(){
					//var form = $("#solutiondetaisform");
					//关闭当前窗口
					dialog.dialog("close");
				}
			},
		]
	});
	
}


</script>


</body>
</html>
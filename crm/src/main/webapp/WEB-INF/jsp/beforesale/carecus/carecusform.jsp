<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<form id="carecusForm" method="post" style="margin:0;padding:10px 30px;">

	<input type="hidden" value="${c.id}" name="id">

	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="topic" value="${c.topic}" required="true" label="关怀主题：" style="width: 350px">
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="content" required="required" data-options="multiline:true" label="关怀内容：" value="${c.content}" style="width:350px;height:60px">
	</div>
	
	<c:choose>
		<c:when test="${c == null }">
			
			<!-- 新增客户关怀时客户不能手写，只能从已有的客户列表里选择 -->
			<div style="margin-bottom: 10px;">
				<input class="easyui-textbox"   required="required" label="对应联系人："  id="con" editable="false" style="width: 350px;">
				
				<div style="margin-top: 10px;margin-left: 85px">
        			<input class="easyui-searchbox" id="con2" name="cusid"  data-options="prompt:'根据id和姓名搜索联系人',searcher:doSearch" style="width:200px;">
				</div>
			
				<div style="float: right;margin-right: 26px;margin-bottom: 20px;padding-left: 50px">
						<table id="searchcus"></table>
				</div>
			</div>
		</c:when>
		
		<c:otherwise>
			<!-- 编辑，客户不能修改 -->
			<div style="margin-bottom: 10px">
				<input class="easyui-textbox" name="customer" required="true" value="${c.customer.name}" editable="false" label="对应客户：" style="width: 350px">
			</div>
			<div style="margin-bottom: 10px">
				<input class="easyui-textbox" required="true" value="${c.customer.contacts}" editable="false" label="对应联系人：" style="width: 350px">
			</div>
			
		</c:otherwise>
	</c:choose>
	
	<div style="margin-bottom: 10px">
		<input id="testDate" class="easyui-datebox" name="date" required="required"  editable="false" value="${c.date}" label="日期：" style="width:200px;">
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="feedback"  data-options="multiline:true" label="客户反馈：" value="${c.feedback}" style="width:350px;height:60px">
	</div>
</form>

<script type="text/javascript">
$(function(){
	
	$('#testDate').datebox().datebox('calendar').calendar({
		validator: function(date){
			var now = new Date();
			var d = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			return date<=d;
		}
	});
	
	
});

var grid = $("#searchcus");

function doSearch(value){
	if (value == "") {
		alert("请输入id和姓名搜索相关的联系人信息，再选择添加联系人！");
	}else{
		grid.datagrid({  
	         title:'搜索结果：（只显示前10条结果）',    
	         width:265,              
	         striped:true,  //条纹
	         collapsible:true,	//可折叠的
	         singleSelect:true,
	         url:'carecus/search/'+value,  
	         loadMsg:'数据加载中......',  
	         fitColumns:true,// 允许表格自动缩放, 以适应父容器  
	         sortName:'id',  
	         sortOrder:'desc',  
	         remoteSort:false,  
	         columns : [ [ 
	        	 {field : 'id',width : 100,title : 'id'}, 
	        	 {field : 'contacts',width : 100,title : '联系人'},
	        	 {field : 'add',width : 50,title : '选择',formatter : function (value, row, index)  {
	        		 return '<a href="javascript:void(0);"onclick="change('+index+')"> 添加 </a>'; 
				 }}
	        	 ] ], 
	     });
	}
	 
 	
};

function change(index) {
	//实时获取所点击那一行的信息
	$('#searchcus').datagrid('selectRow',index);
	var row = $('#searchcus').datagrid('getSelected');  
    if (row){  
		//将搜索框的内容与选中的值相对应
		$("#con").textbox('setValue', row.contacts);
		//将id值赋给隐藏的input
		$("#con2").val(row.id);
    }  
	
}

</script>
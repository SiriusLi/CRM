<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<style>
.uploadBtn{
	border: 1px solid #DDDDDD;
	position: relative;
	height: 22px;
	line-height: 22px;
}
.uploadBtn:hover{
	background-color:#E6E6E6;
}
.uploadBtn .file{
	position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    opacity: 0;
}
.upload .file-list ul.list{
	list-style: none;
	margin: 20px 0;
    padding: 0;
}
.upload .file-list ul.list li{
	padding: 5px 0;
	position: relative;
	margin: 3px 0;
}
.upload .file-list ul.list li .process{
	position: absolute;
	left: 0;
	top :0;
	height: 100%;
	width: 0;
	background-color: #C7EDCC;
	z-index: 0;
	border-radius: 3px;
}
.upload .file-list ul.list li .info{
	position: relative;
	width:260px;
}
</style>
<form id="demandForm" method="post" style="margin:0;padding:10px 30px;" enctype="multipart/form-data">

	<input type="hidden" value="${d.id }" name="id">
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="topic" value="${d.topic }" required="true" label="需求主题：" style="width: 350px">
	</div>
	

	<c:if test="${d != null }">
		<!-- 编辑，客户不能修改 -->
		<div style="margin-bottom: 10px">
			<input class="easyui-textbox" required="true" value="${d.salesOpp.customer.name }" editable="false" label="客户：" style="width: 350px">
		</div>
	</c:if>

	
	<c:choose>
		<c:when test="${d != null }">
			<!-- 编辑时只显示销售机会，不可修改 -->
			<div style="margin-bottom: 10px;">
				<input class="easyui-textbox" label="对应机会：" editable="false" value="${d.salesOpp.topic }" style="width: 350px">
			</div>
			<input type="hidden" name="saleid" value="${d.salesOpp.id }">
			
		</c:when>
		<c:otherwise>
			<!-- 新增时，就需要联动显示机会列表了 -->
			<div style="margin-bottom: 10px;margin-top: 20px">	
				<select class="easyui-combobox" id="salesOpp" required="required" label="对应机会：" panelHeight="auto" style="width:350px;">
			                
				</select>
				
				<input id="saleid" type="hidden" name="saleid">
				
			</div>
		
		</c:otherwise>
	</c:choose>
	
	<!-- 新增时，创建时间默认为当前时间，编辑时才显示 -->
	<c:if test="${d != null }">
		<div style="margin-bottom: 10px">
			<input id="testDate" class="easyui-datebox" name="recordTime" value="${d.recordTime }" label="记录时间：" style="width:200px;">
			<script type="text/javascript">
			/* 日历选择框只能选择当天之前的日期 */
			$('#testDate').datebox().datebox('calendar').calendar({
				validator: function(date){
					var now = new Date();
					var d = new Date(now.getFullYear(), now.getMonth(), now.getDate());
					return date<=d;
				}
			});

			</script>
		</div>
	</c:if>
	
	<div style="margin-bottom: 10px">
		<select class="easyui-combobox" editable="false" name="importance" label="重要程度：" panelHeight="auto"  style="width:200px;">
	                <option value="">未选</option>
	                <option value="非常重要" <c:if test="${d.importance == '非常重要' }">selected</c:if> >非常重要</option>
	                <option value="重要" <c:if test="${d.importance == '重要' }">selected</c:if> >重要</option>
	                <option value="一般" <c:if test="${d.importance == '一般重要' }">selected</c:if> >一般</option>
	                <option value="不重要" <c:if test="${d.importance == '不重要' }">selected</c:if> >不重要</option>
		</select>
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="content" data-options="multiline:true" required="true" label="需求内容：" value="${d.content }" style="width:350px;height:100px">
	</div>
	
	<div style="margin-bottom: 20px">
		<input class="easyui-textbox" name="note" data-options="multiline:true" label="备注：" value="${d.note }" style="width:350px;height:60px">
	</div>
	
	<c:choose>
		
		<c:when test="${d != null }">
			<!-- 只有编辑时才能上传附件 -->
			<c:if test="${not empty d.attachment }">
				<label>相关附件：</label>
				<ul style="margin-top: -16px;margin-left: 47px;">
					<c:forEach items="${d.attachment }" var="item">
						<li id="${item.id }" style="margin-bottom: 10px;">
						<a href="file/download/${item.id }">${item.fileName }</a>
<%-- 						<a class="easyui-linkbutton" href="file/delete/${d.id }/${item.id}" style="float: right;margin-right: 25px">删除</a>
 --%>						</li>
					</c:forEach>
				</ul>
			</c:if>
			<label>附件上传：</label>
			<div class="upload" style="width: 15%;margin-left: 23%;margin-top: -16px;">
				<div class="btn uploadBtn" id="uploader">
					&nbsp;选择文件
					<input class="file" multiple accept="image/*,application/x-zip-compressed,text/*,application/vnd.ms-excel,application/vnd.ms-works,application/vnd.ms-powerpoint,application/pdf,application/msword"  type="file" style="margin-left: 12%"/>
				</div>
				<div class="file-list">
					<ul class="list">
						
					</ul>
				</div>
				<button class="easyui-linkbutton" type="button" id="uploaderBtn" style="margin-top: -10px">上传</button>
			</div>
		</c:when>
		<c:otherwise>
			<div>
				<label style="color: gray;">Tips：必须先保存数据后才能上传附件。</label>
			</div>
		</c:otherwise>
	</c:choose>
		
	
</form>


<script type="text/javascript">
	
	$(function(){ 		
		$("#salesOpp").combobox({          
			url:'opp/getOppByEmp',
			valueField:'id',
			textField:'topic', 
			});
	}); 	
 
	    //对应机会下拉框选择后，获取机会id
	    $('#salesOpp').combobox({
			onSelect : function(record) {
				$("#saleid").val(record.id);
			}
		});
	    
	    
	    /* 文件上传 */
		var uploader = $("#uploader");
		var fileList = $(".file-list .list");
		var uploaderBtn = $("#uploaderBtn");
		var demandForm = $("#demandForm");
		
		var files = [];
		
		//为uploader元素下面的input.file元素绑定change事件
		uploader.on("change","input.file",function(){
			console.log(this.files);
			for(var i = 0;i < this.files.length;i++){
				var file = this.files.item(i);
				//将file添加到files后面，用push的话，是在尾部添加，并且不会改变原数列的顺序
				files.push(file);
				//生成所选择的文件的列表，process主要是用来做一个动画效果，当文件上传成功时，列表背景变成绿色
				var li = $("<li/>");
				li.append('<div class="process"/>');
				li.append('<div class="info">'+file.name+'</div>');
				fileList.append(li);
			}
		});
		
		//点击上传按钮所触发的事件
		uploaderBtn.on("click",function(){
			if (files.length) {
				//遍历每一个需要上传的文件
				$.each(files,function(index,file){
					var formData = new FormData();
					//将所需要上传的文件添加到表单数据中，方便file/upload处理
					formData.append("upload_file",file);
					$.ajax({
						url:"file/upload",
						type:"post",
						data:formData,
						contentType:false,
						//序列化data
						processData:false,
						success:function(data){
							if (data.success) {
								//上传成功后，创建对应文件的隐藏id，保存文件与demand的关联时要用到
								demandForm.append('<input type="hidden" name="atts" value="'+data.att.id+'">');
							}else{
								alert(data.msg);
							}
						},
						//上传成功后添加背景色
						xhr:function(){
							var xhr = $.ajaxSettings.xhr();
							xhr.upload.onprogress = function(e){
								var loaded = (e.loaded/e.total)*300;
								$("li .process",fileList).eq(index).css("width",loaded+"%");
							}
							return xhr;
						}
					});
				});
			}
		});

</script>
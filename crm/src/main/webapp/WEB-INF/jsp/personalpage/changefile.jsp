<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改资料</title>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
	var changeFlag = false;//标识文本框值是否改变，为true，标识已变

	$().ready(function() {

		//文本框值改变即触发
		$("input[type='text']").change(function() {
			changeFlag = true;
		});
	});

	/**
	 * 如果文本框值改变的话，则提示保存
	 */
	//离开页面时保存文档
	window.onbeforeunload = function() {
		if (changeFlag == true) {//如果changeFlag的值为true则提示
			if (confirm("页面值已经修改，是否保存？")) {
				alert("好吧");
			} else {
				alert("不好吧");
			}
		}

	}
</script>
</head>
<body>
	<form id="changeFile" method="post">
		<table cellpadding="5">
			<tr>

				<td><input class="easyui-textbox" value="id" type="hidden"
					name="id"></input></td>
			</tr>
			<tr>
				<td>姓名:</td>
				<td><input class="easyui-textbox" type="text" name="name"></input></td>
			</tr>
			<tr>
				<td>电子邮箱:</td>
				<td><input class="easyui-textbox" type="text" name="email"
					data-options="required:true,validType:'email'"></input></td>
			</tr>
			<tr>
				<td>手机:</td>
				<td><input class="easyui-textbox" type="text" name="subject"
					data-options="required:true"></input></td>
			</tr>

		</table>
		<input type="submit" value="提交" style="margin-left: 80px">
	</form>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>修改密码</title>
<style type="text/css">
#modifyPwdForm input{
margin-bottom: 40px;
}
</style>
</head>
<body>
    <form id="modifyPwdForm" method="post">   
    <input name="ids" type="hidden" />   
    <table class="tableForm">
    <tr>   
    <th>旧密码</th>   
    <td> <input name="oldpwd" type="password" class="easyui-validatebox" required="true" missingMessage="请填写旧密码" />  
    </td>   
    </tr>      
    <tr>   
    <th>新密码</th>   
    <td> <input name="cpwd" type="password" class="easyui-validatebox" required="true" missingMessage="请填写新密码" />  
    </td>   
    </tr>   
    <tr>  
     <th>重复</th>   
     <td>  
     <input name="recpwd" type="password" class="easyui-validatebox" required="true" missingMessage="请再次填写新密码" validType="eqPassword['#modifyPwdForm input[name=cpwd]']" />  
     </td>   
     </tr> 
      
     </table> 
     <input type="submit" value="提交" style="text-align: center;margin-left:40px;">   
     </form>   
	<script>
$.extend($.fn.validatebox.defaults.rules, {   
	eqPassword : { validator : function(value, param) {  
	return value == $(param[0]).val();   
	},   
	message : '密码不一致！'   
	}   
	});
</script>    
</body>
</html>
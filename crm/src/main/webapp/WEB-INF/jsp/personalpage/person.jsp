<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>个人中心主页</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script>
	$(function(){
		//实例化菜单树
		$("#navTree").tree({
			lines:true,
			onSelect:function(node){
				if(node.href){
					if($('#tabs').tabs("exists",node.text)){
						$('#tabs').tabs("select",node.text)
					}else{
						$('#tabs').tabs('add',{
			                title: node.text,
			                href: node.href,
			                closable: true
			            });
					}
				}
				
			}
		})
	});
	</script>
<style type="text/css">
*{
margin:0;

}
</style>
</head>
<body class="easyui-layout">
<div data-options="region:'north'" style="height:80px">
个人中心资料
</div>
<div data-options="region:'west'" style="width:200px;" title="导航">
<ul id="navTree" class="easyui-tree">
<li data-options="href:'personal/show'">个人资料</li>

<li  data-options="state:'closed'">
<span>安全设置</span>
<ul>
	<li  data-options="href:'personal/changepwd'">修改密码</li>
	<li  data-options="href:'personal/changefile'">修改基本资料</li>
	<li  data-options="href:'personal/upload'">上传头像</li>
</ul>
</li>
</ul>
</div>
<div data-options="region:'center',border:false" >
	<div id="tabs" class="easyui-tabs" fit="true">
	    <div title="桌面" style="padding:20px;">
	        权限案例的桌面
	       
	    </div>
	    </div>
	</div>
</body>
</html>
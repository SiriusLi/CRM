<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div
	style="height: 165px; width: 100%; box-shadow: 5px 5px 5px #888888; margin: 0 auto;">
	<div>
		<h5>销售简报</h5>
		<div style="padding-left: 80%; margin-top: -30px">
			<select class="kuang" onchange="ch()" editable="false" panelHeight="auto"
				style="width: 200px;">
				<option value="1">本日</option>
				<option value="2">本周</option>
				<option value="3">本月</option>
			</select>
		</div>
	</div>
	<div style="height: 145px;">
		<table style="width: 100%; height: 100%" cellspacing="0"
			class="briefing">
			<tr>
				<td>| 成交</td>
				<td></td>
				<td></td>
				<td>| 新建</td>
				<td></td>
				<td></td>
			</tr>
			<!-- border-right -->
			<tr class="show bdr">
			
				<td class="total">${total}</td>
				<td class="money">￥ ${money }</td>
				<td class="rmoney">￥${rmoney }</td>
				<td class="clue">${clue }</td>
				<td class="client">${client }</td>
				<td style="border-right: 0" class="oop">${oop }</td>
			</tr>
			<!-- border-bottom -->
			<tr class="show bdr bdb">
				<td>订单数</td>
				<td>订单总金额</td>
				<td>已汇款金额</td>
				<td>线索数</td>
				<td>客户数</td>
				<td style="border-right: 0">机会数</td>
			</tr>
			<tr>
				<td>| 跟进</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr class="show bdr">
				<td class="action">${action }</td>
				<td class="care">${care }</td>

			</tr>
			<tr class="show bdr">
				<td>跟进次数</td>
				<td>关怀次数</td>

			</tr>

		</table>
	</div>
</div>
<div style="margin: 0 auto; height: 400px; margin-top: 3%">
	<div class="drag-item"
		style="height: 100%; width: 29%; box-shadow: 5px 5px 5px #888888; float: left; margin-left: 3%">
		<h5>消息</h5>
		<table cellspacing="0" class="message"
			style="width: 100%; line-height: 30px;" id="message-dg">
			<c:forEach items="${messages }" var="m">
			<tr>
				<td width="35px;" rowspan="2"><img src="images/nh-sy-bg3.jpg" width="30px"></td>
				<td>业务消息：${m.topic }</td>
				<fmt:formatDate value="${m.time}" pattern="yyyy年MM月dd日     HH时mm分"  var="tt"/>
				<td>${tt }</td>
			</tr>
			<tr  style="border:1px solid black;">
				<td colspan="2">${ m.content}</td>
			</tr>
			</c:forEach>
		</table> 
		<!-- <table url="message/list" class="easyui-datagrid"
			style="width: 100%;">
			<thead>
				<tr>
					<td field="topic" width="80">业务消息：</td>
					<td field="time" width="80" sortable="true">时间</td>
					<td field="content" width="80" sortable="true">内容</td>
				</tr>
			</thead>
		</table> -->
	</div>
	<div class="drag-item"
		style="height: 100%; width: 29%; box-shadow: 5px 5px 5px #888888; float: left; margin-left: 3%">
		<h5>待办任务</h5>
		<table cellspacing="0" class="action" style="width: 100%">
		<c:forEach items="${tasks }" var="tasks">
			<tr class="bdb">
				<fmt:formatDate value="${tasks.createDate}" pattern="yyyy年MM月dd日     HH时mm分"  var="tc"/>
				<fmt:formatDate value="${tasks.completeDate}" pattern="yyyy年MM月dd日     HH时mm分"  var="tcd"/>
				<td>任务内容:${tasks.topic }</br> 执行者:${tasks.operator.empName } </br> 
				</br>发布日期:${tc}</br>结束日期:${tcd }
				</td>
				
			</tr>
			</c:forEach>
		</table>
	</div>
	<div class="drag-item"
		style="height: 100%; width: 29%; box-shadow: 5px 5px 5px #888888; float: left; margin-left: 3%">
		<h5>提醒</h5>
		<table cellspacing="0" class="remind" style="width: 100%">
		<tr>
				<td>提醒内容</td>
				<td>创建时间</td>
				<td>提示类型</td>
				<td>查看状态</td>
			</tr>
		<c:forEach items="${reminds }" var="r">
			<tr>
				<td>${r.content }</td>
				<fmt:formatDate value="${r.date}" pattern="yyyy年MM月dd日     HH时mm分"  var="rr"/>
				<td>${rr }</td>
				<td>${r.type }</td>
				<td>未查看</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</div>
<style type="text/css">
.drag-item {
	list-style-type: none;
	display: block;
	padding: 5px;
	border: 1px solid #ccc;
	margin: 2px;
	width: 300px;
	background: #fafafa;
	color: #444;
}

.indicator {
	position: absolute;
	font-size: 9px;
	width: 10px;
	height: 10px;
	display: none;
	color: red;
}
</style>
<script>
//改变下拉框重新加载数据
	function ch(){
	var a = $(".kuang").val();
	//alert(a);
	$.ajax({
		url:'crm/main',
		type:"POST",
		data:{
			style:a
			},
		dataType:"json",
		success:function(data){
			console.log(data);
			$(".total").text(data[0]);
			$(".money").text(data[1]);
			$(".rmoney").text(data[2]);
			$(".clue").text(data[3]);
			$(".client").text(data[4]);
			$(".oop").text(data[5]);
			$(".action").text(data[6]);
			$(".care").text(data[7]);
		}
	});
};

	/* $(function() {
		
		cardview= $.extend({}, $.fn.datagrid.defaults.view, {
				renderRow: function(target, fields, frozen, rowIndex, rowData){
					var cc = [];
					cc.push('<td colspan=' + fields.length + ' style="padding:10px 5px;border:0;">');
					if (!frozen){
						cc.push('<img src="images/nh-sy-bg3.jpg" style="width:30px;float:left">');
						cc.push('<div style="float:left;margin-left:20px;">');
						for(var i=0; i<fields.length; i++){
							var copts = $(target).datagrid('getColumnOption', fields[i]);
							cc.push('<p><span class="c-label">' + copts.title + ':</span> ' + rowData[fields[i]] + '</p>');
						}
						cc.push('</div>');
					}
					cc.push('</td>');
					return cc.join('');
				}
			});
		$('#tt').datagrid({
			view: cardview,
		});
		
		
		
		var indicator = $('<div class="indicator">>></div>').appendTo('body');
		$('.drag-item').draggable({
			revert : true,
			deltaX : 0,
			deltaY : 0
		}).droppable({
			//当可拖动元素被拖过时触发
			onDragOver : function(e, source) {
				indicator.css({
					display : 'block',
					left : $(this).offset().left - 10,
					top : $(this).offset().top + $(this).outerHeight() - 5
				});
			},
			//当可拖动元素被拖离开时触发
			onDragLeave : function(e, source) {
				indicator.hide();
			},
			//当可拖动元素被放下时触发
			onDrop : function(e, source) {
				$(source).insertAfter(this);
				indicator.hide();
			}
		}); */
	});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c" %>
<form id="cusleadsForm" method="post" style="margin: 0;padding: 10px 30px;">
<!-- 	此处的form，如果是处理的话除了生命周期和分配给相关负责人，其他的不能改
 -->	
	<input type="hidden" value="${c.id}" name="id">
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="name" value="${c.name}" required="true" label="公司：" style="width: 350px" <c:if test="${c != null}">editable="false"</c:if> >		
	</div>
	
	<!-- 新建才显示，处理的话就不显示了 -->
	<div style="margin-bottom: 10px">
	<c:choose>
	<c:when test="${c == null }">
		<select class="easyui-combobox" editable="false" name="source" value="${c.source }"  label="客户来源：" panelHeight="auto"  style="width:200px;">
		                <option value="">未选</option>
		                <option value="电话来访">电话来访</option>
		                <option value="客户介绍">客户介绍</option>
		                <option value="独立开发">独立开发</option>
		                <option value="媒体宣传">媒体宣传</option>
		                <option value="促销活动">促销活动</option>
		                <option value="老客户">老客户</option>
		                <option value="代理商">代理商</option>
		                <option value="合作伙伴">合作伙伴</option>
		                <option value="公开招标">公开招标</option>
		                <option value="互联网">互联网</option>
		                <option value="其他">其他</option>
			</select>

	</c:when>
	<c:otherwise>
		<input class="easyui-textbox" name="source" editable="false" value="${c.source }"  label="客户来源：" style="width: 200px" >		
	</c:otherwise>
	</c:choose>
	</div>
	
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="contacts" required="true" value="${c.contacts }"  label="联系人：" style="width: 350px" <c:if test="${c != null}">editable="false"</c:if> >		
	</div>	
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="tel"  value="${c.tel }"  label="电话：" style="width: 350px" >		
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="email" value="${c.email }"  label="邮箱：" style="width: 350px" validType="email">		
	</div>
	
	<!-- 新建时才可以选择什么性别，修改时就只能查看 -->
	<div style="margin-bottom: 10px">
	<c:choose>
	<c:when test="${c == null }">
		<select class="easyui-combobox" editable="false" name="sex" required="required"  label="性别：" panelHeight="auto"  style="width:200px;">
	                <option value="男">男</option>
	                <option value="女">女</option>
		</select>

	</c:when>
	<c:otherwise>
		<input class="easyui-textbox" name="sex" editable="false" value="${c.sex }"  label="性别：" style="width: 200px" >		
	</c:otherwise>
	</c:choose>
	</div>
	<div style="margin-bottom: 10px">
		<input id="birDate" class="easyui-datebox" name="birthday" editable="false" value="${c.birthday }" label="生日：" style="width:200px;">	
	</div>
	
	<div style="margin-bottom: 10px">
		<input class="easyui-textbox" name="note" value="${c.note }" data-options="multiline:true" label="备注：" style="width:350px;height:60px">
	</div>
	
	<!-- 销售人员处理的时候才能看到（新建的时候看不到，需要使用双重判断） -->
	<c:if test="${c != null && false}">
	<div>
		<select class="easyui-combobox" editable="false" name="lifecycle" required="required"  label="生命周期：" panelHeight="auto"  style="width:200px;">
			<option value="">未选</option>
			<option value="潜在客户" <c:if test="${c.lifecycle == '潜在客户'}">selected</c:if>>潜在客户
			<option value="失效客户" <c:if test="${c.lifecycle == '失效'}">selected</c:if>>失效</option>
		</select>

	</div>
	</c:if>
	
	<!-- 如果是管理人员在处理，就显示此处，将这条信息分给相应的销售人员 -->
	<c:if test="${c != null }">
		<div>
				<input class="easyui-textbox"  required="required" label="分配员工："  id="emp" value="${c.employee.empName }" editable="false" style="width: 350px;">
			
				<div style="margin-top: 10px;margin-left: 85px">
        			<input class="easyui-searchbox" id="emp2" name="empid" value="${c.employee.id }"   data-options="prompt:'根据姓名或id搜索员工',searcher:doSearch" style="width:200px;">
				</div>
		
				<div style="float: right;margin-right: 26px;margin-bottom: 20px">
					<table id="searchemp"></table>
				</div>
				
		</div>
	</c:if>
</form>

<script type="text/javascript">

	var grid = $("#searchemp");
	
    function doSearch(value){
    	if (value == "") {
    		alert("请输入id和姓名搜索相关的员工信息，再选择添加员工！");
    	}else{
    	 grid.datagrid({  
             title:'搜索结果：（只显示前10条结果）',    
             width:265,              
             striped:true,  //条纹
             collapsible:true,	//可折叠的
             singleSelect:true,
             url:'cusleads/search/'+value,  
             loadMsg:'数据加载中......',  
             fitColumns:true,// 允许表格自动缩放, 以适应父容器  
             sortName:'id',  
             sortOrder:'desc',  
             remoteSort:false,  
             columns : [ [ 
            	 {field : 'id',width : 100,title : 'id'}, 
            	 {field : 'empName',width : 100,title : '用户名'},
            	 {field : 'add',width : 50,title : '选择',formatter : function (value, row, index)  {
            		 return '<a href="javascript:void(0);"onclick="change('+index+')"> 添加 </a>'; 
    			 }}
            	 ] ], 
         });
    	}
    };
		
    function change(index) {
    	//实时获取所点击那一行的信息
    	$('#searchemp').datagrid('selectRow',index);
    	var row = $('#searchemp').datagrid('getSelected');  
        if (row){  
			//将搜索框的内容与选中的值相对应
 			$("#emp").textbox('setValue', row.empName);
			$("#emp2").val(row.id);
        }  
		
	}
    	

</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>客户线索</title>
</head>
<body>
	<table id="cusleadsGird"></table>
	
	<div id="cusleadsToolbar">
	    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
	    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">处理</a>
	    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
	</div>

	<script src="js/system/cusleads/cusleads.js"></script>	

</body>
</html>
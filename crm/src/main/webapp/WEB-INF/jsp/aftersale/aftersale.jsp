<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!--维修工单页面 -->
<base href="${ctxPath }">
<script src="js/datagrid-detailview.js"></script>
<script src="js/datagrid-filter.js"></script>

	<table id="repairwx" class="easyui-datagrid"
		
		toolbar="#aftersale-toolbar" style="min-width: 500px;" url="aftersale/data" rownumbers="true"
		fitColumns="true" singleSelect="true" fit="true" striped="true" remoteFilter="true"
		data-options="method:'get'" pagination="true">
		<thead>

			<tr>
			
				<th data-options="field:'number',align:'center',width:100">维修编号</th>
				<th data-options="field:'customer',align:'center',width:100,formatter:function(row,value,index){return new Object(value['customer']).name;}">对应客户</th>
				<th data-options="field:'content',align:'center',width:100,formatter:function(row,value,index){return new Object(value['product']).name;}">维修产品</th>
				<th data-options="field:'date',align:'center',width:170,formatter:function(value,row,index){return dateFormatter(value);}" >接单日期</th>
				<th data-options="field:'delivery',align:'center',width:170,formatter:function(value,row,index){return dateFormatter(value);}"">截止日期</th>
				<th data-options="field:'tel',align:'center',width:100">联系方式</th>
				<th data-options="field:'money',align:'center',width:100">费用</th>
				<th data-options="field:'progress',align:'center',width:100">维修进度</th>
 				<th data-options="field:'state',align:'center',width:100,formatter:function(value){return	value==true? '已结束':'未结束'}">工单状态</th>
				<th data-options="field:'employee',align:'center',width:100,formatter:function(row,value,index){return new Object(value['employee']).empName;}">处理人</th>
				
				<th data-options="field:'cz',align:'center',width:140,formatter:caozuo">操作</th>
				
			</tr>
		
		</thead>

	</table>
	<!-- 工具栏 -->
	<div id="aftersale-toolbar" style="padding: 5px; height: 30px">
		<!-- 新增 -->
		<div class="zj" style="margin-bottom: 5px; float: left;">
			<a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
		</div>
		<!-- 刷新 -->
		<div class="sx" style="margin-bottom: 5px; float: left;">
			<a class="easyui-linkbutton add" iconCls="icon-reload" plain="true">刷新</a>
		</div>
		<!-- 高级查询 -->
		<div class="gjcx" style="margin-bottom: 5px; float: left;">
			<a class="easyui-linkbutton add" iconCls="icon-search" plain="true">高级查询</a>
		</div >
		<!-- 数据导出 -->
		<div  class="sjdc" style="margin-bottom: 5px; float: left;" >
			<a class="easyui-linkbutton add"  plain="true">数据导出</a>
		</div>
		<!-- 简单查询 -->
		<div style="float: right">
			<input class="easyui-searchbox"
				data-options="prompt:'请输入工单编号',menu:'#mm',searcher:doSearch"
				style="width: 300px"></input>
			<div id="mm">
				<div data-options="name:'all',iconCls:'icon-ok'">全部数据</div>
				<div data-options="name:'待接单'">待接单</div>
				<div data-options="name:'维修中'">维修中</div>
				<div data-options="name:'维修完'">维修完</div>
				<div data-options="name:'回访中'">回访中</div>
				<div data-options="name:'已完成'">已完成</div>
			</div>
		</div>
	</div>
<script>
function caozuo(row,value,index){
	var str = '';
	str += '<a onclick="edit()">编辑</a>  ';	
	return str;
}
//转换日期格式  
function dateFormatter (value) {
    var date = new Date(value);
    var year = date.getFullYear().toString();
    var month = (date.getMonth() + 1);
    var day = date.getDate().toString();
    var hour = date.getHours().toString();
    var minutes = date.getMinutes().toString();
    var seconds = date.getSeconds().toString();
    if (month < 10) {
        month = "0" + month;
    }
    if (day < 10) {
        day = "0" + day;
    }
    if (hour < 10) {
        hour = "0" + hour;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
}

	/*
	*
	**按数据查询
	*/	
	function doSearch(value,name){
			//alert(value);
			$("#repairwx").datagrid({
				title : '维修工单',
				width : "100%",
				url:'aftersale/search/'+name+'/'+value,
				view : detailview,
				detailFormatter : function(rowIndex, rowData) {
					//返回工单明细表格
					return '<table class="detail"></table>';
				},
				onExpandRow : function(index, row) {
					//找到该父类Formatter的子类表格
					var detail = $(this).datagrid('getRowDetail',index).find('table.detail');
					//初始化表格
					detail.datagrid({
						url:'aftersale/detail/'+row.id,
						fitColumns:true,
		                singleSelect:true,
		                rownumbers:true,
		                loadMsg:'加载中，请稍后...',
		                height:'auto',
		                columns:[[
		                    {field:'progress',title:'维修进度',width:100,align:'center',sortable:true}, 
		                    {field:'time',title:'开始时间',width:100,align:'center',sortable:true,formatter:function(value,row,index){return dateFormatter(value);}},
		                    {field:'content',title:'备注',width:160,align:'center',sortable:true}, 
		                    {field:'employee',title:'操作员工',width:100,align:'center',sortable:true,formatter:function(row,value,index){return new Object(value['employee']).empName;}}, 


		                ]],
					}) 
					$('#repairwx').datagrid('fixDetailRowHeight', index);
				}
			});
			//开启过滤
			//$("#wx").datagrid('enableFilter',[{}]);
		}
	function show(){
		var row = $('#repairwx').datagrid('getSelected');
		if(row){
			window.open("aftersale/show/"+row.id);
		}
	}

	//编辑表单
	function edit(){
		var grid = $('#repairwx');
		var row = grid.datagrid('getSelected');
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑维修工单",
				top:60,
				width: 610,
				modal:true,
				href:'aftersale/edit/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							alert(row.id);
							var form = $("#editForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("aftersale/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										//重载表单
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}
</script>
<script src="js/system/aftersale/aftersale.js"></script>
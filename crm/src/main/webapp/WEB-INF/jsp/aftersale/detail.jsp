<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单详情</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="css/aftersale/detail.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
</head>
<body>
<h2>维修详情</h2>
<div class="easyui-tabs tab" style="width:700px;height:250px">
		<div title="单号基本信息" id="repair_basic" style="padding:10px">
			<ul>
				<li>工单流水号:</li>
				<li>接单人:</li>
				<li>接件日期时间:</li>
				<li>对应客户:</li>
				<li>联系人:</li>
				<li>联系人电话:</li>
				<li>接件员工</li>
				<li>维修产品:</li>
				<li>维修费用:</li>
			</ul>
		</div>
		<div title="工单详情" id="repair_detail" style="padding:10px">
			<ul>
				<li>维修进度:</li>
				<li>执行员工:</li>
				<li>执行内容:</li>
			</ul>
		</div>
		
	</div>
</body>
</html>
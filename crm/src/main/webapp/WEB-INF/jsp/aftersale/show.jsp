<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>详情展示</title>
<link rel="stylesheet" type="text/css" href="css/aftersale/show.css">
</head>
<body>
<div class="main">
<h3>维修详情展示</h3>
<!-- 单号与接件 -->
<div class="repair">
<div><span>单号与接件</span></div>
<ul>
	<li>维修单号:${repair.number }</li>
	<li>维修主题:${repair.topic }</li>
	<li>对应客户:${repair.customer.name }</li>
	<li>客户电话:${repair.tel }</li>
	<li>接单日期:${repair.date }</li>
	<li>工单截止日期:${repair.delivery }</li>
	<li>维修产品:${repair.product.name }</li>
	<li>维修费用:${repair.money }元</li>
	<li>目前进度:${repair.progress }</li>
	<li>工单状态:<c:if test="${repair.state==false}">未结束</c:if>
				<c:if test="${repair.state==true}">已结束</c:if>
	
	</li>
	<li>目前处理员工:${repair.employee.empName }</li>
</ul>
</div>

<!-- 详情记录 -->
<h3>维修工单详情</h3>
<div class="repair_detail">
	
<c:forEach items="${details }" var="d">
<div class="progress">
<ul>
	<li>历史进度:${d.progress }</li>
	<li>处理员工:${d.employee.empName }</li>
	<li >开始处理时间:${d.time }</li>
	<li>备注:${d.content }</li>
</ul>
</div>
</c:forEach>
</div>


</div>
</body>
</html>
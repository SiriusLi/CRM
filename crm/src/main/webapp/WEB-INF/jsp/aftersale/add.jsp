<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
#repairForm div{
	display: inline-block;
	float:left;
	margin:10px 20px 10px 2px;
}



label{
text-align: center;

}

#repairForm div input{
width:240px;
}
.beizhu{
clear:both;
}

#result{
position: relative;
left:-310px;
top:-52px;
}
#result2{
position: relative;
left:-40px;
top:-8px;
}
#sel_status{
width:163px;
margin-left:35px;
height:28px;

}
</style>

</head>   
<script>
	function dos(value){
		//用户点击搜索就会触发这个方法
		$("#tt").datagrid({  
	         iconCls:'icon-ok',  
	         width:130,
	        border:false,
	         url:'aftersale/findclient?cname='+value, //触发此action,带上参数searcValue 
	         columns : [ [  {  
	                field : 'name',  
	                width : 130,  
	                title : '用户名'  
	            }] ], 
	            onLoadSuccess:function(data){  
	            	$("#result").attr("style","display:block");
	                if(data==null||data.total ==0){ 
	                	
	                $("#tt").datagrid('appendRow',  
	                        { 
	                            name:'<div class="norecord" style="text-align:left;color:red">没有相关记录！</div>',  
	                           
	                    //合并单元格  
	                        }).datagrid('mergeCells',{  
	                            index : 0,  
	                            field : 'name2'
	                        })  
	            }else{
	            	  $("#tt").datagrid('appendRow',  
		                        { 
		                          
		                        }).datagrid('mergeCells',{  
		                            index : 0,  
		                            field : 'name'
		                        }) 
	            }
	                },
	    		onClickRow:function(index, row){
	    			//alert(row["name"]);
	    			if(row["name"].indexOf('div')!=-1){
	    				$("#find_client").searchbox('setValue',"");
	    			}else{
	    				$("#find_client").searchbox('setValue',row["name"]);
	    			}
	    				
		    			$("#result").attr("style","display:none");
	    		} 
		})
		
		}
	
	
	
	function dot(value){
		$("#product").datagrid({  
	         iconCls:'icon-ok',  
	         width:130,
	        border:false,
	         url:'aftersale/findproduct?pname='+value, //触发此action,带上参数searcValue 
	         columns : [ [  {  
	                field : 'name',  
	                width : 130,  
	                title : '产品名'  
	            }] ], 
	            onLoadSuccess:function(data){ 
	            	$("#result2").attr("style","display:block");
	                if(data==null||data.total ==0){ 
	                $("#product").datagrid('appendRow',  
	                        { 
	                            name:'<div class="norecord" style="text-align:left;color:red">没有相关记录！</div>',  
	                            rownumbers : false
	                    //合并单元格  
	                        }).datagrid('mergeCells',{  
	                            index : 0,  
	                            field : 'name2'
	                        })  
	            }else{
	            	
	            	$("#product").datagrid('appendRow',  
	                        { 
	                           
	                            rownumbers : false
	                    //合并单元格  
	                        }).datagrid('mergeCells',{  
	                            index : 0,  
	                            field : 'name'
	                        }) 
	            }
	                },
	    		onClickRow:function(index, row){
	    			//alert(row["name"]);
	    			if(row["name"].indexOf('div')!=-1){
	    				$("#find_product").searchbox('setValue',"");
	    			}else{
	    				$("#find_product").searchbox('setValue',row["name"]);
	    			}
	    				
		    			$("#result2").attr("style","display:none");
	    		} 
		})
	}


</script>
<form id="repairForm" method="post"  style="margin:0;padding:10px 30px;">
<div>
	<input name="number" class="easyui-textbox" value="${num }" required="true" label="工单编号" >
</div>
<div>
	
	<input name="topic" class="easyui-textbox" required="true" label="维修主题" >
</div>

<div >
 
  <fmt:formatDate value="${createdate}" pattern="yyyy-MM-dd  HH:mm:ss" var="d"/>
<input name="date" class="easyui-textbox" label="操作时间" value="${d }" >
</div>

<div>
<fmt:formatDate value="${enddate}" pattern="yyyy-MM-dd  HH:mm:ss" var="e"/>
	<input name="delivery" class="easyui-textbox" value="${e }" label="截止日期">
</div>
<div id="name">
<input name="cname" class="easyui-searchbox" 

 id="find_client"   searcher="dos" prompt="请输入用户名"   required="true" label="客户名称" >
</div>
<div>
<input name="name" class="easyui-textbox"  required="true" label="联系人">
</div>  
<div>
<input name="tel" class="easyui-textbox"  required="true" label="客户电话">
</div>
<div id="pname">
<input name="content" id="find_product" searcher="dot" prompt="请输入产品名"  class="easyui-searchbox" required="true" label="产品名称">
</div>
<!--  <div>
<input name="ename" class="easyui-textbox"  required="true" label="接触员工">
</div>-->
<div>
维修状态<select id="sel_status" name="progress">
	<option>待接单</option>
	<option>维修中</option>
	<option>维修完</option>
	<option>回访中</option>
	<option>已完成</option>
</select>
</div>
<div>
<!-- 此处应该是当前登录员工 -->

<!-- <div class="wenjian">
<input class="easyui-filebox" name="attachment" label="选择文件" style="width:100%;height:20px">
</div> -->
<div class="repair_xq " style="margin: 0px">
<input value="repair_detail_id" type="hidden">
</div>
<div class="beizhu">
<input name="bz" style="width:350px;height:120px;margin-top: -100px" class="easyui-textbox bz" data-options="multiline:true" label="备注">
</div>
</div>



</form>
<div id="result" region="center" border="false"> 
<!-- 结果框 --> 
                <table id="tt"></table>  
            </div>
            
<div id="result2" region="center" border="false"> 
<!-- 产品结果框 --> 
                <table id="product"></table>  
            </div>

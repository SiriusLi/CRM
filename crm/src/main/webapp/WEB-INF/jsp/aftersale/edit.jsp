<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
#editForm div{
	display: inline-block;
	float:left;
	margin:10px 20px 10px 2px;
}

#sel_status{
width:240px;
margin-left:35px;
height:28px;

}

label{
text-align: center;

}

#editForm div input{
width:240px;
}
.beizhu{
clear:both;
}

.bz{


}
</style>
</head>
<form id="editForm" method="post" style="margin:0;padding:10px 30px;">
<input class="id" name="id" value="${repair.id }" type="hidden">
<div>
	<input name="number" value="${repair.number}" class="easyui-textbox" required="true" label="工单编号" >
</div>

<!-- 自动生成操作时间，无法修改 -->
<div >
<fmt:formatDate value="${editTime}" pattern="yyyy-MM-dd  HH:mm:ss" var="e"/>
<input value="${e }" name="date" class="easyui-textbox" label="编辑时间" >
</div>
<div >
<input name="delivery" value="${repair.delivery }" class="easyui-textbox" label="结束时间" >
</div>

<div id="cname">
<input name="cname" value="${repair.customer.name }" class="easyui-textbox" required="true" label="客户名称" >
</div>
<div>
<input name="tel" value="${repair.tel }" class="easyui-textbox" required="true" label="客户电话">
</div>
<div id="pname">
<input name="content" value="${repair.product.name }" class="easyui-textbox" required="true" label="产品名称">
</div>
<!-- 
<div>
<input name="ename"  value="张三" class="easyui-textbox"  required="true" label="接触员工">
</div>此处应该是当前登录员工 -->
<!-- 回访中状态就要输入金钱 -->
<c:if test="${repair.progress=='回访中' }">
<div class="money" >
<input name="money"  class="easyui-textbox"  required="true" label="维修费用">
</div>
</c:if>

<div>
维修状态<select id="sel_status" name="progress">
	<option>待接单</option>
	<option>维修中</option>
	<option>维修完</option>
	<option>回访中</option>
	<option>已完成</option>
</select>
</div>


<div class="wenjian">
<input class="easyui-filebox" name="file1" label="选择文件" style="width:100%;height:20px">
</div>

<div class="beizhu">
<input name="bz" style="width:350px;height:120px;" class="easyui-textbox bz" data-options="multiline:true" label="备注">
</div>
</form>

</html>
<script>

</script>

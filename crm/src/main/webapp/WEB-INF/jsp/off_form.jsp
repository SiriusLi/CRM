<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>官网</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script
	src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">

<style type="text/css">
* {
	margin: 0px;
	padding: 0px;
}

.cbdiv_f0990ff7 {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center;
	background-color: rgba(0, 0, 0, 0);
	color: rgb(217, 217, 217);
	min-height: 20px;
	margin-bottom: 0px;
}

.cb_oth_47144ed3 {
	-webkit-box-flex: 1;
	-ms-flex: 1 1 0%;
	flex: 1 1 0%;
	margin-right: 20px;
	margin-left: 10px;
	border-top-color: currentcolor !important;
}

.cbhead_10b15f9e {
	line-height: 1.3em;
	margin-top: 10px;
	font-family: Roboto;
	padding-left: 2px;
	padding-right: 2px;
	margin-bottom: 10px;
}

.cb_oth_c9930970 {
	-webkit-box-flex: 1;
	-ms-flex: 1 1 0%;
	flex: 1 1 0%;
	margin-left: 20px;
	margin-right: 10px;
	border-top-color: currentcolor !important;
	border-type: border-top;
}

.cbdiv_3d534c9f {
	min-height: 0px;
	text-align: center;
	color: black;
	-webkit-box-flex: 0;
	-ms-flex: 0 0 auto;
	flex: 0 0 auto;
	max-width: 60%;
}

.cbhead_003ac6d7 {
	font-family: SiYuanBold;
	margin-top: 10px;
	line-height: 1em;
	font-weight: bold;
}

.cbdiv_f8d05154 {
	width: 100%;
	height: 300px;
}

.cblink_c77d7a88 {
	position: absolute;
	bottom: 0;
	right: 0;
	top: 0;
	left: 0;
}

.aa {
	width: 400px;
	height: 300px;
	float: left;
	margin-right: 21px;
}

.bottom {
	height: 700px;
}

.bottom1 {
	width: 800px;
	height: 700px;
	float: left;
}

.bottom2 {
	background-color: #EFEFEF;
	width: 886px;
	height: 700px;
	float: left;
}

.cbhead_0d398895 {
	width: 880px;
	height: 200px;
	margin-bottom: 30px;
}

.cblist_fc529321 {
	list-style-type: none;
	padding-left: 0px;
}

.cblist_b2874864 {
	min-height: 20px;
	padding-left: 300px;
	padding-right: 5px;
	font-size: 13px;
	margin-bottom: 20px;
}

.cbhead_19f8a7b2 {
	font-family: Caudex;
	padding-left: 8px;
	padding-top: 10px;
}
</style>
</head>

<body>
	<div style="height: 128px;">
		<div style="width: 400px; height: 70px">
			<img src="images/website/logo.png">
			<div style="height: 10px"></div>
			<div style="font-size: 25px; color: #C5C5C5">
				<strong>CRM：给销售团队一颗云大脑</strong>
			</div>
		</div>

		<div
			style="margin-left: 1500px; font-size: 20px; height: 20px; line-height: 20px">
			<a href="lee/off2">联系我们</a>
		</div>
	</div>

	<div id="myCarousel" class="carousel slide">
		<!-- 轮播（Carousel）指标 -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>

		</ol>
		<!-- 轮播（Carousel）项目 -->
		<div class="carousel-inner">
			<div class="item active">
				<img src="images/website/image1.jpg" alt="First slide"
					style="height: 800px; width: 1700px">
			</div>
			<div class="item">
				<img src="images/website/image2.jpg" alt="Second slide"
					style="height: 800px; width: 1700px">
			</div>

		</div>
		<!-- 轮播（Carousel）导航 -->
		<a class="left carousel-control" href="#myCarousel" role="button"
			data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
			aria-hidden="true"></span> <span class="sr-only">Previous</span>
		</a> <a class="right carousel-control" href="#myCarousel" role="button"
			data-slide="next"> <span
			class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

	</div>

	<div class="cbdiv_f0990ff7" style="height: 200px">

		<hr class="cb_oth_47144ed3">
		<div class="cbdiv_3d534c9f">
			<h4 class="cbhead_003ac6d7">推荐产品</h4>
			<h4 class="cbhead_10b15f9e">HERO PRODUCTS</h4>
		</div>
		<hr class="cb_oth_c9930970">
	</div>

	<div class="cbdiv_f8d05154">

		<div class="aa">
			<div>
				<img src="images/website/jj1.png" style="width: 300px">
			</div>
		</div>

		<div class="aa">
			<div>
				<img src="images/website/jj2.png" style="width: 300px">
			</div>
		</div>

		<div class="aa">
			<div>
				<img src="images/website/jj3.png" style="width: 300px">
			</div>
		</div>

		<div class="aa">
			<div>
				<img src="images/website/jj4.png" style="width: 300px">
			</div>
		</div>
	</div>

	<div class="bottom">
		<div class="bottom1">
			<img src="images/website/jj5.jpg" style="width: 800px; height: 690px">
		</div>
		<div class="bottom2">
			<div class="cbhead_0d398895">
				<ul class="cblist_fc529321">
					<li class="cblist_b2874864"><h2 class="cbhead_19f8a7b2">CONTACT</h2></li>
					<li class="cblist_b2874864">地址：上海市长宁区仙霞路321号百科苑大楼</li>
					<li class="cblist_b2874864">邮箱：support@21epub.com</li>
					<li class="cblist_b2874864">电话号码： 4008000000</li>
				</ul>
			</div>

			<div class="cbhead_0d398895">
				<ul class="cblist_fc529321">
					<li class="cblist_b2874864"><h2 class="cbhead_19f8a7b2">SOCIAL
							LINKS</h2></li>
					<li class="cblist_b2874864">微信：意派科技</li>
					<li class="cblist_b2874864">新浪微博：意派科技</li>
					<li class="cblist_b2874864">QQ群：227979505</li>
				</ul>
			</div>

			<div class="cbhead_0d398895">
				<ul class="cblist_fc529321">
					<li class="cblist_b2874864"><h2 class="cbhead_19f8a7b2">ABOUT</h2></li>
					<li class="cblist_b2874864">关于我们：上海意派科技，致力于专业级数字媒体在线设计平台的研发与运营，
					</li>
					<li class="cblist_b2874864">目前已推出专业级H5在线设计工具Epub360、专业级响应式网页设计工具Coolsite360。</li>
					<li class="cblist_b2874864">电话号码： 4008000000</li>
				</ul>
			</div>




		</div>



	</div>




</body>
</html>
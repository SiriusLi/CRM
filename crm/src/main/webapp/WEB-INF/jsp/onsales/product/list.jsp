<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style type="text/css">
body{
	font-size: 14px;
}
</style>
</head>
<body>
<table id="productDatagrid"></table>
<div id="productToolbar">
	<a class="easyui-linkbutton add"    iconCls="icon-add" plain="true">新增</a>
	<a class="easyui-linkbutton edit"   iconCls="icon-edit" plain="true">编辑</a>
	<a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">下架</a>
</div>
<script type="text/javascript">
    $(function(){
    	var pd = $('#productDatagrid');
    	var pt = $('#productToolbar');
    	//表格初始化
        pd.datagrid({
            view: detailview,
            width:'100%',
            height:400,
            singleSelect:true,
            fitColumns:'true',
            fit:'true',
            pagination:true,
            url:'product/all',
            toolbar:'#productToolbar',
            idField:'id',
            columns:[[
            	{field:'id',title:'id',width:100, align: 'center'},
            	{field:'number',title:'商品编号',width:100, align: 'center'},
            	{field:'name',title:'商品名字',width:100, align: 'center'},
            	{field:'price',title:'价格',width:100, align: 'center'},
            	{field:'unit',title:'单位',width:30, align: 'center'},
            	{field:'type',title:'商品类别',width:100, align: 'center'},
            	{field:'stockSize',title:'库存量',width:30, align: 'center'},
            	{field:'station',title:'状态',width:30,align:'center',formatter:function(value,row,index){
            		return value? "上架":"下架";
            	}}
            ]],
            detailFormatter:function(index,row){
                return "<table><tr>"+
                		"<td rowspan=2 style='border:0'><img src='image/"+row.image+"' width='180px'></td>"+
                		"<td style='border:0'>"+
                		"<p>规格:200*123</p>"+
                		"<p>备注:"+row.note+"</p><p>仓库名："+row.stockName+"</p></td>"+
                		"</tr></table>";
            },
        });
      	//开启过滤
        pd.datagrid('enableFilter',[
        	{
        		field:'stockSize',
        		type:'numberbox',
        		op:['equal','notequal','less','greater','lessorequal','greaterorequal']
        	},
        	{
        		field:'price',
        		type:'numberbox',
        		op:['equal','notequal','less','greater','lessorequal','greaterorequal']
        	},
        	{
	        	field:'type',
	        	type:'combobox',
	        	options:{
	        		panelHeight:'auto',
	        		//指定字段和值
	        		valueField:'value',
	        		textField:'text',
	        		url:'product/getType',
	        		onChange:function(value){
	        			//选中全部
	        			if(value==''){
	        				//移除过滤规则
	        				 pd.datagrid('removeFilterRule','type');
	        			}else{
	        				 pd.datagrid('addFilterRule',{
	        					 //添加一个完全匹配的过滤规则
	        					 field:'type',
	        					 op:'equal',
	        					 value:value
	        				 });
	        			}
	        			pd.datagrid('doFilter');
	        		}
	        	}
	        },
	        {
	        	field:'unit',
	        	type:'combobox',
	        	options:{
	        		panelHeight:'auto',
	        		//指定字段和值
	        		valueField:'value',
	        		textField:'text',
	        		url:'product/getUnit',
	        		onChange:function(value){
	        			//选中全部
	        			if(value==''){
	        				//移除过滤规则
	        				 pd.datagrid('removeFilterRule','unit');
	        			}else{
	        				 pd.datagrid('addFilterRule',{
	        					 //添加一个完全匹配的过滤规则
	        					 field:'unit',
	        					 op:'equal',
	        					 value:value
	        				 });
	        			}
	        			pd.datagrid('doFilter');
	        		}
	        	}
	        },
	        {
	        	field:'station',
	        	type:'combobox',
	        	options:{
	        		panelHeight:'auto',
	        		data:[
	        			{value:'',text:'全部'},
	        			{value:'上架',text:'上架'},
	        			{value:'下架',text:'下架'}
	        		],
	        		onChange:function(value){
	        			//选中全部
	        			if(value==''){
	        				//移除过滤规则
	        				 pd.datagrid('removeFilterRule','station');
	        			}else{
	        				 pd.datagrid('addFilterRule',{
	        					 //添加一个完全匹配的过滤规则
	        					 field:'station',
	        					 op:'equal',
	        					 value:value
	        				 });
	        			}
	        			pd.datagrid('doFilter');
	        		}
	        	}
	        }
	        
        ]);
      	//工具栏初始化
        pt.on('click','a.add',function(){
        	//添加按钮初始化
        	var dialog = $('<div/>').dialog({
        		title:'新增产品',
        		width:550,
        		height:550,
        		modal:true,
        		href:'product/form',
        		onClose:function(){
        			//关闭窗户后，立即销毁窗口
        			$(this).dialog('destroy');
        		},
        		buttons:[
        			{
        				iconCls:'icon-ok',
        				text:'保存',
        				handler:function(){
        					//获得表格的信息
        					var form = $("#productForm");
        					if(form.form("validate")){
        						$.post("product/save",form.serialize(),function(rs){
        							if(rs.err){
        								alert(rs.err);
        							}else{
        								dialog.dialog("close");
        								pd.datagrid("reload");
        							}
        						})
        					}
        				}
        			},
        			{
        				iconCls:'icon-cancel',
        				text:'取消',
        				handler:function(){
        					dialog.dialog("close");
        				}
        			}
        		]
        	});
        }).on('click','a.edit',function(){
        	//编辑按钮初始化
        	var row = pd.datagrid('getSelected');
        	//如果row存在
        	if(row){
        		var dialog = $('<div/>').dialog({
        			title:'编辑商品',
        			width:550,
        			height:550,
        			modal:true,
        			href:'product/form/'+row.id,
        			onClose:function(){
        				$(this).dialog('destroy');
        			},
        			buttons:[
        				{
        					iconCls:'icon-ok',
        					text:'保存',
        					handler:function(){
        						//获得表格的信息
            					var form = $("#productForm");
            					if(form.form("validate")){
            						$.post("product/save",form.serialize(),function(rs){
            							if(rs.err){
            								alert(rs.err);
            							}else{
            								dialog.dialog("close");
            								pd.datagrid("reload");
            							}
            						})
            					}
        					}
        				},
        				{
        					iconCls:'icon-cancel',
        					text:'取消',
        					handler:function(){
        						dialog.dialog("close");
        					}
        				}
        			]
        		});
        	}
        }).on('click','a.delete',function(){
        	//编辑按钮初始化
        	var row = pd.datagrid('getSelected');
        	//如果row存在且为上架状态
        	if(row&&row.station){
        		$.post("product/updateStation",{id:row.id},function(rs){
        			if(rs.err){
        				alert(rs.err);
        			}else {
						pd.datagrid("reload");
					}
        		})
        	}
        })
    });
</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<form  id="productForm" method="post" style="margin: 0; padding: 10px 30px">
	<input type="hidden" name="id" value="${p.id}">
    <!-- 
    	产品名称，编号，图片，价格，规格，单位（写死供选择），分类，状态，库存，仓库名称
     -->
     <table style="line-height: 40px">
     	<tr>
     		 <input class="easyui-switchbutton" name="enable"
			data-options="onText:'上架',offText:'下架',value:'1',checked:${p==null || p.station?'true':'false'}">
     	</tr>
     	<tr>
     		<td>产品名称：</td><td><input name="name" class="easyui-textdbox" required="true"  style="width:200px" value="${p.name }"></td>
     	</tr>
     	<tr>
     		<td>编号：</td><td><input name="number" class="easyui-textdbox" required="true"  style="width:200px" value="${p.number }"></td>
     	</tr>
     	<tr>
     		<td>图片：</td>
     		<td><input multiple="multiple" accept="image/*,application/x-zip-compressed" id="img_upload" type="file">
    			<input type="button" value="上传" id="uploadBtn">
    			<input id="imgPathValue" type="hidden" name="image" value="${p.image}">
    		</td>
     	</tr>
     	<tr><td></td>
     		<td><img id="uploadImg" src="upload/${p.image}"></td>
     	</tr>
     	<tr>
     		<td>价格：</td>
     		<td><input name="price" class="easyui-textdbox" required="true"  style="width:200px" value="${p.price }"></td>
     	</tr>
     	<tr>
     		<td>规格：</td>
     		<td><input name="standard" class="easyui-textdbox" required="true"  style="width:200px" value="${p.standard }"></td>
     	</tr>
     	<tr>
     		<td>单位：</td>
     		<td>
     			<select id="unitSelect" name="unit" unit="${p.unit}" style="width: 100px;text-align: center;" >
     				<option value="件">件</option>
     				<option value="台">台</option>
     				<option value="部">部</option>
     				<option value="罐">罐</option>
     				<option value="瓶">瓶</option>
     				<option value="个">个</option>
     				<option value="箱">箱</option>
     				<option value="桶">桶</option>
     				<option value="包">包</option>
     				<option value="千克">千克</option>
     				<option value="克">克</option>
     				<option value="ml">ml</option>
     				<option value="L">L</option>
     			</select>
     		</td>
     	</tr>
     	<tr>
     		<td>分类：</td>
     		<td>
	     		<input name="type" class="easyui-textdbox" required="true" 
	     		 style="width:200px" value="${p.type }">
	     	</td>
     	</tr>
     	<tr>
     		<td>备注：</td><td><input name="note" class="easyui-textdbox" required="true" 
     		 style="width:200px" value="${p.note }"></td>
     	</tr>
     </table>
</form>
<script>
	$(function() {
		//让下拉框选中编辑产品的单位
		var unit = $("#unitSelect").attr("unit");
		console.log(unit);
		$("#unitSelect option[value='"+unit+"']").attr("selected","selected");
		var uploadBtn = $("#uploadBtn");
		uploadBtn.on('click',function(){
			var formData = new FormData();
			console.log($("#img_upload"));
			var file = $("#img_upload")[0].files[0];
			console.log(file);
			formData.append("uploadFile",file);
			console.log(formData.get("uploadFile"));
			$.ajax({
				url:'product/upload',
				type:"POST",
				data:formData,
				processData:false,
				contentType:false,
				success:function(data){
					if(data.success){
						alert("上传成功！");
						//显示图片
						$("#uploadImg").attr("src","upload/"+data.file_path).css("width","100px").css("height","100px");
						$("#imgPathValue").val(data.file_path);
					}else{
						alert(data.msg);
					}
				},
				error:function(){
					console.log("失败请求！");
				}
			});
		});
	})
</script>
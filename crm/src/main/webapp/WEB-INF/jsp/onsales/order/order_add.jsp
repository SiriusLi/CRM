<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/system/onsales/order_add.js"></script>
<table id="orderaddtable" style="line-height: 40px;">
	<tr>
		<td>订单主题:</td>
		<td><input class="easyui-textbox" type="text" name="topic"
			style="height: 25px;"></input></td>
		<td width="80px;"></td>
		<td>签约时间:</td>
		<td><input class="easyui-datetimebox" name="time1"
			style="height: 25px;" value="10/11/2012 0:0:0" required="true"></input></td>
	</tr>
	<tr>
		<td>对应客户:</td>
		<td><select id="orderadd-customer-cc" class="easyui-combogrid"
			name="customerid" style="width: 160px; height: 25px;" required="true"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'name',
			url: 'cus/customerlist',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '姓名',formatter : function (value, row, index)  {
	        		 return row.name;
				 }},
	        	 ] ], 
			fitColumns: true
		">
		</select></td>
		<td width="80px;"></td>
		<td>对应销售机会:</td>
		<td><select id="orderadd-salesOpp-cc" class="easyui-combogrid"
			name="salesOppid" style="width: 160px; height: 25px;"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'topic',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '销售主题',formatter : function (value, row, index)  {
	        		 return row.topic;
				 }},
	        	 ] ], 
			fitColumns: true
		">
	</tr>
	<tr rowspan="2">
		<td valign="top">备注:</td>
		<td colspan="4"><input class="easyui-textbox" type="text"
			name="note" style="width: 100%; height: 35px;"></input></td>
	</tr>
	<tr>
		<td valign="top">收货人/地址：</td>
		<td colspan="4">
			<div class="easyui-accordion" style="width: 100%;">
				<div title="详细信息" style="padding: 10px;" data-toggle="distpicker">
					<table style="line-height: 30px;">
						<tr>
							<th colspan="3" align="left">收货人</th>
						</tr>
						<tr>
							<td width="50px"></td>
							<td width="80px">姓名：</td>
							<td><input class="easyui-textbox" type="text"
								style="width: 300px;" name="receiveMan" /></td>
						</tr>
						<tr>
							<td width="50px"></td>
							<td width="80px">电话：</td>
							<td><input class="easyui-numberbox" type="text"
								name="receiveTel" style="width: 300px;" validtype="mobile"
								id="telvalidtype" /></td>
						</tr>
						<tr>
							<th colspan="3" align="left">地址</th>
						</tr>
						<tr>
							<td width="50px"></td>
							<td width="80px">省份：</td>
							<td><select id="eprovinceName" data-province="浙江省"
								name="provinceName" style="width: 300px; height: 25px;"></select></td>
						</tr>
						<tr>
							<td width="50px"></td>
							<td width="80px">城市：</td>
							<td><select id="ecityName" data-city="杭州市" name="cityName"
								style="width: 300px; height: 25px;"></select></td>
						</tr>
						<tr>
							<td width="50px"></td>
							<td width="80px">区/县：</td>
							<td><select id="edistrictName" data-district="西湖区"
								name="districtName" style="width: 300px; height: 25px;"></select></td>
						</tr>
						<tr>
							<td width="50px"></td>
							<td width="80px">详细地址：</td>
							<td><input class="easyui-textbox" type="text"
								style="width: 300px;" name="address" /></td>
						</tr>
					</table>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="5"><span iconCls="icon-more">保存后方可编辑明细、回款计划</span></td>
	</tr>
	<tr>
		<td valign="top">相关附件:</td>
		<td colspan="3"><input class="easyui-linkbutton" type="file"
			multiple="multiple" name="files" /></td>
		<td>可多选，每个小于10M</td>
	</tr>
</table>

<script type="text/javascript">
	function SearchCus1(value) {
		var table1 = $("#searchcuss");
		var id = $("#eee").val();
		if (value == "") {
			alert("请输入id和姓名搜索相关的客户信息，再选择添加员工！");
		} else {
			table1
					.datagrid({
						title : '搜索结果：（只显示前10条结果）',
						width : 265,
						striped : true, //条纹
						collapsible : true, //可折叠的
						singleSelect : true,
						url : 'cus/search/' + value,
						loadMsg : '数据加载中......',
						fitColumns : true,// 允许表格自动缩放, 以适应父容器  
						sortName : 'id',
						sortOrder : 'desc',
						remoteSort : false,
						columns : [ [
								{
									field : 'id',
									width : 100,
									title : 'id'
								},
								{
									field : 'name',
									width : 100,
									title : '客户名称'
								},
								{
									field : 'add',
									width : 50,
									title : '选择',
									formatter : function(value, row, index) {
										return '<a href="javascript:void(0);"onclick="change1('
												+ index + ')"> 添加 </a>';
									}
								} ] ],
					});

		}
	}

	function change1(index) {
		//实时获取所点击那一行的信息
		$('#searchcuss').datagrid('selectRow', index);
		var row = $('#searchcuss').datagrid('getSelected');
		if (row) {
			//将搜索框的内容与选中的值相对应
			$("#cus1").textbox('setValue', row.name);
			$("#cus2").val(row.id);
		}

	}
	//搜索
	function SearchOpp1(value) {
		var table2 = $("#searchopp");
		if (value == "") {
			alert("请输入id和姓名搜索相关的销售机会信息，再选择添加！");
		} else {
			table2
					.datagrid({
						title : '搜索结果：（只显示前10条结果）',
						width : 265,
						striped : true, //条纹
						collapsible : true, //可折叠的
						singleSelect : true,
						url : 'opp/search/' + value,
						loadMsg : '数据加载中......',
						fitColumns : true,// 允许表格自动缩放, 以适应父容器  
						sortName : 'id',
						sortOrder : 'desc',
						remoteSort : false,
						columns : [ [
								{
									field : 'id',
									width : 100,
									title : 'id'
								},
								{
									field : 'topic',
									width : 100,
									title : '机会主题'
								},
								{
									field : 'add',
									width : 50,
									title : '选择',
									formatter : function(value, row, index) {
										return '<a href="javascript:void(0);"onclick="change2('
												+ index + ')"> 添加 </a>';
									}
								} ] ],
					});

		}
	}

	function change2(index) {
		//实时获取所点击那一行的信息
		$('#searchopp').datagrid('selectRow', index);
		var row = $('#searchopp').datagrid('getSelected');
		if (row) {
			//将搜索框的内容与选中的值相对应
			$("#opp1").textbox('setValue', row.topic);
			$("#opp2").val(row.id);
		}

	}
</script>

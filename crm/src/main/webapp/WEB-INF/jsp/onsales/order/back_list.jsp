<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/system/onsales/back_list.js"></script>
<!-- 退货列表 -->
<!-- 表格 -->
<table id="back-dg" class="easyui-datagrid" title="退货列表"
	toolbar="#backDetail-toolbar" style="min-width: 500px;"
	url="crm/back/list" rownumbers="true" fitColumns="true"
	singleSelect="true" fit="true" striped="true"
	data-options="method:'get'" pagination="true">
</table>
<!-- 工具栏 -->
<div id="backDetail-toolbar" style="padding: 5px; height: 30px;">
	<div style="margin-bottom: 5px; float: left;">
		<a href="javascript:void(0)" class="easyui-linkbutton add"
			iconCls="icon-add" plain="true"
			onclick="javascript:$('#backadd-window').window('open').window('center');">新增</a>
	</div>
</div>
<!-- 新增退货单 -->
<div id="backadd-window" class="easyui-window" title="新增退货单"
	style="width: 850px; height: 550px;" modal="true" closed="true">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center'" style="padding: 10px;">
			<form id="back-form">
				<table style="line-height: 40px;">
					<tr>
						<td>对应客户：</td>
						<td><select id="backadd-customer-cc" class="easyui-combogrid"
							name="customerid" style="width: 160px; height: 25px;"
							required="true"
							data-options="
										panelWidth: 160,
										idField: 'id',
										textField: 'name',
										url: 'cus/customerlist',
										method: 'get',
										columns : [ [ 
								        	 {field : 'id',width : 50,title : 'id'}, 
								        	 {field : 'name',width : 100,title : '姓名',formatter : function (value, row, index)  {
								        		 return row.name;
											 }},
								        	 ] ], 
										fitColumns: true
									">
						</select></td>
						<td width="80px;"></td>
						<td width="100px;" style="text-align: right;">对应订单：</td>
						<td><select id="backadd-order-cc"
							 class="easyui-combogrid"
							name="orderid" style="width: 160px; height: 25px;"
							required="true"
							data-options="
										panelWidth: 160,
										idField: 'id',
										textField: 'topic',
										method: 'get',
										columns : [ [ 
								        	 {field : 'id',width : 50,title : 'id'}, 
								        	 {field : 'topic',width : 100,title : '订单主题'},
								        	 ] ], 
										fitColumns: true
									">
						</select></td>
					</tr>
				</table>
				<c:import url="tuihuo.jsp"></c:import>
			</form>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="saveBack()" style="width: 80px">确认退货</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'"
				href="javascript:void(0)"
				onclick="javascript:$('#backadd-window').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>

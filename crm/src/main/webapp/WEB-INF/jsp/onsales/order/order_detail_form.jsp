<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script src="js/system/onsales/order_detail_form.js"></script>
<!-- 添加订单、发货、退货明细的form表单 -->
<form id="orderdetail-form" method="post">
	<table width="100%" style="line-height: 40px;">
		<tr>
			<td width="80px;" style="text-align: center;">对应订单:</td>
			<td><select name="orderid" class="easyui-combobox"
				style="width: 160px; height: 25px;" editable="false" id="orderid"
				panelHeight="auto" required="true" url="crm/order/orderTopic"
				data-options="valueField:'id', textField:'topic'"></td>
		</tr>
		<tr>
			<td width="80px;" style="text-align: center;">产品名称:</td>
			<td><select name="productid" class="easyui-combobox"
				editable="false" panelHeight="auto" style="width: 160px"
				data-options="url:'product/findByStation',valueField:'id', textField:'name'"></td>
		</tr>
		<tr>
			<td width="80px;" style="text-align: center;">单价:</td>
			<td><input class="easyui-textbox" type="text" editable="false"
				name="price" id="orderdetail-price"></input></td>
		</tr>
		<tr>
			<td width="80px;" style="text-align: center;">数量:</td>
			<td><input class="easyui-numberspinner" style="height: 25px;"
				data-options="min:1,required:true" name="amount"
				id="orderdetail-amount"></input></td>
		</tr>
		<tr>
			<td width="80px;" style="text-align: center;">总价:</td>
			<td><input class="easyui-textbox" type="text" name="money"
				editable="false" id="orderdetail-money"></input></td>
		</tr>

		<tr>
			<td width="80px;" style="text-align: center;">备注:</td>
			<td><input class="easyui-textbox" type="text" name="note"
				style="height: 40px;"></input></td>
		</tr>
	</table>
</form>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="js/system/onsales/auditlist.js"></script>
<div id="audit-tabs" class="easyui-tabs"
	style="width: 100%; height: 100%;">
	<div title="待审批订单"display:none;">
		<!-- 表格 -->
		<table id="auditOrder-dg" class="easyui-datagrid" title="订单列表"
			toolbar="#audit-toolbar" singleSelect="true"
			style="min-width: 500px;" url="crm/order/audit" rownumbers="true"
			fitColumns="true" fit="true" striped="true"
			data-options="method:'get'" pagination="true">
		</table>
	</div>
	<div title="待审批报价单" style="overflow: auto; display: none;">
		<table id="auditQuotation-dg" class="easyui-datagrid" title="订单列表"
			toolbar="#audit-toolbar" singleSelect="true"
			style="min-width: 500px;" url="quotation/audit" rownumbers="true"
			fitColumns="true" fit="true" striped="true"
			data-options="method:'get'" pagination="true">
		</table>
	</div>
</div>
<!-- 审批合同 -->
<div id="auditOrder-dd" class="easyui-dialog" title="订单审核"
	style="width: 400px; height: 250px; padding: 10px 30px;" closed="true"
	data-options="resizable:true,modal:true">
	<div data-options="region:'center'" style="padding: 10px;">
		<p>是否同意当前数据?</p>
		<form  method="post" id="auditOrder-form">
			<input type="hidden" name="id"> <input type="radio" value="通过" required="true"
				name="audit">同意 <input type="radio" value="否决" name="audit" required="true">否决
		</form>
		<p>说明：点击同意或否决，审批当前数据</p>
	</div>
	<div data-options="region:'south',border:false"
		style="text-align: right; padding: 5px 0 0;">
		<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
			href="javascript:void(0)" onclick="saveAuditOrder()"
			style="width: 80px">确认</a> <a class="easyui-linkbutton"
			data-options="iconCls:'icon-cancel'" href="javascript:void(0)"
			onclick="javascript:$('#auditOrder-dd').dialog('close');" style="width: 80px">取消</a>
	</div>
</div>
<!-- 审批报价 -->
<div id="auditQuotation-dd" class="easyui-dialog" title="报价审核"
	style="width: 400px; height: 250px; padding: 10px 30px;" closed="true"
	data-options="resizable:true,modal:true">
	<div data-options="region:'center'" style="padding: 10px;">
		<p>是否同意当前数据?</p>
		<form  method="post" id="auditQuotation-form">
			<input type="hidden" name="id"> <input type="radio" value="通过" required="true"
				name="audit">同意 <input type="radio" value="否决" name="audit" required="true">否决
		</form>
		<p>说明：点击同意或否决，审批当前数据</p>
	</div>
	<div data-options="region:'south',border:false"
		style="text-align: right; padding: 5px 0 0;">
		<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
			href="javascript:void(0)" onclick="saveAuditQuotation()"
			style="width: 80px">确认</a> <a class="easyui-linkbutton"
			data-options="iconCls:'icon-cancel'" href="javascript:void(0)"
			onclick="javascript:$('#auditQuotation-dd').dialog('close');" style="width: 80px">取消</a>
	</div>
</div>

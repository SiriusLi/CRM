<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加回款计划</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<link rel="stylesheet" type="text/css" href="iconfont/iconfont.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/system/onsales/receivable_add.js"></script>
</head>
<body>
	<div id="receivable-window" class="easyui-window" modal="true"
		data-options="iconCls:'icon-save'"
		style="width: 900px; height: 550px;">
		<div style="padding: 10px;">
			<input type="hidden" value="${id }" name="orderid">
			<input type="hidden" value="${order.getOrderMoney()}" name="orderMoney">
			<input type="hidden" value="${order.getReceivableMoney()}"  name="receivableMoney">
			<table style="line-height: 40px;">
				<tr>
					<td>客户:</td>
					<td>【${order.getCustomer().getName() }】</td>
					<td width="180px;"></td>
					<td>订单:</td>
					<td>【${order.getTopic() }】</td>
				</tr>
				<tr>
					<td colspan="5">提示：${order.getOrderMoney()-order.getReceivableMoney()}元未回款</td>
				</tr>
			</table>
		</div>
		<div style="width: 100%; height: 200px;">
			<table id="receivable-add-detail-dg" class="easyui-datagrid"
				rownumbers="true" singleSelect="true"
				striped="true" toolbar="#receivable-add-detail-toolbar"
				data-options="
				iconCls: 'icon-edit',">
			</table>
		</div>
		<div id="receivable-add-detail-toolbar"
			style="padding: 5px; height: 30px;">
			<div style="margin-bottom: 5px; float: left;">
				<a href="javascript:void(0)" class="easyui-linkbutton add"
					iconCls="icon-add" plain="true" onclick="newReceivable()">新增</a>
			</div>
		</div>
		<div style="text-align: center; padding: 10px">
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="saveReceivable()">保存</a> <a
				href="javascript:void(0)" class="easyui-linkbutton">取消</a>
		</div>
		<div style="padding: 10px;">
			<p>说明：</p>
			<ol>
				<li>启用应收款算法功能后，在对应的条件下才出现此界面。</li>
				<li>默认生成全部应收款数值，可根据实际情况调整当期应收款数值。</li>
				<li>计划回款日期支持账期选择、日期录入和选择。</li>
			</ol>
		</div>
	</div>
</body>
</html>
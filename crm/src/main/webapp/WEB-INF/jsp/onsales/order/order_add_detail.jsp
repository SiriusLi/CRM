<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加订单明细</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<link rel="stylesheet" type="text/css" href="iconfont/iconfont.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/system/onsales/order_add_detail.js"></script>
<script src="js/datagrid-dnd.js"></script>
<script src="js/datagrid-filter.js"></script>
</head>
<body>
	<div class="easyui-layout" style="width: 100%; height: 700px;">
		<div id="p" data-options="region:'west'" title="West"
			style="width: 30%; padding: 10px">
			<table id="order-add-detail-productDatagrid"></table>
		</div>
		<div data-options="region:'center'" title="Center">
			<div style="padding: 15px;">
				<input type="hidden" value="${order.id }" name="orderid"> 
				<input type="hidden" value="${type }" name="type">
				<span
					style="margin: 5px;"> 订单明细 &nbsp;至:<span
					style="margin: 0 50px 0px 5px; font-weight: bold;">【${order.topic }】</span>日期：<span
					style="margin: 5px 5px; font-weight: bold;"><fmt:formatDate
							value="${order.time }" type="date" dateStyle="medium" /> </span></span><a
					href="javascript:void(0)" class="easyui-linkbutton"
					onclick="saveDetail()" data-options="iconCls:'icon-save'"
					style="float: right;">保存数据</a>
			</div>
			<div style="width: 100%; height: 450px;">
				<table id="order-add-detail-dg" class="easyui-datagrid"
					title="添加订单明细" style="width: 100%; height: 100px;"
					rownumbers="true" fitColumns="true" singleSelect="true" fit="true"
					toolbar="#order-add-detail-toolbar"
					data-options="
				iconCls: 'icon-edit',
				singleSelect: true,
				method:'get',
				hideColumn:'id'
			">
				</table>
			</div>
		</div>
		<div data-options="region:'south'" style="width: 100%; height: 20%;">
			<p>操作说明：</p>
			<ol>
				<li>点击左侧产品名称，选择产品。</li>
				<li>填写数量，空白处点鼠标，自动计算金额</li>
				<li>编辑完成后点击保存明细</li>
			</ol>
		</div>
	</div>
	<div id="order-add-detail-toolbar">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			iconCls="icon-remove" plain="true" onclick="deletea()">删除</a>
	</div>
</body>
</html>
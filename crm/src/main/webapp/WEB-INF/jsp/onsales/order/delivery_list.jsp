<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 发货 -->
<script src="js/system/onsales/delivery_list.js"></script>
<!-- 表格 -->
<table id="delivery-dg" class="easyui-datagrid" title="发货列表"
	toolbar="#deliveryl-toolbar" style="min-width: 500px;"
	url="crm/delivery/list" fitColumns="true" singleSelect="true"
	fit="true" rownumbers="true" striped="true" data-options="method:'get'"
	pagination="true">
</table>
<!-- 工具栏 -->
<div id="deliveryl-toolbar" style="padding: 5px; height: 30px;">
	<div style="margin-bottom: 5px; float: left;">
		<a href="javascript:void(0)" class="easyui-linkbutton add"
			iconCls="icon-add" plain="true" onclick="newDelivery()">新增</a>
	</div>
</div>
<!-- 新增发货单 -->
<div id="deliveryadd-window" class="easyui-window" title="发货"
	modal="true" data-options="iconCls:'icon-save'" closed="true"
	style="width: 800px; height: 550px; padding: 5px;">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:false"
			style="padding: 10px;">
			<div style="width: 80%; margin: 0px auto">
				<form id="deliveryadd-form">
					<table style="line-height: 40px;">
						<tr>
							<td>对应客户:</td>
							<td><select id="customer-cc" class="easyui-combogrid"
								style="width: 160px; height: 25px;" required="true"
								data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'name',
			url: 'cus/customerlist',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 70,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '姓名',formatter : function (value, row, index)  {
	        		 return row.name;
				 }},
	        	 ] ], 
			fitColumns: true
		">
							</select></td>
							<td width="80px;"></td>
							<td>对应订单:</td>
							<td><select id="order-cc" class="easyui-combogrid"
								style="width: 160px; height: 25px;"required="true"
								data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'topic',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 100,title : 'id'}, 
	        	 {field : 'topic',width : 100,title : '主题',formatter : function (value, row, index)  {
	        		 return row.topic;
				 }},
	        	 ] ], 
			fitColumns: true
		">
							</select></td>
						</tr>
						<tr>
							<td>姓名：</td>
							<td><input type="text" name="receiveMan"
								 style="height: 25px;" /></td>
							<td width="80px"></td>
							<td>电话：</td>
							<td><input type="text" name="receiveTel"  style="height: 25px;" /></td>
						</tr>
						<tr>
							<td>收货地址:</td>
							<td colspan="4"><input type="text" name="receiveAddress"
								style="width: 300px; height: 25px;"
								 /></td>
						</tr>
					</table>
				</form>
				<c:import url="fahuo.jsp"></c:import>
			</div>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="savefahuo()" style="width: 80px">确认发货</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'"
				href="javascript:void(0)"
				onclick="javascript:$('#deliveryadd-window').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="js/system/onsales/orderlist.js"></script>
<script src="js/distpicker.data.js"></script>
<script src="js/distpicker.js"></script>
<script src="js/main.js"></script>
<script src="js/system/JQuery_form.js"></script>
<script src="js/echarts.js"></script>
<!-- 当前登录的用户 -->
<input type="hidden" name="loginRoleName" value="${roleName }">
<!-- 表格 -->
<div style="width: 100%; height: 350px;">
	<table id="order-dg" class="easyui-datagrid" toolbar="#order-toolbar"
		style="min-width: 500px;" url="crm/order/list" rownumbers="true"
		fitColumns="true" singleSelect="true" fit="true" striped="true"
		showFooter="false" data-options="method:'get'" pagination="true">
	</table>
</div>
<<c:import url="ordertongji.jsp"></c:import>
<!-- 工具栏 -->
<div id="order-toolbar" style="padding: 5px; height: 30px;">
	<div style="margin-bottom: 5px; float: left;">
		<a href="javascript:void(0)" class="easyui-linkbutton add"
			iconCls="icon-add" plain="true" onclick="newOrder()">新增订单</a><a
			href="javascript:void(0)" class="easyui-linkbutton"
			iconCls="icon-edit" plain="true" onclick="editOrder()">编辑订单</a> <a
			href="javascript:void(0)" class="easyui-linkbutton"
			iconCls="icon-remove" plain="true" onclick="destroyOrder()">删除</a>
	</div>
	<div style="float: right;">
		<input class="easyui-searchbox" data-options="prompt:'输入订单号'"
			style="width: 200px;"> <a href="javascript:void(0)"
			class="easyui-linkbutton" iconCls="icon-more" plain="true"
			onclick="moreSelect()">更多查询</a>
	</div>
</div>
<!-- 新增订单 -->
<div id="order-adddlg" class="easyui-window" style="width: 900px; height: 600px;" closed="true" modal="true">

	<div class="easyui-layout" data-options="fit:true,border:false,">
		<div data-options="region:'center',border:false,">
			<div style="width: 80%; margin: 0px auto">
				<form id="order-form" method="post" enctype="multipart/form-data"
					action="crm/order/save">
					<c:import url="order_add.jsp"></c:import>
				</form>
			</div>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="addOrder()" style="width: 80px">保存</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'"
				href="javascript:void(0)"
				onclick="javascript:$('#order-adddlg').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>
<!-- 编辑订单 -->
<div id="order-editdlg" class="easyui-window" style="width: 850px"
	modal="true" height="600px;" closed="true">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:false"
			style="padding: 10px;">
			<div style="width: 80%; margin: 0px auto">
				<form id="orderedit-form">
					<c:import url="order_edit.jsp"></c:import>
				</form>
			</div>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="saveEditOrder()"
				style="width: 80px">保存</a> <a class="easyui-linkbutton"
				data-options="iconCls:'icon-cancel'" href="javascript:void(0)"
				onclick="javascript:$('#order-editdlg').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>
<!-- 更多查询 -->
<div id="moreSelect-dlg" class="easyui-dialog" style="width: 400px"
	closed="true" buttons="#dlg-buttons">
	<c:import url="order_moreSelect.jsp"></c:import>
</div>
<!-- 发货 -->
<div id="order-fahuodlg" class="easyui-window" title="发货" modal="true"
	data-options="iconCls:'icon-save'" closed="true"
	style="width: 900px; height: 550px; padding: 5px;">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center',border:false"
			style="padding: 10px;">
			<div style="width: 80%; margin: 0px auto">
				<form id="confirm-information">
					<input name="id" type="hidden">
					<table style="line-height: 40px;">
						<tr>
							<td>对应客户:</td>
							<td><input type="text" name="customer" readonly="true"
								style="height: 25px; border: none;"></input></td>
							<td width="80px;"></td>
							<td>对应订单:</td>
							<td><input type="text" name="topic" readonly="true"
								style="height: 25px; border: none;"></input></td>
						</tr>
						<tr>
							<td>姓名：</td>
							<td><input type="text" name="receiveMan" readonly="true"
								style="height: 25px; border: none;"
								data-options="valueField:'id', textField:'name'" /></td>
							<td width="80px"></td>
							<td>电话：</td>
							<td><input type="text" name="receiveTel" readonly="true"
								style="height: 25px; border: none;" /></td>
						</tr>
						<tr>
							<td>收货地址:</td>
							<td colspan="4"><input type="text" name="receiveAddress"
								style="width: 300px; height: 25px; border: none;"
								readonly="true" /></td>
						</tr>
						<tr>
							<td colspan="2"><input type="radio" name="delivery"
								value="0">全部发货</td>
							<td width="80px"></td>
							<td colspan="2"><input type="radio" name="fahuo-radio"
								value="1">部分发货</td>
						</tr>
					</table>
				</form>
				<c:import url="fahuo.jsp"></c:import>
			</div>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="savefahuo()" style="width: 80px">确认发货</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'"
				href="javascript:void(0)"
				onclick="javascript:$('#order-fahuodlg').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>
<!-- 退货 -->
<div id="order-backdlg" class="easyui-window"
	style="width: 850px; height: 550px;" modal="true" closed="true">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center'" style="padding: 10px;">
			<form id="order-back-form">
				<c:import url="tuihuo.jsp"></c:import>
			</form>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="savetuihuo()" style="width: 80px">确认退货</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'"
				href="javascript:void(0)"
				onclick="javascript:$('#order-backdlg').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
</div>
<div id="edit-orderDetail" class="easyui-window"
	style="width: 750px; height: 450px;" modal="true" closed="true">
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'center'" style="padding: 10px;">
			<c:import url="fahuo.jsp"></c:import>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'"
				href="javascript:void(0)" onclick="savetuihuo()" style="width: 80px">保存数据</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'"
				href="javascript:void(0)"
				onclick="javascript:$('#edit-orderDetail').window('close');"
				style="width: 80px">取消</a>
		</div>
	</div>
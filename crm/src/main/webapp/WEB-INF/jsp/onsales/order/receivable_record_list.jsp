<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script src="js/system/onsales/receivable_record_list.js"></script>
<!-- 回款记录表格 -->
<table id="receicable-record-dg" class="easyui-datagrid" title="回款列表"
	toolbar="#plan-receicable-toolbar" style="min-width: 500px;"
	url="crm/receivableRecord/list" rownumbers="true" fitColumns="true"
	singleSelect="true" fit="true" striped="true"
	data-options="method:'get'" pagination="true">

</table>
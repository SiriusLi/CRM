<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 确认发货 -->
<table id="confirm-order-detail" class="easyui-datagrid" title="订单明细"
	style="width: 100%;" rownumbers="true" fitColumns="true" >
</table>
<script>
$(function() {
	$("#confirm-order-detail").datagrid({
		columns : [ [
				{field:"check", checkbox:true},
				{field:'product',width:120,formatter:function(value,row,index){
					return row.product.name;
				},align:'center',title:"产品"},
				{field:"price", width:80 ,
					editor:"{type:'numberbox',options:{precision:1}}",title:"单价"},
				{field:"amount", width:80 ,
					editor:"{type:'numberbox',options:{precision:0}}",title:"数量"},
				{field:"money" ,width:80,
					editor:"{type:'numberbox',options:{precision:1}}",title:"总价"},
				{field:'state',width:100,editor:'text',title:"状态"},
				{field:'note',width:150,editor:'text',title:"备注"},
		] ],
	});
});
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div style="padding: 10px 30px 20px 50px; width: 280px;">
	<form id="moreSelect-form" method="post">
		<input type="hidden" value="${id }">
		<table style="line-height: 40px;">
			<tr>
				<td>订单主题:</td>
				<td><input class="easyui-textbox" type="text" name="email"></input></td>
			</tr>
			<tr>
				<td>订单编号:</td>
				<td><input class="easyui-textbox" type="text" name="name"></input></td>
			</tr>
			<tr>
				<td>对应客户:</td>
				<td><select class="easyui-combobox" name="state"
					style="width: 130px;">
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
				</select></td>
			</tr>
			<tr>
				<td>对应销售机会:</td>
				<td><select class="easyui-combobox" name="state"
					style="width: 160px;">
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
				</select></input></td>
			</tr>
			<tr>
				<td>签约时间:</td>
				<td><input class="easyui-datebox" name="subject"></input></td>
			</tr>
			<tr>
				<td>审核人:</td>
				<td><select class="easyui-combobox" name="state"
					style="width: 160px;" style="width:160px;">
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
				</select></td>
			</tr>
			<tr>
				<td>是否封存:</td>
				<td><input class="easyui-switchbutton" name="enable"
					data-options="onText:'不封存',offText:'封存',value:'1',checked:${r==null || r.enable?'true':'false'}">
				</td>
			</tr>
			<tr>
				<td>是否退货:</td>
				<td><input class="easyui-switchbutton" name="enable"
					data-options="onText:'否',offText:'是',value:'1',checked:${r==null || r.enable?'true':'false'}">
				</td>
			</tr>
			<tr>
				<td>所有者:</td>
				<td><select class="easyui-combobox" name="state"
					style="width: 160px;">
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
				</select></td>
			</tr>
			<tr>
				<td>状态:</td>
				<td><input type="radio" name="subject" />执行中<input
					type="radio" name="subject" />已结束<input type="radio"
					name="subject" />意外终止</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td>审核状态:</td>
				<td><input type="radio" name="subject" />未审核<input
					type="radio" name="subject" />已审核</td>
			</tr>
			<tr>
				<td>审核结果:</td>
				<td><input type="radio" name="subject">通过</input><input
					type="radio" name="subject">不通过</input></td>
			</tr>
		</table>
	</form>
	<div style="text-align: center; padding: 5px">
		<a href="javascript:void(0)" class="easyui-linkbutton"
			onclick="submitForm()">保存</a> <a href="javascript:void(0)"
			class="easyui-linkbutton" onclick="clearForm()">取消</a>
	</div>
</div>
<script>
	function submitForm() {
		$('#order-form').form('submit');
	}
	function clearForm() {
		$('#order-form').form('clear');
	}
</script>
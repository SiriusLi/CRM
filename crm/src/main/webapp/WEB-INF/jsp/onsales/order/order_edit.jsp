<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<input type="hidden" name="id">
<table style="line-height: 40px;">
	<tr>
		<td>订单主题:</td>
		<td><input class="easyui-textbox" type="text" name="topic"
			style="height: 25px;"></input></td>
		<td width="80px;"></td>
		<td>签约时间:</td>
		<td><input class="easyui-datetimebox" name="time1"
			style="height: 25px;" value="10/11/2012 0:0:0"></input></td>
	</tr>
	<tr>
		<td>对应客户:</td>
		<td><select id="orderadd-customer-cc" class="easyui-combogrid"
			readonly="readonly" name="customerid"
			style="width: 160px; height: 25px; border: none;"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'name',
			url: 'cus/customerlist',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '姓名',formatter : function (value, row, index)  {
	        		 return row.name;
				 }},
	        	 ] ], 
			fitColumns: true
		">
		</select></td>
		<td width="80px;"></td>
		<td>对应销售机会:</td>
		<td><select id="orderadd-salesOpp-cc" class="easyui-combogrid"
			name="salesOppid" style="width: 160px; height: 25px;"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'topic',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'name',width : 100,title : '销售主题',formatter : function (value, row, index)  {
	        		 return row.topic;
				 }},
	        	 ] ], 
			fitColumns: true
		">
	</tr>
	<tr>
		<td>订单编号：</td>
		<td><input name="number" class="easyui-textbox" type="text"
			style="height: 25px;" /></td>
		<td width="80px;"></td>
		<td>订单状态：</td>
		<td><input name="state" class="easyui-textbox" type="text"
			style="height: 25px;" /></td>
	</tr>
	<tr>
		<td>审核状态：</td>
		<td><input name="audit" class="easyui-textbox"
			style="height: 25px;" /></td>
		<td width="80px;"></td>
		<td>审核人：</td>
		<td><select class="easyui-combogrid" name="auditEmployeeid"
			style="width: 160px; height: 25px;" readonly="readonly"
			data-options="
			idField: 'id',
			textField: 'empName',
			url: 'employee/empNamelist',
		">
		</select></td>
	</tr>
	<tr>
		<td>审核日期：</td>
		<td><input name="auditDate1" style="height: 25px; border: none;"
			value="10/11/2012 0:0:0" readonly="readonly"></input></td>
		<td width="80px;"></td>
		<td>发货状态：</td>
		<td><input name="delivery" style="height: 25px; border: none;"
			readonly="readonly" /></td>
	</tr>
	<tr>
		<td>是否退货：</td>
		<td><input name="returnGoods" style="height: 25px; border: none;"
			readonly="readonly" /></td>
		<td width="80px;"></td>
		<td>是否封存：</td>
		<td><input name="sequestration"
			style="height: 25px; border: none;" readonly="readonly" /></td>
	</tr>
	<tr>
		<td>合同金额:</td>
		<td><input type="text" class="easyui-textbox"
			style="height: 25px;" name="orderMoney"></td>
		<td width="80px;"></td>
		<td>回款金额:</td>
		<td><input type="text" class="easyui-textbox"
			style="height: 25px;" name="receivableMoney"></td>
	</tr>
	<tr>
		<td>所有者：</td>
		<td><select id="orderadd-customer-cc" class="easyui-combogrid"
			name="employeeid" style="width: 160px; height: 25px;" required="true"
			data-options="
			panelWidth: 160,
			idField: 'id',
			textField: 'empName',
			url: 'employee/empNamelist',
			method: 'get',
			columns : [ [ 
	        	 {field : 'id',width : 50,title : 'id'}, 
	        	 {field : 'empName',width : 100,title : '姓名',formatter : function (value, row, index)  {
	        		 return row.empName;
				 }},
	        	 ] ], 
			fitColumns: true
		">
		</select></td>
	</tr>
	<tr>
		<td>姓名：</td>
		<td><input class="easyui-textbox" type="text" name="receiveMan" /></td>
		<td width="80px;"></td>
		<td>电话：</td>
		<td><input class="easyui-numberbox" type="text" name="receiveTel"
			validtype="mobile" id="telvalidtype" /></td>
	</tr>
	<tr rowspan="2">
		<td valign="top">地址:</td>
		<td colspan="4"><input class="easyui-textbox" type="text"
			name="receiveAddress" style="width: 100%; height: 35px;"></input></td>
	</tr>
	<tr rowspan="2">
		<td valign="top">备注:</td>
		<td colspan="4"><input class="easyui-textbox" type="text"
			name="note" style="width: 100%; height: 35px;"></input></td>
	</tr>
</table>

<div style="width: 100%; padding: 15px 0px;">
	<c:import url="fahuo.jsp"></c:import>
	<p></p>
	<table id="receivable-add-detail-dg" class="easyui-datagrid"
		title="回款明细" rownumbers="true" fitColumns="true" style="width: 100%;"
		toolbar="#receivable-add-detail-toolbar">
		<thead>
			<tr>
				<th data-options="field:'installment',width:40">期数</th>
				<th field="planTime" width="80"
					data-options="formatter : function(value){
                var date = new Date(value);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                var d = date.getDate();
                return value==null?null:y + '-' +m + '-' + d;
            },">计划回款时间</th>
				<th field="planMoney" width="80"
					editor="{type:'numberbox',options:{precision:1}}">计划回款金额</th>
				<th field="employee" width="50" align="center"
					data-options="formatter:function(value){
	        	return value ==null ? null:value.empName;
	        }">负责人</th>
				<th field="state" width="50" align="center">回款状态</th>
				<th data-options="field:'note',width:100,editor:'text'">备注</th>
			</tr>
		</thead>
	</table>
</div>

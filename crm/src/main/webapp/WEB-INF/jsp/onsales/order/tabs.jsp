<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选项卡</title>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<link rel="stylesheet" type="text/css" href="iconfont/iconfont.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>
<script src="js/system/onsales/orderlist.js"></script>
</head>
<body>
	<div id="tt" class="easyui-tabs" style="width: 500px; height: 250px;">
		<div title="Tab1" style="padding: 20px; display: none;">tab1</div>
		<div title="Tab2" data-options="closable:true"
			style="overflow: auto; padding: 20px; display: none;">tab2</div>
		<div title="Tab3" data-options="iconCls:'icon-reload',closable:true"
			style="padding: 20px; display: none;">tab3</div>
	</div>
</body>
</html>
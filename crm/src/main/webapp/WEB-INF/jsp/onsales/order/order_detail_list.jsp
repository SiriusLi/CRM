<!-- 订单明细 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/system/onsales/order_detail_list.js"></script>
<!-- 表格 -->
<table id="orderDetail-dg" class="easyui-datagrid" title="订单明细列表"
	toolbar="#orderDetail-toolbar" style="min-width: 500px;"
	url="crm/orderDetail/list" rownumbers="true" fitColumns="true"
	singleSelect="true" fit="true" striped="true"
	data-options="method:'get'" pagination="true">
</table>

<!-- 工具栏 -->
<div id="orderDetail-toolbar" style="padding: 5px; height: 30px;">
	<div style="margin-bottom: 5px; float: left;">
		<a href="javascript:void(0)" class="easyui-linkbutton add"
			iconCls="icon-add" plain="true" onclick="newOrderdetail()">新增</a>
	</div>
</div>

<div id="orderdetail-adddlg" class="easyui-window" style="width: 450px"
	title="新增明细" closed="true" modal="true"
	buttons="#orderdetail-dlg-buttons">
	<div style="padding: 10px 30px 20px 70px; width: 280px; height: 300px;">
		<c:import url="order_detail_form.jsp"></c:import>
	</div>
	<div style="text-align: center; padding: 30px">
		<a class="easyui-linkbutton" onclick="saveOne('订单')">保存</a> <a
			class="easyui-linkbutton" onclick="clearOrderDetailadd()"
			style="margin-left: 10px;">取消</a>
	</div>
</div>

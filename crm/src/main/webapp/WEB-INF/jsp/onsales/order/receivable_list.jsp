<!-- 回款计划 -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script src="js/system/onsales/receivable_list.js"></script>
<!-- 表格 -->
<table id="plan-receicable-dg" class="easyui-datagrid" title="回款列表"
	toolbar="#plan-receicable-toolbar" style="min-width: 500px;"
	url="crm/receivable/list" rownumbers="true" fitColumns="true"
	singleSelect="true" fit="true" striped="true"
	data-options="method:'get'" pagination="true">

</table>
<!-- 工具栏 -->
<div id="plan-receicable-toolbar" style="padding: 5px; height: 30px;">
	<div style="margin-bottom: 5px; float: left;">
		<a href="javascript:void(0)" class="easyui-linkbutton add"
			iconCls="icon-add" plain="true" onclick="newReceicablePlan()">新增回款计划</a>
	</div>
</div>
<!-- 新增回款计划 -->
<div id="receicableadd-window" class="easyui-window"
	style="width: 450px; height: 400px;" title="新增回款计划" closed="true"
	modal="true">
	<div class="easyui-layout" data-options="fit:true,border:false,">
		<div data-options="region:'center',border:false,">
			<div style="width: 80%; margin: 0px auto">
				<form id="receicableadd-form">
					<table style="width: 100%; line-height: 40px;">
						<tbody>
							<tr>
								<td width="100px;" style="text-align: right;">对应客户：</td>
								<td><select id="receicableadd-customer-cc"
									class="easyui-combogrid" name="customerid"
									style="width: 160px; height: 25px;" required="true"
									data-options="
										panelWidth: 160,
										idField: 'id',
										textField: 'name',
										url: 'cus/customerlist',
										method: 'get',
										columns : [ [ 
								        	 {field : 'id',width : 50,title : 'id'}, 
								        	 {field : 'name',width : 100,title : '姓名',formatter : function (value, row, index)  {
								        		 return row.name;
											 }},
								        	 ] ], 
										fitColumns: true
									">
								</select></td>
							</tr>
							<tr>
								<td width="100px;" style="text-align: right;">对应订单：</td>
								<td><select id="receicableadd-order-cc"
									class="easyui-combogrid" name="orderid"
									style="width: 160px; height: 25px;"
									data-options="
										panelWidth: 160,
										idField: 'id',
										textField: 'topic',
										method: 'get',
										columns : [ [ 
								        	 {field : 'id',width : 50,title : 'id'}, 
								        	 {field : 'name',width : 100,title : '订单主题',formatter : function (value, row, index)  {
								        		 return row.topic;
											 }},
								        	 ] ], 
										fitColumns: true
									">
								</select></td>
								</td>
							</tr>
							<tr>
								<td width="100px;" style="text-align: right;">计划回款时间：</td>
								<td><input class="easyui-datetimebox" required="true"
									id="time" style="height: 25px;" value="10/11/2012 0:0:0"
									name="time"></td>
							</tr>
							<tr>
								<td width="100px;" style="text-align: right;">计划回款金额：</td>
								<td><select id="receicableadd-planMoney-cc"
									class="easyui-combogrid"  name="planMoney"
									style="width: 160px; height: 25px;"
									data-options="
										panelWidth: 160,
										idField: 'value',
										textField: 'value',
										method: 'get',
										columns : [ [ 
								        	 {field : 'value',width : 100,title : '回款金额',formatter : function (value, row, index)  {
								        		 return '￥'+value;
											 }}, 
								        	 {field : 'money',width : 50,title : '百分比',formatter : function (value, row, index)  {
								        		 return row.money;
											 }},
								        	 ] ], 
										fitColumns: true
									">
								</select>
							</td>
							</tr>
							<tr>
								<td width="100px;" style="text-align: right;">期 数：</td>
								<td><input class="easyui-numberspinner"
									style="height: 25px;" data-options="min:1,max:10,required:true"
									name="installment"></input></td>
							</tr>
							<tr>
								<td width="100px;" style="text-align: right;"></td>
							</tr>

							<tr>
								<td width="100px;" style="text-align: right;">备注：</td>
								<td><input class="easyui-textbox" type="text" name="note"
									style="height: 50px;"></input></td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div data-options="region:'south',border:false"
			style="text-align: right; padding: 5px 0 0;">
			<a class="easyui-linkbutton" onclick="saveReceicable()" data-options="iconCls:'icon-ok'">保存</a> <a
				class="easyui-linkbutton" onclick="clearReceicableadd()" data-options="iconCls:'icon-cancel'"
				style="margin-left: 10px;">取消</a>
		</div>
	</div>
</div>
<!-- 还款 -->
<div id="receicable-window" class="easyui-window" style="width: 450px"
	title="回款" closed="true" modal="true">
	<div style="width: 80%; margin: 10px auto;">
		<form id="receicable-form">
			<input name="id" type="hidden">
			<table>
				<tr>
					<td>选择付款方式：</td>
					<td><select class="easyui-combobox" name="paymentWay">
							<option value="支票">支票</option>
							<option value="现金">现金</option>
							<option value="电汇">电汇</option>
							<option value="网上银行">网上银行</option>
					</select></td>
				</tr>
				<tr>
					<td>是否需要发票：</td>
					<td><input type="radio" name="receipt" value="1">需要 <input
						type="radio" name="receipt" value="0">不需要</td>
				</tr>
			</table>
		</form>
	</div>
	<div style="text-align: center; padding: 30px">
		<a class="easyui-linkbutton" onclick="saveReceicable1()">保存</a> <a
			class="easyui-linkbutton" onclick="clearReceicableadd1()"
			style="margin-left: 10px;">取消</a>
	</div>
</div>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="${ctxPath }">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>联系我们</title>
<link rel="stylesheet" href="themes/default/easyui.css" />
<link rel="stylesheet" href="themes/icon.css" />
<script src="js/jquery.min.js"></script>
<script src="js/jquery.easyui.min.js"></script>
<script src="js/locale/easyui-lang-zh_CN.js"></script>

<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

.div_blank {
	height: 70px;
}

.div_blank ul {
	list-style: none;
	line-height: 100px;
}

.div_blank ul li {
	width: 200px;
	height: 70px;
	float: left;
	margin-left: 300px;
	background-color: #009900;
	color: #fff;
	font-size: 20px;
	display: block;
	line-height: 70px;
	text-align: center;
}

.div_message {
	height: 500px;
	background-color: #F7F7F7;
}
</style>
</head>
<body>

	<div style="height: 128px;">
		<div style="width: 400px; height: 70px">
			<img src="images/website/logo.png">
			<div style="height: 20px"></div>
			<div style="font-size: 25px; color: #C5C5C5">
				<strong>CRM：给销售团队一颗云大脑</strong>
			</div>
		</div>
	</div>
	
	

	<div style="height: 420px">
		<img src="images/website/jj6.jpg" style="width: 100%; height: 431px">
	</div>


	<div class="div_blank">
		<ul>
			<li>留言薄</li>

		</ul>

	</div>
	

	<div class="div_message">
		<div style="width:600px;float:left" >
		<div style="clear: both; overflow: hidden; height: auto"></div>
		<div class="row"
			style="padding: 2.5em; line-height: 4em">
			<form id="myform" method="post">
				<table class='message_table'>

					<tr>
						<td class='text'>姓名</td>
						<td class='input'><input name='name' type='text'
							style="height: 40px" /> <span class='info'>*</span></td>
					</tr>


					<tr>
						<td class='text'>邮箱</td>
						<td class='input'><input name='email' type='text'
							style="height: 40px" /> <span class='info'>*</span></td>
					</tr>

					<tr>
						<td class='text'>电话</td>
						<td class='input'><input name='tel' type='text'
							style="height: 40px" /> <span class='info'> *</span></td>
					</tr>


					<tr>
						<td class='text'>生日</td>
						<td class='input'><input name='birthday' class='easyui-datebox'
							style="height: 40px" /> <span class='info'> *</span></td>
					</tr>

					<tr>
						<td class='text'>性别</td>

						<td>男 <input type="radio" name="sex" value="男"> 女 <input
							type="radio" name="sex" value="女"> <span class='info'>*</span>
						</td>

					</tr>

					<tr>
						<td class='text'></td>
						<td class='submint'><input type='submit' id='Submit'
							value='提交留言' class='submit button orange'></td>
					</tr>
				</table>
				<!-- 客户来源，此处的默认是来源于互联网 -->
				<input type="hidden" name="source" value="互联网">
			</form>
		</div>
		
	</div>
	

	<div style="width:800px;height:400px;float:left;margin-top:50px;margin-left:240px">
				<p><span style="font-size: 26px;color: #666;line-height: 20px;">联系我们</span>
					<span style="font-size: 26px;color: #c6c6c6;text-transform: uppercase;"> CONTACT US</span>
				</p>
				
			<ul style="margin-top:80px">
				<li style="list-style:none;height:60px">地址：东莞市大朗镇佛新社区富民工业二园佛新工业区25号
				</li>
				<li style="list-style:none;height:60px">电话：+86 0769 89252699</li>
				<li style="list-style:none;height:60px">邮箱：Sales@idensityenergy.com</li>
				
			</ul>
	</div>
	
	
</div>
<script type="text/javascript">
//提交表单
$("#Submit").click(function(){
	
	var form = $("#myform");
	    //校验表单数据
		if(form.form("validate")){
			$.post("cus/getcus",form.serialize(),function(rs){
				if(rs.err){
					alert(rs.err);
				}else{
					alert("提交成功");
					//location.href='lee/off2';
				}
			});
		}
});



</script>



</body>
</html>
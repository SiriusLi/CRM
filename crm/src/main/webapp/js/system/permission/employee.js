$(function() {
	var grid;
	grid = $('#employee-grid').datagrid({
		border : false,
		singleSelect : true,
		pagination : true,
		url : 'employee/list',
		idField : 'id',
		toolbar : '#employee-toolbar',
		columns:[[
	        {title:'姓名',field:'empName',width:80},/*
	        {title:'相片',field:'picture',width:180,formatter: function(value){	        	
				if(value != null) {
//					return "<img style='width:30px;height:30px;' src='/file/"+ value+"'>";
					return "<img style='width:30px;height:30px;' src=\"/file/20180601102600.jpg\"/>";
					return "<img src='images/employee_photo/20180506031708.jpg'>";
				}
        	}},*/
	        {title:'角色',field:'role',width:100,formatter: function(value){	        	
	        	/*var roleNames = [];
        		$.each(value,function(){
        			roleNames.push(this.roleName);
        		});
        		return roleNames.join(",");*/
	        	if(value!=null) {
	        		return value.roleName;
	        	}
	        }},
	        {title:'电话',field:'telephone',width:110},
	        {title:'电子邮箱',field:'email',width:180},
	        {title:'上司',field:'leader',width:80,formatter: function(value){	        	
					if(value != null) {
						return "<a href='javascript:void(0)' onClick='showInfo("+ value.id +");'>" + value.empName + '</a>';
					}
	        	}
	        },
	        {title:'状态',field:'state',formatter: function(value){
				if(value == true) return '在职';
				else return '离职';
				}
	        },
	        {field:'id',title:'操作',width:80,formatter:function(value,row,index){
	        	if(value != null) {
					return "<a href='javascript:void(0)' onClick='showInfo("+ value +");'>" + '查看详情' + '</a>';
				}
	        }}
	    ]]
	});
	
	$('#employee-toolbar').on('click','a.add',function() {
		var dialog = $("<div/>").dialog({
		title: "新增员工",
		modal:true,
		shadow:false,
		width: 450,
		//为按钮绑定form
		href:'	employee/form',
		method:'GET',
		onClose: function(){
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-ok",
				text:"保存",
				handler:function(){
//					var form = $("#employeeForm");
//					if(form.form("validate")){
//						$.post("employee/save",form.serialize(),function(rs){
//							if(rs.err){
//								alert(rs.err);
//							}else{
//								dialog.dialog("close");
//								grid.datagrid("reload");
//							}
//						});
//					}
					$('#employeeForm').form('submit', {    
					    url:'employee/save', 
					    method:'POST',
					    onSubmit: function(params){    
					    	return $('#employeeForm').form('validate'); 
					    },    
					    success:function(data){    
					    	dialog.dialog("close");
							grid.datagrid("reload");
					    }    
					});  

				}
			},
			{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					dialog.dialog("close");
				}
			}
		]
		});
		moveDialogToCenter(dialog);
	}).on('click','a.alter',function(){
		var row = $('#employee-grid').datagrid('getSelected');
		if (row) {
			var dialog = $("<div/>").dialog({
				title: "修改员工",
				width: 450,
				modal:true,
				shadow:false,
				//为按钮绑定form，此处变为有id后缀的链接
				href:'employee/form/'+row.id,
				onClose: function(){
					$(this).dialog('destroy');
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
//							var form = $("#employeeForm");
//							if(form.form("validate")){
//								$.post("employee/save",form.serialize(),function(rs){
//									if(rs.err){
//										alert(rs.err);
//									}else{
//										dialog.dialog("close");
//										grid.datagrid("reload");
//									}
//								});
//							}
							$('#employeeForm').form('submit', {    
							    url:'employee/save', 
							    method:'POST',
							    onSubmit: function(params){    
							    	return $('#employeeForm').form('validate'); 
							    },    
							    success:function(data){    
							    	dialog.dialog("close");
									grid.datagrid("reload");
							    }    
							}); 
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
			moveDialogToCenter(dialog);
		
		}
	}).on('click','a.delete',function() {
		var row = $('#employee-grid').datagrid('getSelected');
	    if (row){
	        $.messager.confirm('提示','确定删除此员工?',function(r){
	            if (r){
	                $.post('employee/delete/'+row.id,function(result){
	                    if (result.success){
	                    	grid.datagrid('reload');  
	                    } else {
	                        $.messager.show({    
	                            title: 'Error',
	                            msg: result.errorMsg
	                        });
	                    }
	                },'json');
	            }
	        });
	    }
	});
});

function showInfo(id) {
	var dialog = $("<div/>").dialog({
		title: "员工信息",
		width: 450,
		modal:true,
		shadow:false,
		href:'employee/info/'+id,
		onClose: function(){
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function() {
					dialog.dialog("close");
				}
			}
		]
		});
	dialog.dialog('vcenter');
}


function moveDialogToCenter(dialog) {
	dialog.panel("move",{top:$(document).scrollTop() + ($(window).height()-500) * 0.5});  
}
	
$(function() {
	var grid;
	grid = $('#role-grid').datagrid({
		border : false,
		singleSelect : true,
		pagination : true,
		shadow:false,
		url : 'role/list',
		idField : 'id',
		toolbar : '#role-toolbar',
		columns:[[
	        {title:'角色名称',field:'roleName'},
	        {title:'角色说明',width:500,field:'note',formatter:function(value) {
	        	if(value==null || value=='') {
	        		return '无';
	        	}
	        }},
	        {title:'状态',field:'enable',formatter: function(value){
				if(value == true) return '启用';
				else return '禁用';
				}
	        }
	    ]]
	});
	
	//新增按钮
	$('#role-toolbar').on('click','a.add',function() {
		var dialog = $("<div/>").dialog({
			title: "新增角色",
			width: 450,
			height: 300,
			shadow:false,
			modal:true,
			//为按钮绑定form
			href:'role/form',
			onClose: function(){
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#roleForm");
						if(form.form("validate")){
							$.post("role/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		})
	});
	//修改按钮
	$('#role-toolbar').on('click','a.alter',function() {
		var row = $('#role-grid').datagrid('getSelected');
		if (row) {
			var dialog = $("<div/>").dialog({
				title: "修改角色1",
				width: 450,
				height: 300,
				shadow:false,
				modal:true,
				//为按钮绑定form
				href:'role/form/'+row.id,
				onClose: function(){
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#roleForm");
							if(form.form("validate")){
								$.post("role/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							
						}
					}
				]
			})
		}
	});
	//删除按钮
	$('#role-toolbar').on('click','a.delete',function() {
		var row = $('#role-grid').datagrid('getSelected');
	    if (row){
	        $.messager.confirm('提示','确定删除此角色?',function(r){
	            if (r){
	            	$.post('role/delete/'+row.id,function(result){
	                    if (result.success){
	                    	grid.datagrid('reload');    // reload the user data
	                    } else {
	                        $.messager.show({    // show error message
	                            title: 'Error',
	                            msg: result.errorMsg
	                        });
	                    }
	                },'json');
	            }
	        });
	    }
	});
	//授权按钮
	$('#role-toolbar').on('click','a.authorize',function() {
		var row = grid.datagrid("getSelected");
		var roleGrantTree;
		if(row){
			var dialog = $("<div/>").dialog({
				title: "为"+row.roleName+"角色授权",
				width: 450,
				modal:true,
				shadow:false,
				href:'role/grant/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				onLoad:function(){
					 roleGrantTree = $('#roleGrantTree',this);
					//初始化资源树
					roleGrantTree.tree({
						checkbox:true
					});
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存权限",
						handler:function(){
							//获取 tree 组件中节点为打勾和实心的
							var nodes =  roleGrantTree.tree('getChecked', ['checked','indeterminate']);
							var roleIds = [];
							$.each(nodes,function(){
								roleIds.push(this.id);
							});
							var parma = "roleId="+roleGrantTree.data("roleId");
							if(roleIds.length > 0){
								parma += "&rid="+roleIds.join("&rid=");
							}
							console.log(parma);
							$.post("role/grant",parma,function(){
								dialog.dialog("close");
								$.messager.alert("提示","授权成功!");
							});
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
		moveDialogToCenter(dialog);
	});
});
function moveDialogToCenter(dialog) {
	dialog.panel("move",{top:$(document).scrollTop() + ($(window).height()-450) * 0.5});  
}

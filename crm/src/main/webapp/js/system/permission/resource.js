$(function() {
	var grid = $('#resource-grid');
	grid.treegrid({
		fit:true,
		border:false,
	    url:'resource/list',
	    idField:'id',
	    treeField:'text',
	    toolbar:"#resource-toolbar",
	    columns:[[
	        {title:'名称',field:'text',width:180},
	        {field:'menuHref',title:'资源链接',width:200},
	        {title:'类型',field:'type',width:60,formatter:function(value,row,index){
        		return types[value];
	        }},
	        {field:'enable',title:'状态',width:60,formatter:function(value,row,index){
	        		return value==true ? "可用":"禁用";
	        }},
	        {field:'id',title:'操作',width:80,formatter:function(value,row,index){
	        	if(value != null) {
					return "<a href='javascript:void(0)' onClick='showInfo("+ value +");'>" + '查看详情' + '</a>';
				}
	        }}
	    ]]
	});
	
	//新增按钮
	$('#resource-toolbar').on('click','a.add',function() {
		var dialog = $("<div/>").dialog({
			title: "新增资源",
			width: 450,
			modal:true,
			shadow:false,
			//为按钮绑定form
			href:'resource/form',
			onClose: function(){
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						$('#resourceForm').form('submit', {    
						    url:'resource/save',
						    onSubmit: function(params){    
						    	return $('#resourceForm').form('validate'); 
						    },    
						    success:function(data){    
						    	dialog.dialog("close");
								grid.treegrid("reload");
						    }    
						}); 
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		});
//		dialog.window('vcenter');
		dialog.panel("move",{top:$(document).scrollTop() + ($(window).height()-500) * 0.5}); 
	});
	//修改按钮
	$('#resource-toolbar').on('click','a.alter',function() {
		var row = $('#resource-grid').datagrid('getSelected');
		if (row) {
			var dialog = $("<div/>").dialog({
				title: "修改资源",
				width: 450,
				modal:true,
				shadow:false,
				//为按钮绑定form
				href:'resource/form/'+row.id,
				onClose: function(){
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							$('#resourceForm').form('submit', {    
							    url:'resource/save', 
							    method:'post',
							    onSubmit: function(params){    
							    	return $('#resourceForm').form('validate'); 
							    },    
							    success:function(data){    
							    	dialog.dialog("close");
									grid.treegrid("reload");
							    }    
							});
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			})
		};
		dialog.panel("move",{top:$(document).scrollTop() + ($(window).height()-500) * 0.5}); 
	});
	//删除按钮
	$('#resource-toolbar').on('click','a.delete',function() {
		var row = grid.treegrid("getSelected");
		if(row && (!row.children || row.children.length==0)){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.post("resource/delete/"+row.id,function(){
                    		grid.treegrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		} else {
			$.messager.show({
				title:'提示信息',
				msg:'只能删除叶子结点',
				showType:'slide'
			});

		}
	});
});
var types = {
		'MENU':'菜单',
		'FUNCTION':'功能',
		'PAGE':'页面'
};
function showInfo(id) {
	var dialog = $("<div/>").dialog({
		title: "资源详情",
		width: 450,
		modal:true,
		shadow:false,
		href:'resource/info/'+id,
		onClose: function(){
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-cancel",
				text:"关闭",
				handler:function() {
					dialog.dialog("close");
				}
			}
		]
		});
	$(dialog).dialog('vcenter');
}
function addUrl() {
	var url = $('#url-input').val();
	//检查url是否有存在
	if(!isExisting(url) && url != '') {
//		var li = "<li value='"+ url +"'>"+url+"</li>";
		$('#url-ul').datagrid('appendRow',{
			url : url
		});
		$('#url-input').textbox('clear');
		refreshUrls();
	}
}

function delUrl() {
	var row = $('#url-ul').datagrid('getSelected');
	var index = $('#url-ul').datagrid('getRowIndex', row);
	$('#url-ul').datagrid('deleteRow',index);
	refreshUrls();
}
function isExisting(url) {
	var flag = false;
	var rows = $('#url-ul').datagrid('getRows');
	for (var i = 0; i < rows.length; i++) { 
		if(url == rows[i]['url']) {
			flag = true;
			break;
		}
	}
	return flag;
}
function refreshUrls() {
	var rows = $('#url-ul').datagrid('getRows');
	var urls = '';
	var set = new Set();
	for (var i = 0; i < rows.length; i++) { 
//		urls = urls + rows[i]['url'] + ';';
		set.add(rows[i]['url']);
	}
	for(let s of set) {
		urls = urls + s + ';';
	}
	$('#urls-input').textbox('setValue',urls);
}

function moveDialogToCenter(dialog) {
	/*dialog.panel("move",{top:$(document).scrollTop() + ($(window).height()-500) * 0.5}); */
	$(dialog).dialog('vcenter');
}
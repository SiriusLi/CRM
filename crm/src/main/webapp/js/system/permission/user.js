$(function() {
	var grid;
	grid = $('#userGrid').datagrid({
		fit : true,
		border : false,
		singleSelect : true,
		pagination : false,
		url : '',
		idField : 'id',
		toolbar : '#user-toolbar',
	});
});
//新增
function newUser() {
	$('#user-dlg').dialog('open').dialog('center').dialog('setTitle', '新增用户');
	$('#user-form').form('clear');
}
//修改
function alterUser() {
	var row = $('#user-dg').datagrid('getSelected');
	if(row) {
		$('#user-alter-dlg').dialog('open').dialog('center').dialog('setTitle', '修改用户');
		$('#user-alter-form').form('clear');
	}
}
//删除
function deleteUser() {
	var row = $('#user-dg').datagrid('getSelected');
    if (row){
        $.messager.confirm('提示','确定删除此用户?',function(r){
            if (r){
            	alert('deleted');
//                $.post('easyui/delete/'+row.userId,function(result){
//                    if (result.success){
//                        $('#dg').datagrid('reload');    // reload the user data
//                    } else {
//                        $.messager.show({    // show error message
//                            title: 'Error',
//                            msg: result.errorMsg
//                        });
//                    }
//                },'json');
            }
        });
    }
}
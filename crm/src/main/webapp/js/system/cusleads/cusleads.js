$(function () {
	var grid = $("#cusleadsGird");
	//初始化表格
	grid.datagrid({
		fit:true,
		border:true,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		rownumbers:true,
		url:'cusleads/list',
		idField:'id',
		toolbar:'#cusleadsToolbar',
		columns:[[
			{field:'id',title:'id',width:50,align:'center'},
			{field:'name',title:'公司名',width:100,align:'center'},
			{field:'contacts',title:'联系人姓名',width:100,align:'center'},
			{field:'tel',title:'联系人电话',width:100,align:'center'},
			{field:'email',title:'邮箱',width:100,align:'center'},
			{field:'sex',title:'性别',width:100,align:'center'},
			{field:'source',title:'客户来源',width:100,align:'center'},
			/*员工是否已处理此条线索，默认为未处理，当经理处理后，就将数据转移到销售人员的获客线索页面*/
			{field:'saState',title:'状态',width:100,align:'center',formatter : function (value, row, index)  {
				return value ? "已处理":"未处理";
			}},
		]]
	});
	
	//为工具栏按钮添加绑定事件
	$("#cusleadsToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增获客线索",
			width:450,
			top:150,
			modal:true,
			href:'cusleads/cusleadsform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#cusleadsForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("cusleads/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "处理获客线索",
				width: 450,
				top:70,
				modal:true,
				href:'cusleads/cusleadsform/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#cusleadsForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("cusleads/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
        if (row){
            $.messager.confirm('警告','您确定要删除此信息吗？',function(r){
                if (r){
                    $.get('cusleads/delete/'+row.id,function(result){
                        if (result.success){
                        	grid.datagrid("reload");  
                        } else {
                        	$.messager.alert("提示！","系统错误，删除失败！");
                        }
                    });
                }
            });
        }
	});
	
	Date.prototype.Format = function(fmt) {    
		var o = {
			"M+" : this.getMonth() + 1, //月份   
			"d+" : this.getDate(), //日   
			"H+" : this.getHours(), //小时   
			"m+" : this.getMinutes(), //分   
			"s+" : this.getSeconds(), //秒   
			"q+" : Math.floor((this.getMonth() + 3) / 3),
			"S" : this.getMilliseconds()
		//毫秒   
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1,
						(RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k])
								.substr(("" + o[k]).length)));
		return fmt;
	};
	
});
 
$(function () {
	var grid = $("#actionGird");
	//初始化表格
	grid.datagrid({
		fit:true,
		border:true,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		rownumbers:true,
		url:'',
		idField:'id',
		toolbar:'#actionToolbar',
		columns:[[
			{field:'id',title:'id',width:50,align:'center'},
			{field:'topic',title:'跟进主题',width:100,align:'center'},
			{field:'content',title:'跟进内容',width:200,align:'center'},
			{field:'customer',title:'对应客户',width:100,align:'center'},
			{field:'employee',title:'所有者',width:100,align:'center'},
			{field:'date',title:'跟进日期',width:100,formatter : function(value, row, index) {
				var time = new Date(value).Format("yyyy-MM-dd");
				return time;
			}},
		]]
	});
	
	//为工具栏按钮添加绑定事件
	$("#actionToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增跟进记录",
			width:450,
			height:300,
			modal:true,
			href:'action/actionform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#actionForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("(保存)",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(true){
			var dialog = $("<div/>").dialog({
				title: "编辑跟进记录",
				width: 450,
				height: 270,
				modal:true,
				/*href:'action/actionmoreform'+row.id,*/
				href:'action/actionform',
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#actionMoreForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("(保存)",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
        if (row){
            $.messager.confirm('警告','您确定要删除此信息吗？',function(r){
                if (r){
                    $.post(''+row.id,function(result){
                        if (result.success){
                        	grid.datagrid("reload");  
                        } else {
                        	$.messager.alert("提示！","系统错误，删除失败！");
                        }
                    });
                }
            });
        }
	});
	
	Date.prototype.Format = function(fmt) {    
		var o = {
			"M+" : this.getMonth() + 1, //月份   
			"d+" : this.getDate(), //日   
			"H+" : this.getHours(), //小时   
			"m+" : this.getMinutes(), //分   
			"s+" : this.getSeconds(), //秒   
			"q+" : Math.floor((this.getMonth() + 3) / 3),
			"S" : this.getMilliseconds()
		//毫秒   
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1,
						(RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k])
								.substr(("" + o[k]).length)));
		return fmt;
	};
	
});
 
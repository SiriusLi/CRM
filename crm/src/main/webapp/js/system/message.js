//系统消息
$(function() {
	$("#message-dg").datagrid({
		width : "100%",
		columns:[[
			{field:'id',hidden:true},  
	        {field:'check',checkbox:true},  
	        {field:'number',title:'订单编号',width:60},  
	        {field:'topic',title:'主题',width:150}, 
	        {field:'time',title:'时间',width:60,formatter : function(value){
                var date = new Date(value);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                var d = date.getDate();
                return y + '-' +m + '-' + d;
            }},  
	        {field:'salesOpp',title:'对应销售机会',width:120,formatter:function(value,row,index){
	        	return value ==null ? null:value.topic;
	        }},  
	        
	        {field:'orderMoney',title:'合同金额',width:60},  
	        {field:'customer',title:'对应客户',width:90,formatter:function(value,row,index){
	        	return value ==null ? null:value.name;
	        }}, 
	        {field:'state',title:'状态',width:60},  
	        {field:'delivery',title:'发货',width:60},  
	        
	        {field:'audit',title:'审核状态',width:40},
	        {field:'auditDate',title:'审核日期',width:60,formatter : function(value){
                var date = new Date(value);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                var d = date.getDate();
                return value==null?null:y + '-' +m + '-' + d;
            }}, 
	        {field:'auditEmployee',title:'审核人',width:60,formatter:function(value){
	        	return value ==null ? null:value.empName;
	        }},  
	        {field:'employee',title:'所有者',width:50,formatter:function(value,row,index){
	        	return value ==null ? null:value.empName+'<a title="视图" onclick="showOrder()"><i class="iconfont icon-shenhexiangmu"></i></a>';
	        }},  
	        {field:'operation',title:'操作',width:100,  
		        formatter:function(value,rec,index){  
		              var del = '<a title="视图" onclick="showOrder()"><i class="iconfont icon-shenhexiangmu"></i></a>'+
		              '<a title="删除" onclick="destroyOrder()"><i class="iconfont icon-msnui-trash"></i></a>'+
		              '<a title="编辑" onclick="editOrder()"><i class="iconfont icon-bianji"></i></a>'+
		              '<a title="退货" onclick="backOrder()"><i class="iconfont icon-tuihuo"></i></a>';  
		              return del;    
		        }}  
	        ]]
		//onLoadSuccess : compute,
	});
});
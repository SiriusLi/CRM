$(function() {
	// 基于准备好的dom，初始化echarts实例
	var myChart = echarts.init(document.getElementById('main'));
	// 指定图表的配置项和数据
	var option = {
		title : {
			text : '订单状态分布',
			x : 'center'
		},
		tooltip : {
			trigger : 'item',
			formatter : "{a} <br/>{b} : {c} ({d}%)"
		},
		legend : {
			orient : 'vertical',
			left : 'left',
			data : [ '已完成', '执行中' ]
		},
		series : [ {
			name : '订单状态',
			type : 'pie',
			radius : '55%',
			center : [ '50%', '60%' ],
			data : [ {
				value : 335,
				name : '已完成'
			}, {
				value : 310,
				name : '执行中'
			}, ],
			itemStyle : {
				emphasis : {
					shadowBlur : 10,
					shadowOffsetX : 0,
					shadowColor : 'rgba(95, 190, 170, 1)'
				}
			}
		} ]
	};
	// 使用刚指定的配置项和数据显示图表。
	myChart.setOption(option);
	var myChart1 = echarts.init(document.getElementById('main1'));
	// 指定图表的配置项和数据
	var option1 = {
		title : {
			text : '订单总数统计'
		},
		tooltip : {},
		legend : {
			data : [ '总数' ]
		},
		xAxis : {
			data :[]
		},
		yAxis : {
			type : 'value'
		},
		series : [ {
			color : 'rgba(30, 159, 255,0.5)',
			name : '总数',
			type : 'line',
			data : []
		} ]
	};
	// 使用刚指定的配置项和数据显示图表。
	myChart1.setOption(option1);
})// 基于准备好的dom，初始化echarts实例

function changeTheme(obj) {
	var myChart1 = echarts.init(document.getElementById('main1'));
	alert(obj);
	console.log(obj);
	var names ;
	if(obj.value=="本星期"){
		 ['今天', '昨天', '前天', '前三天', '前四天', '前五天', '前六天' ]; 
	}
	// 类别数组（实际用来盛放X轴坐标值）
	var nums = []; // 销量数组（实际用来盛放Y坐标值）
	$.ajax({
		type : "post",
		async : true, // 异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
		url : "crm/order/selectNumByDay", // 请求发送到TestServlet处
		success : function(result) {
			if (result) {
				myChart1.setOption({ // 加载数据图表
					xAxis : {
						data : names
					},
					series : [ {
						// 根据名字对应到相应的系列
						name : '总数',
						data : result
					} ]
				});

			}

		},
		error : function(errorMsg) {
			// 请求失败时执行该函数
			alert("图表请求数据失败!");
			myChart.hideLoading();
		}
	})
}
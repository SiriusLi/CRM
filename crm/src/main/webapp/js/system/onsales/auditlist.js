$(function() {
	$('#auditOrder-dg').datagrid( {
		width : '100%',
		columns : [ [
				{ field : 'number', width : 60, title : '订单编号' },
				{ field : 'topic', width : 150, title : '主题' },
				{ field : 'time', width : 60, title : '申请时间', formatter : function(value) {
						var date = new Date(value);
						var y = date.getFullYear();
						var m = date.getMonth() + 1;
						var d = date.getDate();
						return value == null ? null : y + '-' + m + '-' + d;
				} },
				{ field : 'orderMoney', width : 60, title : '合同金额' },
				{ field : 'customer', width : 90, title : '对应客户', formatter : function(value) {
						return value == null ? null : value.name;
				} },
				{ field : 'state', width : 60, title : '状态' },
				{ field : 'audit', width : 40, title : '审核状态' },
				{ field : 'employee', align : 'center', width : 50, title : '申请人', formatter : function(value) {
						return value == null ? null : value.empName;
				}  },
				{ field : 'op', align : 'center', width : 60, title : '操作', formatter : function(value, rec, index) {
						var del = '<a title="审核" onclick="auditOrder()"><i class="iconfont icon-xiaoshoudingdanshenhe"></i></a>';
						return del;
					}
				}, 
			] ],				
	});
	$('#auditOrder-dg').datagrid('enableFilter', [ {
		field : 'orderMoney',
		type : 'text',
	} ]);
	var qd = $('#auditQuotation-dg');
	qd
			.datagrid({
				singleSelect : true,
				rownumbers : true,
				title : '报价列表',
				fitColumns : true,
				fit : true,
				width : '100%',
				height : 400,
				pagination : true,
				toolbar : '#quotationToolbar',
				idField : 'id',
				columns : [ [
						{
							field : 'salesOpp',
							title : '对应销售机会',
							width : 80,
							align : 'center',
							formatter : function(value, row, index) {
								return value == null ? "无" :value.topic;
							}
						},
						{
							field : 'time',
							title : '创建时间',
							width : 50,
							align : 'center', formatter : function(value) {
								var date = new Date(value);
								var y = date.getFullYear();
								var m = date.getMonth() + 1;
								var d = date.getDate();
								return value == null ? "无" : y + '-' + m + '-' + d;
						} 
						},
						{
							field : 'money',
							title : '报价金额',
							width : 40,
							align : 'center',
							formatter : function(value, row, index) {
								return value.toFixed(2);
							}
						},
						{
							field : 'approval',
							title : '审批状态',
							width : 50,
							align : 'center',
							formatter : function(value, row, index) {
								return value == null ? null :row.approval;
							}
						},

						{
							field : 'employee',
							title : '报价员',
							width : 50,
							align : 'center',
							formatter : function(value, row, index) {
								return value == null ? null :row.empName;
							}
						},
						{
							field : "op",
							title : "操作",
							width : 50,
							align : 'center',
							formatter : function(value, row, index) {
								return '<a title="查看报价详情" onclick="quotationDetail('+index+')"><i class="iconfont icon-shenhexiangmu"></i></a>'+
								'<a title="审批" onclick="auditQuotation()"><i class="iconfont icon-xiaoshoudingdanshenhe"></i></a>'
		;
							}
						} ] ]
			});
	// 开启过滤
	qd.datagrid('enableFilter', [
			{
				field : 'approvalStatus',
				type : 'combobox',
				options : {
					panelHeight : 'auto',
					// 下拉选项的定义
					data : [ {
						value : '',
						text : '全部'
					}, {
						value : '已审批',
						text : '已审批'
					}, {
						value : '未审批',
						text : '未审批'
					} ],
					onChange : function(value, row, index) {
						// 选中全部
						if (value == '') {
							// 移除过滤规则
							qd.datagrid('removeFilterRule', 'approvalStatus');
						} else {
							qd.datagrid('addFilterRule', {
								field : 'approvalStatus',
								op : 'equal',
								value : value
							});
						}
						// 执行
						qd.datagrid('doFilter');
					}
				}
			},
			{
				field : 'approvalResult',
				type : 'combobox',
				options : {
					panelHeight : 'auto',
					// 下拉选项的定义
					data : [ {
						value : '',
						text : '全部'
					}, {
						value : '已通过',
						text : '已通过'
					}, {
						value : '未通过',
						text : '未通过'
					}, {
						value : '未审批',
						text : '未审批'
					} ],
					onChange : function(value, row, index) {
						// 选中全部
						if (value == '') {
							// 移除过滤规则
							qd.datagrid('removeFilterRule', 'approvalResult');
						} else {
							qd.datagrid('addFilterRule', {
								field : 'approvalResult',
								op : 'equal',
								value : value
							});
						}
						// 执行
						qd.datagrid('doFilter');
					}
				}
			},
			{
				field : 'money',
				type : 'numberbox',
				options : {
					precision : 2
				},
				op : [ 'equal', 'notequal', 'less', 'greater', 'lessorequal',
						'greaterorequal' ]
			} ])
});
// 打开审批合同窗口
function auditOrder() {
	var row = $('#auditOrder-dg').datagrid("getSelected");
	if (row) {
		$("#auditOrder-dd").window('open').window('center').window('setTitle',
				'订单审核');
		$('#auditOrder-form').form('load', row);
	}
}
// 保存审批订单结果
function saveAuditOrder() {
	var form = $("#auditOrder-form");
	// 校验表单数据
	if (form.form("validate")) {
		$.post("crm/audit/saveOrder", form.serialize(), function(rs) {
			if (rs.err) {
				alert(rs.err);
			} else {
				$.messager.alert('消息', '审核成功！');
				// 关闭当前窗口
				$("#auditOrder-dd").window("close");
				$("#auditOrder-dg").datagrid("reload");
			}
		});
	}
}
//查看报价单详情
function quotationDetail(index){
	var qd= $("#auditQuotation-dg")
	var row =qd.datagrid('getSelected');
	if(row){
		var dialog = $('<div/>').dialog({
			title:'报价详情',
			width:700,
			height:400,
			modal:true,
			href:'quotation/detail/'+row.id,
			onClose:function(){
				$(this).dialog('destroy');
			},
			buttons:[
				{
					iconCls:'icon-ok',
					text:'确定',
					handler:function(){
						dialog.dialog("close");
						qd.datagrid('reload');
					}
				},
				{
					iconCls:'icon-cancel',
					text:'取消',
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		});
	}
}
// 审批报价单
function auditQuotation() {
	var row = $('#auditQuotation-dg').datagrid("getSelected");
	if (row) {
		$("#auditQuotation-dd").window('open').window('center').window('setTitle',
				'订单审核');
		$('#auditQuotation-form').form('load', row);
	}
}
function saveAuditQuotation(){
	var form = $("#auditQuotation-form");
	// 校验表单数据
	if (form.form("validate")) {
		$.post("crm/audit/saveQuotation", form.serialize(), function(rs) {
			if (rs.err) {
				alert(rs.err);
			} else {
				$.messager.alert('消息', '审核成功！');
				// 关闭当前窗口
				$("#auditQuotation-dd").window("close");
				$("#auditQuotation-dg").datagrid("reload");
			}
		});
	}
}
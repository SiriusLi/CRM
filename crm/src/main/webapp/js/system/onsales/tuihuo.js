var isEditing=false;//是否处于编辑状态
$(function() {
	$("#confirm-fahuo-detail").datagrid({
		columns : [ [ 
			{field:"check",checkbox:true,align:'center'},
			
			{field:'product',width:120,align:'center',title:"产品",formatter:function(value,row,index){
				return row.product.name;
			}},
			{field:"price" ,width:80, align:"right",title:"单价"},
			{field:"amount" ,width: 80, align:"right",title:"数量",
				editor:{type:'numberspinner',options:{min: 1,}}},
			{field:"money" ,width:"80", align:"right",title:"总价"},
			{field:'note',width:250,editor:'text',title:"备注"},
			{field:'op',width:250,formatter:function(value,row,index){
				if (isEditing){
					var s = '<a title="保存" href="javascript:void(0)" onclick="saverow(this)"><i class="iconfont icon-queding"></i></a> ';
					var c = '<a title="取消" href="javascript:void(0)" onclick="cancelrow(this)"><i class="iconfont icon-cuowuguanbiquxiao-yuankuang"></i></a>';
					return s+c;
				} else {
					var e = '<a title="编辑" href="javascript:void(0)" onclick="editrow(this)"><i class="iconfont icon-bianji"></i></a> ';
					var d = '<a title="删除" href="javascript:void(0)" onclick="deleterow(this)"><i class="iconfont icon-msnui-trash"></i></a>';
					return e+d;
				}
			},title:"操作"},
		] ],

		onEndEdit : function(index, row) {

		},
		onBeforeEdit : function(index, row) {
			isEditing = true;
			$("#confirm-fahuo-detail").datagrid('refreshRow', index);
		},
		onAfterEdit : function(index, row) {
			isEditing = false;
			$("#confirm-fahuo-detail").datagrid('refreshRow', index);
		},
		onCancelEdit : function(index, row) {
			isEditing = false;
			$("#confirm-fahuo-detail").datagrid('refreshRow', index);
		}
	});
});
function cancelrow(target) {
	$('#receivable-add-detail-dg').datagrid('cancelEdit', getRowIndex(target));
}
// 编辑
function getRowIndex(target) {
	var tr = $(target).closest('tr.datagrid-row');
	return parseInt(tr.attr('datagrid-row-index'));
}
function editrow(target) {
	$('#confirm-fahuo-detail').datagrid('beginEdit', getRowIndex(target));
}
function deleterow(target) {
	$.messager.confirm('提示', '确定要删除吗?', function(r) {
		if (r) {
			$('#confirm-fahuo-detail').datagrid('deleteRow',
					getRowIndex(target));
		}
	});
}
function saverow(target) {
	var index=getRowIndex(target);
	var rows = $('#receivable-add-detail-dg').datagrid('getRows');// 获取当前的数据行
	var edm=$('#receivable-add-detail-dg').datagrid('getEditor', {index:index,field:'planMoney'});
	var edt=$('#receivable-add-detail-dg').datagrid('getEditor', {index:index,field:'planTime'});
	var planMoney = $(edm.target).numberbox('getValue');
	var planTime = $(edt.target).datebox('getValue');
	console.log(getMoney());
	if (planTime<nowtime()) {
		isEditing = true;
		$.messager.alert("提示","计划回款时间不能小于当前时间！");
	}else if(planMoney>getMoney()){
		isEditing = true;
		$.messager.alert("提示","添加失败了！没有这么多要回款的金额！");
	}else{
		if(rows.length>1){
			//获取上一个回款计划的时间
			var planTime1=rows[rows.length-2].planTime;
			console.log(planTime1);
			console.log(planTime);
			if (planTime<planTime1) {
				isEditing = true;
				$.messager.alert("提示","计划回款时间不能小于上一个计划回款时间","error");
			}else{
				$('#receivable-add-detail-dg').datagrid('endEdit', getRowIndex(target));
			}
		}else{
			$('#receivable-add-detail-dg').datagrid('endEdit', getRowIndex(target));
		}
	}
}
$(function(){
	$('#delivery-dg').datagrid({
		width : "100%",
		columns:[[
			{ field:'number',width:80,title:'发货单号'},
			{ field:'date',width:50,title:'时间',formatter : function(value){
                var date = new Date(value);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                var d = date.getDate();
                return value==null?null:y + '-' +m + '-' + d;
            }},
			{ field:'receiveMan',width:60,title:'收货人姓名'},
			{ field:'receiveTel',width:80,title:'电话'},
			{ field:'receiveAddress',width:120,title:'地址'},
			{ field:'state',width:60,title:'发货状态'},
			{ field:'customer',width:120,title:'对应客户',formatter:function(value,row,index){
	        	return value ==null ? null:value.name;
	        }},
			{ field:'order',width:120,title:'对应订单',formatter:function(value,row,index){
	        	return value ==null ? null:value.topic;
	        }},
			{ field:'note',width:80,title:'备注',formatter:function(value){
				return value ==null ? "无":value;
			}},
			{ field:'op',width:80,title:'操作',formatter:function(value,row,index){ 
				if("已发货"==row.state){
					return '<a title="结束" onclick="finishDelivery('+index+')"><i class="iconfont icon-biaodanwancheng2"></i></a>';
				}
			}},
		]],
	});
	$("#customer-cc").combogrid({
		onSelect : function(record) {
			$("#order-cc").combogrid({
				url:'crm/order/getOrderByCusDeAu',
				queryParams: {
					customerid:$('#customer-cc').combogrid('getValue'),
				},
			});
		}
	});
	//联动，根据所选订单得到对应的姓名，电话，地址,以及表格的内容
	$("#order-cc").combogrid({
		onSelect : function(row) {
			var r = $('#order-cc').combogrid('grid').datagrid('getSelected');
			$("#deliveryadd-form input[name='receiveMan']").val(r.receiveMan);
			$("#deliveryadd-form input[name='receiveTel']").val(r.receiveTel);
			$("#deliveryadd-form input[name='receiveAddress']").val(r.receiveAddress);
			
			$("#confirm-order-detail").datagrid({
				url:"crm/orderDetail/getDetailByOrder/"+r.id,
				method:"GET",
				queryParams: {
					state: '未发货',
				},
				onLoadSuccess:function(){
					$('#confirm-order-detail').datagrid("selectAll");
				}
			})
		}
	});
})
function newDelivery(){
	$('#deliveryadd-window').window('open').window('center');
	$("#deliveryadd-form").form("clear");
}
//保存发货单
function savefahuo() {
	var orderid = $('#order-cc').combogrid('grid').datagrid('getSelected').id;
	// 返回复选框选中的所有行.
	var getChecked = $('#confirm-order-detail').datagrid("getSelections");
	var rows = $('#confirm-order-detail').datagrid('getRows')// 获取当前的数据行
	if (rows.length<1) {
		$.messager.alert("提示", "没有产品呐！", "warning");
	} else {
		$.ajax({
			url : 'crm/deliveryDetail/saveDeliveryDetail/' + orderid,
			type : 'post',
			dataType : 'json',
			data : {
				"orderDetails" : JSON.stringify(rows),
			// JSON.stringify() 方法用于将JavaScript值转换为 JSON
			// 字符串，缺少此句传到后台的则为空值
			},
			success : function(response) { // 后台返回的要显示的消息
				if(response.err){
					$.messager.alert("消息",response.err,"error");
				}else{
					$.messager.alert("消息","添加发货成功！","info");
					$('#order-fahuodlg').window('close');
					$("#order-dg").datagrid("reload");
				}
			}
		});
	}
}
//完成发货单
function finishDelivery(index){
	$('#delivery-dg').datagrid('selectRow',index);
	var row = $('#delivery-dg').datagrid('getSelected');
	if (row) {
		$.messager.confirm("提示","您确定要结束该发货单吗？",function(r){
			if(r){
				$.get("crm/delivery/finishDelivery/"+row.id,function(rs){
					$.messager.alert("提示",rs.mesage);
					$('#delivery-dg').datagrid("reload");
                }).error(function(){
                	$.messager.alert("提示","请求失败了!",'warning');
                });
			}
		});
	}
}

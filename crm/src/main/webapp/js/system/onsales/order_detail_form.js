$(function(){
	var pruduct=$('#orderdetail-form select[name="productid"]');
	var price=$('#orderdetail-price');
	var amount=$('#orderdetail-amount');
	var money=$('#orderdetail-money');
	//联动，选择产品之后确定单价
		pruduct.combobox({
		onSelect : function(record) {
			price.textbox('setValue', record.price);
			money.textbox('setValue', amount.numberspinner('getValue')*price.numberspinner('getValue'));
		}
	});
	//联动，修改数量之后修改总价
	amount.numberspinner({
		onChange:function(){
			money.textbox('setValue', amount.numberspinner('getValue')*price.numberspinner('getValue'));
		}
	});
})
//取消
function clearOrderDetailadd() {
	$('#orderdetail-adddlg').window('close');
}
//保存订单明细
function saveOne(type){
	var form = $("#orderdetail-form");
	console.log(form.serialize());
	//校验表单数据
	if(form.form("validate")){
		$.post("crm/orderDetail/saveone?type"+type,form.serialize(),function(rs){
			alert(rs);
			if(rs.err){
				alert(rs.err);
			}else{
				alert("保存成功！");
            	//关闭当前窗口
				$('#orderdetail-adddlg').window('close');
				$("#orderDetail-dg").datagrid("reload");
			}
		});
	}
}
$(function() {
	$("#orderDetail-dg") .datagrid( {
		width : "100%",
		columns : [ [				
			{
				field : 'product',
				title : '产品',
				width : 120,
				formatter : function(value, row, index) {
					return value ==null ? null:value.name
							+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
				}
			},					
			{
				field : 'price',
				title : '单价',
				width : 60
			},					
			{
				field : 'amount',
				title : '数量',
				width : 60,
			},
			{
				field : 'money',
				title : '总价',
				width : 60,
			},
			{
				field : 'customer',
				title : '对应客户',
				width : 120,
				formatter : function(value, row, index) {
					return value ==null ? null:value.name
							+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
				}
			},
			{
				field : 'order',
				title : '对应订单',
				width : 120,
				formatter : function(value, row, index) {
					return value ==null ? null:value.topic
							+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
				}
			},
			{
				field : 'state',
				title : '状态',
				width : 60,
			},
			{
				field : 'note',
				title : '备注',
				width : 120
			},
		] ],
	});
	//联动，选择产品之后确定单价
	$('#product').combobox({
		onSelect : function(record) {
			$("#price").val(record.price);
		}
	});
	//联动，修改数量之后修改总价
	$("#amount").spinner({
		onSpinUp:function(){
			$("#money").val($("#amount").getValue()*$("#price").val());
		},
		onSpinDown:function(){
			$("#money").val($("#amount").getValue()*$("#price").val());
		}
	})
})
function newOrderdetail() {
	$('#orderdetail-adddlg').window('open').window('center');
}
function clearOrderDetailadd() {
	$('#orderdetail-adddlg').window('close');
}


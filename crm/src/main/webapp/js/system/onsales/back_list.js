$(function(){
	$('#back-dg') .datagrid( {
		width : "100%",
		columns : [ [
			{field:'number',title:'退货单号',width:60},
			{field:'date',title:'时间',width:60,formatter : function(value){
                var date = new Date(value);
                var y = date.getFullYear();
                var m = date.getMonth() + 1;
                var d = date.getDate();
                return y + '-' +m + '-' + d;
            }},
			{field:'money',title:'应退金额',width:60},
			{field:'retiredmoney',title:'已退金额',width:60},
			{field:'state',title:'状态',width:60},
			{field:'employee',title:'执行人',width:60,formatter:function(value,row,index){
	        	return value ==null ? null:value.empName;
	        }},
			{field:'order',title:'对应订单',width:60,formatter:function(value,row,index){
	        	return value ==null ? null:value.topic;
	        }},
	        {field:'note',title:'备注',width:60,formatter:function(value,row,index){
	        	return value ==null ? "无":value;
	        }},
	        {field:'operation',title:'操作',width:100,  
		        formatter:function(value,row,index){ 
		        	return '<a title="结束" onclick="finishBack('+index+')"><i class="iconfont icon-biaodanwancheng2"></i></a>';
		    }},
		] ],			
	});
	$('#backadd-customer-cc').combogrid({
		onSelect : function(record) {
			$("#backadd-order-cc").combogrid({
				url:'crm/order/getOrderByCus?customerid='+$('#backadd-customer-cc').combogrid('getValue'),
			});
		}
	});
	$('#backadd-order-cc').combogrid({
		onSelect : function(record) {
			$('#confirm-fahuo-detail').datagrid({
				url : "crm/orderDetail/getDetailByOrder/" + $('#backadd-order-cc').combogrid('getValue'),
				method : "GET",
				queryParams : {
					state: '已发货',
				},
				onLoadSuccess : function() {
					$('#confirm-order-detail').datagrid("selectAll");
					$.messager.alert("消息","数据加载成功！<br>注：如果没有可以退货的产品表格会显示空的。","info");
				},
				onLoadError:function(){
					$.messager.alert("消息","数据加载失败了","error");
				}
			});
//			$("#confirm-fahuo-detail").form('load', row);
//			$("input[name='order-back-orderid']").val(row.id);
		}
	
	});
})
//保存退货
function saveBack(){
	var form = $("#back-form");
	var rows;
	// 校验表单数据
	if(form.form("validate")){
		var orderid = $("#back-form input[name='orderid']").val();
		var tp = $("#back-form input[name='tp']:checked").val();
		var note = $("#back-form input[name='note']").val();
		if(tp==0){//整单退
			rows = $('#confirm-fahuo-detail').datagrid('getRows')// 获取当前的数据行
		}else{
			// 返回复选框选中的所有行.
			rows = $('#confirm-fahuo-detail').datagrid("getSelections");
		}
		if (rows.length == 0) {
			$.messager.alert("提示", "您还没有选中产品呐！", "warning");
		} else {
			$.ajax({
				url : 'crm/backDetail/saveBackDetail/' + orderid,
				type : 'post',
				dataType : 'json',
				data : {
					"orderDetails" : JSON.stringify(rows),
					"note":note,
				// JSON.stringify() 方法用于将JavaScript值转换为 JSON
				// 字符串，缺少此句传到后台的则为空值
				},
				success : function(response) { // 后台返回的要显示的消息
					if(response.err){
						$.messager.alert("消息","错误了！","error");
					}else{
						$.messager.alert("消息","退货成功！","info");
						$('#backdlg').window('close');
						$("#order-dg").datagrid("reload");
					}
				}
			});
		}
	}
}
//结束退货单
function finishBack(index){
	$('#back-dg').datagrid('selectRow',index);
	var row = $('#back-dg').datagrid('getSelected');
	if (row) {
		$.messager.confirm("提示","您确定要完成该退货单吗？",function(r){
			if(r){
				$.get("crm/back/finishBack/"+row.id,function(rs){
					console(rs);
					if(rs.err){
						$.messager.alert("提示","后台失败了！失败的原因你猜一下。");
					}else{
					}
					$.messager.alert("提示",rs.mesage);
					$('#back-dg').datagrid("reload");
                }).error(function(){
                	$.messager.alert("提示","请求失败了!",'warning');
                });
			}
		});
	}
}
function newback(){
	$("#backadd-window").window("open").window('center');
}
$(function() {
	// 初始化表格
	$('#plan-receicable-dg')
			.datagrid(
					{
						width : "100%",
						columns : [ [
								{
									field : 'installment',
									title : '期数',
									width : 40
								},
								{
									field : 'planMoney',
									title : '计划回款金额',
									width : 60
								},
								{
									field : 'planTime',
									title : '计划回款时间',
									width : 60,
									formatter : function(value) {
										var date = new Date(value);
										var y = date.getFullYear();
										var m = date.getMonth() + 1;
										var d = date.getDate();
										return y + '-' + m + '-' + d;
									}
								},
								{
									field : 'customer',
									title : '对应客户',
									width : 120,
									formatter : function(value, row, index) {
										return value==null?null:value.name
												+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
									}
								},
								{
									field : 'order',
									title : '对应订单',
									width : 120,
									formatter : function(value, row, index) {
										return value==null?null:value.topic
												+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
									}
								},
								{
									field : 'state',
									title : '回款状态',
									width : 60
								},
								{
									field : 'employee',
									title : '负责人',
									width : 120,
									formatter : function(value, row, index) {
										return value ==null ? null:value.empName
												+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
									}
								},
								{
									field : 'note',
									title : '备注',
									width : 120
								},
								{
									field : 'op',
									title : '操作',
									width : 60,
									formatter : function() {
										return '<a title="回款" onclick="receicable()"><i class="iconfont icon-link"></i></a>';
									}
								},
						] ],
					});
	// 联动
	$('#receicableadd-customer-cc').combogrid({
		onSelect : function(record) {
			$("#receicableadd-order-cc").combogrid({
				url:'crm/order/getOrderByCus?customerid='+$('#receicableadd-customer-cc').combogrid('getValue'),
			});
		}
	});
	$('#receicableadd-order-cc').combogrid({
		onSelect : function(record) {
			$("#receicableadd-planMoney-cc").combogrid({
				url:'crm/order/getMoney?orderid='+$('#receicableadd-order-cc').combogrid('getValue'),
			});
		}
	});
})

function newReceicablePlan() {
	$("#receicableadd-window").window("open").window('center');
	$("#receicableadd-form").form('clear');
}
function clearReceicableadd() {
	$("#receicableadd-window").window("close");
}
// 回款
function receicable() {
	var row = $('#plan-receicable-dg').datagrid("getSelected");
	console.log();
	if (row) {
		$("#receicable-window").window("open").window('center');
		$("#receicable-form").form('load', row);
	}
}
function clearReceicableadd1() {
	$("#receicable-window").window("close");
}
// 保存回款
function saveReceicable1() {
	var form = $("#receicable-form");
	// 校验表单数据
	if (form.form("validate")) {
		$.post("crm/receivable/save", form.serialize(), function(rs) {
			if (rs.err) {
				alert(rs.err);
			} else {
				alert("保存成功！");
				// 关闭当前窗口
				$("#receicableadd-window").dialog("close");
				$('#plan-receicable-dg').datagrid("reload");
			}
		});
	}
}
// 新建保存
function saveReceicable() {
	var form = $("#receicableadd-form");
	// 校验表单数据
	if (form.form("validate")) {
		$.post("crm/receivable/save", form.serialize(), function(rs) {
			if (rs.err) {
				alert(rs.err);
			} else {
				alert("保存成功！");
				// 关闭当前窗口
				$("#receicableadd-window").dialog("close");
				$('#plan-receicable-dg').datagrid("reload");
			}
		});
	}
}
$("#changeMoney").change(function() {
	$("#planMoney").val();
});
//获取当前日期yyyy-MM-dd
function nowtime(){//将当前时间转换成yyyymmdd格式
    var mydate = new Date();
    var str = "" + mydate.getFullYear();
    var mm = mydate.getMonth()+1
    if(mydate.getMonth()>9){
     str +="-" +mm;
    }
    else{
     str += "-0" + mm;
    }
    if(mydate.getDate()>9){
     str += "-"+mydate.getDate();
    }
    else{
     str += "-0" + mydate.getDate();
    }
    return str;
  }
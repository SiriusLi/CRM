$(function(){
	$('#deliveryDetail-dg') .datagrid( {
		width : "100%",
		columns : [ [
			{ field:'product',title:'产品',width:60,formatter:function(value,row,index){
	        	return value ==null ? null:value.name;
	        }},  
			{ field:'amount',title:'数量',width:60},  
			{ field:'money',title:'总价',width:60},  
			{ field:'costomer',title:'对应客户',width:120,formatter:function(value,row,index){
	        	return value ==null ? null:value.name;
	        }},  
			{ field:'order',title:'对应订单',width:120,formatter:function(value,row,index){
	        	return value ==null ? null:value.topic;
	        }},  
			{ field:'note',title:'备注',width:60},  

		] ],			
	});
});
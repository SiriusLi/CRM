$(function() {
	var leftTable = $("#order-add-detail-productDatagrid");
	var rightTable = $('#order-add-detail-dg');
	// 产品表
	leftTable.datagrid({
				title : "产品列表",
				url : 'product/all',
				fitColumns : true,
				rownumbers : true,
				fit : true,
				singleSelect : true,
				pagination : true,
				idField : 'id',
				onLoadSuccess : function() {
					$(this).datagrid('enableDnd');// 设计可拖动
				},
				columns : [ [
						{
							field : 'id',
							title : 'id',
							width : 10
						},
						{
							field : 'name',
							title : '商品名字',
							width : 30
						},
						{
							field : 'unit',
							title : '单位',
							width : 15
						},
						{
							field : 'standard',
							title : '规格',
							width : 15
						},
						{
							field : 'price',
							title : '售价',
							width : 15
						},
						{
							field : 'type',
							title : '类型',
							width : 15
						},
						{
							field : 'operation',
							title : '操作',
							width : 10,
							formatter : function() {
								return '<a title="添加" onclick="add()"><i class="iconfont icon-gouwucheman"></i></a>'
							}
						}, ] ],
			});
	// 开启过滤
	leftTable.datagrid('enableFilter', [
			{
				field : 'type',
				type : 'combobox',
				options : {
					panelHeight : 'auto',
					data : [ {
						value : '',
						text : '全部'
					}, {
						value : '饼干',
						text : '饼干'
					}, {
						value : '饮料',
						text : '饮料'
					}, {
						value : '瓜子',
						text : '瓜子'
					} ],
					onChange : function(value, row, index) {
						if (value == '') {
							leftTable.datagrid('removeFilterRule', 'type')
						} else {
							leftTable.datagrid('addFilterRule', {
								field : 'type',
								op : 'equal',
								value : value
							});
						}
						leftTable.datagrid('doFilter');
					}
				}
			},
			{
				field : 'price',
				type : 'numberbox',
				options : {
					precision : 2
				},
				op : [ 'equal', 'notequal', 'less', 'greater', 'lessorequal',
						'greaterorequal' ]
			}, ]);
	var lastIndex;
	var orderid=$("input[name='orderid']").val();
	var type=$("input[name='type']").val();
	var url;
	if(type!=null){
		url="crm/orderDetail/getDetailByOrder/"+orderid;
	}else{
		url:"crm/orderDetail/getQuotationDetails/"+orderid;
	}
	rightTable
			.datagrid({
				url:url,
				onLoadSuccess : function() {
					addLastRow();
					updateLastRow();
				},
				columns : [ [
						{
							field : 'pid',
							title : 'id',
							width : 10
						},
						{
							field : 'productName',
							title : '商品名',
							width : 30
						},
						{
							field : 'price',
							title : '单价',
							width : 15,
							editor:"numberbox" ,
							precision:1,
						},
						{
							field : 'amount',
							title : '数量',
							width : 15,
							editor : "numberspinner"
						},
						{
							field : 'money',
							title : '总价',
							width : 15,
							editor:"numberbox",
						},
						{
							field : 'note',
							title : '备注',
							width : 15 , 
							editor:"text"
						},
						{
							field : 'op',
							title : '操作',
							width : 10,
							formatter : function() {
								return '<a title="删除" onclick="deletea()"><i class="iconfont icon-msnui-trash"></i></a>'+
								'<a title="编辑" onclick="edita()"><i class="iconfont icon-bianji"></i></a>'
							}
						}, ] ],
			});
});

// 添加一行
function add() {
	var lrow = $("#order-add-detail-productDatagrid").datagrid('getSelected');
	if (lrow != null) {
		var rows = $('#order-add-detail-dg').datagrid('getRows')// 获取当前的数据行
		// 判断该产品是否已经在报价明细表里
		var ifExist = false;
		var index = -1;
		for (var i = 0; i < rows.length - 1; i++) {
			if (lrow.id == rows[i]['pid']) {
				index = i;
				ifExist = true;
				break;
			}
		}
		if (ifExist) {
			// 已存在
			$('#order-add-detail-dg').datagrid(
					'updateRow',
					{
						index : index,
						row : {
							amount : parseInt(rows[index]['amount']) + 1,
							money : (parseFloat(rows[index]['amount']) + 1)
									* (parseFloat(rows[index]['price'])),
						}
					});
		} else {
			var op = '<a title="删除" onclick="deletea()"><i class="iconfont icon-msnui-trash"></i></a>';
			var rrow = $('#order-add-detail-dg').datagrid('getRows')// 获取当前的数据行
			// // 添加新的一行
			$('#order-add-detail-dg').datagrid('insertRow', {
				index : rrow.length - 1, // index start with 0
				row : {
					pid : lrow.id,
					productName : lrow.name,
					price : lrow.price,
					amount : 1,
					money : lrow.price * 1,
					op : op,
				}
			});
		}
		updateLastRow();
	}
}
// 删除一行
function deletea() {
	var row = $('#order-add-detail-dg').datagrid('getSelected');
	if (row) {
		var rowIndex = $('#order-add-detail-dg').datagrid('getRowIndex', row);
		$.messager.confirm('提示', '确定要删除该产品吗?', function(r) {
			if (r) {
				$('#order-add-detail-dg').datagrid('deleteRow', rowIndex);
				updateLastRow();
			}
		});
	}
}
function edita(){
	var row = $('#order-add-detail-dg').datagrid('getSelected');
	if(row){
		var rowIndex = $('#order-add-detail-dg').datagrid('getRowIndex', row);
		$.messager.prompt('修改数量', '请输入数量:', function(r){
			if(r){
				if(/^\d+$/.test(r)){
					$('#order-add-detail-dg').datagrid('updateRow',{
						index: rowIndex,
						row: {
							pid: row.pid,
							productName:row.productName,
							price: row.price,
							amount:r,
							money:row.price*r,
							note:row.note ,
							op:row.op,
						}
					});
					updateLastRow();
				}else{
					$.messager.alert("提示","非法输入！",'warning');
				}
				
			}
		});
		
	}
}
// 更新最后一行
function updateLastRow() {
	var rows = $('#order-add-detail-dg').datagrid('getRows')// 获取当前的数据行
	var ptotal = 0// 计算listprice的总和
	, utotal = 0;// 统计unitcost的总和
	for (var i = 0; i < rows.length - 1; i++) {
		ptotal += parseFloat(rows[i]['amount']);
		utotal += parseFloat(rows[i]['money']);
	}
	// 更新最后一行
	$('#order-add-detail-dg').datagrid('updateRow', {
		index : rows.length - 1,
		row : {
			amount : ptotal,
			money : utotal,
		}
	});
}
// 添加一行至末尾
function addLastRow() {
	$('#order-add-detail-dg').datagrid('appendRow', {
		pid : '总计',
		amount : 0,
	});
}
// 保存右边表格数据，提交到后台
function saveDetail() {
	var orderid = $("input[name='orderid']").val();
	var rows = $('#order-add-detail-dg').datagrid('getRows')// 获取当前的数据行
	if (rows.length < 2) {// 如果没有产品
		$.messager.confirm('提示', '还没有添加任何产品，要继续吗?', function(r) {
			if (r) {
				$.messager .confirm( '提示', '是否要建立回款明细？', function(r) {
					if (r) {
						window.location.href = 'http://localhost:8080/crm/crm/order/addReceivable/'
								+ orderid;
					}		
				});
			}
		});
	} else {
		var totalMoney = 0;
		var param = new Array();
		for (var i = 0; i < rows.length - 1; i++) {
			var object = new Object();
			totalMoney = +rows[i]['money'];
			object.pid = rows[i]['pid'];
			object.amount = rows[i]['amount'];
			object.price = rows[i]['price'];
			object.money = rows[i]['money'];
			object.note = rows[i]['note'];
			param.push(object);
		}
		$.ajax({
			type : "POST",
			url : "crm/orderDetail/save/" + orderid,
			data : {
				"param" : JSON.stringify(param)
			},
			dataType : "json",
			traditional : true,
			contentType : "application/x-www-form-urlencoded; charset=utf-8",
			async : false,
			success : function(data) {
				if(data.order.orderMoney-data.order.receivableMoney>0){
					$.messager .confirm( '消息', '保存订单明细成功！<br><br>点击确认，新建回款计划', function(r) {
						if (r) {
							window.location.href = 'http://localhost:8080/crm/crm/order/addReceivable/'
								+ data.order.id;}
					});
				}else{
					$.messager .alert( '消息', '保存订单明细成功');
					window.top.opener = null; 
					window.close();
				}
			},
			error : function() {
				alert("保存失败");
			}		
		});
	}

}
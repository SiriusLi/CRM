$(function(){
	$("#receicable-record-dg").datagrid({
		width:'100%',
		columns:[[
			{
				field : 'installment',
				title : '期数',
				width : 40
			},
			{
				field : 'actuaMoney',
				title : '回款金额',
				width : 60
			},
			{
				field : 'actualTime',
				title : '回款时间',
				width : 60,
				formatter : function(value) {
					var date = new Date(value);
					var y = date.getFullYear();
					var m = date.getMonth() + 1;
					var d = date.getDate();
					return y + '-' + m + '-' + d;
				}
			},
			{
				field : 'customer',
				title : '对应客户',
				width : 120,
				formatter : function(value, row, index) {
					return value ==null ? null:value.name
							+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
				}
			},
			{
				field : 'order',
				title : '对应订单',
				width : 120,
				formatter : function(value, row, index) {
					return value ==null ? null:value.topic
							+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
				}
			},
			{
				field : 'paymentWay',
				title : '付款方式',
				width : 60
			},
			{
				field : 'receipt',
				title : '是否开发票',
				width : 60,
				formatter : function(value, row, index) {
					return value ? '是':'否';
				}
			},
			{
				field : 'payee',
				title : '记录人',
				width : 120,
				formatter : function(value, row, index) {
					return value ==null ? null:value.empName
							+ '<a title="详情" onclick="showOrder()"><i class="iconfont icon-link"></i></a>';
				}
			},
			{
				field : 'note',
				title : '备注',
				width : 120,
			},
			{
				field : 'op',
				title : '操作',
				width : 60,
			},

		]]
	});
})
//$(function(){
//	$.ajaxSetup({
//		error : function(XMLHttpRequest, textStatus, errorThrown) {
//			if (XMLHttpRequest.status == 403) {
//				$.messager.alert('提示', '限制访问此资源或进行此操作', 'info');
//				return false;
//			}
//		},
//		complete : function(XMLHttpRequest, textStatus) {
//			alert();
//			var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus"); // 通过XMLHttpRequest取得响应头,sessionstatus，
//			if (sessionstatus == 'timeout') {
//				// 如果超时就处理 ，指定要跳转的页面
//				alert("哈哈，这里执行了！");
//			}
//		}
//	});
//});
$(function(){
	$.ajaxSetup({
	    type: 'POST',
	    complete: function(xhr,status) {
	        var sessionStatus = xhr.getResponseHeader('sessionstatus');
	        if(sessionStatus == 'timeout') {
	            var yes = confirm('您的身份已过期, 请重新登录');
	            $("#login-dialog").dialog('open');
	        }
	    }
	});
	$.ajaxSetup({
	    type: 'GET',
	    complete: function(xhr,status) {
	        var sessionStatus = xhr.getResponseHeader('sessionstatus');
	        if(sessionStatus == 'timeout') {
	            var yes = confirm('您的身份已过期, 请重新登录.');
	            $("#login-dialog").dialog('open');
	        }
	    }
	});
})

$(function() {
	function dateFormatter (value) {
	    var date = new Date(value);
	    var year = date.getFullYear().toString();
	    var month = (date.getMonth() + 1);
	    var day = date.getDate().toString();
	    var hour = date.getHours().toString();
	    var minutes = date.getMinutes().toString();
	    var seconds = date.getSeconds().toString();
	    if (month < 10) {
	        month = "0" + month;
	    }
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (hour < 10) {
	        hour = "0" + hour;
	    }
	    if (minutes < 10) {
	        minutes = "0" + minutes;
	    }
	    if (seconds < 10) {
	        seconds = "0" + seconds;
	    }
	    return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;
	}
	
	 $("#repairwx").datagrid({
		title : '维修工单',
		width : "100%",
		height:"800",
		view : detailview,
		pagination:true,
		pageSize:10,
		
		detailFormatter : function(rowIndex, rowData) {
			//返回工单明细表格
			return '<table class="detail"></table>';
		},
		onExpandRow : function(index, row) {
			//找到该父类Formatter的子类表格
			var detail = $(this).datagrid('getRowDetail',index).find('table.detail');
			//初始化表格
			detail.datagrid({
				url:'aftersale/detail/'+row.id,
				fitColumns:true,
                singleSelect:true,
                rownumbers:true,
                loadMsg:'加载中，请稍后...',
                height:'auto',
                columns:[[
                    {field:'progress',title:'维修进度',width:100,align:'center',sortable:true}, 
                    {field:'time',title:'开始时间',width:100,align:'center',sortable:true,formatter:function(value,row,index){return dateFormatter(value);}},
                    {field:'content',title:'备注',width:160,align:'center',sortable:true}, 
                    {field:'employee',title:'操作员工',width:100,align:'center',sortable:true,formatter:function(row,value,index){return new Object(value['employee']).empName;}}, 


                ]],
			})
			$('#repairwx').datagrid('fixDetailRowHeight', index);
		}
	});
	
	//开启过滤
	//$("#repairwx").datagrid('enableFilter').datagrid('doFilter');
	// 为工具栏按钮绑定事件
	 
	 //为数据导出绑定事件
	 $(".sjdc a").click(function(){
		 $.ajax({
			 type:"POST",
			 url:"aftersale/export",
			 success:function(data){
				 window.open('aftersale/export'); 
			 }
		 })
	 })
	 
	 //为刷新添加功能
	 $(".sx a").click(function(){
		 $("#repairwx").datagrid("reload");
	 })
	
	//为高级查询按钮绑定事件
	$(".gjcx a").click(function() {
		console.log("进入了高级查询");
		var dialog = $("<div/>").dialog(
						{
							title : "高级查询",
							width : 450,
							modal : true,
							//跳到对应的表单
							href : 'aftersale/highcheck',
							onClose : function() {
								//关闭窗户后，立即销毁窗口
								$(this).dialog("destroy");
							},
							buttons : [
									{
										iconCls : "icon-ok",
										text : "查询",
										handler : function() {
											var form = $("#highcheck");
											//校验表单数据
											if (form.form("validate")) {
												$.post(
														"aftersale/highcheck/submit",
														form.serialize(),
														function(rs) {
																	if (rs.err) {
																		alert(rs.err);
																	} else {
																		//关闭当前窗口
																		console.log(rs);
																		dialog.dialog("close");
																		
																		$("#repairwx").datagrid("loadData",{'total':rs.total, rows:rs.rows });
																		
																	}
																});
											}
										}
									},
									{
										iconCls : "icon-cancel",
										text : "取消",
										handler : function() {
											//关闭当前窗口
											dialog.dialog("close");
										}
									} ]
						});
	});
	
	//为工具栏按钮绑定事件
	$(".zj a").click(function() {
						console.log("进入了");
						var dialog = $("<div/>").dialog(
										{
											title : "新增维修工单",
											top:40,
											width : 610,
											modal : true,
											//跳到对应的表单
											href : 'aftersale/add',
											onClose : function() {
												//关闭窗户后，立即销毁窗口
												$(this).dialog("destroy");
											},
											buttons : [
													{
														iconCls : "icon-ok",
														text : "保存",
														handler : function() {
															var form = $("#repairForm");
															//校验表单数据
															if (form.form("validate")) {
																$.post(
																		"aftersale/save",
																		form.serialize(),function(rs) {
																					if (rs.err) {
																						alert(rs.err);
																					} else {
																						//alert("222");
																						//关闭当前窗口
																						dialog.dialog("close");
																						$("#repairwx").datagrid("reload");
																						
																					}
																				});
															}
														}
													},
													{
														iconCls : "icon-cancel",
														text : "取消",
														handler : function() {
															//关闭当前窗口
															dialog.dialog("close");
														}
													} ]
										});
					});
});
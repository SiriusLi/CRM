$(function () {
	var grid = $("#cusGird");
	//初始化表格
	grid.datagrid({
		fit:true,
		border:true,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		rownumbers:true,
		url:'cus/list',
		idField:'id',
		toolbar:'#cusToolbar',
		columns:[[
			{field:'id',title:'id',width:50,align:'center'},
			{field:'name',title:'客户名称',width:100,align:'center',formatter:function(value, row, index){
				return '<a style="color:blue" href="/crm/cus/view/'+row.id+'">'+row.name+'</a>';
				
			}},
			{field:'contacts',title:'联系人',width:100,align:'center'},
			{field:'lifecycle',title:'生命周期',width:100,align:'center'},
			{field:'employee',title:'所有者',width:100,align:'center',formatter : function(value, row, index) {
				return value.empName;
			}},
			{field:'createDate',title:'创建日期',width:100,formatter : function(value, row, index) {
				var time = new Date(value).Format("yyyy-MM-dd");
				return time;
			}},
			{field:'orderMoney',title:'合同金额',width:100,align:'center'},
			{field:'receivableMoney',title:'回款金额（未回）',width:100,align:'center'},
			

		]]
	});
	
	//为工具栏按钮添加绑定事件
	$("#cusToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增客户",
			width:450,
			height:410,
			modal:true,
			href:'cus/cusform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#cusForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("cus/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(true){
			var dialog = $("<div/>").dialog({
				title: "编辑客户",
				width: 800,
				height: 660,
				modal:true,
				href:'cus/cusmoreform/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#cusMoreForm");
							//校验表单数据
							if(form.form("validate")){
								console.log(form.serialize());
								$.post("cus/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
        if (row){
            $.messager.confirm('警告','您确定要删除此信息吗？',function(r){
                if (r){
                    $.post('cus/delete/'+row.id,function(result){
                        if (result.success){
                        	grid.datagrid("reload");  
                        } else {
                        	$.messager.alert("提示！","系统错误，删除失败！");
                        }
                    });
                }
            });
        }
	});
	
	Date.prototype.Format = function(fmt) {    
		var o = {
			"M+" : this.getMonth() + 1, //月份   
			"d+" : this.getDate(), //日   
			"H+" : this.getHours(), //小时   
			"m+" : this.getMinutes(), //分   
			"s+" : this.getSeconds(), //秒   
			"q+" : Math.floor((this.getMonth() + 3) / 3),
			"S" : this.getMilliseconds()
		//毫秒   
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1,
						(RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k])
								.substr(("" + o[k]).length)));
		return fmt;
	};
	
});
 
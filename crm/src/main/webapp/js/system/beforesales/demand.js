$(function () {
	var grid = $("#demandGird");
	//初始化表格
	grid.datagrid({
		fit:true,
		border:true,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		rownumbers:true,
		url:'demand/list',
		idField:'id',
		toolbar:'#demandToolbar',
		columns:[[
			{field:'id',title:'id',width:50,align:'center'},
			{field:'topic',title:'需求主题',width:100,align:'center'},
			{field:'salesOpp',title:'对应机会',width:100,align:'center',formatter : function(value, row, index) {
				return value.topic;
			}},
			{field:'importance',title:'重要程度',width:100,align:'center'},
			{field:'empName',title:'需求提供人',width:100,align:'center',formatter: function(value,row,index){
				return new Object(row["salesOpp"]["customer"]["employee"]).empName;
			}},
			{field:'recordTime',title:'记录时间',width:100,align:'center',formatter : function(value, row, index) {
				var time = new Date(value).Format("yyyy-MM-dd");
				return time;
			}}
		]]
	});
	
	//为工具栏按钮添加绑定事件
	$("#demandToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增需求",
			width:450,
			top:70,
			modal:true,
			href:'demand/demandform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#demandForm");
						//校验表单数据
						console.log(form.serialize());
						if(form.form("validate")){
							$.post("demand/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑需求",
				width: 450,
				top:70,
				modal:true,
				href:'demand/demandform/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#demandForm");
							//校验表单数据
							
							if(form.form("validate")){
								$.post("demand/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
        if (row){
            $.messager.confirm('警告','您确定要删除此信息吗？',function(r){
                if (r){
                    $.get('demand/delete/'+row.id,function(result){
                        if (result.success){
                        	grid.datagrid("reload");  
                        } else {
                        	$.messager.alert("提示！","系统错误，删除失败！");
                        }
                    });
                }
            });
        }
	});
	
	Date.prototype.Format = function(fmt) {    
		var o = {
			"M+" : this.getMonth() + 1, //月份   
			"d+" : this.getDate(), //日   
			"H+" : this.getHours(), //小时   
			"m+" : this.getMinutes(), //分   
			"s+" : this.getSeconds(), //秒   
			"q+" : Math.floor((this.getMonth() + 3) / 3),
			"S" : this.getMilliseconds()
		//毫秒   
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1,
						(RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k])
								.substr(("" + o[k]).length)));
		return fmt;
	};
	
});
 
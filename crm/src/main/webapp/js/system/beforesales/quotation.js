$(function(){
	var qd = $('#quotationDatagrid'); 
	var qt = $('#quotationToolbar');
	$.extend($.fn.datagrid.defaults.filters,{
		dataRange:{
			init:function(container,options){
				var c = $("<div style='display:inline-block'>&nbsp;&nbsp;从：<input class='d1'>&nbsp;&nbsp;&nbsp;&nbsp;到：<input class='d2'></div>").appendTo(container);
				c.find('.d1,.d2').datebox();
				return c;
			},
			destory:function(target){
				$(target).find('.d1,.d2').datebox('destroy');
			},
			getValue:function(target){
				var d1 = $(target).find('.d1');
				var d2 = $(target).find('.d2');
				//拼接两个日期
				return d1.datebox('getValue')+':'+d2.datebox('getValue');
			},
			setValue:function(target,value){
				console.log("setValue传进来的参数："+value);
				var d1 = $(target).find('.d1');
				var d2 = $(target).find('.d2');
				var vv = value.split(':');
				d1.datebox('setValue',vv[0]);
				d2.datebox('setValue',vv[1]);
				
			},
			resize:function(target,width){
				$(target)._outerWidth(width)._outerHeight(22);
				//各占一半
				$(target).find('.d1,.d2').datebox('resize',width/3);
			},
		}
	});
	qd.datagrid({
		fitColumns:true,
		fit:true,
		width:'100%',
		height:400,
		singleSelect:true,
		pagination:true,
		url:'quotation/all',
		toolbar:'#quotationToolbar',
		idField:'id',
		columns:[[
			{field:'id',title:'id',width:20,align:'center'},
			{field:'money',title:'报价金额',width:40,align:'center',formatter:function(value,row,index){
				return value.toFixed(2);
			}},
			{field:'approval',title:'审批结果',width:30,align:'center'},
			
			{field:'auditDate',title:'审核时间',width:120,align:'center',formatter:function(value,row,index){
				if(value==undefined){
					return "";
				}
				var date = new Date(value);
				var year = date.getYear()+1900;
				var month = date.getMonth()+1;
				if (month<10) {
					month = "0"+month;
				}
				var day = date.getDate();
				if(day<10){
					day = "0"+day;
				}
				var hour = date.getHours();
				if(hour<10){
					hour = "0"+hour;
				}
				var min = date.getMinutes();
				if(min<10){
					min = "0"+min;
				}
				var sec = date.getSeconds();
				if(sec<10){
					sec = "0"+sec;
				}
				return year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec;
			}},
			{field:'salesOpp',title:'对应销售机会',width:50,align:'center',formatter:function(value,row,index){
				return value.topic;
			}},
			{field:'customer',title:'客户',width:50,align:'center',formatter:function(value,row,index){
				return value.name;
			}},
			{field:'employee',title:'报价员',width:20,align:'center',formatter:function(value,row,index){
				return value.empName;
			}},
			{field:'approvalEmployee',title:'审批员',width:20,align:'center',formatter:function(value,row,index){
				return row.approvalStatus ? value.empName:'未审批';
			}},
			{field:'time',title:'创建时间',width:120,align:'center',formatter:function(value,row,index){
				if(value==undefined){
					return "";
				}
				var date = new Date(value);
				var year = date.getYear()+1900;
				var month = date.getMonth()+1;
				if (month<10) {
					month = "0"+month;
				}
				var day = date.getDate();
				if(day<10){
					day = "0"+day;
				}
				var hour = date.getHours();
				if(hour<10){
					hour = "0"+hour;
				}
				var min = date.getMinutes();
				if(min<10){
					min = "0"+min;
				}
				var sec = date.getSeconds();
				if(sec<10){
					sec = "0"+sec;
				}
				return year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec;
			}}
			
		]]
	});
	//开启过滤
	qd.datagrid('enableFilter',[
		{
			field:'approvalStatus',
			type:'combobox',
			options:{
				panelHeight:'auto',
				//下拉选项的定义
				data:[
					{value:'',text:'全部'},
	    			{value:'已审批',text:'已审批'},
	    			{value:'未审批',text:'未审批'}
				],
				onChange:function(value,row,index){
					//选中全部
					if(value==''){
						//移除过滤规则
						qd.datagrid('removeFilterRule','approvalStatus');
					}else {
						qd.datagrid('addFilterRule',{
							field:'approvalStatus',
							op:'equal',
							value:value
						});
					}
					//执行
					qd.datagrid('doFilter');
				}
			}
		},
		{
			field:'approvalResult',
			type:'combobox',
			options:{
				panelHeight:'auto',
				//下拉选项的定义
				data:[
					{value:'',text:'全部'},
	    			{value:'已通过',text:'已通过'},
	    			{value:'未通过',text:'未通过'},
	    			{value:'未审批',text:'未审批'}
				],
				onChange:function(value,row,index){
					//选中全部
					if(value==''){
						//移除过滤规则
						qd.datagrid('removeFilterRule','approvalResult');
					}else {
						qd.datagrid('addFilterRule',{
							field:'approvalResult',
							op:'equal',
							value:value
						});
					}
					//执行
					qd.datagrid('doFilter');
				}
			}
		},
		{
			field:'money',
			type:'numberbox',
			options:{precision:2},
			op:['equal','notequal','less','greater','lessorequal','greaterorequal']
		},
		{
			field:'time',
			type:'dataRange',
			op:['between']
		},
		{
			field:'auditDate',
			type:'dataRange',
			op:['between']
		}
	]);
	//初始化按钮
	qt.on('click','a.add',function(){
		//添加按钮初始化
		
    	var dialog = $('<div/>').dialog({
    		title:'新增报价单',
    		width:400,
    		height:200,
    		modal:true,
    		href:'quotation/form',
    		onClose:function(){
    			//关闭窗户后，立即销毁窗口
    			$(this).dialog('destroy');
    		},
    		buttons:[
    			{
    				iconCls:'icon-ok',
    				text:'保存',
    				handler:function(){
    					//获得表格的信息
    					var form = $("#quotationForm");
    					console.log(form);
    					console.log(form.serialize());
    					if(form.form("validate")){
    						$.post("quotation/save",form.serialize(),function(rs){
    							if(rs.err){
    								alert(rs.err);
    							}else{
    								$.messager.confirm('消息', '保存报价单成功！<br><br>点击确认，新建报价明细', function(r){
    									if (r){
    										window.open("http://localhost:8080/crm/quotation/adddetail/"+rs.newOrder.id);
    									}
    									// 关闭当前窗口
    									dialog.dialog("close");
    									qd.datagrid("reload");
    								});
    							}
    						})
    					}
    				}
    			},
    			{
    				iconCls:'icon-cancel',
    				text:'取消',
    				handler:function(){
    					dialog.dialog("close");
    				}
    			}
    		]
    	});
	}).on('click','a.edit',function(){
		//编辑按钮初始化
    	var row = qd.datagrid('getSelected');
    	//如果row存在
    	if(row){
    		$('#order-editdlg1').window('open').window('center').window('setTitle',
    		'编辑报价单');
    		console.log(row);
    		$('#orderedit-form1').form('load', row);
    		//start````````
    		$('#orderedit-form1').form('load',{
    			audit1:row.approval==null?null:row.approval,
    			salesOppid:row.salesOpp==null?null:row.salesOpp.id,
    			customerid:row.customer==null?null:row.customer.id,
    			employeeid:row.employee==null?null:row.employee.id,
    			auditDate1:row.auditDate==null?null:GMTToStr(row.auditDate),
    			auditEmployeeid:row.auditEmployee==null?null:row.auditEmployee.id,
    			time1:row.time==null?null:GMTToStr(row.time),
    		});
    		//end``````````````
    		//表里的内容
    		$("#confirm-quotation-detail").datagrid({
    			url:"crm/quotationDetail/getDetailByOrder/"+row.id,
    			method:"get"
    		});
    	}
	}).on('click','a.delete',function(){
		var row = qd.datagrid('getSelected');
		alert("确定删除吗？");
		if(row){
			$.post("quotation/delete/"+row.id,function(data){
				console.log(data);
				if(data.success){
					alert("删除成功！");
					qd.datagrid("reload");
				}
			})
		}
	}).on('click','a.detail',function(){//查看详情
		var row = qd.datagrid('getSelected');
		if(row){
			var dialog = $('<div/>').dialog({
    			title:'报价详情',
    			width:700,
    			height:400,
    			modal:true,
    			href:'quotation/detail/'+row.id,
    			onClose:function(){
    				$(this).dialog('destroy');
    			},
    			buttons:[
    				{
    					iconCls:'icon-ok',
    					text:'保存',
    					handler:function(){
    						alert('保存编辑。。。');
    						dialog.dialog("close");
    						qd.datagrid('reload');
    					}
    				},
    				{
    					iconCls:'icon-cancel',
    					text:'取消',
    					handler:function(){
    						dialog.dialog("close");
    					}
    				}
    			]
    		});
		}
	});
	function GMTToStr(time){
		console.log(time);
		console.log("fkaks");
	    let date = new Date(time)
	    let Str=date.getFullYear() + '-' +
	    (date.getMonth() + 1) + '-' + 
	    date.getDate() + ' ' + 
	    date.getHours() + ':' + 
	    date.getMinutes() + ':' + 
	    date.getSeconds()
	    console.log(Str);
	    return Str;
	}
	$("#saveupdate").click(function(){
		alert(233);
		//获取表单的内容
		
		var form = $("#orderedit-form1");
		// 校验表单数据
		if(form.form("validate")){
			$.post("quotation/save2",form.serialize(),function(rs){
				if(rs.err){
					$.messager.alert("消息",rs.err,"err");
				}else{
					$.messager.alert('消息', '保存成功！',"info");
					$("#order-editdlg1").dialog("close");
					qd.datagrid("reload");
				}
			});
		}
		//验证数据
		//发送请求
	})
})
 
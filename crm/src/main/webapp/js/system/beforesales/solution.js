$(function () {
	var grid = $("#solutionGrid");
	//为工具栏按钮添加绑定事件
	$("#solutionToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增解决方案",
			width:700,
			height:700,
			modal:true,
			href:'solution/solutioneditform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#solutionform");
						//校验表单数据
						if(form.form("validate")){
							$.post("solution/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.delete",function(){
		var rows = grid.datagrid("getSelections");
        if (rows){
        	 $.messager.confirm('警告','您确定要删除该用户及其关联的视图吗？',function(r){
             	
                 if (r){
                 	var firmIds = [];
             		for (var i = 0; i<rows.length; i++) {
             			var ids = rows[i].id;
             			firmIds.push(ids);
             		}
                     $.post('solution/delete/'+firmIds,function(result){
                         if (result.success){
                         	grid.datagrid("reload");  
                         } else {
                         	$.messager.alert("提示！","系统错误，删除失败！");
                         }
                     });
                 }
             });
        }
	});
	
	
});
 
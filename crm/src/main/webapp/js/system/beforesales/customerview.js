$(function () {
			$("#aa").click(function() {
				$(".details").show();
				
				// var id=$(".mysteve").attr("id");
				$('#add').show();
				// alert(id);
				
			});

			$("#add").click(function() {
				$(".details").hide();
				$('#add').hide();
			});
			
			// 点击编辑时加载编辑表单
			$("#bianji").click(function(){	
				// 获取到客户的id值，方便进行信息编辑
				var id = $("#cusid").val();
					var dialog = $("<div/>").dialog({
						title: "编辑客户",
						width: 800,
						height: 660,
						modal:true,
						href:'cus/cusmoreform/'+id,
						onClose: function(){
							//关闭窗户后，立即销毁窗口
							$(this).dialog("destroy");
						},
						buttons:[
							{
								iconCls:"icon-ok",
								text:"保存",
								handler:function(){
									var form = $("#cusMoreForm");
									//校验表单数据
									if(form.form("validate")){
										console.log(form.serialize());
										$.post("cus/save",form.serialize(),function(rs){
											if(rs.err){
												alert(rs.err);
											}else{
												//关闭当前窗口
												dialog.dialog("close");
												grid.datagrid("reload");
											}
										});
									}
								}
							},
							{
								iconCls:"icon-cancel",
								text:"取消",
								handler:function(){
									//关闭当前窗口
									dialog.dialog("close");
								}
							}
						]
					});
			
			});
			
			
			$("#shanchu").click(function(){
				var id = $("#cusid").val();
				alert(id);
				$.messager.confirm({
				    width: 400,
				    height: 140, 
				    title: '操作确认',
				    msg: '确定删除本数据及其视图下关联数据吗？？',
				    fn: function (r) {
				    	if (r){
			                $.post('cus/delete/'+id,function(result){
			                    if (result.success){
			                    	// 自动跳转回主页
			                    	window.location.href="http://localhost:8080/crm/crm/main";
			                    } else {
			                    	$.messager.alert("提示！","系统错误，删除失败！");
			                    }
			                });
			            }
				        }
				    });
			
				
			});
			
			$("#zhuanyi").click(function(){
				var id = $("#cusid").val();
				// 加载客户转移表单
				var dialog = $("<div/>").dialog({
					title: "客户转移",
					width: 400,
					height: 400,
					modal:true,
					// 跳转到客户转移界面，暂时写死
					href:'cus/change/'+id,
					onClose: function(){
						// 关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								var eeid = $("input[name='change']:checked").val();
								var cumid =  $("input[name='cumid']").val();
								var cuid =  $("input[name='cuid']").val();
								if(eeid==cumid){
									alert("您已经拥有该客户，请重新选择！");
								}else{
									$.ajax({
										type : "GET",
										data:{eeid:eeid,cuid:cuid}, 
										url : "cus/savechange",
										success : function(data) {
											$.messager.alert("消息",data.message,"info");
											// 关闭当前窗口
											dialog.dialog("close");
										},
										error : function() {
											$.messager.alert("消息","请求转移失败！","error");
										}
									});
								}
								}
							},
						
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								// 关闭当前窗口
								dialog.dialog("close");
							}
						}
					]
				});
			});
			
			$("#gongxiang").click(function(){
				// 加载客户转移表单
				var dialog = $("<div/>").dialog({
					title: "客户转移",
					width: 500,
					height: 500,
					
					modal:true,
					// 跳转到客户转移界面，暂时写死
					href:'cus/share/'+1,
					onClose: function(){
						// 关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								var form = $("#sharecustomerform");
								// 校验表单数据
								if(form.form("validate")){
									$.post("opp/form",form.serialize(),function(rs){
										if(rs.err){
											alert(rs.err);
										}else{
											// 关闭当前窗口
											dialog.dialog("close");
											grid.datagrid("reload");
										}
									});
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								// 关闭当前窗口
								dialog.dialog("close");
							}
						}
					]
				});
			});
		
			// 加载合同
			$("#middleli2").click(function(){
				var id = $("#cusid").val();
				$(this).addClass("current").siblings().removeClass("current"); 
				 $("#panel").panel({
					 width:600,
					 
					 href:'cus/pact/'+id,
				 });
				
			});
			// 加载销售机会
			$("#middleli3").click(function(){
				var id = $("#cusid").val();
				$(this).addClass("current").siblings().removeClass("current"); 
				 $("#panel").panel({
					 width:600,
					 border:0,
					 href:'cus/xiaoshou/'+id
				 });
				
			});
			// 加载行动任务
			$("#middleli4").click(function(){
				var id = $("#cusid").val();
				$(this).addClass("current").siblings().removeClass("current"); 
				 $("#panel").panel({
					 width:600,
					 border:0,
					// 加载对客户的行动
					 href:'cus/action/'+id,
				 });
				
			});
			
		    $("#sanding").click(function(){
		    	var id = $("#cusid").val();
				var dialog = $("<div/>").dialog({
					title: "三定",
					width: 400,
					height:400,
					modal:true,
					href:'cus/sanding/'+id,
					onClose: function(){
						// 关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								var form = $("#sandingForm");
								// 校验表单数据
								if(form.form("validate")){
									$.post("cus/savecus/"+id,form.serialize(),function(rs){
										if(rs.err){
											alert(rs.err);
										}else{
											// 关闭当前窗口
											dialog.dialog("close");
											// grid.datagrid("reload");
										}
									});
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								// 关闭当前窗口
								dialog.dialog("close");
							}
						}
					]
				});
		    });
});		
			
			
			
	
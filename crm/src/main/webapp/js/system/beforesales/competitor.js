$(function () {
	var grid = $("#comGird");
	//初始化表格
	grid.datagrid({
		fit:true,
		border:true,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		rownumbers:true,
		url:'com/list',
		idField:'id',
		toolbar:'#comToolbar',
		columns:[[
			{field:'id',title:'id',width:50,align:'center'},
			{field:'companyName',title:'公司名称',width:150,align:'center'},
			{field:'money',title:'价格',width:100,align:'center'},
			{field:'power',title:'竞争能力',width:100,align:'center'},
			{field:'salesOpp',title:'对应机会',width:100,align:'center',formatter : function(value, row, index) {
				return value.topic;
			}},
			{field:'name',title:'对应客户',width:100,align:'center',formatter: function(value,row,index){
				return new Object(row["salesOpp"]["customer"]).name;
			}}
		]]
	});
	
	//为工具栏按钮添加绑定事件
	$("#comToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增竞争对手信息",
			width: 450,
			top:70,
			modal:true,
			href:'com/comform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#comForm");
						//校验表单数据
						
						if(form.form("validate")){
							console.log(form.serialize());
							$.post("com/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑竞争对手信息",
				width: 450,
				top:70,
				modal:true,
				href:'com/comform/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#comForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("com/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
        if (row){
            $.messager.confirm('警告','您确定要删除此信息吗？',function(r){
                if (r){
                    $.get('com/delete/'+row.id,function(result){
                        if (result.success){
                        	grid.datagrid("reload");  
                        } else {
                        	$.messager.alert("提示！","系统错误，删除失败！");
                        }
                    });
                }
            });
        }
	});
	

	
});
 
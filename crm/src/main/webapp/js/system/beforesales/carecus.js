$(function () {
	var grid = $("#carecusGird");
	//初始化表格
	grid.datagrid({
		fit:true,
		border:true,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		rownumbers:true,
		url:'carecus/list',
		idField:'id',
		toolbar:'#carecusToolbar',
		columns:[[
			{field:'id',title:'id',width:50,align:'center'},
			{field:'topic',title:'关怀主题',width:100,align:'center'},
			{field:'customer',title:'联系人',width:100,align:'center',formatter : function(value, row, index) {
				return value.contacts;
			}},
			{field:'date',title:'关怀日期',width:100,align:'center',formatter : function(value, row, index) {
				var time = new Date(value).Format("yyyy-MM-dd");
				return time;
			}},
			{field:'employee',title:'执行人',width:100,align:'center',formatter : function(value, row, index) {
				return value.empName;
			}},
		]]
	});
	
	//为工具栏按钮添加绑定事件
	$("#carecusToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title:"新增关怀记录",
			width:450,
			top:150,
			modal:true,
			href:'carecus/carecusform',
			onClose:function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#carecusForm");
						console.log(form.serialize());
						//校验表单数据
						if(form.form("validate")){
							$.post("carecus/save",form.serialize(),function(rs){
								
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑关怀记录",
				width: 450,
				top:150,
				modal:true,
				href:'carecus/carecusform/'+row.id,	
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#carecusForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("carecus/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							//关闭当前窗口
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
        if (row){
            $.messager.confirm('警告','您确定要删除此信息吗？',function(r){
                if (r){
                    $.get('carecus/delete/'+row.id,function(result){
                        if (result.success){
                        	grid.datagrid("reload");  
                        } else {
                        	$.messager.alert("提示！","系统错误，删除失败！");
                        }
                    });
                }
            });
        }
	});
	
	Date.prototype.Format = function(fmt) {    
		var o = {
			"M+" : this.getMonth() + 1, //月份   
			"d+" : this.getDate(), //日   
			"H+" : this.getHours(), //小时   
			"m+" : this.getMinutes(), //分   
			"s+" : this.getSeconds(), //秒   
			"q+" : Math.floor((this.getMonth() + 3) / 3),
			"S" : this.getMilliseconds()
		//毫秒   
		};
		if (/(y+)/.test(fmt))
			fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
					.substr(4 - RegExp.$1.length));
		for ( var k in o)
			if (new RegExp("(" + k + ")").test(fmt))
				fmt = fmt.replace(RegExp.$1,
						(RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k])
								.substr(("" + o[k]).length)));
		return fmt;
	};
	
});
 
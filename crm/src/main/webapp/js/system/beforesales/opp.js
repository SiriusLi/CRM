
$(function(){
//给表格重命名
var grid = $('#dg');

grid.datagrid({
	title : '机会',
	width : "100%",
});
//添加高级查询行列的内容
grid.datagrid('enableFilter', [ {
	field : 'topic',
	type : 'text',
}, {
	field : 'expectedMoney',
	type : 'numberbox',
	options : {
		precision : 1
	},
	op : [ 'equal', 'notequal', 'less', 'greater' ]
} ]);


//点击menuToolbar上的add按钮，加载add表格
$("#menuToolbar").on("click","a.add",function(){
	var dialog = $("<div/>").dialog({
		title: "新增销售机会",
		width:800,
		height: 700,
		modal:true,
		href:'opp/oppaddform',
		onClose: function(){
			//关闭窗户后，立即销毁窗口
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-ok",
				text:"保存",
				handler:function(){
					//var form = $("#oppaddForm");
					//var updateTime=$("input[name='updateTime']").val();
					//var expectedTime=$("input[name='expectedTime']").val();
					var topic=$("input[name='topic']").val();
					var customerid=$("input[name='customer.id']").val();
					var status=$("input[name='status']").val();
					var stage=$("input[name='stage']").val();
					var priority=$("input[name='priority']").val();
					var possibility=$("input[name='possibility']").val();
					var expectedMoney=$("input[name='expectedMoney']").val();
					//console.log(expectedTime);
					//校验表单数据
					//if(form.form("validate")){
					$.ajax({
						url:'opp/save',
						type:"GET",
						data:{updateTime:updateTime,expectedTime:expectedTime,
							topic:topic,customerid:customerid,
							status:status,stage:stage,
							priority:priority,possibility:possibility,expectedMoney:expectedMoney
						},
							success : function(data) {
								$.messager.alert("消息",data.message,"info");
								// 关闭当前窗口
								dialog.dialog("close");
							},
							error : function() {
								$.messager.alert("消息","失败！","error");
							}
						})
					
					
					
//						$.get("opp/save",
//							{updateTime:updateTime,expectedTime:expectedTime,
//							topic:topic,customerid:customerid,
//							status:status,stage:stage,
//							priority:priority,possibility:possibility,
//							expectedMoney:expectedMoney},function(rs){
//							if(rs.err){
//								alert(rs.err);
//							}else{
//								//关闭当前窗口
//								dialog.dialog("close");
//								grid.datagrid("reload");
//							}
//						});
					//}
				}
			},
			{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					//关闭当前窗口
					dialog.dialog("close");
				}
			}
		]
	});
}).on("click","a.delete",function(){
	
	var rows = grid.datagrid("getSelections");
    if (rows){
        $.messager.confirm('警告','您确定要删除该用户及其关联的视图吗？',function(r){
        	
            if (r){
            	var firmIds = [];
        		for (var i = 0; i<rows.length; i++) {
        			var ids = rows[i].id;
        			firmIds.push(ids);
        		}
                $.post('opp/delete/'+firmIds,function(result){
                    if (result.success){
                    	grid.datagrid("reload");  
                    } else {
                    	$.messager.alert("提示！","系统错误，删除失败！");
                    }
                });
            }
        });
    }
});

});